package com.ruoyi.auth.constant;


public class UserTypeConstant {

    public static final String BUYER = "buyer";
    public static final String SHOP = "shop";
}
