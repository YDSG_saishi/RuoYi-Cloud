package com.ruoyi.auth.convert;


import cn.binarywang.wx.miniapp.bean.WxMaJscode2SessionResult;
import cn.binarywang.wx.miniapp.bean.WxMaPhoneNumberInfo;
import com.ruoyi.auth.dto.WxMiniLoginResultDto;
import com.ruoyi.auth.dto.WxMiniUserMobileInfoDto;


public class WxMiniConverter {

    public static WxMiniUserMobileInfoDto toUserMobileInfoDto(WxMaPhoneNumberInfo wxMaPhoneNumberInfo) {
        if ( wxMaPhoneNumberInfo == null ) {
            return null;
        }

        WxMiniUserMobileInfoDto wxMiniUserMobileInfoDto = new WxMiniUserMobileInfoDto();

        wxMiniUserMobileInfoDto.setWatermark( wxMaPhoneNumberInfo.getWatermark() );
        wxMiniUserMobileInfoDto.setPhoneNumber( wxMaPhoneNumberInfo.getPhoneNumber() );
        wxMiniUserMobileInfoDto.setPurePhoneNumber( wxMaPhoneNumberInfo.getPurePhoneNumber() );
        wxMiniUserMobileInfoDto.setCountryCode( wxMaPhoneNumberInfo.getCountryCode() );

        return wxMiniUserMobileInfoDto;
    }

    public static WxMiniLoginResultDto toLoginResultDto(WxMaJscode2SessionResult wxMaJscode2SessionResult) {
        if ( wxMaJscode2SessionResult == null ) {
            return null;
        }

        WxMiniLoginResultDto wxMiniLoginResultDto = new WxMiniLoginResultDto();

        wxMiniLoginResultDto.setOpenId( wxMaJscode2SessionResult.getOpenid() );
        wxMiniLoginResultDto.setUnionId( wxMaJscode2SessionResult.getUnionid() );
        wxMiniLoginResultDto.setSessionKey( wxMaJscode2SessionResult.getSessionKey() );

        return wxMiniLoginResultDto;
    }

}
