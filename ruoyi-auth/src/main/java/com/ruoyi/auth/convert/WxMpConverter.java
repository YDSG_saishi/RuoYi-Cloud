package com.ruoyi.auth.convert;


import com.ruoyi.auth.dto.WxMpUserLoginResultDto;
import me.chanjar.weixin.common.bean.WxOAuth2UserInfo;


public class WxMpConverter {

    public WxMpUserLoginResultDto toLoginResultDto(WxOAuth2UserInfo wxOAuth2UserInfo) {
        if ( wxOAuth2UserInfo == null ) {
            return null;
        }

        WxMpUserLoginResultDto wxMpUserLoginResultDto = new WxMpUserLoginResultDto();

        wxMpUserLoginResultDto.setNickName( wxOAuth2UserInfo.getNickname() );
        wxMpUserLoginResultDto.setUnionId( wxOAuth2UserInfo.getUnionId() );
        wxMpUserLoginResultDto.setSex( wxOAuth2UserInfo.getSex() );
        wxMpUserLoginResultDto.setHeadImgUrl( wxOAuth2UserInfo.getHeadImgUrl() );

        return wxMpUserLoginResultDto;
    }

}
