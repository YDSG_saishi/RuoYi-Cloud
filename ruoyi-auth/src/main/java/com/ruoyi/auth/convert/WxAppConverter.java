package com.ruoyi.auth.convert;


import com.ruoyi.auth.dto.WxAppLoginResultDto;
import com.ruoyi.auth.dto.WxAppUserInfoResultDto;
import me.chanjar.weixin.common.bean.WxOAuth2UserInfo;
import me.chanjar.weixin.common.bean.oauth2.WxOAuth2AccessToken;

import java.util.Arrays;

public class WxAppConverter {


    public static WxAppLoginResultDto toLoginResultDto(WxOAuth2AccessToken wxOAuth2AccessToken) {
        if ( wxOAuth2AccessToken == null ) {
            return null;
        }
        WxAppLoginResultDto wxAppLoginResultDto = new WxAppLoginResultDto();
        wxAppLoginResultDto.setAccessToken( wxOAuth2AccessToken.getAccessToken() );
        wxAppLoginResultDto.setExpiresIn( wxOAuth2AccessToken.getExpiresIn() );
        wxAppLoginResultDto.setRefreshToken( wxOAuth2AccessToken.getRefreshToken() );
        wxAppLoginResultDto.setOpenId( wxOAuth2AccessToken.getOpenId() );
        wxAppLoginResultDto.setScope( wxOAuth2AccessToken.getScope() );
        wxAppLoginResultDto.setUnionId( wxOAuth2AccessToken.getUnionId() );
        return wxAppLoginResultDto;
    }

    public static WxAppUserInfoResultDto toUserInfoDto(WxOAuth2UserInfo wxOAuth2UserInfo) {
        if ( wxOAuth2UserInfo == null ) {
            return null;
        }

        WxAppUserInfoResultDto wxAppUserInfoResultDto = new WxAppUserInfoResultDto();

        wxAppUserInfoResultDto.setOpenid( wxOAuth2UserInfo.getOpenid() );
        wxAppUserInfoResultDto.setNickname( wxOAuth2UserInfo.getNickname() );
        wxAppUserInfoResultDto.setSex( wxOAuth2UserInfo.getSex() );
        wxAppUserInfoResultDto.setCity( wxOAuth2UserInfo.getCity() );
        wxAppUserInfoResultDto.setProvince( wxOAuth2UserInfo.getProvince() );
        wxAppUserInfoResultDto.setCountry( wxOAuth2UserInfo.getCountry() );
        wxAppUserInfoResultDto.setHeadImgUrl( wxOAuth2UserInfo.getHeadImgUrl() );
        wxAppUserInfoResultDto.setUnionId( wxOAuth2UserInfo.getUnionId() );
        String[] privileges = wxOAuth2UserInfo.getPrivileges();
        if ( privileges != null ) {
            wxAppUserInfoResultDto.setPrivileges( Arrays.copyOf( privileges, privileges.length ) );
        }
        wxAppUserInfoResultDto.setSnapshotUser( wxOAuth2UserInfo.getSnapshotUser() );

        return wxAppUserInfoResultDto;
    }

}
