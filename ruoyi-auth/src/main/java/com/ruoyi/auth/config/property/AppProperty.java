package com.ruoyi.auth.config.property;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.List;

@RefreshScope
@Component
@Primary
@ConfigurationProperties(prefix = "app")
@Data
public class AppProperty {
    private Wx wx;

    private Security security;

    @Data
    public static class Security {
        private boolean webSecurityConfigEnabled;
    }

    @Data
    public static class Wx {
        private AccessToken accessToken;

        private Mini mini;

        private Mp mp;

        private Pay pay;

        @Data
        public static class AccessToken {
            private String redisKey1;

            private String redisKey2;

            private Ttl ttl;

            @Data
            public static class Ttl {
                private String minute;
            }

            private Schedule schedule;

            @Data
            public static class Schedule {
                private String cron;
            }

            private TestAccessToken testAccessToken;

            @Data
            public static class TestAccessToken {
                private String envEnabled;
            }
        }

        @Data
        public static class Mini {
            private List<Config> configs;

            @Data
            public static class Config {
                /**
                 * 设置微信小程序的appid
                 */
                private String appId;

                /**
                 * 设置微信小程序的Secret
                 */
                private String appSecret;

                /**
                 * 设置微信小程序消息服务器配置的token
                 */
                private String token;

                /**
                 * 设置微信小程序消息服务器配置的EncodingAESKey
                 */
                private String aesKey;

                /**
                 * 消息格式，XML或者JSON
                 */
                private String msgDataFormat;
            }
        }

        @Data
        public static class Mp {

            private App app;

            @Data
            public static class App {

                private List<Config> configs;

                @Data
                public static class Config {

                    private boolean useRedis;

                    /**
                     * 设置微信公众号的appid
                     */
                    private String appId;

                    /**
                     * 设置微信公众号的app secret
                     */
                    private String appSecret;

                    /**
                     * 设置微信公众号的token
                     */
                    private String token;

                    /**
                     * 设置微信公众号的EncodingAESKey
                     */
                    private String aesKey;

                }
            }
        }

        @Data
        public static class Pay {

            private ServiceProvider serviceProvider;

            /**
             * 地址
             */
            private String notificationHost;

            @Data
            public static class ServiceProvider {
                /**
                 * mchId：商户号
                 */
                private String mchId;

                /**
                 * 证书
                 */
                private String cert;

                /**
                 * 商户证书序列号
                 */
                private String certSerialNo;

                /**
                 * 商户私钥
                 */
                private String certPrivateKey;

                /**
                 * api密钥
                 */
                private String apiPrivateKey;

                /**
                 * v3 api密钥
                 */
                private String apiv3PrivateKey;
            }
        }
    }
    private Des des;

    @Data
    public static class Des {

        private String key;

    }

}
