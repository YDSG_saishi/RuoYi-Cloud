package com.ruoyi.auth.form;

import lombok.Data;

/**
 * 用户登录对象
 * 
 * @author ruoyi
 */
@Data
public class LoginBody
{
    /**
     * 用户名
     */
    private String username;

    /**
     * 用户密码
     */
    private String password;
    /**
     * 手机号
     */
    private String phone;
    /**
     * 验证码
     */
    private String code;

    /**
     * 登录 1  找回密码2
     * @return
     */
    private Integer authType;

    /**
     * 微信授权app登录获取的token
     */
    private String accessToken;


}
