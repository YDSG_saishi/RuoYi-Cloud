package com.ruoyi.auth.controller;

import com.ruoyi.auth.form.LoginBody;
import com.ruoyi.auth.service.AppService;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.security.service.TokenService;
import com.ruoyi.system.api.model.LoginUser;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * token 控制
 */
@RestController
@RequestMapping(value = "app")
public class AppController {
    @Resource
    private TokenService tokenService;

    @Resource
    private AppService appService;

    /**
     * 登录接口
     * @param form 用户信息
     * @return 成功信息
     */
    @PostMapping("login")
    public R<Map<String, Object>> login(@RequestBody LoginBody form) {
        LoginUser loginUser = appService.loginUser(form);
        return R.ok(tokenService.createToken(loginUser));
    }

    /**
     * 获取验证码
     * @param phone 手机号码
     * @return 是否成功
     */
    @GetMapping(value = "/sendCode/{phone}")
    public R<Boolean> sendCode(@PathVariable("phone") String  phone) throws Exception {
        return R.ok(appService.sendCode(phone));
    }


}
