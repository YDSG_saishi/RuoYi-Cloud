package com.ruoyi.auth.controller;

import com.ruoyi.auth.constant.UserTypeConstant;
import com.ruoyi.auth.convert.WxAppConverter;
import com.ruoyi.auth.dto.WxAppLoginResultDto;
import com.ruoyi.auth.dto.WxAppUserInfoResultDto;
import com.ruoyi.auth.form.LoginBody;
import com.ruoyi.auth.service.WXService;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.utils.JwtUtils;
import com.ruoyi.common.security.service.TokenService;
import com.ruoyi.system.api.model.LoginUser;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.bean.WxOAuth2UserInfo;
import me.chanjar.weixin.common.bean.oauth2.WxOAuth2AccessToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping(value = "wx")
public class WXController {
    @Resource
    private WXService wxLogin;

    @Resource
    private TokenService tokenService;

    /**
     * 登录
     * @param code
     * @return
     */
    @PostMapping("/login")
    public R<?> login(@RequestParam String code){
        log.info("微信登录code，{}",code);
        LoginUser loginResultDto = wxLogin.login(UserTypeConstant.BUYER,code);
        if (!loginResultDto.isNewUser()){
            return R.ok(tokenService.createToken(loginResultDto));
        }
        Map<String, Object> rspMap = new HashMap<String, Object>();
        LoginUser loginUser = new LoginUser();
        loginUser.setNewUser(loginResultDto.isNewUser());
        loginUser.setToken(loginResultDto.getToken());
        rspMap.put("login_user", loginUser);
        return R.ok(loginResultDto);
    }

    /**
     *通过openId从redis中获取数据，数据不为空则创建用户，并登录
     * @param loginBody
     * @return
     */
    @PostMapping("registerByWxAuth")
    public R<?> registerByWxAuth(@RequestBody LoginBody loginBody){
        log.info("通过openId绑定用户手机号注册并登录");
        LoginUser loginUser = wxLogin.registerByWxAuth(loginBody);
        return R.ok(tokenService.createToken(loginUser));
    }
    /**
     * 获取accessToken
     */
    @PostMapping("getAccessToken")
    public R<WxOAuth2AccessToken> getAccessToken(@RequestParam String code) {
        return R.ok(wxLogin.getAccessToken(code));
    }
    /**
     * 获取用户信息
     * @param accessToken
     * @return
     */
    @PostMapping("getUserInfo")
    public R<WxAppUserInfoResultDto> getBuyerUserInfo(@RequestParam String accessToken) {
        WxOAuth2UserInfo wxOAuth2UserInfo = wxLogin.getNewUserInfo(UserTypeConstant.BUYER, accessToken);
        return R.ok(WxAppConverter.toUserInfoDto(wxOAuth2UserInfo));
    }

}
