package com.ruoyi.auth.dto;

import cn.binarywang.wx.miniapp.bean.Watermark;
import lombok.Data;

@Data
public class WxMiniUserMobileInfoDto{
    /**
     * 水印
     */
    private Watermark watermark;

    /**
     * 手机号码
     */
    private String phoneNumber;

    /**
     * 没有区号的手机号码
     */
    private String purePhoneNumber;

    /**
     * 区号
     */
    private String countryCode;
}
