package com.ruoyi.auth.dto;

import lombok.Data;

@Data
public class WxMpUserLoginResultDto{
    private String openId;

    private String unionId;

    private String nickName;

    private Integer sex;

    private String headImgUrl;
}
