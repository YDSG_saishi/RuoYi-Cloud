package com.ruoyi.auth.dto;

import lombok.Data;

@Data
public class WxMiniLoginResultDto{
    private String openId;

    private String sessionKey;

    private String unionId;
}
