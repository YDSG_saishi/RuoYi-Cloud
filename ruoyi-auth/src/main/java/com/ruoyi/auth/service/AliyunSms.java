package com.ruoyi.auth.service;


import com.alibaba.fastjson2.JSONObject;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.QuerySendDetailsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.QuerySendDetailsResponse;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import com.ruoyi.auth.constant.AuthConstant;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Random;

/**
 * Created on 17/6/7.
 * 短信API产品的DEMO程序,工程中包含了一个SmsDemo类，直接通过
 * 执行main函数即可体验短信产品API功能(只需要将AK替换成开通了云通信-短信产品功能的AK即可)
 * 工程依赖了2个jar包(存放在工程的libs目录下)
 * 1:aliyun-java-sdk-core.jar
 * 2:aliyun-java-sdk-dysmsapi.jar
 * <p>
 * 备注:Demo工程编码采用UTF-8
 * 国际短信发送请勿参照此DEMO
 */
@Slf4j
@Component
public class AliyunSms {


    private static final Logger logger = LoggerFactory.getLogger(AliyunSms.class);

    /**
     * 发送密码初始化通知
     *
     * @param phone    手机号
     * @param password 密码
     */
    public  String sendInitPwd(String phone, String password) throws Exception {
        String templateCode = "SMS_225368407"; // 密码初始化 短信通知模版
        String templateParam = "{\"password\":\"" + password + "\"}";
        SendSmsResponse response;
        try {
            response = sendSms(phone, AuthConstant.SIGN_NAME, templateCode, templateParam);
        } catch (ClientException e) {
            e.printStackTrace();
            return "";
        }
        logger.info("templateParam = " + templateParam);
        logger.info("Phone = " + phone);
        logger.info("Code = " + response.getCode());
        logger.info("Message = " + response.getMessage());
        logger.info("RequestId = " + response.getRequestId());
        logger.info("BizId = " + response.getBizId());
        if (!("OK".equalsIgnoreCase(response.getCode()) && "OK".equalsIgnoreCase(response.getMessage()))) {
            throw new Exception("发送失败");
        }
        return password;
    }

    /**
     * 验证码
     */
    public Boolean sendCode(String phone, String templateCode, Map<String, Object> codeMap) throws Exception {
        SendSmsResponse response;
        try {
            response = sendSms(phone, AuthConstant.SIGN_NAME, templateCode, JSONObject.toJSONString(codeMap));
        } catch (ClientException e) {
            e.printStackTrace();
            return false;
        }
        logger.info("Phone = " + phone);
        logger.info("Code = " + response.getCode());
        logger.info("Message = " + response.getMessage());
        logger.info("RequestId = " + response.getRequestId());
        logger.info("BizId = " + response.getBizId());
        if (!("OK".equalsIgnoreCase(response.getCode()) && "OK".equalsIgnoreCase(response.getMessage()))) {
            throw new Exception("发送失败");
        }
        return true;
    }

    /**
     * 短信通知
     */
    public  Boolean sendMsg(String phone, String templateCode, String templateParam) {
        SendSmsResponse response;
        try {
            response = sendSms(phone, AuthConstant.SIGN_NAME, templateCode, templateParam);
        } catch (ClientException e) {
            e.printStackTrace();
            return false;
        }
        logger.info("templateParam = " + templateParam);
        logger.info("Phone = " + phone);
        logger.info("Code = " + response.getCode());
        logger.info("Message = " + response.getMessage());
        logger.info("RequestId = " + response.getRequestId());
        logger.info("BizId = " + response.getBizId());
        return true;
    }

    /**
     * 获取随机验证码
     */
    public  String getCode() {
        Random random = new Random();
        String s = String.valueOf(random.nextInt());
        String code = s.substring(s.length() - 7, s.length() - 1);
        if (code.length() != 6){
            code = getCode();
        }
        return code;
    }

    private  SendSmsResponse sendSms(String phone, String signName, String templateCode, String templateParam) throws ClientException {
        //可自助调整超时时间
        System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
        System.setProperty("sun.net.client.defaultReadTimeout", "10000");
        //初始化acsClient,暂不支持region化
        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", AuthConstant.accessKeyId, AuthConstant.accessKeySecret);
        DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", AuthConstant.product, AuthConstant.domain);
        IAcsClient acsClient = new DefaultAcsClient(profile);
        //组装请求对象-具体描述见控制台-文档部分内容
        SendSmsRequest request = new SendSmsRequest();
        //必填:待发送手机号
        request.setPhoneNumbers(phone);
        //必填:短信签名-可在短信控制台中找到
        request.setSignName(signName);
        //必填:短信模板-可在短信控制台中找到
        request.setTemplateCode(templateCode);
        //可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
        request.setTemplateParam(templateParam);
        //选填-上行短信扩展码(无特殊需求用户请忽略此字段)
        //request.setSmsUpExtendCode("90997");
        //可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
        //request.setOutId("yourOutId");
        //hint 此处可能会抛出异常，注意catch
        return acsClient.getAcsResponse(request);
    }

    private  QuerySendDetailsResponse querySendDetails(String bizId) throws ClientException {
        //可自助调整超时时间
        System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
        System.setProperty("sun.net.client.defaultReadTimeout", "10000");
        //初始化acsClient,暂不支持region化
        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", AuthConstant.accessKeyId, AuthConstant.accessKeySecret);
        DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", AuthConstant.product, AuthConstant.domain);
        IAcsClient acsClient = new DefaultAcsClient(profile);
        //组装请求对象
        QuerySendDetailsRequest request = new QuerySendDetailsRequest();
        //必填-号码
        request.setPhoneNumber("13265925915");
        //可选-流水号
        request.setBizId(bizId);
        //必填-发送日期 支持30天内记录查询，格式yyyyMMdd
        SimpleDateFormat ft = new SimpleDateFormat("yyyyMMdd");
        request.setSendDate(ft.format(new Date()));
        //必填-页大小
        request.setPageSize(10L);
        //必填-当前页码从1开始计数
        request.setCurrentPage(1L);
        //hint 此处可能会抛出异常，注意catch
        return acsClient.getAcsResponse(request);
    }

}