package com.ruoyi.system.domain.vo;

import lombok.Data;
import lombok.ToString;

import java.time.LocalDateTime;


/**
 * {@code @Description}: 聊天信息封装
 */
@ToString
@Data
public class LiveChatMessageVO {
    // 在当前直播间的id
    private Integer id;
    // 身份code 0=连接消息 1=用户交互消息
    private Integer code;
    // 直播间号
    private String liveRoom;
    // 主播用户名
    private String anchorUserId;
    // 发送者用户名
    private String senderUserId;
    // 发送者userId
    private Integer userId;
    // 发送者的昵称
    private String senderUserName;
    // 发送者头像地址
    private String senderHeadUrl;
    // 发送者身份 0:观众 1:主播 2:系统消息
    private Integer identity;
    // 发送的消息
    private String message;
    // 隐藏状态 0=不隐藏 1=隐藏
    private Integer hideStatus;
    // 引用id
    private Integer quoteId;
    // 发送时间
    private LocalDateTime sendDate;
    // 引用的消息
    private LiveChatMessageVO quoteMessage;
    // 点赞数量
    private Integer likeCount;
    // 固定弹出的商品id
    private Integer ejectGoodsId;

}