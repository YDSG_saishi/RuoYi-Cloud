package com.ruoyi.system.controller;


import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.ruoyi.system.domain.BaseTournament;
import com.ruoyi.system.service.IBaseTournamentService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;

import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 赛事Controller
 * 
 * @author wangbj
 * @date 2024-04-16
 */
@ApiOperation("赛事")
@RestController
@RequestMapping("/tournament")
public class BaseTournamentController extends BaseController
{
    @Autowired
    private IBaseTournamentService baseTournamentService;

    /**
     * 查询赛事列表
     */
    @RequiresPermissions("system:tournament:list")
    @GetMapping("/list")
    @Operation(summary = "查询赛事列表", description = "查询赛事列表")
    public TableDataInfo list(BaseTournament baseTournament)
    {
        if (ObjectUtil.isNull(baseTournament.getContestId())){
            throw new RuntimeException("联赛id不能为空");
        }

        startPage();
        return baseTournamentService.selectBaseTournamentList(baseTournament);
    }

//    /**
//     * 导出赛事列表
//     */
//    @Operation(summary = "导出赛事列表", description = "导出赛事列表")
//    @RequiresPermissions("system:tournament:export")
//    @Log(title = "赛事", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, BaseTournament baseTournament)
//    {
//        List<BaseTournament> list = baseTournamentService.selectBaseTournamentList(baseTournament);
//        ExcelUtil<BaseTournament> util = new ExcelUtil<BaseTournament>(BaseTournament.class);
//        util.exportExcel(response, list, "赛事数据");
//    }

    /**
     * 获取赛事详细信息
     */
    @Operation(summary = "获取赛事详细信息", description = "获取赛事详细信息")
    @RequiresPermissions("system:tournament:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(baseTournamentService.selectById(id));
    }

    /**
     * 新增赛事
     */
    @Operation(summary = "新增赛事", description = "新增赛事")
    @RequiresPermissions("system:tournament:add")
    @Log(title = "赛事", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseTournament baseTournament)
    {
        if (StrUtil.isBlank(baseTournament.getTournamentName())){
            return AjaxResult.error("赛事名称不能为空");
        }

        return toAjax(baseTournamentService.insertBaseTournament(baseTournament));
    }

    /**
     * 修改赛事
     */
    @Operation(summary = "修改赛事", description = "修改赛事")
    @RequiresPermissions("system:tournament:edit")
    @Log(title = "赛事", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseTournament baseTournament)
    {
        return toAjax(baseTournamentService.updateBaseTournament(baseTournament));
    }

    /**
     * 删除赛事
     */
    @Operation(summary = "删除赛事", description = "删除赛事")
    @RequiresPermissions("system:tournament:remove")
    @Log(title = "赛事", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseTournamentService.deleteBaseTournamentByIds(ids));
    }

    /**
     * 切换比赛小节
     */
    @Operation(summary = "切换比赛小节", description = "切换比赛小节")
    @GetMapping("/switchGameSections")
    public AjaxResult switchGameSections(@RequestParam Long id,
                                 @RequestParam Long subsection)
    {
        int abstention = baseTournamentService.switchGameSections(id, subsection);
        if (abstention == -1 ){
            return AjaxResult.error("当前比赛不存在");
        }
        if (abstention == -2){
            return AjaxResult.error("不可跨小节");
        }
        return AjaxResult.success();
    }

    /**
     * 弃权
     */
    @Operation(summary = "弃权", description = "弃权")
    @GetMapping("/abstention")
    public AjaxResult abstention(@RequestParam Long id,
                                 @RequestParam Long abstentionId)
    {
        int abstention = baseTournamentService.abstention(id, abstentionId);
        if (abstention == -1 ){
            return AjaxResult.error("当前比赛不存在");
        }
        return AjaxResult.success();
    }

    /**
     * 结束比赛
     */
    @Operation(summary = "结束比赛", description = "结束比赛")
    @GetMapping("/overTournament")
    public AjaxResult overTournament(@RequestParam Long id)
    {
        int abstention = baseTournamentService.overTournament(id);
        if (abstention == -1 ){
            return AjaxResult.error("当前比赛不存在");
        }
        return AjaxResult.success();
    }
}
