package com.ruoyi.system.domain;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import com.ruoyi.common.core.annotation.Excel;
import lombok.*;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 比赛简介对象 base_brief
 * 
 * @author ruoyi
 * @date 2024-07-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("base_brief")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BaseBrief extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @Schema(name = "id", description = "主键")
    private Long id;

    /** 联赛id */
    @Excel(name = "联赛id")
    @Schema(name = "contestId", description = "联赛id")
    private Long contestId;

    /** 赛事简介logo */
    @Excel(name = "赛事简介logo")
    @Schema(name = "logo", description = "赛事简介logo")
    private String logo;

    /** 比赛开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "比赛开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    @Schema(name = "startTime", description = "比赛开始时间")
    private Date startTime;

    /** 比赛结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "比赛结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    @Schema(name = "endTime", description = "比赛结束时间")
    private Date endTime;

    /** 比赛地点 */
    @Excel(name = "比赛地点")
    @Schema(name = "addres", description = "比赛地点")
    private String addres;

    /** 比赛项目 */
    @Excel(name = "比赛项目")
    @Schema(name = "item", description = "比赛项目")
    private String item;

    /** 比赛规模 */
    @Excel(name = "比赛规模")
    @Schema(name = "scale", description = "比赛规模")
    private String scale;

    /** 比赛介绍 */
    @Excel(name = "比赛介绍")
    @Schema(name = "introduce", description = "比赛介绍")
    private String introduce;

    /** 主办单位 */
    @Excel(name = "主办单位")
    @Schema(name = "auspiceName", description = "主办单位")
    private String auspiceName = "";

    /** 承办单位 */
    @Excel(name = "承办单位")
    @Schema(name = "undertakeName", description = "承办单位")
    private String undertakeName = "";

    /** 协办单位 */
    @Excel(name = "协办单位")
    @Schema(name = "jointName", description = "协办单位")
    private String jointName = "";

    /** 冠名单位 */
    @Excel(name = "冠名单位")
    @Schema(name = "titleName", description = "冠名单位")
    private String titleName = "";

    /** 赞助单位 */
    @Excel(name = "赞助单位")
    @Schema(name = "sponsorName", description = "赞助单位")
    private String sponsorName = "";

    /** 图片,分割 */
    @Excel(name = "图片,分割")
    @Schema(name = "img", description = "图片,分割")
    private String img;

    /** 所属机构id */
    @Excel(name = "所属机构id")
    @Schema(name = "organizationId", description = "所属机构id")
    private Long organizationId;

    /** 创建人id */
    @Excel(name = "创建人id")
    @Schema(name = "createUserId", description = "创建人id")
    private Long createUserId;

    /** 创建人姓名 */
    @Excel(name = "创建人姓名")
    @Schema(name = "createUserName", description = "创建人姓名")
    private String createUserName;

    /** 修改人id */
    @Excel(name = "修改人id")
    @Schema(name = "updateUserId", description = "修改人id")
    private Long updateUserId;

    /** 修改人姓名 */
    @Excel(name = "修改人姓名")
    @Schema(name = "updateUserName", description = "修改人姓名")
    private String updateUserName;

    /** 是否已删除 0:未删除;1:已删除 */
    @Excel(name = "是否已删除 0:未删除;1:已删除")
    @Schema(name = "isDeleted", description = "是否已删除 0:未删除;1:已删除")
    private Long isDeleted;

    @TableField(exist = false)
    private String contestName;

}
