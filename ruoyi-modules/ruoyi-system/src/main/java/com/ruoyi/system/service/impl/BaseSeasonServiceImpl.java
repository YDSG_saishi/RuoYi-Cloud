package com.ruoyi.system.service.impl;

import java.util.List;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.system.domain.BaseContest;
import com.ruoyi.system.mapper.BaseContestMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.system.mapper.BaseSeasonMapper;
import com.ruoyi.system.domain.BaseSeason;
import com.ruoyi.system.service.IBaseSeasonService;

import javax.annotation.Resource;

/**
 * 赛季Service业务层处理
 * 
 * @author wangbj
 * @date 2024-04-16
 */
@Service
public class BaseSeasonServiceImpl extends ServiceImpl<BaseSeasonMapper, BaseSeason> implements IBaseSeasonService
{
    @Resource
    private BaseSeasonMapper baseSeasonMapper;
    @Resource
    private LoginUserSetUtil loginUserSetUtil;

    /**
     * 查询赛季
     * 
     * @param id 赛季主键
     * @return 赛季
     */
    @Override
    public BaseSeason selectBaseSeasonById(Long id)
    {
        return baseSeasonMapper.selectBaseSeasonById(id);
    }

    /**
     * 查询赛季列表
     * 
     * @param baseSeason 赛季
     * @return 赛季
     */
    @Override
    public List<BaseSeason> selectBaseSeasonList(BaseSeason baseSeason)
    {
        return baseSeasonMapper.selectBaseSeasonList(baseSeason);
    }

    /**
     * 新增赛季
     * 
     * @param baseSeason 赛季
     * @return 结果
     */
    @Override
    public int insertBaseSeason(BaseSeason baseSeason)
    {
        LambdaQueryWrapper<BaseSeason> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(BaseSeason::getContestId,SecurityUtils.getContestId());
        queryWrapper.eq(BaseSeason::getType,1);
        List<BaseSeason> seasonList = baseMapper.selectList(queryWrapper);

        if (seasonList.size()>0){
            throw new RuntimeException("当前存在进行中的赛季！");
        }


        baseSeason.setCreateTime(DateUtils.getNowDate());
        baseSeason.setType(1);
        if (baseSeason.getCreateTime().before(baseSeason.getSeasonBeginTime())){
            baseSeason.setType(0);
        }
        loginUserSetUtil.populateFields(baseSeason,1);
        return baseSeasonMapper.insertBaseSeason(baseSeason);
    }

    /**
     * 修改赛季
     * 
     * @param baseSeason 赛季
     * @return 结果
     */
    @Override
    public int updateBaseSeason(BaseSeason baseSeason)
    {
        if (baseSeason.getType() == 1){
            LambdaQueryWrapper<BaseSeason> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(BaseSeason::getContestId,SecurityUtils.getContestId());
            queryWrapper.eq(BaseSeason::getType,1);
            queryWrapper.ne(BaseSeason::getId,baseSeason.getId());
            List<BaseSeason> seasonList = baseMapper.selectList(queryWrapper);

            if (seasonList.size()>0){
                throw new RuntimeException("当前存在进行中的赛季！");
            }

        }
        baseSeason.setUpdateTime(DateUtils.getNowDate());
        loginUserSetUtil.populateFields(baseSeason,2);
        return baseSeasonMapper.updateBaseSeason(baseSeason);
    }

    /**
     * 批量删除赛季
     * 
     * @param ids 需要删除的赛季主键
     * @return 结果
     */
    @Override
    public int deleteBaseSeasonByIds(Long[] ids)
    {
        return baseSeasonMapper.deleteBaseSeasonByIds(ids);
    }

    /**
     * 删除赛季信息
     * 
     * @param id 赛季主键
     * @return 结果
     */
    @Override
    public int deleteBaseSeasonById(Long id)
    {
        return baseSeasonMapper.deleteBaseSeasonById(id);
    }
}
