package com.ruoyi.system.controller;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.system.domain.BaseContest;
import com.ruoyi.system.domain.BaseLiveTournament;
import com.ruoyi.system.domain.BasePlayerLog;
import com.ruoyi.system.domain.BaseTeamGrouping;
import com.ruoyi.system.domain.vo.*;
import com.ruoyi.system.mapper.BaseLiveTournamentMapper;
import com.ruoyi.system.service.*;
import com.ruoyi.system.service.impl.LiveService;
import com.tencentcloudapi.mps.v20190612.models.DescribeTaskDetailResponse;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author Wangbj
 * @date 2024年05月08日 11:27
 */
@ApiOperation("联赛H5")
@RestController
@RequestMapping("/league/h5")
public class BaseContestH5Controller extends BaseController {
    @Resource
    private IBaseContestService baseContestService;

    @Resource
    private IBaseTournamentService baseTournamentService;

    @Resource
    private IBasePlayerLogService basePlayerLogService;

    @Resource
    private BaseLiveTournamentMapper baseLiveTournamentMapper;

    @Resource
    private IBaseTeamGroupingService iBaseTeamGroupingService;
    @Autowired
    private IBaseBriefService baseBriefService;
    /**
     * 查询联赛列表
     */
    @GetMapping("/contestList")
    @Operation(summary = "查询联赛列表", description = "查询联赛列表")
    public TableDataInfo contestList(BaseContest baseContest)
    {
        startPage();
        baseContest.setIsDeleted(0l);
        List<BaseContest> list = baseContestService.list(
                Wrappers.<BaseContest>lambdaQuery().setEntity(baseContest)
                        .orderByDesc(BaseContest::getCreateTime));
        return getDataTable(list);
    }


    /**
     * 查询赛事列表
     */
    @GetMapping("/tournamentList")
    @Operation(summary = "查询赛事列表", description = "查询赛事列表")
    public TableDataInfo tournamentList(@RequestParam Long contestId,
                                        @RequestParam Integer state,Integer pageNum,Integer pageSize,Long teamId)
    {
        if (ObjectUtil.isNull(contestId)){
            throw new RuntimeException("联赛id不能为空");
        }

//        startPage();
        return baseTournamentService.selectBaseTournamentList(contestId,state,teamId,pageNum,pageSize);
    }

    /**
     * 获取赛事详细信息
     */
    @Operation(summary = "获取赛事详细信息", description = "获取赛事详细信息")
    @GetMapping(value = "/{id}")
    public AjaxResult getTournamentInfo(@PathVariable("id") Long tournamentId)
    {
        return success(baseTournamentService.selectBaseTournamentById(tournamentId));
    }

    /**
     * 赛况
     */
    @GetMapping("/playerLogList")
    @Operation(summary = "赛况", description = "赛况")
    public TableDataInfo playerLogList(BasePlayerLog basePlayerLog)
    {
        startPage();
        List<BasePlayerLogVO> list = basePlayerLogService.selectBasePlayerLogList(basePlayerLog);
        return getDataTable(list);
    }

    /**
     * 比赛总结
     */
    @GetMapping("/getSummary")
    @Operation(summary = "比赛总结", description = "比赛总结")
    public AjaxResult getSummary(@RequestParam Long tournamentId)
    {
        return AjaxResult.success(basePlayerLogService.getSummary(tournamentId));
    }

    /**
     * 实时数据
     */
    @GetMapping("/realTimeData")
    @Operation(summary = "数据模块 两个球队球员数据", description = "数据模块 两个球队球员数据")
    public AjaxResult realTimeData(BasePlayerLog basePlayerLog)
    {
        return success(basePlayerLogService.realTimeData(basePlayerLog));
    }

    /**
     * 实时数据比赛得分栏
     */
    @GetMapping("/realTimeDataTeam")
    @Operation(summary = "实时数据比赛得分栏", description = "实时数据比赛得分栏")
    public AjaxResult realTimeDataTeam(BasePlayerLog basePlayerLog)
    {
        return success(basePlayerLogService.realTimeDataTeam(basePlayerLog));
    }

    /**
     * 排行榜
     */
    @GetMapping("/rankingList")
    @Operation(summary = "排行榜", description = "排行榜,contestId:联赛id， type：1:得分；2:三分；3:篮板；4:助攻；")
    public List<BaseRankingListVO> rankingList(@RequestParam Long contestId, @RequestParam int type,String types)
    {
        return basePlayerLogService.rankingList(contestId,type,types);
    }


    /**
     * 本场最佳
     */
    @GetMapping("/bestOfTheGame")
    @Operation(summary = "本场最佳", description = "本场最佳 tournamentId 比赛id")
    public BestOfTheGameResponseVO bestOfTheGame(@RequestParam Long tournamentId)
    {
        return basePlayerLogService.bestOfTheGame(tournamentId);
    }


    /**
     * 球队比较
     */
    @GetMapping("/teamCompare")
    @Operation(summary = "球队比较", description = "球队比较 tournamentId 比赛id")
    public BaseTeamCompareResponseVO teamCompare(@RequestParam Long tournamentId)
    {
        return basePlayerLogService.teamCompare(tournamentId);
    }
    /**
     * 比分
     */
    @Operation(summary = "比分", description = "比分")
    @GetMapping(value = "/getScore")
    public AjaxResult getScore(@RequestParam Long tournamentId)
    {
        //比赛id
        return success(basePlayerLogService.getTournamentScore(tournamentId));
    }
    /**
     * 获取比赛直播房间地址
     */
    @Operation(summary = "获取比赛直播房间地址", description = "获取比赛直播房间地址")
    @GetMapping(value = "/getLiveViewUrl")
    public AjaxResult getInfo(@RequestParam Long tournamentId) {
        LambdaQueryWrapper<BaseLiveTournament> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(BaseLiveTournament::getTournamentId,tournamentId);
        queryWrapper.eq(BaseLiveTournament::getType,0);
        queryWrapper.last("limit 1");
        BaseLiveTournament baseLiveTournament = baseLiveTournamentMapper.selectOne(queryWrapper);
        if (ObjectUtil.isNull(baseLiveTournament)){
            return AjaxResult.error("比赛不存在");
        }
        baseLiveTournament.setPushUrl(null);
        return success(baseLiveTournament.getViewUrl());
    }
    /**
     * 球队分组排行榜
     */
    @GetMapping("/team/rankingList")
    @Operation(summary = "球队分组排行榜", description = "排行榜,contestId:联赛id")
    public List<BaseTeamGrouping> teamRankingList(@RequestParam Long contestId)
    {
        return iBaseTeamGroupingService.teamRankingList(contestId);
    }

    /**
     * 欢喜-直播接口
     */
    @GetMapping("/league/getTeamStatsCaption")
    @Operation(summary = "欢喜-直播接口", description = "scheduleId:联赛id")
    public Map<String,Object> getTeamStatsCaption(@RequestParam Long scheduleId)
    {
        return baseTournamentService.getTeamStatsCaption(scheduleId);
    }
    /**
     * 欢喜-直播接口
     */
    @GetMapping("/league/getTournamentData")
    @Operation(summary = "欢喜-直播接口", description = "tournamentId:比赛id")
    public LinkedHashMap<String,Object> getTournamentData(@RequestParam Long tournamentId,@RequestParam Long teamId)
    {
        return baseTournamentService.getTournamentData(tournamentId,teamId);
    }

    /**
     * 安卓-获取联赛水印图
     */
    @GetMapping("/contest/getLogo")
    @Operation(summary = "安卓-获取联赛水印图", description = "tournamentId:联赛id")
    public BaseContest getContestLogo(@RequestParam Long tournamentId)
    {
        return baseTournamentService.getContestLogo(tournamentId);
    }

    /**
     * 获取比赛简介详细信息
     */
    @Operation(summary = "获取比赛简介详细信息", description = "获取比赛简介详细信息")
    @RequiresPermissions("system:brief:query")
    @GetMapping("/getBriefInfo")
    public AjaxResult getBriefInfo(Long contestId)
    {

        return success(baseBriefService.selectBaseBriefH5ById(contestId));
    }
    /**
     * 获取轮播图
     */
    @Operation(summary = "获取轮播图", description = "获取轮播图")
    @GetMapping(value = "/getContestBanner")
    public AjaxResult getContestBanner(@RequestParam Long contestId)
    {
        //比赛id
        return success(basePlayerLogService.getContestBanner(contestId));
    }
}
