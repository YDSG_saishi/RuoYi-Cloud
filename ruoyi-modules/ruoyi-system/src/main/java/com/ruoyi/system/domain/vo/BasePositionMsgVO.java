package com.ruoyi.system.domain.vo;

import com.ruoyi.system.domain.BasePlayerPositionLog;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author Wangbj
 * @date 2024年04月27日 22:19
 */
@Data
@Accessors(chain = true)
public class BasePositionMsgVO extends BasePlayerPositionLog {

    @Schema(name = "playerName", description = "球员姓名")
    private String playerName;

    @Schema(name = "playerNumber", description = "球员号码")
    private String playerNumber;

}
