package com.ruoyi.system.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.domain.BaseBrief;

/**
 * 比赛简介Service接口
 * 
 * @author ruoyi
 * @date 2024-07-16
 */
public interface IBaseBriefService extends IService<BaseBrief>
{
    /**
     * 查询比赛简介
     * 
     * @param id 比赛简介主键
     * @return 比赛简介
     */
    public BaseBrief selectBaseBriefById(Long contestId);
    public Map selectBaseBriefH5ById(Long contestId);

    /**
     * 查询比赛简介列表
     * 
     * @param baseBrief 比赛简介
     * @return 比赛简介集合
     */
    public List<BaseBrief> selectBaseBriefList(BaseBrief baseBrief);

    /**
     * 新增比赛简介
     * 
     * @param baseBrief 比赛简介
     * @return 结果
     */
    public int insertBaseBrief(BaseBrief baseBrief);

    /**
     * 修改比赛简介
     * 
     * @param baseBrief 比赛简介
     * @return 结果
     */
    public int updateBaseBrief(BaseBrief baseBrief);

    /**
     * 批量删除比赛简介
     * 
     * @param ids 需要删除的比赛简介主键集合
     * @return 结果
     */
    public int deleteBaseBriefByIds(Long[] ids);

    /**
     * 删除比赛简介信息
     * 
     * @param id 比赛简介主键
     * @return 结果
     */
    public int deleteBaseBriefById(Long id);
}
