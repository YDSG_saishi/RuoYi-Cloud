package com.ruoyi.system.domain.vo;

import com.ruoyi.common.core.enums.PlayerLogType;
import com.ruoyi.common.core.utils.CommonStreamUtil;
import com.ruoyi.system.domain.BasePlayerLog;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

/**
 * @author Wangbj
 * @date 2024年05月08日 22:30
 */
@Data
@Accessors(chain = true)
public class BaseTeamCompareVO {
    @Schema(name = "lb", description = "篮板")
    private Integer lb = 0;

    @Schema(name = "ef", description = "二分")
    private Integer ef = 0;
    @Schema(name = "ef", description = "二分不中")
    private Integer efNot = 0;

    @Schema(name = "sf", description = "三分")
    private Integer sf = 0;
    @Schema(name = "sfNot", description = "三分不中")
    private Integer sfNot = 0;

    @Schema(name = "fq", description = "罚球")
    private Integer fq = 0;

    @Schema(name = "fqNot", description = "罚球不中")
    private Integer fqNot = 0;

    @Schema(name = "gm", description = "盖帽")
    private Integer gm = 0;

    @Schema(name = "zg", description = "助攻")
    private Integer zg = 0;

    @Schema(name = "qd", description = "抢断")
    private Integer qd = 0;

    @Schema(name = "fg", description = "犯规")
    private Integer fg = 0;

    @Schema(name = "sw", description = "失误")
    private Integer sw = 0;

    @Schema(name = "score", description = "总分")
    private Long score = 0L;

    @Schema(name = "score", description = "罚球命中率")
    private BigDecimal pointRate = BigDecimal.ZERO;

    @Schema(name = "score", description = "二分命中率")
    private BigDecimal twoPointRate = BigDecimal.ZERO;

    @Schema(name = "score", description = "三分命中率")
    private BigDecimal threePointRate = BigDecimal.ZERO;

    public static BaseTeamCompareVO build(List<BasePlayerLog> playerLogList,Long teamId){
        if (Objects.nonNull(teamId)){
            playerLogList = CommonStreamUtil.filter(playerLogList,o -> Objects.nonNull(o.getBasketballTeamId()) && Objects.equals(o.getBasketballTeamId(),teamId));
        }
        BaseTeamCompareVO vo = new BaseTeamCompareVO();
        vo.setLb(CommonStreamUtil.filter(playerLogList,o -> Objects.equals(o.getType(), PlayerLogType.BACKBOARD.getCode())).size());
        vo.setEf(CommonStreamUtil.filter(playerLogList,o -> Objects.equals(o.getType(), PlayerLogType.TWO_POINT_HIT.getCode())).size());
        vo.setSf(CommonStreamUtil.filter(playerLogList,o -> Objects.equals(o.getType(), PlayerLogType.THREE_POINT_HIT.getCode())).size());
        vo.setFq(CommonStreamUtil.filter(playerLogList,o -> Objects.equals(o.getType(), PlayerLogType.POINT_HIT.getCode())).size());
        vo.setGm(CommonStreamUtil.filter(playerLogList,o -> Objects.equals(o.getType(), PlayerLogType.BLOCK_SHOT.getCode())).size());
        vo.setZg(CommonStreamUtil.filter(playerLogList,o -> Objects.equals(o.getType(), PlayerLogType.ASSIST.getCode())).size());
        vo.setQd(CommonStreamUtil.filter(playerLogList,o -> Objects.equals(o.getType(), PlayerLogType.STRAL.getCode())).size());
        vo.setFg(CommonStreamUtil.filter(playerLogList,o -> Objects.equals(o.getType(), PlayerLogType.FOUL.getCode())).size());
        List<BasePlayerLog> filter = CommonStreamUtil.filter(playerLogList, o -> Objects.equals(o.getIsScore(), 0));
        vo.setScore(filter.stream().map(BasePlayerLog::getScore).reduce(0L, Long::sum));
        return vo;
    }
}
