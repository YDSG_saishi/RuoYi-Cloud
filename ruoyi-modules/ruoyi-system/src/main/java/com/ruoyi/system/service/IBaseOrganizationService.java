package com.ruoyi.system.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.domain.BaseOrganization;

/**
 * 机构Service接口
 * 
 * @author wangbj
 * @date 2024-04-16
 */
public interface IBaseOrganizationService extends IService<BaseOrganization>
{
    /**
     * 查询机构
     * 
     * @param id 机构主键
     * @return 机构
     */
    public BaseOrganization selectBaseOrganizationById(Long id);

    /**
     * 查询机构列表
     * 
     * @param baseOrganization 机构
     * @return 机构集合
     */
    public List<BaseOrganization> selectBaseOrganizationList(BaseOrganization baseOrganization);

    /**
     * 新增机构
     * 
     * @param baseOrganization 机构
     * @return 结果
     */
    public BaseOrganization insertBaseOrganization(BaseOrganization baseOrganization);

    /**
     * 修改机构
     * 
     * @param baseOrganization 机构
     * @return 结果
     */
    public int updateBaseOrganization(BaseOrganization baseOrganization);

    /**
     * 批量删除机构
     * 
     * @param ids 需要删除的机构主键集合
     * @return 结果
     */
    public int deleteBaseOrganizationByIds(Long[] ids);

    /**
     * 删除机构信息
     * 
     * @param id 机构主键
     * @return 结果
     */
    public int deleteBaseOrganizationById(Long id);
}
