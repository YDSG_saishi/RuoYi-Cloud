package com.ruoyi.system.domain;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LiveStreamCode {

    /**
     * 直播间唯一标识
     */
    @Schema(name = "streamName", description = "直播间唯一标识")
    private String streamName;

    /**
     * obs服务器
     */
    @Schema(name = "obsServer", description = "obs服务器")
    private String obsServer;

    /***
     * 推流码
     */
    @Schema(name = "streamCode", description = "推流码")
    private String streamCode;

    /**
     * 播放码
     */
    @Schema(name = "playCode", description = "播放码")
    private String playCode;

}
