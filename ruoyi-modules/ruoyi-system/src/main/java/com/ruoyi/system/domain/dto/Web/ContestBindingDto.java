package com.ruoyi.system.domain.dto.Web;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ContestBindingDto {

    /**
     * 指定绑定id
     */
    private Long id;
    /**
     * 选项绑定ids
     */
    private List<Long> ids = new ArrayList<>();

}
