package com.ruoyi.system.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.security.utils.SecurityUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.domain.BaseBrief;
import com.ruoyi.system.service.IBaseBriefService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;

import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 比赛简介Controller
 * 
 * @author ruoyi
 * @date 2024-07-16
 */
@ApiOperation("比赛简介")
@RestController
@RequestMapping("/brief")
public class BaseBriefController extends BaseController
{
    @Autowired
    private IBaseBriefService baseBriefService;

    /**
     * 查询比赛简介列表
     */
    @RequiresPermissions("system:brief:list")
    @GetMapping("/list")
    @Operation(summary = "查询比赛简介列表", description = "查询比赛简介列表")
    public TableDataInfo list(BaseBrief baseBrief)
    {
        startPage();
        List<BaseBrief> list = baseBriefService.selectBaseBriefList(baseBrief);
        return getDataTable(list);
    }

    /**
     * 导出比赛简介列表
     */
    @Operation(summary = "导出比赛简介列表", description = "导出比赛简介列表")
    @RequiresPermissions("system:brief:export")
    @Log(title = "比赛简介", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseBrief baseBrief)
    {
        List<BaseBrief> list = baseBriefService.selectBaseBriefList(baseBrief);
        ExcelUtil<BaseBrief> util = new ExcelUtil<BaseBrief>(BaseBrief.class);
        util.exportExcel(response, list, "比赛简介数据");
    }

    /**
     * 获取比赛简介详细信息
     */
    @Operation(summary = "获取比赛简介详细信息", description = "获取比赛简介详细信息")
    @RequiresPermissions("system:brief:query")
    @GetMapping("/getInfo")
    public AjaxResult getInfo()
    {
        Long contestId = SecurityUtils.getContestId();
        return success(baseBriefService.selectBaseBriefById(contestId));
    }

    /**
     * 新增比赛简介
     */
    @Operation(summary = "新增比赛简介", description = "新增比赛简介")
    @RequiresPermissions("system:brief:add")
    @Log(title = "比赛简介", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseBrief baseBrief)
    {
        return toAjax(baseBriefService.insertBaseBrief(baseBrief));
    }

    /**
     * 修改比赛简介
     */
    @Operation(summary = "修改比赛简介", description = "修改比赛简介")
    @RequiresPermissions("system:brief:edit")
    @Log(title = "比赛简介", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseBrief baseBrief)
    {
        return toAjax(baseBriefService.updateBaseBrief(baseBrief));
    }

    /**
     * 删除比赛简介
     */
    @Operation(summary = "删除比赛简介", description = "删除比赛简介")
    @RequiresPermissions("system:brief:remove")
    @Log(title = "比赛简介", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseBriefService.deleteBaseBriefByIds(ids));
    }
}
