package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.web.domain.BaseEntity;

import java.math.BigDecimal;

/**
 * 联赛球队分组和球队关联对象 base_tean_grouping_play_team
 * 
 * @author ruoyi
 * @date 2024-05-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("base_tean_grouping_play_team")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BaseTeanGroupingPlayTeam extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @TableId(type = IdType.AUTO)
    @Schema(name = "id", description = "主键")
    private Long id;

    /** 球队id */
    @Excel(name = "球队id")
    @Schema(name = "basketballTeamId", description = "球队id")
    private Long basketballTeamId;

    /** 积分数量 */
    @Excel(name = "积分数量")
    @Schema(name = "points", description = "积分数量")
    private Long points;

    /** 胜利次数 */
    @Excel(name = "胜利次数")
    @Schema(name = "winNum", description = "胜利次数")
    private Long winNum;

    /** 失败次数 */
    @Excel(name = "失败次数")
    @Schema(name = "lossNum", description = "失败次数")
    private Long lossNum;

    /** 净胜分 */
    @Excel(name = "净胜分")
    @Schema(name = "goalDifference", description = "净胜分")
    private BigDecimal goalDifference;

    /** 队伍分组id */
    @Excel(name = "队伍分组id")
    @Schema(name = "teamGroupingId", description = "队伍分组id")
    private Long teamGroupingId;

    /** 创建人id */
    @Excel(name = "创建人id")
    @Schema(name = "createUserId", description = "创建人id")
    private Long createUserId;

    /** 创建人姓名 */
    @Excel(name = "创建人姓名")
    @Schema(name = "createUserName", description = "创建人姓名")
    private String createUserName;

    /** 修改人id */
    @Excel(name = "修改人id")
    @Schema(name = "updateUserId", description = "修改人id")
    private Long updateUserId;

    /** 修改人姓名 */
    @Excel(name = "修改人姓名")
    @Schema(name = "updateUserName", description = "修改人姓名")
    private String updateUserName;

    /** 是否已删除 0:未删除;1:已删除 */
    @Excel(name = "是否已删除 0:未删除;1:已删除")
    @Schema(name = "isDeleted", description = "是否已删除 0:未删除;1:已删除")
    private Long isDeleted;

    @TableField(exist = false)
    private BaseBasketballTeam baseBasketballTeam;

    @TableField(exist = false)
    private BigDecimal chanceWin;
    @TableField(exist = false)
    private String name;
    @TableField(exist = false)
    private String teamName;
    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setBasketballTeamId(Long basketballTeamId) 
    {
        this.basketballTeamId = basketballTeamId;
    }

    public Long getBasketballTeamId() 
    {
        return basketballTeamId;
    }
    public void setPoints(Long points) 
    {
        this.points = points;
    }

    public Long getPoints() 
    {
        return points;
    }
    public void setTeamGroupingId(Long teamGroupingId) 
    {
        this.teamGroupingId = teamGroupingId;
    }

    public Long getTeamGroupingId() 
    {
        return teamGroupingId;
    }
    public void setCreateUserId(Long createUserId) 
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId() 
    {
        return createUserId;
    }
    public void setCreateUserName(String createUserName) 
    {
        this.createUserName = createUserName;
    }

    public String getCreateUserName() 
    {
        return createUserName;
    }
    public void setUpdateUserId(Long updateUserId) 
    {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateUserId() 
    {
        return updateUserId;
    }
    public void setUpdateUserName(String updateUserName) 
    {
        this.updateUserName = updateUserName;
    }

    public String getUpdateUserName() 
    {
        return updateUserName;
    }
    public void setIsDeleted(Long isDeleted) 
    {
        this.isDeleted = isDeleted;
    }

    public Long getIsDeleted() 
    {
        return isDeleted;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("basketballTeamId", getBasketballTeamId())
            .append("points", getPoints())
            .append("teamGroupingId", getTeamGroupingId())
            .append("remark", getRemark())
            .append("createUserId", getCreateUserId())
            .append("createUserName", getCreateUserName())
            .append("createTime", getCreateTime())
            .append("updateUserId", getUpdateUserId())
            .append("updateUserName", getUpdateUserName())
            .append("updateTime", getUpdateTime())
            .append("isDeleted", getIsDeleted())
            .toString();
    }
}
