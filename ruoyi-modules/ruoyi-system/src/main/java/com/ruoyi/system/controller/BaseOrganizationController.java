package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.system.domain.BaseOrganization;
import com.ruoyi.system.service.IBaseOrganizationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;

import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 机构Controller
 * 
 * @author wangbj
 * @date 2024-04-16
 */
@ApiOperation("机构")
@RestController
@RequestMapping("/organization")
public class BaseOrganizationController extends BaseController
{
    @Autowired
    private IBaseOrganizationService baseOrganizationService;

    /**
     * 查询机构列表
     */
    @RequiresPermissions("system:organization:list")
    @GetMapping("/list")
    @Operation(summary = "查询机构列表", description = "查询机构列表")
    public TableDataInfo list(BaseOrganization baseOrganization)
    {
        startPage();
        List<BaseOrganization> list = baseOrganizationService.selectBaseOrganizationList(baseOrganization);
        return getDataTable(list);
    }

    /**
     * 导出机构列表
     */
    @Operation(summary = "导出机构列表", description = "导出机构列表")
    @RequiresPermissions("system:organization:export")
    @Log(title = "机构", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseOrganization baseOrganization)
    {
        List<BaseOrganization> list = baseOrganizationService.selectBaseOrganizationList(baseOrganization);
        ExcelUtil<BaseOrganization> util = new ExcelUtil<BaseOrganization>(BaseOrganization.class);
        util.exportExcel(response, list, "机构数据");
    }

    /**
     * 获取机构详细信息
     */
    @Operation(summary = "获取机构详细信息", description = "获取机构详细信息")
    @RequiresPermissions("system:organization:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(baseOrganizationService.selectBaseOrganizationById(id));
    }

    /**
     * 新增机构
     */
    @Operation(summary = "新增机构", description = "新增机构")
    @RequiresPermissions("system:organization:add")
    @Log(title = "机构", businessType = BusinessType.INSERT)
    @PostMapping
    public BaseOrganization add(@RequestBody BaseOrganization baseOrganization)
    {
        return baseOrganizationService.insertBaseOrganization(baseOrganization);
    }

    /**
     * 修改机构
     */
    @Operation(summary = "修改机构", description = "修改机构")
    @RequiresPermissions("system:organization:edit")
    @Log(title = "机构", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseOrganization baseOrganization)
    {
        return toAjax(baseOrganizationService.updateBaseOrganization(baseOrganization));
    }

    /**
     * 删除机构
     */
    @Operation(summary = "删除机构", description = "删除机构")
    @RequiresPermissions("system:organization:remove")
    @Log(title = "机构", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseOrganizationService.deleteBaseOrganizationByIds(ids));
    }
}
