package com.ruoyi.system.domain.vo;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author Wangbj
 * @date 2024年05月08日 17:47
 */
@Data
@Accessors(chain = true)
public class BaseSummaryVO {

    private Long playerId;
    private Long teamId;
    private String playerName;
    private String teamName;
    //得分
    private Integer score;
    //篮板
    private Integer backboard;


}
