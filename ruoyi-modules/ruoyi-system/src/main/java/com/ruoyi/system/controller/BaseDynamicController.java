package com.ruoyi.system.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.domain.BaseDynamic;
import com.ruoyi.system.service.IBaseDynamicService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;

import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 动态Controller
 * 
 * @author ruoyi
 * @date 2024-04-26
 */
@ApiOperation("动态")
@RestController
@RequestMapping("/dynamic")
public class BaseDynamicController extends BaseController
{
    @Autowired
    private IBaseDynamicService baseDynamicService;

    /**
     * 查询动态列表
     */
    @RequiresPermissions("system:dynamic:list")
    @GetMapping("/list")
    @Operation(summary = "查询动态列表", description = "查询动态列表")
    public TableDataInfo list(BaseDynamic baseDynamic)
    {
        startPage();
        List<BaseDynamic> list = baseDynamicService.selectBaseDynamicList(baseDynamic);
        return getDataTable(list);
    }

    /**
     * 导出动态列表
     */
    @Operation(summary = "导出动态列表", description = "导出动态列表")
    @RequiresPermissions("system:dynamic:export")
    @Log(title = "动态", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseDynamic baseDynamic)
    {
        List<BaseDynamic> list = baseDynamicService.selectBaseDynamicList(baseDynamic);
        ExcelUtil<BaseDynamic> util = new ExcelUtil<BaseDynamic>(BaseDynamic.class);
        util.exportExcel(response, list, "动态数据");
    }

    /**
     * 获取动态详细信息
     */
    @Operation(summary = "获取动态详细信息", description = "获取动态详细信息")
    @RequiresPermissions("system:dynamic:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(baseDynamicService.selectBaseDynamicById(id));
    }

    /**
     * 新增动态
     */
    @Operation(summary = "新增动态", description = "新增动态")
    @RequiresPermissions("system:dynamic:add")
    @Log(title = "动态", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseDynamic baseDynamic)
    {
        return toAjax(baseDynamicService.insertBaseDynamic(baseDynamic));
    }

    /**
     * 修改动态
     */
    @Operation(summary = "修改动态", description = "修改动态")
    @RequiresPermissions("system:dynamic:edit")
    @Log(title = "动态", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseDynamic baseDynamic)
    {
        return toAjax(baseDynamicService.updateBaseDynamic(baseDynamic));
    }

    /**
     * 删除动态
     */
    @Operation(summary = "删除动态", description = "删除动态")
    @RequiresPermissions("system:dynamic:remove")
    @Log(title = "动态", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseDynamicService.deleteBaseDynamicByIds(ids));
    }
}
