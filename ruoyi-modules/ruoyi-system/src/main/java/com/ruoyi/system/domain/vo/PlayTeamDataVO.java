package com.ruoyi.system.domain.vo;

import lombok.Data;

@Data
public class PlayTeamDataVO {
    /**
     * 能力 全场柱状图
     */
   private BaseTeamCompareVO teamCompare;
    /**
     * 能力 场均柱状图
     */
   private CopyBaseTeamCompareVO copyTeamCompare;
    /**
     * 胜场
     */
   private Integer winNum = 0;
    /**
     * 负场
     */
    private Integer loseNum = 0;
    /**
     * 平场
     */
    private String num;
    /**
     * 计分方式:0:5v5计分;1:3v3计分;
     */
    private Long scoringWay = 0L;
    public PlayTeamDataVO(){
        teamCompare = new BaseTeamCompareVO();
        copyTeamCompare = new CopyBaseTeamCompareVO();
    }
}
