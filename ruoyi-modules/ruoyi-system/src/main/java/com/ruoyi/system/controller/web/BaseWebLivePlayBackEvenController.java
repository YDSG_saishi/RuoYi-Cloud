package com.ruoyi.system.controller.web;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.core.web.page.WebTableDataInfo;
import com.ruoyi.system.api.domain.SysUser;
import com.ruoyi.system.domain.BaseBasketballPlayer;
import com.ruoyi.system.domain.BaseLivePlayBackEven;
import com.ruoyi.system.domain.vo.BaseTournamentResponseVO;
import com.ruoyi.system.service.IBaseBasketballPlayerService;
import com.ruoyi.system.service.IBaseLivePlayBackEvenService;
import com.ruoyi.system.service.IBaseTournamentService;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 比赛直播房间Controller
 * 
 * @author ruoyi
 * @date 2024-05-10
 */
@ApiOperation("比赛直播房间")
@RestController
@RequestMapping("/web/live/playback/even")
public class BaseWebLivePlayBackEvenController extends BaseController {


    @Resource
    private IBaseLivePlayBackEvenService iBaseLivePlayBackEvenService;
    @Resource
    private IBaseTournamentService iBaseTournamentService;
    @Resource
    private IBaseBasketballPlayerService iBaseBasketballPlayerService;

    /**
     * 查询比赛回放列表
     */
    @GetMapping("/list")
    @Operation(summary = "查询比赛直播房间列表", description = "查询比赛直播房间列表")
    public WebTableDataInfo list(BaseLivePlayBackEven baseLiveTournament) {
        List<BaseLivePlayBackEven> list = iBaseLivePlayBackEvenService.list(
                Wrappers.<BaseLivePlayBackEven>lambdaQuery()
                        .eq(BaseLivePlayBackEven::getTournamentId,baseLiveTournament.getTournamentId())
                        .eq(BaseLivePlayBackEven::getType, Objects.isNull(baseLiveTournament.getType())?0:1)
                        .in(BaseLivePlayBackEven::getName,"全场集锦","个人全场集锦","直播视频回放")
                        .in(BaseLivePlayBackEven::getStatus,3)
                        .orderByDesc(BaseLivePlayBackEven::getCreateTime));
        if (CollUtil.isEmpty(list)){
            return getWebDataTable(list);
        }
        Set<Long> playIds = list.stream().map(BaseLivePlayBackEven::getPlayId).collect(Collectors.toSet());
        List<BaseBasketballPlayer> baseBasketballPlayers = iBaseBasketballPlayerService.listByIds(playIds);
        Map<Long, BaseBasketballPlayer> playMap = baseBasketballPlayers.stream().collect(Collectors.toMap(BaseBasketballPlayer::getId, Function.identity()));
        list.forEach(o ->{
            if (Objects.nonNull(o.getPlayId()) && playMap.containsKey(o.getPlayId())){
                BaseBasketballPlayer play = playMap.get(o.getPlayId());
                o.setName(play.getName() +"-"+o.getName());
            }
        });
        return getWebDataTable(list);
    }


}


