package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 机构对象 base_organization
 * 
 * @author wangbj
 * @date 2024-04-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("base_organization")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BaseOrganization extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @TableId(type = IdType.AUTO)
    @Schema(name = "id", description = "主键")
    private Long id;

    /** 机构名称 */
    @Excel(name = "机构名称")
    @Schema(name = "name", description = "机构名称")
    private String name;

    /** 机构类型:1:协会;2:公司;3:学校;4:其他; */
    @Excel(name = "机构类型:1:协会;2:公司;3:学校;4:其他;")
    @Schema(name = "type", description = "机构类型:1:协会;2:公司;3:学校;4:其他;")
    private Long type;

    /** 机构logo */
    @Excel(name = "机构logo")
    @Schema(name = "organizationLogo", description = "机构logo")
    private String organizationLogo;

    /** 机构封面 */
    @Excel(name = "机构封面")
    @Schema(name = "organizationPhoto", description = "机构封面")
    private String organizationPhoto;

    /** 地区 */
    @Excel(name = "地区")
    @Schema(name = "area", description = "地区")
    private String area;

    /** 详细地址 */
    @Excel(name = "详细地址")
    @Schema(name = "address", description = "详细地址")
    private String address;

    /** 联系电话 */
    @Excel(name = "联系电话")
    @Schema(name = "phoneNumber", description = "联系电话")
    private String phoneNumber;

    /** 邮箱 */
    @Excel(name = "邮箱")
    @Schema(name = "email", description = "邮箱")
    private String email;

    /** 创建人id */
    @Excel(name = "创建人id")
    @Schema(name = "createUserId", description = "创建人id")
    private Long createUserId;

    /** 创建人姓名 */
    @Excel(name = "创建人姓名")
    @Schema(name = "createUserName", description = "创建人姓名")
    private String createUserName;

    /** 修改人id */
    @Excel(name = "修改人id")
    @Schema(name = "updateUserId", description = "修改人id")
    private Long updateUserId;

    /** 修改人姓名 */
    @Excel(name = "修改人姓名")
    @Schema(name = "updateUserName", description = "修改人姓名")
    private String updateUserName;

    /** 是否已删除 0:未删除;1:已删除 */
    @Excel(name = "是否已删除 0:未删除;1:已删除")
    @Schema(name = "isDeleted", description = "是否已删除 0:未删除;1:已删除")
    private Long isDeleted;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }
    public void setOrganizationLogo(String organizationLogo) 
    {
        this.organizationLogo = organizationLogo;
    }

    public String getOrganizationLogo() 
    {
        return organizationLogo;
    }
    public void setOrganizationPhoto(String organizationPhoto) 
    {
        this.organizationPhoto = organizationPhoto;
    }

    public String getOrganizationPhoto() 
    {
        return organizationPhoto;
    }
    public void setArea(String area) 
    {
        this.area = area;
    }

    public String getArea() 
    {
        return area;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }
    public void setPhoneNumber(String phoneNumber) 
    {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber() 
    {
        return phoneNumber;
    }
    public void setEmail(String email) 
    {
        this.email = email;
    }

    public String getEmail() 
    {
        return email;
    }
    public void setCreateUserId(Long createUserId) 
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId() 
    {
        return createUserId;
    }
    public void setCreateUserName(String createUserName) 
    {
        this.createUserName = createUserName;
    }

    public String getCreateUserName() 
    {
        return createUserName;
    }
    public void setUpdateUserId(Long updateUserId) 
    {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateUserId() 
    {
        return updateUserId;
    }
    public void setUpdateUserName(String updateUserName) 
    {
        this.updateUserName = updateUserName;
    }

    public String getUpdateUserName() 
    {
        return updateUserName;
    }
    public void setIsDeleted(Long isDeleted) 
    {
        this.isDeleted = isDeleted;
    }

    public Long getIsDeleted() 
    {
        return isDeleted;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("type", getType())
            .append("organizationLogo", getOrganizationLogo())
            .append("organizationPhoto", getOrganizationPhoto())
            .append("area", getArea())
            .append("address", getAddress())
            .append("remark", getRemark())
            .append("phoneNumber", getPhoneNumber())
            .append("email", getEmail())
            .append("createUserId", getCreateUserId())
            .append("createUserName", getCreateUserName())
            .append("createTime", getCreateTime())
            .append("updateUserId", getUpdateUserId())
            .append("updateUserName", getUpdateUserName())
            .append("updateTime", getUpdateTime())
            .append("isDeleted", getIsDeleted())
            .toString();
    }
}
