package com.ruoyi.system.controller.web;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.WebTableDataInfo;
import com.ruoyi.system.domain.BaseBanner;
import com.ruoyi.system.domain.dto.Web.BannerDto;
import com.ruoyi.system.service.IBaseBannerService;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * 管理后台-赛事推荐
 */
@ApiOperation("联赛")
@RestController
@RequestMapping("/web/banner")
public class BaseWebBannerController extends BaseController
{
    @Autowired
    private IBaseBannerService baseBannerService;


    /**
     * 查询banner列表
     */
    @GetMapping("/list")
    @Operation(summary = "查询banner列表", description = "查询banner列表")
    public WebTableDataInfo list(BannerDto dto)
    {
        startPage();
        List<BaseBanner> list = baseBannerService.selectList(dto);
        return getWebDataTable(list);
    }

    /**
     * 新增banner
     */
    @PostMapping("/save")
    @Operation(summary = "新增banner", description = "新增banner")
    public AjaxResult save(@RequestBody BaseBanner baseBanner)
    {
        return toAjax(baseBannerService.insertBanner(baseBanner));
    }

    /**
     * 删除banner
     */
    @PostMapping("/delete/{ids}")
    @Operation(summary = "删除banner", description = "删除banner")
    public AjaxResult delete(@PathVariable Long[] ids)
    {
        BaseBanner baseBanner = new BaseBanner();
        baseBanner.setIsDeleted(1L);
        return toAjax(baseBannerService.update(baseBanner,
                Wrappers.<BaseBanner>lambdaQuery().in(BaseBanner::getId, ids)));
    }
}
