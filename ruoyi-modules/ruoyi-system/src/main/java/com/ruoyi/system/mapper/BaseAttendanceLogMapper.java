package com.ruoyi.system.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.BaseAttendanceLog;

/**
 * 球员出勤记录Mapper接口
 * 
 * @author wangbj
 * @date 2024-04-16
 */
public interface BaseAttendanceLogMapper extends BaseMapper<BaseAttendanceLog>
{
    /**
     * 查询球员出勤记录
     * 
     * @param id 球员出勤记录主键
     * @return 球员出勤记录
     */
    public BaseAttendanceLog selectBaseAttendanceLogById(Long id);

    /**
     * 查询球员出勤记录列表
     * 
     * @param baseAttendanceLog 球员出勤记录
     * @return 球员出勤记录集合
     */
    public List<BaseAttendanceLog> selectBaseAttendanceLogList(BaseAttendanceLog baseAttendanceLog);

    /**
     * 新增球员出勤记录
     * 
     * @param baseAttendanceLog 球员出勤记录
     * @return 结果
     */
    public int insertBaseAttendanceLog(BaseAttendanceLog baseAttendanceLog);

    /**
     * 修改球员出勤记录
     * 
     * @param baseAttendanceLog 球员出勤记录
     * @return 结果
     */
    public int updateBaseAttendanceLog(BaseAttendanceLog baseAttendanceLog);

    /**
     * 删除球员出勤记录
     * 
     * @param id 球员出勤记录主键
     * @return 结果
     */
    public int deleteBaseAttendanceLogById(Long id);

    /**
     * 批量删除球员出勤记录
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseAttendanceLogByIds(Long[] ids);
}
