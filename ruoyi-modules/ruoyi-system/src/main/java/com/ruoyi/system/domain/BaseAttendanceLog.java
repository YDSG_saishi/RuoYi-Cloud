package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 球员出勤记录对象 base_attendance_log
 * 
 * @author wangbj
 * @date 2024-04-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("base_attendance_log")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BaseAttendanceLog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @TableId(type = IdType.AUTO)
    @Schema(name = "id", description = "主键")
    private Long id;

    /** 赛事id */
    @Excel(name = "赛事id")
    @Schema(name = "tournamentId", description = "赛事id")
    private Long tournamentId;

    /** 球员号码 */
    @Excel(name = "球员号码")
    @Schema(name = "playerNumber", description = "球员号码")
    private String  playerNumber;

    /** 球员id */
    @Excel(name = "球员id")
    @Schema(name = "basketballPlayerId", description = "球员id")
    private Long basketballPlayerId;

    /** 球队id */
    @Excel(name = "球队id")
    @Schema(name = "basketballTeamId", description = "球队id")
    private Long basketballTeamId;

    /** 赛季id */
    @Excel(name = "赛季id")
    @Schema(name = "seasonId", description = "赛季id")
    private Long seasonId;

    /** 机构id */
    @Excel(name = "机构id")
    @Schema(name = "organizationId", description = "机构id")
    private Long organizationId;

    /** 联赛id */
    @Excel(name = "联赛id")
    @Schema(name = "contestId", description = "联赛id")
    private Long contestId;

    /** 是否到场:0:否;1:是; */
    @Excel(name = "是否到场:0:否;1:是;")
    @Schema(name = "isAttendance", description = "是否到场:0:否;1:是;")
    private Long isAttendance;

    /** 比赛小节 */
    @Excel(name = "比赛小节")
    @Schema(name = "subsection", description = "比赛小节")
    private Long subsection;

    /** 创建人id */
    @Excel(name = "创建人id")
    @Schema(name = "createUserId", description = "创建人id")
    private Long createUserId;

    /** 创建人姓名 */
    @Excel(name = "创建人姓名")
    @Schema(name = "createUserName", description = "创建人姓名")
    private String createUserName;

    /** 修改人id */
    @Excel(name = "修改人id")
    @Schema(name = "updateUserId", description = "修改人id")
    private Long updateUserId;

    /** 修改人姓名 */
    @Excel(name = "修改人姓名")
    @Schema(name = "updateUserName", description = "修改人姓名")
    private String updateUserName;

    /** 是否已删除 0:未删除;1:已删除 */
    @Excel(name = "是否已删除 0:未删除;1:已删除")
    @Schema(name = "isDeleted", description = "是否已删除 0:未删除;1:已删除")
    private Long isDeleted;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setTournamentId(Long tournamentId) 
    {
        this.tournamentId = tournamentId;
    }

    public Long getTournamentId() 
    {
        return tournamentId;
    }
    public void setBasketballPlayerId(Long basketballPlayerId) 
    {
        this.basketballPlayerId = basketballPlayerId;
    }

    public Long getBasketballPlayerId() 
    {
        return basketballPlayerId;
    }
    public void setBasketballTeamId(Long basketballTeamId) 
    {
        this.basketballTeamId = basketballTeamId;
    }

    public Long getBasketballTeamId() 
    {
        return basketballTeamId;
    }
    public void setSeasonId(Long seasonId) 
    {
        this.seasonId = seasonId;
    }

    public Long getSeasonId() 
    {
        return seasonId;
    }
    public void setOrganizationId(Long organizationId) 
    {
        this.organizationId = organizationId;
    }

    public Long getOrganizationId() 
    {
        return organizationId;
    }
    public void setContestId(Long contestId) 
    {
        this.contestId = contestId;
    }

    public Long getContestId() 
    {
        return contestId;
    }
    public void setIsAttendance(Long isAttendance) 
    {
        this.isAttendance = isAttendance;
    }

    public Long getIsAttendance() 
    {
        return isAttendance;
    }
    public void setSubsection(Long subsection) 
    {
        this.subsection = subsection;
    }

    public Long getSubsection() 
    {
        return subsection;
    }
    public void setCreateUserId(Long createUserId) 
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId() 
    {
        return createUserId;
    }
    public void setCreateUserName(String createUserName) 
    {
        this.createUserName = createUserName;
    }

    public String getCreateUserName() 
    {
        return createUserName;
    }
    public void setUpdateUserId(Long updateUserId) 
    {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateUserId() 
    {
        return updateUserId;
    }
    public void setUpdateUserName(String updateUserName) 
    {
        this.updateUserName = updateUserName;
    }

    public String getUpdateUserName() 
    {
        return updateUserName;
    }
    public void setIsDeleted(Long isDeleted) 
    {
        this.isDeleted = isDeleted;
    }

    public Long getIsDeleted() 
    {
        return isDeleted;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("tournamentId", getTournamentId())
            .append("basketballPlayerId", getBasketballPlayerId())
            .append("basketballTeamId", getBasketballTeamId())
            .append("seasonId", getSeasonId())
            .append("organizationId", getOrganizationId())
            .append("contestId", getContestId())
            .append("isAttendance", getIsAttendance())
            .append("subsection", getSubsection())
            .append("remark", getRemark())
            .append("createUserId", getCreateUserId())
            .append("createUserName", getCreateUserName())
            .append("createTime", getCreateTime())
            .append("updateUserId", getUpdateUserId())
            .append("updateUserName", getUpdateUserName())
            .append("updateTime", getUpdateTime())
            .append("isDeleted", getIsDeleted())
            .toString();
    }
}
