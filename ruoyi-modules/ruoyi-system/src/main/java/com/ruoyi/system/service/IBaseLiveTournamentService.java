package com.ruoyi.system.service;

import java.time.LocalDateTime;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.domain.BaseLiveTournament;
import com.ruoyi.system.domain.LiveStreamCode;

/**
 * 比赛直播房间Service接口
 * 
 * @author ruoyi
 * @date 2024-05-10
 */
public interface IBaseLiveTournamentService extends IService<BaseLiveTournament>
{
    /**
     * 查询比赛直播房间
     * 
     * @param id 比赛直播房间主键
     * @return 比赛直播房间
     */
    public BaseLiveTournament selectBaseLiveTournamentById(Long id);

    /**
     * 查询比赛直播房间列表
     * 
     * @param baseLiveTournament 比赛直播房间
     * @return 比赛直播房间集合
     */
    public List<BaseLiveTournament> selectBaseLiveTournamentList(BaseLiveTournament baseLiveTournament);

    /**
     * 新增比赛直播房间
     * 
     * @param baseLiveTournament 比赛直播房间
     * @return 结果
     */
    public int insertBaseLiveTournament(BaseLiveTournament baseLiveTournament);

    /**
     * 批量删除比赛直播房间
     * 
     * @param ids 需要删除的比赛直播房间主键集合
     * @return 结果
     */
    public int deleteBaseLiveTournamentByIds(Long[] ids);

    /**
     * 删除比赛直播房间信息
     * 
     * @param id 比赛直播房间主键
     * @return 结果
     */
    public int deleteBaseLiveTournamentById(Long id);

    /**
     * 开始直播
     * @param tournamentId
     * @return
     */
    LiveStreamCode beginLive(Long tournamentId, LocalDateTime endTime,Integer type);

    /**
     * 关闭直播
     * @param streamName
     * @return
     */
    int endLive(String streamName);

    void liveNum(Long tournamentId);

    /**
     * 查询 比赛id type为0的比赛直播
     * @param tournamentId
     */
    BaseLiveTournament selectByTournamentId(Long tournamentId);
}
