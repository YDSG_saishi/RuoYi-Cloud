package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.system.domain.BaseAttendanceLog;
import com.ruoyi.system.service.IBaseAttendanceLogService;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;

import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 球员出勤记录Controller
 * 
 * @author wangbj
 * @date 2024-04-16
 */
@Api("球员出勤记录")
@RestController
@RequestMapping("/attendanceLog")
public class BaseAttendanceLogController extends BaseController
{
    @Autowired
    private IBaseAttendanceLogService baseAttendanceLogService;

    /**
     * 查询球员出勤记录列表
     */
    @RequiresPermissions("system:log:list")
    @GetMapping("/list")
    @Operation(summary = "查询球员出勤记录列表", description = "查询球员出勤记录列表")
    public TableDataInfo list(BaseAttendanceLog baseAttendanceLog)
    {
        startPage();
        List<BaseAttendanceLog> list = baseAttendanceLogService.selectBaseAttendanceLogList(baseAttendanceLog);
        return getDataTable(list);
    }

    /**
     * 导出球员出勤记录列表
     */
    @Operation(summary = "导出球员出勤记录列表", description = "导出球员出勤记录列表")
    @RequiresPermissions("system:log:export")
    @Log(title = "球员出勤记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseAttendanceLog baseAttendanceLog)
    {
        List<BaseAttendanceLog> list = baseAttendanceLogService.selectBaseAttendanceLogList(baseAttendanceLog);
        ExcelUtil<BaseAttendanceLog> util = new ExcelUtil<BaseAttendanceLog>(BaseAttendanceLog.class);
        util.exportExcel(response, list, "球员出勤记录数据");
    }

    /**
     * 获取球员出勤记录详细信息
     */
    @Operation(summary = "获取球员出勤记录详细信息", description = "获取球员出勤记录详细信息")
    @RequiresPermissions("system:log:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(baseAttendanceLogService.selectBaseAttendanceLogById(id));
    }

    /**
     * 新增球员出勤记录
     */
    @Operation(summary = "新增球员出勤记录", description = "新增球员出勤记录")
    @RequiresPermissions("system:log:add")
    @Log(title = "球员出勤记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody List<BaseAttendanceLog> baseAttendanceLog)
    {
        return toAjax(baseAttendanceLogService.insertBaseAttendanceLog(baseAttendanceLog));
    }

    /**
     * 修改球员出勤记录
     */
    @Operation(summary = "修改球员出勤记录", description = "修改球员出勤记录")
    @RequiresPermissions("system:log:edit")
    @Log(title = "球员出勤记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody List<BaseAttendanceLog> baseAttendanceLog)
    {
        return toAjax(baseAttendanceLogService.updateBaseAttendanceLog(baseAttendanceLog));
    }

    /**
     * 删除球员出勤记录
     */
    @Operation(summary = "删除球员出勤记录", description = "删除球员出勤记录")
    @RequiresPermissions("system:log:remove")
    @Log(title = "球员出勤记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseAttendanceLogService.deleteBaseAttendanceLogByIds(ids));
    }
}
