package com.ruoyi.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.BaseLivePlayBackEven;
import com.ruoyi.system.domain.BaseLiveTournament;

import java.util.List;

/**
 * 比赛直播房间Mapper接口
 * 
 * @author ruoyi
 * @date 2024-05-10
 */
public interface BaseLivePlayBackEvenMapper extends BaseMapper<BaseLivePlayBackEven>
{

}
