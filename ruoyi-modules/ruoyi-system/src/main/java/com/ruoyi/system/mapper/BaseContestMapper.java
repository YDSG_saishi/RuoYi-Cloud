package com.ruoyi.system.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.BaseContest;

/**
 * 联赛Mapper接口
 * 
 * @author wangbj
 * @date 2024-04-16
 */
public interface BaseContestMapper extends BaseMapper<BaseContest>
{
    /**
     * 查询联赛
     * 
     * @param id 联赛主键
     * @return 联赛
     */
    public BaseContest selectBaseContestById(Long id);

    /**
     * 查询联赛列表
     * 
     * @param baseContest 联赛
     * @return 联赛集合
     */
    public List<BaseContest> selectBaseContestList(BaseContest baseContest);

    /**
     * 新增联赛
     * 
     * @param baseContest 联赛
     * @return 结果
     */
    public int insertBaseContest(BaseContest baseContest);

    /**
     * 修改联赛
     * 
     * @param baseContest 联赛
     * @return 结果
     */
    public int updateBaseContest(BaseContest baseContest);

    /**
     * 删除联赛
     * 
     * @param id 联赛主键
     * @return 结果
     */
    public int deleteBaseContestById(Long id);

    /**
     * 批量删除联赛
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseContestByIds(Long[] ids);

    /**
     * 获取我的联赛
     * @param userid
     * @return
     */
    List<BaseContest> getMyList(Long userid);
}
