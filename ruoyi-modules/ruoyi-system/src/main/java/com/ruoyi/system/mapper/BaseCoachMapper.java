package com.ruoyi.system.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.BaseCoach;

/**
 * 教练组成员Mapper接口
 * 
 * @author wangbj
 * @date 2024-04-16
 */
public interface BaseCoachMapper extends BaseMapper<BaseCoach>
{
    /**
     * 查询教练组成员
     * 
     * @param id 教练组成员主键
     * @return 教练组成员
     */
    public BaseCoach selectBaseCoachById(Long id);

    /**
     * 查询教练组成员列表
     * 
     * @param baseCoach 教练组成员
     * @return 教练组成员集合
     */
    public List<BaseCoach> selectBaseCoachList(BaseCoach baseCoach);

    /**
     * 新增教练组成员
     * 
     * @param baseCoach 教练组成员
     * @return 结果
     */
    public int insertBaseCoach(BaseCoach baseCoach);

    /**
     * 修改教练组成员
     * 
     * @param baseCoach 教练组成员
     * @return 结果
     */
    public int updateBaseCoach(BaseCoach baseCoach);

    /**
     * 删除教练组成员
     * 
     * @param id 教练组成员主键
     * @return 结果
     */
    public int deleteBaseCoachById(Long id);

    /**
     * 批量删除教练组成员
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseCoachByIds(Long[] ids);
}
