package com.ruoyi.system.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.BaseBasketballTeam;

/**
 * 球队Mapper接口
 * 
 * @author wangbj
 * @date 2024-04-16
 */
public interface BaseBasketballTeamMapper extends BaseMapper<BaseBasketballTeam>
{
    /**
     * 查询球队
     * 
     * @param id 球队主键
     * @return 球队
     */
    public BaseBasketballTeam selectBaseBasketballTeamById(Long id);

    /**
     * 查询球队列表
     * 
     * @param baseBasketballTeam 球队
     * @return 球队集合
     */
    public List<BaseBasketballTeam> selectBaseBasketballTeamList(BaseBasketballTeam baseBasketballTeam);

    /**
     * 新增球队
     * 
     * @param baseBasketballTeam 球队
     * @return 结果
     */
    public int insertBaseBasketballTeam(BaseBasketballTeam baseBasketballTeam);

    /**
     * 修改球队
     * 
     * @param baseBasketballTeam 球队
     * @return 结果
     */
    public int updateBaseBasketballTeam(BaseBasketballTeam baseBasketballTeam);

    /**
     * 删除球队
     * 
     * @param id 球队主键
     * @return 结果
     */
    public int deleteBaseBasketballTeamById(Long id);

    /**
     * 批量删除球队
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseBasketballTeamByIds(Long[] ids);
}
