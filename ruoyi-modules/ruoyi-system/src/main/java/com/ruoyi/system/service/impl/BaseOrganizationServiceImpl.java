package com.ruoyi.system.service.impl;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.system.domain.BaseUserOrganization;
import com.ruoyi.system.service.IBaseUserOrganizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.system.mapper.BaseOrganizationMapper;
import com.ruoyi.system.domain.BaseOrganization;
import com.ruoyi.system.service.IBaseOrganizationService;

import javax.annotation.Resource;

/**
 * 机构Service业务层处理
 *
 * @author wangbj
 * @date 2024-04-16
 */
@Service
public class BaseOrganizationServiceImpl extends ServiceImpl<BaseOrganizationMapper, BaseOrganization> implements IBaseOrganizationService {
    @Autowired
    private BaseOrganizationMapper baseOrganizationMapper;
    @Resource
    private IBaseUserOrganizationService iBaseUserOrganizationService;

    /**
     * 查询机构
     *
     * @param id 机构主键
     * @return 机构
     */
    @Override
    public BaseOrganization selectBaseOrganizationById(Long id) {
        return baseOrganizationMapper.selectBaseOrganizationById(id);
    }

    /**
     * 查询机构列表
     *
     * @param baseOrganization 机构
     * @return 机构
     */
    @Override
    public List<BaseOrganization> selectBaseOrganizationList(BaseOrganization baseOrganization) {
        List<BaseUserOrganization> list = iBaseUserOrganizationService.getUserId(SecurityUtils.getUserId());
        if (CollUtil.isEmpty(list)){
            return CollUtil.newArrayList();
        }
        Set<Long> collect = list.stream().map(BaseUserOrganization::getOrganizationId).collect(Collectors.toSet());
        return baseOrganizationMapper.selectBatchIds(collect);
    }

    /**
     * 新增机构
     *
     * @param baseOrganization 机构
     * @return 结果
     */
    @Override
    public BaseOrganization insertBaseOrganization(BaseOrganization baseOrganization) {
        baseOrganization.setCreateTime(DateUtils.getNowDate());
        baseOrganizationMapper.insert(baseOrganization);
        //新增机构 用户关联表
        iBaseUserOrganizationService.savePO(baseOrganization.getId(), SecurityUtils.getUserId());
        return baseOrganization;
    }

    /**
     * 修改机构
     *
     * @param baseOrganization 机构
     * @return 结果
     */
    @Override
    public int updateBaseOrganization(BaseOrganization baseOrganization) {
        baseOrganization.setUpdateTime(DateUtils.getNowDate());
        return baseOrganizationMapper.updateBaseOrganization(baseOrganization);
    }

    /**
     * 批量删除机构
     *
     * @param ids 需要删除的机构主键
     * @return 结果
     */
    @Override
    public int deleteBaseOrganizationByIds(Long[] ids) {
        return baseOrganizationMapper.deleteBaseOrganizationByIds(ids);
    }

    /**
     * 删除机构信息
     *
     * @param id 机构主键
     * @return 结果
     */
    @Override
    public int deleteBaseOrganizationById(Long id) {
        return baseOrganizationMapper.deleteBaseOrganizationById(id);
    }
}
