package com.ruoyi.system.domain.dto.H5;

import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.sql.Date;

@Data
public class KeyFiguresDto {
    /**
     * 比赛id
     */
    @Schema(name = "tournamentId", description = "比赛id")
    @NotNull(message = "比赛id不能为空")
    private Long tournamentId;

    /**
     * 关键数据 1详情 2更多数据
     */
    private Integer type;

    /**
     * 节数 1st 2nd 全场不传
     */
    @Schema(name = "subsection", description = "类型 1st 2nd")
    private Integer subsection;

    /**
     * 得分表现类型 枚举
     */
    private Integer playerType;
}
