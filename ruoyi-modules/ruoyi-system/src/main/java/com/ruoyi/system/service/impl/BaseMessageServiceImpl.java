package com.ruoyi.system.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.redis.service.RedisService;
import com.ruoyi.common.security.service.TokenService;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.system.domain.vo.BaseMsgReplyVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.system.mapper.BaseMessageMapper;
import com.ruoyi.system.domain.BaseMessage;
import com.ruoyi.system.service.IBaseMessageService;

import javax.annotation.Resource;

/**
 * 留言反馈Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-05-07
 */
@Service
public class BaseMessageServiceImpl extends ServiceImpl<BaseMessageMapper, BaseMessage> implements IBaseMessageService
{
    @Resource
    private BaseMessageMapper baseMessageMapper;

    @Resource
    LoginUserSetUtil loginUserSetUtil;

    @Resource
    RedisService redisService;

    @Resource
    TokenService tokenService;

    public final static String MSG_PREFIX_ID = "basketball:massage:reply:id:";


    /**
     * 查询留言反馈
     * 
     * @param id 留言反馈主键
     * @return 留言反馈
     */
    @Override
    public BaseMessage selectBaseMessageById(Long id)
    {
        BaseMessage baseMessage = baseMessageMapper.selectBaseMessageById(id);
        baseMessage.setReplyVOS(redisService.getCacheList((MSG_PREFIX_ID.concat(id.toString()))));
        return baseMessage;
    }

    /**
     * 查询留言反馈列表
     * 
     * @param baseMessage 留言反馈
     * @return 留言反馈
     */
    @Override
    public List<BaseMessage> selectBaseMessageList(BaseMessage baseMessage)
    {
        baseMessage.setContestId(SecurityUtils.getContestId());
        return baseMessageMapper.selectBaseMessageList(baseMessage);
    }

    /**
     * 新增留言反馈
     * 
     * @param baseMessage 留言反馈
     * @return 结果
     */
    @Override
    public int insertBaseMessage(BaseMessage baseMessage)
    {

        loginUserSetUtil.populateFields(baseMessage,1);
        int insert = baseMessageMapper.insert(baseMessage);
        List<BaseMsgReplyVO> list = new ArrayList<>();

        String key = MSG_PREFIX_ID.concat(baseMessage.getId().toString());
        //构建参数
        BaseMsgReplyVO msgReplyVO = new BaseMsgReplyVO();
        msgReplyVO.setMsg("感谢反馈,如有需要我们专人会与您联系!")
                .setName(tokenService.getLoginUser().getSysUser().getNickName())
                .setReplyTime(new Date())
                .setMsgId(baseMessage.getId());
        list.add(msgReplyVO);
        redisService.setCacheObject(key,list);
        return insert;
    }

    /**
     * 修改留言反馈
     * 
     * @param baseMessage 留言反馈
     * @return 结果
     */
    @Override
    public int updateBaseMessage(BaseMessage baseMessage)
    {
        loginUserSetUtil.populateFields(baseMessage,2);
        return baseMessageMapper.updateBaseMessage(baseMessage);
    }

    /**
     * 批量删除留言反馈
     * 
     * @param ids 需要删除的留言反馈主键
     * @return 结果
     */
    @Override
    public int deleteBaseMessageByIds(Long[] ids)
    {
        for (int i = 0; i < ids.length; i++) {
            redisService.deleteObject(MSG_PREFIX_ID.concat(ids[i].toString()));
        }

        return baseMessageMapper.deleteBaseMessageByIds(ids);
    }

    /**
     * 删除留言反馈信息
     * 
     * @param id 留言反馈主键
     * @return 结果
     */
    @Override
    public int deleteBaseMessageById(Long id)
    {
        redisService.deleteObject(MSG_PREFIX_ID.concat(id.toString()));
        return baseMessageMapper.deleteBaseMessageById(id);
    }

    @Override
    public List<BaseMessage> getMyList() {

        LambdaQueryWrapper<BaseMessage> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(BaseMessage::getCreateUserId, SecurityUtils.getUserId());

        return baseMessageMapper.selectList(queryWrapper);
    }

    /**
     * 回复留言
     *
     * @param id
     * @param msg
     * @return
     */
    @Override
    public List<BaseMsgReplyVO> reply(Long id, String msg) {

        List<BaseMsgReplyVO> list = new ArrayList<>();

        String key = MSG_PREFIX_ID.concat(id.toString());
        //构建参数
        BaseMsgReplyVO msgReplyVO = new BaseMsgReplyVO();
        msgReplyVO.setMsg(msg)
                .setName(tokenService.getLoginUser().getSysUser().getNickName())
                .setReplyTime(new Date())
                .setMsgId(id);
        //是否需要存入原来的
        if (redisService.hasKey(key)){
            list = redisService.getCacheList(key);
        }

        list.add(msgReplyVO);
        redisService.setCacheObject(key,list);
        return list;
    }
}
