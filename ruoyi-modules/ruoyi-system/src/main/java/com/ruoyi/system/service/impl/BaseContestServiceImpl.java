package com.ruoyi.system.service.impl;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.csp.sentinel.util.AssertUtil;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.common.core.constant.UserConstants;
import com.ruoyi.common.core.enums.ContestAuditStatusEnum;
import com.ruoyi.common.core.utils.CommonStreamUtil;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.redis.service.RedisService;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.system.api.domain.SysUser;
import com.ruoyi.system.domain.BaseContestBinding;
import com.ruoyi.system.domain.BaseTournamentPersonnel;
import com.ruoyi.system.domain.BaseUserOrganization;
import com.ruoyi.system.domain.dto.Web.ContestBindingDto;
import com.ruoyi.system.mapper.BaseContestBindingMapper;
import com.ruoyi.system.mapper.SysUserMapper;
import com.ruoyi.system.service.IBaseTournamentPersonnelService;
import com.ruoyi.system.service.IBaseUserOrganizationService;
import com.ruoyi.system.service.ISysUserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.system.mapper.BaseContestMapper;
import com.ruoyi.system.domain.BaseContest;
import com.ruoyi.system.service.IBaseContestService;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

import static com.ruoyi.common.core.utils.PageUtils.startPage;

/**
 * 联赛Service业务层处理
 * 
 * @author wangbj
 * @date 2024-04-16
 */
@Slf4j
@Service
public class BaseContestServiceImpl extends ServiceImpl<BaseContestMapper, BaseContest> implements IBaseContestService
{
    @Resource
    private BaseContestMapper baseContestMapper;
    @Resource
    private IBaseTournamentPersonnelService iBaseTournamentPersonnelService;

    @Resource
    LoginUserSetUtil loginUserSetUtil;

    @Resource
    private IBaseUserOrganizationService iBaseUserOrganizationService;

    @Resource
    RedisService redisService;

    @Resource
    private SysUserMapper sysUserMapper;

    @Resource
    private BaseContestBindingMapper baseContestBindingMapper;


    public final static String CONTEST_PREFIX_ID = "basketball:contest:id:";


    /**
     * 查询联赛
     * 
     * @param id 联赛主键
     * @return 联赛
     */
    @Override
    public BaseContest selectBaseContestById(Long id)
    {
        return baseContestMapper.selectBaseContestById(id);
    }

    /**
     * 查询联赛列表
     * 
     * @param baseContest 联赛
     * @return 联赛
     */
    @Override
    public List<BaseContest> selectBaseContestList(BaseContest baseContest)
    {
        List<BaseUserOrganization> list = iBaseUserOrganizationService.getUserId(SecurityUtils.getUserId());
        if (CollUtil.isEmpty(list)){
            return CollUtil.newArrayList();
        }
        List<BaseTournamentPersonnel> list1 = iBaseTournamentPersonnelService.list(Wrappers.<BaseTournamentPersonnel>lambdaQuery()
                .in(BaseTournamentPersonnel::getUserId, SecurityUtils.getUserId()));
        List<Long> collect1 = list1.stream().map(BaseTournamentPersonnel::getContestId).collect(Collectors.toList());
        if (CollUtil.isEmpty(collect1)){
            return CollUtil.newArrayList();
        }
        Set<Long> collect = list.stream().map(BaseUserOrganization::getOrganizationId).collect(Collectors.toSet());
        List<BaseContest> baseContests = baseContestMapper.selectList(Wrappers.<BaseContest>lambdaQuery()
                .in(BaseContest::getOrganizationId, collect)
                .in(BaseContest::getId,collect1)
        );

        return baseContests;
    }

    /**
     * 新增联赛
     * 
     * @param baseContest 联赛
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseContest insertBaseContest(BaseContest baseContest)
    {
        checkContestName(baseContest.getContestName());
        baseContest.setCreateTime(DateUtils.getNowDate());
        baseContest.setCreateUserId(SecurityUtils.getUserId());
        baseContest.setAuditStatus(ContestAuditStatusEnum.AUDIT.getCode());
        int i = baseContestMapper.insert(baseContest);


        iBaseTournamentPersonnelService.save(BaseTournamentPersonnel.builder()
                .userId(SecurityUtils.getUserId())
                .type(6L)
                .level(0L)
                .organizationId(baseContest.getOrganizationId())
                .contestId(baseContest.getId())
                .build());
        BaseContest contest = baseContestMapper.selectBaseContestList(baseContest).get(0);
        redisService.setCacheObject(CONTEST_PREFIX_ID.concat(contest.getId().toString()),baseContest);

        iBaseUserOrganizationService.savePO(baseContest.getOrganizationId(),SecurityUtils.getUserId());
        return contest;
    }

    private void checkContestName(String contestName) {
        List<BaseContest> list = this.list(Wrappers.<BaseContest>lambdaQuery().eq(BaseContest::getContestName, contestName).eq(BaseContest::getIsDeleted,0));
        if (CollUtil.isNotEmpty(list)){
            throw new RuntimeException("联赛名称已经存在");
        }
    }

    /**
     * 修改联赛
     * 
     * @param baseContest 联赛
     * @return 结果
     */
    @Override
    public int updateBaseContest(BaseContest baseContest)
    {
        loginUserSetUtil.populateFields(baseContest,2);
        int i = baseContestMapper.updateBaseContest(baseContest);
        redisService.setCacheObject(CONTEST_PREFIX_ID.concat(baseContest.getId().toString()),baseMapper.selectBaseContestById(baseContest.getId()));
        return i;
    }

    /**
     * 批量删除联赛
     * 
     * @param ids 需要删除的联赛主键
     * @return 结果
     */
    @Override
    public int deleteBaseContestByIds(Long[] ids)
    {
        for (int i = 0; i < ids.length; i++) {
            redisService.deleteObject(CONTEST_PREFIX_ID.concat(ids[i].toString()));
        }
        return baseContestMapper.deleteBaseContestByIds(ids);
    }

    /**
     * 删除联赛信息
     * 
     * @param id 联赛主键
     * @return 结果
     */
    @Override
    public int deleteBaseContestById(Long id)
    {

        redisService.deleteObject(CONTEST_PREFIX_ID.concat(id.toString()));
        return baseContestMapper.deleteBaseContestById(id);
    }

    @Override
    public List<BaseContest> gerMyList(Long userid) {
        return baseContestMapper.getMyList(userid);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int auditStatus(BaseContest baseContest) {
        AssertUtil.notNull(baseContest.getId(),"联赛id不能为空");
        AssertUtil.notNull(baseContest.getAuditStatus(),"联赛审核状态不能为空");
        BaseContest byId = this.getById(baseContest.getId());
        AssertUtil.notNull(byId,"联赛异常!");

        LambdaUpdateWrapper<BaseContest> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.set(BaseContest::getAuditStatus,baseContest.getAuditStatus());
        updateWrapper.set(Objects.nonNull(baseContest.getTournamentNum()),BaseContest::getTournamentNum,baseContest.getTournamentNum());
        updateWrapper.eq(BaseContest::getId,baseContest.getId());
        //审核通过 主账号也需要联赛权限
        if (Objects.equals(baseContest.getAuditStatus(),1)){
            //主账号
            iBaseTournamentPersonnelService.save(BaseTournamentPersonnel.builder()
                    .userId(UserConstants.USER_ID)
                    .type(6L)
                    .level(0L)
                    .organizationId(byId.getOrganizationId())
                    .contestId(baseContest.getId())
                    .build());
            iBaseUserOrganizationService.savePO(byId.getOrganizationId(),SecurityUtils.getUserId());
        }
        return update(updateWrapper)?1:0;
    }

    @Override
    public List<BaseContest> selectList(BaseContest baseContest) {
        List<BaseContest> baseContests = baseContestMapper.selectList(Wrappers.<BaseContest>lambdaQuery()
                .like(StrUtil.isNotBlank(baseContest.getContestName()),BaseContest::getContestName,baseContest.getContestName())
                .ge(Objects.nonNull(baseContest.getStartTime()),BaseContest::getContestBeginTime,baseContest.getStartTime())
                .le(Objects.nonNull(baseContest.getEndTime()),BaseContest::getContestBeginTime,baseContest.getEndTime())
                .eq(Objects.nonNull(baseContest.getAuditStatus()),BaseContest::getAuditStatus,baseContest.getAuditStatus())
                .ne(Objects.nonNull(baseContest.getId()),BaseContest::getId,baseContest.getId())
                .orderByDesc(BaseContest::getContestBeginTime)
        );
        if (CollUtil.isEmpty(baseContests)){
            return new ArrayList<>();
        }
        List<Long> userIds = baseContests.stream().map(BaseContest::getCreateUserId).collect(Collectors.toList());
        List<SysUser> sysUsers = sysUserMapper.selectUserByIds(userIds);
        Map<Long, SysUser> userMap = sysUsers.stream().collect(Collectors.toMap(SysUser::getUserId, Function.identity()));

        baseContests.forEach(o ->{
            if (userMap.containsKey(o.getCreateUserId())){
                SysUser sysUser = userMap.get(o.getCreateUserId());
                o.setPhone(sysUser.getPhonenumber());
            }
        });
        return baseContests;
    }

    @Override
    public int saveImg(BaseContest baseContest) {
        AssertUtil.notNull(baseContest.getId(),"联赛id不能为空");
        BaseContest byId = getById(baseContest.getId());
        AssertUtil.notNull(byId,"联赛不存在");
        return updateById(baseContest)?1:0;
    }

    @Override
    public List<BaseContest> getContestBinding(BaseContest baseContest) {
        List<BaseContest> baseContests = baseContestMapper.selectList(Wrappers.<BaseContest>lambdaQuery()
                .like(StrUtil.isNotBlank(baseContest.getContestName()),BaseContest::getContestName,baseContest.getContestName())
                .ne(Objects.nonNull(baseContest.getId()),BaseContest::getId,baseContest.getId())
                .orderByDesc(BaseContest::getContestBeginTime)
        );
        List<BaseContestBinding> baseContestBindings = baseContestBindingMapper.selectListById(baseContest.getId());
        if (CollUtil.isEmpty(baseContestBindings)){
            return baseContests;
        }
        Set<Long> longs = CommonStreamUtil.transSet(baseContestBindings, BaseContestBinding::getContestBindingId);
        longs.addAll( CommonStreamUtil.transSet(baseContestBindings, BaseContestBinding::getContestId));
        baseContests.forEach(item ->{
            if (longs.contains(item.getId())) {
                item.setContestFlag(1);
            }
        });
        return baseContests;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int contestBinding(ContestBindingDto contestBindingDto) {
        AssertUtil.notNull(contestBindingDto.getId(),"指定绑定id不能为空");

        List<BaseContestBinding> baseContestBindings = baseContestBindingMapper.selectListById(contestBindingDto.getId());
        if (CollUtil.isNotEmpty(baseContestBindings)){
            baseContestBindingMapper.deleteBatchIds(baseContestBindings);
        }
        contestBindingDto.getIds().forEach(item ->{
            BaseContestBinding contestBinding = new BaseContestBinding();
            contestBinding.setContestId(contestBindingDto.getId());
            contestBinding.setContestBindingId(item);
            baseContestBindingMapper.insert(contestBinding);
        });
        return 1;
    }
}
