package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.system.domain.BaseSeason;
import com.ruoyi.system.service.IBaseSeasonService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.convert.Delimiter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;

import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 赛季Controller
 * 
 * @author wangbj
 * @date 2024-04-16
 */
@ApiOperation("赛季")
@RestController
@RequestMapping("/season")
public class BaseSeasonController extends BaseController
{
    @Autowired
    private IBaseSeasonService baseSeasonService;

    /**
     * 查询赛季列表
     */
    @RequiresPermissions("system:season:list")
    @GetMapping("/list")
    @Operation(summary = "查询赛季列表", description = "查询赛季列表")
    public TableDataInfo list(BaseSeason baseSeason)
    {
        startPage();
        List<BaseSeason> list = baseSeasonService.selectBaseSeasonList(baseSeason);
        return getDataTable(list);
    }

    /**
     * 导出赛季列表
     */
    @Operation(summary = "导出赛季列表", description = "导出赛季列表")
    @RequiresPermissions("system:season:export")
    @Log(title = "赛季", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseSeason baseSeason)
    {
        List<BaseSeason> list = baseSeasonService.selectBaseSeasonList(baseSeason);
        ExcelUtil<BaseSeason> util = new ExcelUtil<BaseSeason>(BaseSeason.class);
        util.exportExcel(response, list, "赛季数据");
    }

    /**
     * 获取赛季详细信息
     */
    @Operation(summary = "获取赛季详细信息", description = "获取赛季详细信息")
    @RequiresPermissions("system:season:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(baseSeasonService.selectBaseSeasonById(id));
    }

    /**
     * 新增赛季
     */
    @Operation(summary = "新增赛季", description = "新增赛季")
    @RequiresPermissions("system:season:add")
    @Log(title = "赛季", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseSeason baseSeason)
    {
        return toAjax(baseSeasonService.insertBaseSeason(baseSeason));
    }

    /**
     * 修改赛季
     */
    @Operation(summary = "修改赛季", description = "修改赛季")
    @RequiresPermissions("system:season:edit")
    @Log(title = "赛季", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseSeason baseSeason)
    {
        return toAjax(baseSeasonService.updateBaseSeason(baseSeason));
    }

    /**
     * 删除赛季
     */
    @Operation(summary = "删除赛季", description = "删除赛季")
    @RequiresPermissions("system:season:remove")
    @Log(title = "赛季", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseSeasonService.deleteBaseSeasonByIds(ids));
    }
}
