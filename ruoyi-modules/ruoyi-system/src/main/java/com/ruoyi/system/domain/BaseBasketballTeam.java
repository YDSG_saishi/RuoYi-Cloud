package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.web.domain.BaseEntity;

import java.util.List;

/**
 * 球队对象 base_basketball_team
 * 
 * @author wangbj
 * @date 2024-04-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("base_basketball_team")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BaseBasketballTeam extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @TableId(type = IdType.AUTO)
    @Schema(name = "id", description = "主键")
    private Long id;

    /** 球队名称 */
    @Excel(name = "球队名称")
    @Schema(name = "teamName", description = "球队名称")
    private String teamName;

    /** 球队logo地址 */
    @Excel(name = "球队logo地址")
    @Schema(name = "teamLogo", description = "球队logo地址")
    private String teamLogo;

    /** 是否在报名列表展示:0:不展示; 1:展示; */
    @Excel(name = "是否在报名列表展示:0:不展示; 1:展示;")
    @Schema(name = "isShow", description = "是否在报名列表展示:0:不展示; 1:展示;")
    private int isShow;

    /** 所属联赛id */
    @Excel(name = "所属联赛id")
    @Schema(name = "leagueId", description = "所属联赛id")
    private Long leagueId;

    /** 创建人id */
    @Excel(name = "创建人id")
    @Schema(name = "createUserId", description = "创建人id")
    private Long createUserId;

    /** 创建人姓名 */
    @Excel(name = "创建人姓名")
    @Schema(name = "createUserName", description = "创建人姓名")
    private String createUserName;

    /** 修改人id */
    @Excel(name = "修改人id")
    @Schema(name = "updateUserId", description = "修改人id")
    private Long updateUserId;

    /** 修改人姓名 */
    @Excel(name = "修改人姓名")
    @Schema(name = "updateUserName", description = "修改人姓名")
    private String updateUserName;

    /** 是否已删除 0:未删除;1:已删除 */
    @Excel(name = "是否已删除 0:未删除;1:已删除")
    @Schema(name = "isDeleted", description = "是否已删除 0:未删除;1:已删除")
    private Long isDeleted;

    @Schema(name = "baseBasketballPlayersList", description = "球员信息")
    @TableField(exist = false)
    private List<BaseBasketballPlayer> baseBasketballPlayersList;

    @Schema(name = "baseCoachList", description = "教练组信息")
    @TableField(exist = false)
    private List<BaseCoach> baseCoachList;

    @TableField(exist = false)
    private Integer num = 0;
}
