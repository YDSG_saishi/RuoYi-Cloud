package com.ruoyi.system.domain.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author Wangbj
 * @date 2024年04月26日 00:57
 */
@Data
@Accessors(chain = true)
public class BaseBasketballTimeVO {

    @Schema(name = "tournamentId", description = "比赛id")
    private Long tournamentId;

    @Schema(name = "oneTeamId", description = "队伍1id")
    private Long oneTeamId;

    @Schema(name = "oneTeamTime", description = "队伍1持球时间 单位秒")
    private Integer oneTeamTime;

    @Schema(name = "twoTeamId", description = "队伍2id")
    private Long twoTeamId;

    @Schema(name = "twoTeamTime", description = "队伍2持球时间 单位秒")
    private Integer twoTeamTime;

}
