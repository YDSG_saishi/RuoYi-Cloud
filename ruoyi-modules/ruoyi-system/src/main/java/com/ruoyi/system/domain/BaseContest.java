package com.ruoyi.system.domain;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.web.domain.BaseEntity;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 联赛对象 base_contest
 * 
 * @author wangbj
 * @date 2024-04-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("base_contest")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BaseContest extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @TableId(type = IdType.AUTO)
    @Schema(name = "id", description = "主键")
    private Long id;

    /** 联赛名称 */
    @Excel(name = "联赛名称")
    @Schema(name = "contestName", description = "联赛名称")
    private String contestName;

    /** 联赛logo */
    @Excel(name = "联赛logo")
    @Schema(name = "contestLogo", description = "联赛logo")
    private String contestLogo;

    /** 地区 */
    @Excel(name = "地区")
    @Schema(name = "area", description = "地区")
    private String area;

    /** 联赛开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "联赛开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    @Schema(name = "contestBeginTime", description = "联赛开始时间")
    private Date contestBeginTime;

    /** 联赛结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "联赛结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    @Schema(name = "contestEndTime", description = "联赛结束时间")
    private Date contestEndTime;

    /** 计分方式:0:5v5计分;1:3v3计分; */
    @Excel(name = "计分方式:0:5v5计分;1:3v3计分;")
    @Schema(name = "scoringWay", description = "计分方式:0:5v5计分;1:3v3计分;")
    private Long scoringWay;

    /** 比赛类型，逗号隔开 */
    @Excel(name = "比赛类型，逗号隔开")
    @Schema(name = "competitionType", description = "比赛类型，逗号隔开")
    private String competitionType;

    /** 联赛类型，逗号隔开 */
    @Excel(name = "联赛类型，逗号隔开")
    @Schema(name = "leagueType", description = "联赛类型，逗号隔开")
    private String leagueType;

    /** 所属机构id */
    @Excel(name = "所属机构id")
    @Schema(name = "organizationId", description = "所属机构id")
    private Long organizationId;

    /** 创建人id */
    @Excel(name = "创建人id")
    @Schema(name = "createUserId", description = "创建人id")
    private Long createUserId;

    /** 创建人姓名 */
    @Excel(name = "创建人姓名")
    @Schema(name = "createUserName", description = "创建人姓名")
    private String createUserName;

    /** 修改人id */
    @Excel(name = "修改人id")
    @Schema(name = "updateUserId", description = "修改人id")
    private Long updateUserId;

    /** 修改人姓名 */
    @Excel(name = "修改人姓名")
    @Schema(name = "updateUserName", description = "修改人姓名")
    private String updateUserName;

    /** 是否已删除 0:未删除;1:已删除 */
    @Excel(name = "是否已删除 0:未删除;1:已删除")
    @Schema(name = "isDeleted", description = "是否已删除 0:未删除;1:已删除")
    private Long isDeleted;

    @Schema(name = "isLogo", description = "是否开启水印 0不开启 1开启")
    private Integer isLogo;

    @Schema(name = "logo", description = "水印图片")
    private String logo;

    @Schema(name = "auditStatus", description = "审核状态0审核,1审核通过,2审核未通过")
    private Integer auditStatus;
    @Schema(name = "banner", description = "大屏轮播图,分割多张")
    private String banner;
    @Schema(name = "tournament_num", description = "比赛场次")
    private Integer tournamentNum;


    @TableField(exist = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "开始时间")
    private Date startTime;
    @TableField(exist = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "结束时间")
    private Date endTime;

    @TableField(exist = false)
    private String phone;
    /**
     * 是否联赛绑定 0否 1是
     */
    @TableField(exist = false)
    private Integer contestFlag = 0;

    /**
     * 水印id
     */
    private Long watermarkId;
}
