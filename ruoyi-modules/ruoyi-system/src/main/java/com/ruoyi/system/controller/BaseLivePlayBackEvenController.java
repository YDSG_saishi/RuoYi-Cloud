package com.ruoyi.system.controller;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.system.domain.BaseLivePlayBackEven;
import com.ruoyi.system.domain.BaseTournament;
import com.ruoyi.system.domain.vo.BaseTournamentResponseVO;
import com.ruoyi.system.service.IBaseBasketballPlayerService;
import com.ruoyi.system.service.IBaseLivePlayBackEvenService;
import com.ruoyi.system.service.IBaseTournamentService;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

/**
 * 比赛直播房间Controller
 * 
 * @author ruoyi
 * @date 2024-05-10
 */
@ApiOperation("比赛直播房间")
@RestController
@RequestMapping("/live/playback/even")
public class BaseLivePlayBackEvenController extends BaseController {


    @Resource
    private IBaseLivePlayBackEvenService iBaseLivePlayBackEvenService;
    @Resource
    private IBaseTournamentService iBaseTournamentService;

    /**
     * 查询比赛回放列表
     */
    @GetMapping("/list")
    @Operation(summary = "查询比赛直播房间列表", description = "查询比赛直播房间列表")
    public TableDataInfo list(BaseLivePlayBackEven baseLiveTournament) {
        List<BaseLivePlayBackEven> list = iBaseLivePlayBackEvenService.list(
                Wrappers.<BaseLivePlayBackEven>lambdaQuery()
                        .eq(BaseLivePlayBackEven::getTournamentId,baseLiveTournament.getTournamentId())
                        .eq(BaseLivePlayBackEven::getType, Objects.isNull(baseLiveTournament.getType())?0:1)
                        .orderByDesc(BaseLivePlayBackEven::getCreateTime));
        if (CollUtil.isEmpty(list)){
            return getDataTable(list);
        }
        buildList(list);
        return getDataTable(list);
    }

    private void buildList(List<BaseLivePlayBackEven> list) {
        Integer num = 0;
        Integer index1 = 0;
        Integer index2 = 0;
        for (BaseLivePlayBackEven vo:list) {
            vo.setType(2);
            if (Objects.equals(vo.getName(),"直播视频回放")){
                vo.setType(1);
                index1= num;
            }
            if (Objects.equals(vo.getName(),"全场集锦")){
                vo.setType(1);
                index2 = num;
            }
            num++;
        }
        BaseLivePlayBackEven even = list.get(index1);
        BaseLivePlayBackEven even2 = list.get(index2);
        list.set(index1,even2);
        list.set(index2,even);
    }

    /**
     * 根据球员id查询比赛回放列表
     */
    @GetMapping("/playList")
    @Operation(summary = "根据球员id查询比赛回放列表", description = "根据球员id查询比赛回放列表")
    public TableDataInfo playList(BaseLivePlayBackEven baseLiveTournament) throws ParseException {
        List<BaseLivePlayBackEven> list = iBaseLivePlayBackEvenService.list(
                Wrappers.<BaseLivePlayBackEven>lambdaQuery()
                        .eq(BaseLivePlayBackEven::getTournamentId,baseLiveTournament.getTournamentId())
                        .eq(BaseLivePlayBackEven::getType,0)
                        .eq(BaseLivePlayBackEven::getIsDeleted,0)
                        .orderByDesc(BaseLivePlayBackEven::getCreateTime));
        if (CollUtil.isEmpty(list)){
            return getDataTable(list);
        }
        buildList(list);
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        Date date = sdf.parse(sdf.format("2024-07-12 00:00:00"));
        DateFormat fmt =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = fmt.parse("2024-07-12 00:00:00");

        List<BaseLivePlayBackEven> evenList = new ArrayList<>();
        for (BaseLivePlayBackEven vo:list) {
            if (Objects.equals(vo.getType(),1)){
                evenList.add(vo);
                continue;
            }
            if (vo.getCreateTime().before(date)){
                evenList.add(vo);
                continue;
            }
            if (Objects.equals(vo.getPlayId(),baseLiveTournament.getPlayId())){
                evenList.add(vo);
            }
        }
        return getDataTable(evenList);
    }

    /**
     * 查询所有球员
     */
    @GetMapping("/tournamentPlayList")
    @Operation(summary = "查询所有球员", description = "查询所有球员")
    public BaseTournamentResponseVO tournamentPlayList(BaseLivePlayBackEven baseLiveTournament) {
        return iBaseTournamentService.selectById(baseLiveTournament.getTournamentId());
    }
}


