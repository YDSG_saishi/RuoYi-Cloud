package com.ruoyi.system.mapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.system.domain.BasketballPlayerAndTeam;

/**
 * 球队球员教练组关联Mapper接口
 * 
 * @author wangbj
 * @date 2024-04-16
 */
public interface BasketballPlayerAndTeamMapper extends BaseMapper<BasketballPlayerAndTeam>
{
    /**
     * 查询球队球员教练组关联
     * 
     * @param id 球队球员教练组关联主键
     * @return 球队球员教练组关联
     */
    public BasketballPlayerAndTeam selectBasketballPlayerAndTeamById(Long id);

    /**
     * 查询球队球员教练组关联列表
     * 
     * @param basketballPlayerAndTeam 球队球员教练组关联
     * @return 球队球员教练组关联集合
     */
    public List<BasketballPlayerAndTeam> selectBasketballPlayerAndTeamList(BasketballPlayerAndTeam basketballPlayerAndTeam);

    /**
     * 新增球队球员教练组关联
     * 
     * @param basketballPlayerAndTeam 球队球员教练组关联
     * @return 结果
     */
    public int insertBasketballPlayerAndTeam(BasketballPlayerAndTeam basketballPlayerAndTeam);

    /**
     * 修改球队球员教练组关联
     * 
     * @param basketballPlayerAndTeam 球队球员教练组关联
     * @return 结果
     */
    public int updateBasketballPlayerAndTeam(BasketballPlayerAndTeam basketballPlayerAndTeam);

    /**
     * 删除球队球员教练组关联
     * 
     * @param id 球队球员教练组关联主键
     * @return 结果
     */
    public int deleteBasketballPlayerAndTeamById(Long id);

    /**
     * 批量删除球队球员教练组关联
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBasketballPlayerAndTeamByIds(Long[] ids);

    /**
     * 球员获取球队
      * @param playIds
     * @param type
     * @return
     */
    default List<BasketballPlayerAndTeam> selectByPlayIds(Set<Long> playIds, int type){
        if (CollUtil.isEmpty(playIds)){
            return new ArrayList<>();
        }
        LambdaQueryWrapper<BasketballPlayerAndTeam> wrapper  = Wrappers.<BasketballPlayerAndTeam>lambdaQuery()
                .eq(BasketballPlayerAndTeam::getType,type)
                .in(BasketballPlayerAndTeam::getPlayerId,playIds);
        return selectList(wrapper);
    }

    /**
     * 球队获取球员
     * @param teamId
     * @return
     */
    default List<BasketballPlayerAndTeam> selectByTeamThenPlay(Long teamId){
        LambdaQueryWrapper<BasketballPlayerAndTeam> wrapper  = Wrappers.<BasketballPlayerAndTeam>lambdaQuery()
                .eq(BasketballPlayerAndTeam::getType,0)
                .eq(BasketballPlayerAndTeam::getTeamId,teamId);
        return selectList(wrapper);
    }

}
