package com.ruoyi.system.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import cn.hutool.core.collection.CollUtil;
import com.alibaba.csp.sentinel.util.AssertUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.common.core.enums.CoachTypeEnum;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.system.domain.BaseBasketballTeam;
import com.ruoyi.system.domain.BasketballPlayerAndTeam;
import com.ruoyi.system.service.IBaseBasketballTeamService;
import com.ruoyi.system.service.IBasketballPlayerAndTeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.system.mapper.BaseCoachMapper;
import com.ruoyi.system.domain.BaseCoach;
import com.ruoyi.system.service.IBaseCoachService;

import javax.annotation.Resource;

/**
 * 教练组成员Service业务层处理
 * 
 * @author wangbj
 * @date 2024-04-16
 */
@Service
public class BaseCoachServiceImpl extends ServiceImpl<BaseCoachMapper, BaseCoach> implements IBaseCoachService
{
    @Autowired
    private BaseCoachMapper baseCoachMapper;
    @Resource
    private IBaseBasketballTeamService iBaseBasketballTeamService;


    @Resource
    private IBasketballPlayerAndTeamService iBasketballPlayerAndTeamService;
    /**
     * 查询教练组成员
     * 
     * @param id 教练组成员主键
     * @return 教练组成员
     */
    @Override
    public BaseCoach selectBaseCoachById(Long id)
    {
        return baseCoachMapper.selectBaseCoachById(id);
    }

    /**
     * 查询教练组成员列表
     * 
     * @param baseCoach 教练组成员
     * @return 教练组成员
     */
    @Override
    public List<BaseCoach> selectBaseCoachList(BaseCoach baseCoach)
    {
        AssertUtil.notNull(baseCoach.getPlayerId(),"球队id不能为空");
        //获取关联表内的球员id
        List<Long> collect = iBasketballPlayerAndTeamService.list(Wrappers.<BasketballPlayerAndTeam>lambdaQuery()
                .eq(BasketballPlayerAndTeam::getTeamId, baseCoach.getPlayerId())
                .eq(BasketballPlayerAndTeam::getType, 0)
        ).stream().map(BasketballPlayerAndTeam::getPlayerId).collect(Collectors.toList());

        if (CollUtil.isEmpty(collect)){
            return CollUtil.newArrayList();
        }
        List<BaseCoach> baseCoaches = baseCoachMapper.selectBatchIds(collect);
        baseCoaches.forEach(vo -> {
            vo.setTypeStr(CoachTypeEnum.getValue(vo.getType()));
        });
        return baseCoaches;
    }

    /**
     * 新增教练组成员
     * 
     * @param baseCoach 教练组成员
     * @return 结果
     */
    @Override
    public int insertBaseCoach(BaseCoach baseCoach)
    {
        AssertUtil.notNull(baseCoach.getPlayerId(),"球队id不能为空");
        BaseBasketballTeam byId = iBaseBasketballTeamService.getById(baseCoach.getPlayerId());
        AssertUtil.notNull(byId,"球队没有找到!");

        baseCoach.setCreateTime(DateUtils.getNowDate());
        int i = baseCoachMapper.insert(baseCoach);

        iBasketballPlayerAndTeamService.save(BasketballPlayerAndTeam.builder()
                .playerId(baseCoach.getId())
                .teamId(baseCoach.getPlayerId())
                .type(0L)
                .contestId(byId.getLeagueId())
                .build());
        baseCoach.setCreateTime(DateUtils.getNowDate());
        return i;
    }

    /**
     * 修改教练组成员
     * 
     * @param baseCoach 教练组成员
     * @return 结果
     */
    @Override
    public int updateBaseCoach(BaseCoach baseCoach)
    {
        baseCoach.setUpdateTime(DateUtils.getNowDate());
        return baseCoachMapper.updateBaseCoach(baseCoach);
    }

    /**
     * 批量删除教练组成员
     * 
     * @param ids 需要删除的教练组成员主键
     * @return 结果
     */
    @Override
    public int deleteBaseCoachByIds(Long[] ids)
    {
        return baseCoachMapper.deleteBaseCoachByIds(ids);
    }

    /**
     * 删除教练组成员信息
     * 
     * @param id 教练组成员主键
     * @return 结果
     */
    @Override
    public int deleteBaseCoachById(Long id)
    {
        return baseCoachMapper.deleteBaseCoachById(id);
    }
}
