package com.ruoyi.system.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.system.domain.BaseContest;
import com.ruoyi.system.service.IBaseContestService;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.system.mapper.BaseBriefMapper;
import com.ruoyi.system.domain.BaseBrief;
import com.ruoyi.system.service.IBaseBriefService;

/**
 * 比赛简介Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-07-16
 */
@Service
public class BaseBriefServiceImpl extends ServiceImpl<BaseBriefMapper, BaseBrief> implements IBaseBriefService
{
    @Autowired
    private BaseBriefMapper baseBriefMapper;
    @Autowired
    private IBaseContestService iBaseContestService;

    /**
     * 查询比赛简介
     *
     * @param contestId 比赛简介主键
     * @return 比赛简介
     */
    @Override
    public BaseBrief selectBaseBriefById(Long contestId)
    {
        BaseBrief byContestId = getByContestId(contestId);
        if (Objects.isNull(byContestId)){
            byContestId = new BaseBrief();
        }
        BaseContest byId = iBaseContestService.getById(contestId);
        if (Objects.nonNull(byId)){
            byContestId.setContestName(byId.getContestName());
        }
        return byContestId;
    }
    @Override
    public Map selectBaseBriefH5ById(Long contestId)
    {
        BaseBrief byContestId = getByContestId(contestId);
        if (Objects.isNull(byContestId)){
            byContestId = new BaseBrief();
        }
        BaseContest byId = iBaseContestService.getById(contestId);
        if (Objects.nonNull(byId)){
            byContestId.setContestName(byId.getContestName());
        }
        Map map = JSONObject.parseObject(JSONObject.toJSONString(byContestId), Map.class);
        if (Objects.nonNull(byContestId.getStartTime())&&Objects.nonNull(byContestId.getEndTime())){
            String format = DateFormatUtils.format(byContestId.getStartTime(), "yyyy-MM-dd");
            String format2 = DateFormatUtils.format(byContestId.getEndTime(), "yyyy-MM-dd");
            map.put("createTime",format +" - "+ format2);
        }
        return map;
    }
    private BaseBrief getByContestId(Long contestId) {
        return this.getOne(Wrappers.<BaseBrief>lambdaQuery().eq(BaseBrief::getContestId,contestId).last("limit 1"));
    }

    /**
     * 查询比赛简介列表
     * 
     * @param baseBrief 比赛简介
     * @return 比赛简介
     */
    @Override
    public List<BaseBrief> selectBaseBriefList(BaseBrief baseBrief)
    {
        return baseBriefMapper.selectBaseBriefList(baseBrief);
    }

    /**
     * 新增比赛简介
     * 
     * @param baseBrief 比赛简介
     * @return 结果
     */
    @Override
    public int insertBaseBrief(BaseBrief baseBrief)
    {
        Long contestId = SecurityUtils.getContestId();

        baseBrief.setCreateTime(DateUtils.getNowDate());
        baseBrief.setContestId(contestId);

        BaseBrief byContestId = getByContestId(contestId);
        if (Objects.nonNull(byContestId)){
            baseBrief.setId(byContestId.getId());
        }
        return this.saveOrUpdate(baseBrief)?1:0;
    }

    /**
     * 修改比赛简介
     * 
     * @param baseBrief 比赛简介
     * @return 结果
     */
    @Override
    public int updateBaseBrief(BaseBrief baseBrief)
    {
        baseBrief.setUpdateTime(DateUtils.getNowDate());
        return baseBriefMapper.updateBaseBrief(baseBrief);
    }

    /**
     * 批量删除比赛简介
     * 
     * @param ids 需要删除的比赛简介主键
     * @return 结果
     */
    @Override
    public int deleteBaseBriefByIds(Long[] ids)
    {
        return baseBriefMapper.deleteBaseBriefByIds(ids);
    }

    /**
     * 删除比赛简介信息
     * 
     * @param id 比赛简介主键
     * @return 结果
     */
    @Override
    public int deleteBaseBriefById(Long id)
    {
        return baseBriefMapper.deleteBaseBriefById(id);
    }
}
