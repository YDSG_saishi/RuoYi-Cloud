package com.ruoyi.system.domain.vo.H5;

import lombok.Data;

import java.util.List;

@Data
public class KeyFiguresVO {

    /**
     * 主队名称
     */
    private String teamOneName;
    /**
     * 主队logo
     */
    private String teamOneLogo;
    /**
     * 主队(三分OR一分)
     */
    private Long teamOneScore;
    /**
     * 主队(三分还是一分)名称
     */
    private String teamOneScoreName;
    /**
     * 主队二分
     */
    private Integer teamOneTwoScore;
    /**
     * 主队篮板
     */
    private Integer teamOneBackboard;
    /**
     * 主队数据
     */
    private List<KeyFiguresList> teamOneDataList;

    /**
     * 客队名称
     */
    private String teamTwoName;
    /**
     * 客队logo
     */
    private String teamTwoLogo;
    /**
     * 客队(三分OR一分)
     */
    private Long teamTwoScore;
    /**
     * 客队(三分还是一分)名称
     */
    private String teamTwoScoreName;
    /**
     * 客队二分
     */
    private Integer teamTwoTwoScore;
    /**
     * 客队篮板
     */
    private Integer teamTwoBackboard;
    /**
     * 客队数据
     */
    private List<KeyFiguresList> teamTwoDataList;
    @Data
    public static class KeyFiguresList{
        /**
         * 球员名称
         */
        private String playerName;

        /**
         * 个数
         */
        private Long num;
    }
}
