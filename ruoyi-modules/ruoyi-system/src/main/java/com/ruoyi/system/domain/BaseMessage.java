package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.ruoyi.system.domain.vo.BaseMsgReplyVO;
import io.swagger.v3.oas.annotations.media.Schema;
import com.ruoyi.common.core.annotation.Excel;
import lombok.*;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.web.domain.BaseEntity;

import java.util.List;

/**
 * 留言反馈对象 base_message
 * 
 * @author ruoyi
 * @date 2024-05-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("base_message")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BaseMessage extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @Schema(name = "id", description = "主键")
    @TableId(type = IdType.AUTO)
    private Long id;

    /** 机构id */
    @Excel(name = "机构id")
    @Schema(name = "organizationId", description = "机构id")
    private Long organizationId;

    /** 联赛id */
    @Excel(name = "联赛id")
    @Schema(name = "contestId", description = "联赛id")
    private Long contestId;

    /** 是否展示 0:展示；1:隐藏; */
    @Excel(name = "是否展示 0:展示；1:隐藏;")
    @Schema(name = "state", description = "是否展示 0:展示；1:隐藏;")
    private Long state;

    /** 留言内容描述 */
    @Excel(name = "留言内容描述")
    @Schema(name = "remarkTest", description = "留言内容描述")
    private String remarkTest;

    /** 图片 */
    @Excel(name = "图片")
    @Schema(name = "msgPhoto", description = "图片")
    private String msgPhoto;

    /** 创建人id */
    @Excel(name = "创建人id")
    @Schema(name = "createUserId", description = "创建人id")
    private Long createUserId;

    /** 创建人姓名 */
    @Excel(name = "创建人姓名")
    @Schema(name = "createUserName", description = "创建人姓名")
    private String createUserName;

    /** 修改人id */
    @Excel(name = "修改人id")
    @Schema(name = "updateUserId", description = "修改人id")
    private Long updateUserId;

    /** 修改人姓名 */
    @Excel(name = "修改人姓名")
    @Schema(name = "updateUserName", description = "修改人姓名")
    private String updateUserName;

    /** 是否已删除 0:未删除;1:已删除 */
    @Excel(name = "是否已删除 0:未删除;1:已删除")
    @Schema(name = "isDeleted", description = "是否已删除 0:未删除;1:已删除")
    private Long isDeleted;

    @TableField(exist = false)
    List<BaseMsgReplyVO> replyVOS;

}
