package com.ruoyi.system.domain;

public class LiveMessageCode {

    // 连接成功code 0
    public static final Integer CONNECT_SUCCESS = 0;
    // 连接失败code -1
    public static final Integer CONNECT_FAILED = -1;
    // 发送消息code 1
    public static final Integer SEND_MESSAGE = 1;
    // 更新在线人数code 2
    public static final Integer UPDATE_ONLINE_NUM = 2;
    // 禁言某人 3
    public static final Integer PROHIBITION_USER = 3;
    // 解除禁言 某人 4
    public static final Integer SECURE_PROHIBITION_USER = 4;
    // 撤回消息 5
    public static final Integer WITHDRAW_MESSAGE = 5;
    // 直播结束 6
    public static final Integer LIVE_END = 6;
    // 弹出商品卡片信息 7
    public static final Integer EJECT_GOODS = 7;
    // 直播流更换 8
    public static final Integer LIVE_STREAM_CHANGE = 8;
    // 直播开始 9
    public static final Integer LIVE_START = 9;
    // 带货商品更新 10
    public static final Integer LIVE_GOODS_UPDATE = 10;
    // 直播间设置更新 11
    public static final Integer LIVE_SET_UP_UPDATE = 11;
    // 直播暂时离开 12
    public static final Integer LIVE_TEMPORARILY_LEAVE = 12;
    // 课件更新 13
    public static final Integer COURSEWARE_UPDATE = 13;
    // 进场特效 14
    public static final Integer MOBILIZATION_NOTICE = 14;
    // 分享特效 15
    public static final Integer SHARE_NOTICE = 15;
    // 点赞特效 16
    public static final Integer LIKE_NOTICE = 16;
    //  心跳检测 999
    public static final Integer HEARTBEAT_DETECTION = 999;


}
