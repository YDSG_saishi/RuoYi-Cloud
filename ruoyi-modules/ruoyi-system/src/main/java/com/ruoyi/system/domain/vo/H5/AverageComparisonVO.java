package com.ruoyi.system.domain.vo.H5;

import com.ruoyi.system.domain.vo.BaseTeamCompareVO;
import com.ruoyi.system.domain.vo.CopyBaseTeamCompareVO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

@Data
public class AverageComparisonVO {
    @Schema(name = "oneTeam", description = "第一个队伍")
    CopyBaseTeamCompareVO oneTeam;

    @Schema(name = "twoTeam", description = "第二个队伍")
    CopyBaseTeamCompareVO twoTeam;

}
