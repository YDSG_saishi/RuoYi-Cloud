package com.ruoyi.system.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.domain.BaseSeason;

/**
 * 赛季Service接口
 * 
 * @author wangbj
 * @date 2024-04-16
 */
public interface IBaseSeasonService extends IService<BaseSeason>
{
    /**
     * 查询赛季
     * 
     * @param id 赛季主键
     * @return 赛季
     */
    public BaseSeason selectBaseSeasonById(Long id);

    /**
     * 查询赛季列表
     * 
     * @param baseSeason 赛季
     * @return 赛季集合
     */
    public List<BaseSeason> selectBaseSeasonList(BaseSeason baseSeason);

    /**
     * 新增赛季
     * 
     * @param baseSeason 赛季
     * @return 结果
     */
    public int insertBaseSeason(BaseSeason baseSeason);

    /**
     * 修改赛季
     * 
     * @param baseSeason 赛季
     * @return 结果
     */
    public int updateBaseSeason(BaseSeason baseSeason);

    /**
     * 批量删除赛季
     * 
     * @param ids 需要删除的赛季主键集合
     * @return 结果
     */
    public int deleteBaseSeasonByIds(Long[] ids);

    /**
     * 删除赛季信息
     * 
     * @param id 赛季主键
     * @return 结果
     */
    public int deleteBaseSeasonById(Long id);
}
