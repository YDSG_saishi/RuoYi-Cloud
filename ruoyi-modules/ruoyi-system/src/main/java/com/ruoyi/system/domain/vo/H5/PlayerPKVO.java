package com.ruoyi.system.domain.vo.H5;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class PlayerPKVO {

    /**
     * 得分
     */
    private Long score;

    /**
     * 篮板
     */
    private Long backboard = 0L;

    /**
     * 助攻
     */
    private Long assist = 0L;

    /**
     * 投篮进球
     */
    private Long shootPointNum = 0L;
    /**
     * 投篮总数
     */
    private Long shootTotalNum = 0L;

    /**
     * 抢断
     */
    private Long stral = 0L;
    /**
     * 失误
     */
    private Long mistake = 0L;
    /**
     * 盖帽
     */
    private Long blockShot = 0L;
    /**
     * 犯规
     */
    private Long foul = 0L;
    /**
     * 二分命中率
     */
    private BigDecimal twoPointRate = BigDecimal.ZERO;
    /**
     * 三分命中率
     */
    private BigDecimal threePointRate = BigDecimal.ZERO;
    /**
     * 罚球命中率
     */
    private BigDecimal pointRate = BigDecimal.ZERO;
    /**
     * 效率值
     */
    private Long per = 0L;
    /**
     * 真实命中率
     */
    private BigDecimal rate = BigDecimal.ZERO;
}
