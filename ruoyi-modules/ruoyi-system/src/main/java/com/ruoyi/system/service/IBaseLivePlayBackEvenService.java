package com.ruoyi.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.domain.BaseLivePlayBackEven;
import com.ruoyi.system.domain.BaseLiveTournament;
import com.ruoyi.system.domain.LiveStreamCode;
import com.ruoyi.system.domain.dto.CreateLiveWatermarkRuleDto;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 比赛直播房间Service接口
 * 
 * @author ruoyi
 * @date 2024-05-10
 */
public interface IBaseLivePlayBackEvenService extends IService<BaseLivePlayBackEven>{

    void playbackEvent(String name,Long tournamentId, String videoUrl,Integer tyoe);

    void saveStrName(String strName,Long tournamentId,Long playId,Long playerLogId);

    void updatePlayId(Long basketballPlayerId, Long id);

    void deleteByPlayerLogId(Long id);

    /**
     * 通过比赛id 修正视频锦集
     * @param id
     */
    void batchRemote(Long id);
}

