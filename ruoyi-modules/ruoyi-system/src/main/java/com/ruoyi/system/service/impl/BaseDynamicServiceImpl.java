package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.system.mapper.BaseDynamicMapper;
import com.ruoyi.system.domain.BaseDynamic;
import com.ruoyi.system.service.IBaseDynamicService;

/**
 * 动态Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-26
 */
@Service
public class BaseDynamicServiceImpl extends ServiceImpl<BaseDynamicMapper, BaseDynamic> implements IBaseDynamicService
{
    @Autowired
    private BaseDynamicMapper baseDynamicMapper;

    /**
     * 查询动态
     * 
     * @param id 动态主键
     * @return 动态
     */
    @Override
    public BaseDynamic selectBaseDynamicById(Long id)
    {
        return baseDynamicMapper.selectBaseDynamicById(id);
    }

    /**
     * 查询动态列表
     * 
     * @param baseDynamic 动态
     * @return 动态
     */
    @Override
    public List<BaseDynamic> selectBaseDynamicList(BaseDynamic baseDynamic)
    {
        return baseDynamicMapper.selectBaseDynamicList(baseDynamic);
    }

    /**
     * 新增动态
     * 
     * @param baseDynamic 动态
     * @return 结果
     */
    @Override
    public int insertBaseDynamic(BaseDynamic baseDynamic)
    {
        baseDynamic.setCreateTime(DateUtils.getNowDate());
        return baseDynamicMapper.insertBaseDynamic(baseDynamic);
    }

    /**
     * 修改动态
     * 
     * @param baseDynamic 动态
     * @return 结果
     */
    @Override
    public int updateBaseDynamic(BaseDynamic baseDynamic)
    {
        baseDynamic.setUpdateTime(DateUtils.getNowDate());
        return baseDynamicMapper.updateBaseDynamic(baseDynamic);
    }

    /**
     * 批量删除动态
     * 
     * @param ids 需要删除的动态主键
     * @return 结果
     */
    @Override
    public int deleteBaseDynamicByIds(Long[] ids)
    {
        return baseDynamicMapper.deleteBaseDynamicByIds(ids);
    }

    /**
     * 删除动态信息
     * 
     * @param id 动态主键
     * @return 结果
     */
    @Override
    public int deleteBaseDynamicById(Long id)
    {
        return baseDynamicMapper.deleteBaseDynamicById(id);
    }
}
