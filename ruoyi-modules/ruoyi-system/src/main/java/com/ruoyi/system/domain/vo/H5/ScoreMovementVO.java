package com.ruoyi.system.domain.vo.H5;

import lombok.Data;

import java.util.List;

@Data
public class ScoreMovementVO {

    /**
     * 主队名称
     */
    private String teamOneName;
    /**
     * 主队最高领先分数
     */
    private Long teamOneScoreMax;
    /**
     * 主队比赛小节分数
     */
    private List<ScoreMovementList> teamOneScoreMovementListList;

    /**
     * 客队名称
     */
    private String teamTwoName;
    /**
     * 客队最高领先分数
     */
    private Long teamTwoScoreMax;
    /**
     * 客队比赛小节分数
     */
    private List<ScoreMovementList> teamTwoScoreMovementListList;

    @Data
    public static class ScoreMovementList{
        /**
         * 比赛小节
         */
        private int subsection;

        /**
         * 分数
         */
        private Long score;
    }
}
