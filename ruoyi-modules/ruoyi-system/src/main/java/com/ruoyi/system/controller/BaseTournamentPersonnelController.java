package com.ruoyi.system.controller;

import java.util.LinkedHashMap;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.system.domain.BaseTournamentPersonnel;
import com.ruoyi.system.domain.vo.BaseTournamentPersonnelResponseVO;
import com.ruoyi.system.service.IBaseTournamentPersonnelService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;

import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 赛事工作人员Controller
 * 
 * @author ruoyi
 * @date 2024-04-16
 */
@ApiOperation("赛事工作人员")
@RestController
@RequestMapping("/personnel")
public class BaseTournamentPersonnelController extends BaseController
{
    @Autowired
    private IBaseTournamentPersonnelService baseTournamentPersonnelService;

    /**
     * 查询赛事工作人员列表
     */
    @RequiresPermissions("system:personnel:list")
    @GetMapping("/list")
    @Operation(summary = "查询赛事工作人员列表", description = "查询赛事工作人员列表")
    public TableDataInfo list(BaseTournamentPersonnel baseTournamentPersonnel)
    {
        startPage();
        List<BaseTournamentPersonnel> list = baseTournamentPersonnelService.selectBaseTournamentPersonnelList(baseTournamentPersonnel);
        return getDataTable(list);
    }

    /**
     * 导出赛事工作人员列表
     */
    @Operation(summary = "导出赛事工作人员列表", description = "导出赛事工作人员列表")
    @RequiresPermissions("system:personnel:export")
    @Log(title = "赛事工作人员", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseTournamentPersonnel baseTournamentPersonnel)
    {
        List<BaseTournamentPersonnel> list = baseTournamentPersonnelService.selectBaseTournamentPersonnelList(baseTournamentPersonnel);
        ExcelUtil<BaseTournamentPersonnel> util = new ExcelUtil<BaseTournamentPersonnel>(BaseTournamentPersonnel.class);
        util.exportExcel(response, list, "赛事工作人员数据");
    }

    /**
     * 获取赛事工作人员详细信息
     */
    @Operation(summary = "获取赛事工作人员详细信息", description = "获取赛事工作人员详细信息")
    @RequiresPermissions("system:personnel:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(baseTournamentPersonnelService.selectBaseTournamentPersonnelById(id));
    }

    /**
     * 新增赛事工作人员
     */
    @Operation(summary = "新增赛事工作人员", description = "新增赛事工作人员")
    @RequiresPermissions("system:personnel:add")
    @Log(title = "赛事工作人员", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseTournamentPersonnel baseTournamentPersonnel)
    {
        int tournamentPersonnel = baseTournamentPersonnelService.insertBaseTournamentPersonnel(baseTournamentPersonnel);
        if (tournamentPersonnel == -1){
            return AjaxResult.error("当前工作人员已经有联赛级别的身份了");
        }
        return AjaxResult.success("新增成功");
    }

    /**
     * 修改赛事工作人员
     */
    @Operation(summary = "修改赛事工作人员", description = "修改赛事工作人员")
    @RequiresPermissions("system:personnel:edit")
    @Log(title = "赛事工作人员", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseTournamentPersonnel baseTournamentPersonnel)
    {
        int tournamentPersonnel = baseTournamentPersonnelService.updateBaseTournamentPersonnel(baseTournamentPersonnel);
        if (tournamentPersonnel == -1){
            return AjaxResult.error("当前工作人员已经有联赛级别的身份了");
        }

        return AjaxResult.success("修改成功");
    }

    /**
     * 删除赛事工作人员
     */
    @Operation(summary = "删除赛事工作人员", description = "删除赛事工作人员")
    @RequiresPermissions("system:personnel:remove")
    @Log(title = "赛事工作人员", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseTournamentPersonnelService.deleteBaseTournamentPersonnelByIds(ids));
    }

    /**
     * 工作人员出勤统计
     */
    @Operation(summary = "工作人员出勤统计", description = "工作人员出勤统计")
    @GetMapping(value = "/getAttendanceCount")
    public List<BaseTournamentPersonnelResponseVO> getAttendanceCount(@RequestParam Long seasonId)
    {
        return baseTournamentPersonnelService.getAttendanceCount(seasonId);

    }
}
