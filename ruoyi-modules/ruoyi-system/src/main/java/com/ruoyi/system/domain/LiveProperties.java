package com.ruoyi.system.domain;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "live")
@Data
public class LiveProperties {

    /**
     * 直播的应用名称
     */
    private String appName;

    /**
     * 推流URL
     */
    private String pushURL;

    /**
     * 播放URL
     */
    private String viewURL;

    /**
     * 推流key
     */
    private String pushKey;

    /**
     * 播放key
     */
    private String viewKey;

    /**
     * 自适应转码模板
     */
    private String adaptiveTemplate;

    /**
     * 直播的reids 统计key模板
     */
    private String redisLiveStat;

    /**
     * 直播的reids 用户流量暂存key
     */
    private String redisUserFreezeFlow;

    /**
     * 标清名
     */
    private String SD;

    /**
     * 高清名
     */
    private String HD;

    /**
     * 超清名
     */
    private String FHD;

    /**
     * 截图间隔 s/张
     */
    private Integer screenshotGap;

    /**
     * 审核间隔 s/张
     */
    private Integer processGap;

    private String secretId;
    private String secretKey;


}
