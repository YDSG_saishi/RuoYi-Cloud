package com.ruoyi.system.mapper;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.system.domain.BaseAttendanceLog;
import com.ruoyi.system.domain.BaseTournamentChat;
import com.ruoyi.system.domain.dto.H5.TournamentChatDto;

import java.util.List;
import java.util.Objects;


/**
 */
public interface BaseTournamentChatMapper extends BaseMapper<BaseTournamentChat>{

    default List<BaseTournamentChat> selectByList(TournamentChatDto dto){
        LambdaQueryWrapper<BaseTournamentChat> wrapper = Wrappers.<BaseTournamentChat>lambdaQuery()
                .eq(BaseTournamentChat::getTournamentId,dto.getTournamentId())
                .ge(Objects.nonNull(dto.getDate()),BaseTournamentChat::getCreateTime,dto.getDate())
                .eq(BaseTournamentChat::getIsDeleted,0)
                .orderByAsc(BaseTournamentChat::getCreateTime);
        return selectList(wrapper);
    }
}

