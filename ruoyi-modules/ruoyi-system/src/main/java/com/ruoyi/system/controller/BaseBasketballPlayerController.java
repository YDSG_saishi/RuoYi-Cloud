package com.ruoyi.system.controller;

import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletResponse;

import com.github.pagehelper.PageInfo;
import com.ruoyi.common.core.web.page.PageDomain;
import com.ruoyi.common.core.web.page.TableSupport;
import com.ruoyi.system.domain.BaseBasketballPlayer;
import com.ruoyi.system.service.IBaseBasketballPlayerService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;

import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 球员Controller
 * 
 * @author wangbj
 * @date 2024-04-16
 */
@ApiOperation("球员")
@RestController
@RequestMapping("/basketballPlayer")
public class BaseBasketballPlayerController extends BaseController
{
    @Autowired
    private IBaseBasketballPlayerService baseBasketballPlayerService;

    /**
     * 查询球员列表
     */
    @RequiresPermissions("system:player:list")
    @GetMapping("/list")
    @Operation(summary = "查询球员列表", description = "查询球员列表")
    public TableDataInfo list(BaseBasketballPlayer baseBasketballPlayer)
    {
//        startPage();
        List<BaseBasketballPlayer> list = baseBasketballPlayerService.selectBaseBasketballPlayerList(baseBasketballPlayer);
        PageDomain dto = TableSupport.buildPageRequest();
        List<BaseBasketballPlayer> collect = list.stream()
                .skip((long) (dto.getPageNum() - 1) * dto.getPageSize())
                .limit(dto.getPageSize()).collect(Collectors.toList());
        TableDataInfo tableDataInfo = new TableDataInfo(collect, (int) new PageInfo(list).getTotal());
        tableDataInfo.setCode(200);
        tableDataInfo.setMsg("查询成功");
        return tableDataInfo;
    }

    /**
     * 导出球员列表
     */
    @Operation(summary = "导出球员列表", description = "导出球员列表")
    @RequiresPermissions("system:player:export")
    @Log(title = "球员", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseBasketballPlayer baseBasketballPlayer)
    {
        List<BaseBasketballPlayer> list = baseBasketballPlayerService.selectBaseBasketballPlayerList(baseBasketballPlayer);
        ExcelUtil<BaseBasketballPlayer> util = new ExcelUtil<BaseBasketballPlayer>(BaseBasketballPlayer.class);
        util.exportExcel(response, list, "球员数据");
    }

    /**
     * 获取球员详细信息
     */
    @Operation(summary = "获取球员详细信息", description = "获取球员详细信息")
    @RequiresPermissions("system:player:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(baseBasketballPlayerService.selectBaseBasketballPlayerById(id));
    }

    /**
     * 新增球员
     */
    @Operation(summary = "新增球员", description = "新增球员")
    @RequiresPermissions("system:player:add")
    @Log(title = "球员", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseBasketballPlayer baseBasketballPlayer)
    {
        return toAjax(baseBasketballPlayerService.insertBaseBasketballPlayer(baseBasketballPlayer));
    }

    /**
     * 修改球员
     */
    @Operation(summary = "修改球员", description = "修改球员")
    @RequiresPermissions("system:player:edit")
    @Log(title = "球员", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseBasketballPlayer baseBasketballPlayer)
    {
        return toAjax(baseBasketballPlayerService.updateBaseBasketballPlayer(baseBasketballPlayer));
    }

    /**
     * 删除球员
     */
    @Operation(summary = "删除球员", description = "删除球员")
    @RequiresPermissions("system:player:remove")
    @Log(title = "球员", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseBasketballPlayerService.deleteBaseBasketballPlayerByIds(ids));
    }
}
