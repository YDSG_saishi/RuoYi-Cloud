package com.ruoyi.system.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.domain.BaseMessage;
import com.ruoyi.system.domain.vo.BaseMsgReplyVO;

/**
 * 留言反馈Service接口
 * 
 * @author ruoyi
 * @date 2024-05-07
 */
public interface IBaseMessageService extends IService<BaseMessage>
{
    /**
     * 查询留言反馈
     * 
     * @param id 留言反馈主键
     * @return 留言反馈
     */
    public BaseMessage selectBaseMessageById(Long id);

    /**
     * 查询留言反馈列表
     * 
     * @param baseMessage 留言反馈
     * @return 留言反馈集合
     */
    public List<BaseMessage> selectBaseMessageList(BaseMessage baseMessage);

    /**
     * 新增留言反馈
     * 
     * @param baseMessage 留言反馈
     * @return 结果
     */
    public int insertBaseMessage(BaseMessage baseMessage);

    /**
     * 修改留言反馈
     * 
     * @param baseMessage 留言反馈
     * @return 结果
     */
    public int updateBaseMessage(BaseMessage baseMessage);

    /**
     * 批量删除留言反馈
     * 
     * @param ids 需要删除的留言反馈主键集合
     * @return 结果
     */
    public int deleteBaseMessageByIds(Long[] ids);

    /**
     * 删除留言反馈信息
     * 
     * @param id 留言反馈主键
     * @return 结果
     */
    public int deleteBaseMessageById(Long id);

    List<BaseMessage> getMyList();

    /**
     * 回复留言
     * @param id
     * @param msg
     * @return
     */
    List<BaseMsgReplyVO> reply(Long id, String msg);
}
