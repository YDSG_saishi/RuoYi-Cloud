package com.ruoyi.system.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.domain.BasketballPlayerAndTeam;

/**
 * 球队球员教练组关联Service接口
 * 
 * @author wangbj
 * @date 2024-04-16
 */
public interface IBasketballPlayerAndTeamService extends IService<BasketballPlayerAndTeam>
{
    /**
     * 查询球队球员教练组关联
     * 
     * @param id 球队球员教练组关联主键
     * @return 球队球员教练组关联
     */
    public BasketballPlayerAndTeam selectBasketballPlayerAndTeamById(Long id);

    /**
     * 查询球队球员教练组关联列表
     * 
     * @param basketballPlayerAndTeam 球队球员教练组关联
     * @return 球队球员教练组关联集合
     */
    public List<BasketballPlayerAndTeam> selectBasketballPlayerAndTeamList(BasketballPlayerAndTeam basketballPlayerAndTeam);

    /**
     * 新增球队球员教练组关联
     * 
     * @param basketballPlayerAndTeam 球队球员教练组关联
     * @return 结果
     */
    public int insertBasketballPlayerAndTeam(BasketballPlayerAndTeam basketballPlayerAndTeam);

    /**
     * 修改球队球员教练组关联
     * 
     * @param basketballPlayerAndTeam 球队球员教练组关联
     * @return 结果
     */
    public int updateBasketballPlayerAndTeam(BasketballPlayerAndTeam basketballPlayerAndTeam);

    /**
     * 批量删除球队球员教练组关联
     * 
     * @param ids 需要删除的球队球员教练组关联主键集合
     * @return 结果
     */
    public int deleteBasketballPlayerAndTeamByIds(Long[] ids);

    /**
     * 删除球队球员教练组关联信息
     * 
     * @param id 球队球员教练组关联主键
     * @return 结果
     */
    public int deleteBasketballPlayerAndTeamById(Long id);
}
