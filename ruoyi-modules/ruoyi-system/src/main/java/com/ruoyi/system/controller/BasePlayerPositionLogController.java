package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.system.domain.BasePlayerPositionLog;
import com.ruoyi.system.domain.vo.BasePositionMsgVO;
import com.ruoyi.system.domain.vo.BasePositionResponseVO;
import com.ruoyi.system.domain.vo.BaseSubstitutionRequestVO;
import com.ruoyi.system.service.IBasePlayerPositionLogService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;

import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 球员阵位记录Controller
 * 
 * @author wangbj
 * @date 2024-04-16
 */
@ApiOperation("球员阵位记录")
@RestController
@RequestMapping("/playerPositionLog")
public class BasePlayerPositionLogController extends BaseController
{
    @Autowired
    private IBasePlayerPositionLogService basePlayerPositionLogService;

    /**
     * 查询球员阵位记录列表
     */
    @RequiresPermissions("system:log:list")
    @GetMapping("/list")
    @Operation(summary = "查询球员阵位记录列表", description = "查询球员阵位记录列表")
    public TableDataInfo list(BasePlayerPositionLog basePlayerPositionLog)
    {
        startPage();
        List<BasePlayerPositionLog> list = basePlayerPositionLogService.selectBasePlayerPositionLogList(basePlayerPositionLog);
        return getDataTable(list);
    }

    /**
     * 导出球员阵位记录列表
     */
    @Operation(summary = "导出球员阵位记录列表", description = "导出球员阵位记录列表")
    @RequiresPermissions("system:log:export")
    @Log(title = "球员阵位记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BasePlayerPositionLog basePlayerPositionLog)
    {
        List<BasePlayerPositionLog> list = basePlayerPositionLogService.selectBasePlayerPositionLogList(basePlayerPositionLog);
        ExcelUtil<BasePlayerPositionLog> util = new ExcelUtil<BasePlayerPositionLog>(BasePlayerPositionLog.class);
        util.exportExcel(response, list, "球员阵位记录数据");
    }

    /**
     * 获取球员阵位记录详细信息
     */
    @Operation(summary = "获取球员阵位记录详细信息", description = "获取球员阵位记录详细信息")
    @RequiresPermissions("system:log:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(basePlayerPositionLogService.selectBasePlayerPositionLogById(id));
    }

    /**
     * 新增球员阵位记录
     */
    @Operation(summary = "新增球员阵位记录", description = "新增球员阵位记录")
    @RequiresPermissions("system:log:add")
    @Log(title = "球员阵位记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BasePlayerPositionLog basePlayerPositionLog)
    {
        return toAjax(basePlayerPositionLogService.insertBasePlayerPositionLog(basePlayerPositionLog));
    }

    /**
     * 修改球员阵位记录
     */
    @Operation(summary = "修改球员阵位记录", description = "修改球员阵位记录")
    @RequiresPermissions("system:log:edit")
    @Log(title = "球员阵位记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BasePlayerPositionLog basePlayerPositionLog)
    {
        return toAjax(basePlayerPositionLogService.updateBasePlayerPositionLog(basePlayerPositionLog));
    }

    /**
     * 删除球员阵位记录
     */
    @Operation(summary = "删除球员阵位记录", description = "删除球员阵位记录")
    @RequiresPermissions("system:log:remove")
    @Log(title = "球员阵位记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(basePlayerPositionLogService.deleteBasePlayerPositionLogByIds(ids));
    }

    /**
     * 生成球员阵位
     */
    @Operation(summary = "生成球员阵位", description = "生成球员阵位")
    @Log(title = "球员阵位记录", businessType = BusinessType.INSERT)
    @GetMapping("/createPosition")
    public BasePositionResponseVO createPosition(@RequestParam Long tournamentId)
    {
        BasePositionResponseVO position = basePlayerPositionLogService.createPosition(tournamentId);
        return position;
    }

    /**
     * 拖拽换人
     */
    @Operation(summary = "拖拽换人", description = "拖拽换人")
    @PostMapping("/substitution")
    public List<BasePositionMsgVO>  substitution(@RequestBody BaseSubstitutionRequestVO requestVO)
    {
        List<BasePositionMsgVO>  position = basePlayerPositionLogService.substitution(requestVO);
        return position;
    }

    /**
     * 全体换人
     */
    @Operation(summary = "全体换人", description = "全体换人")
    @PostMapping("/substitution/player")
    public BasePositionResponseVO  substitutionPlayer(@RequestBody BaseSubstitutionRequestVO requestVO)
    {
        BasePositionResponseVO  position = basePlayerPositionLogService.substitutionPlayer(requestVO);
        return position;
    }
}
