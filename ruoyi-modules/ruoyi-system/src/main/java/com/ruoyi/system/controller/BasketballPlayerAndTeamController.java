package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.system.domain.BasketballPlayerAndTeam;
import com.ruoyi.system.service.IBasketballPlayerAndTeamService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;

import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 球队球员教练组关联Controller
 * 
 * @author wangbj
 * @date 2024-04-16
 */
@ApiOperation("球队球员教练组关联")
@RestController
@RequestMapping("/team")
public class BasketballPlayerAndTeamController extends BaseController
{
    @Autowired
    private IBasketballPlayerAndTeamService basketballPlayerAndTeamService;

    /**
     * 查询球队球员教练组关联列表
     */
    @RequiresPermissions("system:team:list")
    @GetMapping("/list")
    @Operation(summary = "查询球队球员教练组关联列表", description = "查询球队球员教练组关联列表")
    public TableDataInfo list(BasketballPlayerAndTeam basketballPlayerAndTeam)
    {
        startPage();
        List<BasketballPlayerAndTeam> list = basketballPlayerAndTeamService.selectBasketballPlayerAndTeamList(basketballPlayerAndTeam);
        return getDataTable(list);
    }

    /**
     * 导出球队球员教练组关联列表
     */
    @Operation(summary = "导出球队球员教练组关联列表", description = "导出球队球员教练组关联列表")
    @RequiresPermissions("system:team:export")
    @Log(title = "球队球员教练组关联", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BasketballPlayerAndTeam basketballPlayerAndTeam)
    {
        List<BasketballPlayerAndTeam> list = basketballPlayerAndTeamService.selectBasketballPlayerAndTeamList(basketballPlayerAndTeam);
        ExcelUtil<BasketballPlayerAndTeam> util = new ExcelUtil<BasketballPlayerAndTeam>(BasketballPlayerAndTeam.class);
        util.exportExcel(response, list, "球队球员教练组关联数据");
    }

    /**
     * 获取球队球员教练组关联详细信息
     */
    @Operation(summary = "获取球队球员教练组关联详细信息", description = "获取球队球员教练组关联详细信息")
    @RequiresPermissions("system:team:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(basketballPlayerAndTeamService.selectBasketballPlayerAndTeamById(id));
    }

    /**
     * 新增球队球员教练组关联
     */
    @Operation(summary = "新增球队球员教练组关联", description = "新增球队球员教练组关联")
    @RequiresPermissions("system:team:add")
    @Log(title = "球队球员教练组关联", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BasketballPlayerAndTeam basketballPlayerAndTeam)
    {
        return toAjax(basketballPlayerAndTeamService.insertBasketballPlayerAndTeam(basketballPlayerAndTeam));
    }

    /**
     * 修改球队球员教练组关联
     */
    @Operation(summary = "修改球队球员教练组关联", description = "修改球队球员教练组关联")
    @RequiresPermissions("system:team:edit")
    @Log(title = "球队球员教练组关联", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BasketballPlayerAndTeam basketballPlayerAndTeam)
    {
        return toAjax(basketballPlayerAndTeamService.updateBasketballPlayerAndTeam(basketballPlayerAndTeam));
    }

    /**
     * 删除球队球员教练组关联
     */
    @Operation(summary = "删除球队球员教练组关联", description = "删除球队球员教练组关联")
    @RequiresPermissions("system:team:remove")
    @Log(title = "球队球员教练组关联", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(basketballPlayerAndTeamService.deleteBasketballPlayerAndTeamByIds(ids));
    }
}
