package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * 比赛直播房间对象 base_live_tournament
 * 
 * @author ruoyi
 * @date 2024-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("base_live_play_back_even")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BaseLivePlayBackEven extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @Schema(name = "id", description = "主键")
    @TableId(type = IdType.AUTO)
    private Long id;

    /** 比赛id */
    @Excel(name = "比赛id")
    @Schema(name = "tournamentId", description = "比赛id")
    private Long tournamentId;

    /** 球员id */
    @Excel(name = "球员id")
    @Schema(name = "playId", description = "球员id")
    private Long playId;

    /** 得分记录id */
    @Excel(name = "得分记录id")
    @Schema(name = "playerLogId", description = "得分记录id")
    private Long playerLogId;

    /** 视频名称*/
    @Schema(name = "name", description = "视频名称")
    private String name;

    /** 播放地址 */
    @Excel(name = "播放地址")
    @Schema(name = "videoUrl", description = "播放地址")
    private String videoUrl;

    /** 创建人id */
    @Excel(name = "创建人id")
    @Schema(name = "createUserId", description = "创建人id")
    private Long createUserId;

    /** 创建人姓名 */
    @Excel(name = "创建人姓名")
    @Schema(name = "createUserName", description = "创建人姓名")
    private String createUserName;

    /** 修改人id */
    @Excel(name = "修改人id")
    @Schema(name = "updateUserId", description = "修改人id")
    private Long updateUserId;

    /** 修改人姓名 */
    @Excel(name = "修改人姓名")
    @Schema(name = "updateUserName", description = "修改人姓名")
    private String updateUserName;

    /** 是否已删除 0:未删除;1:已删除 */
    @Excel(name = "是否已删除 0:未删除;1:已删除")
    @Schema(name = "isDeleted", description = "是否已删除 0:未删除;1:已删除")
    private Long isDeleted;

    private Integer status;

    private String taskId;

    private Integer type;


}
