package com.ruoyi.system.service.impl;

import com.alibaba.csp.sentinel.util.AssertUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.redis.service.RedisService;
import com.ruoyi.common.security.service.TokenService;
import com.ruoyi.system.api.model.LoginUser;
import com.ruoyi.system.domain.BaseTournamentChat;
import com.ruoyi.system.domain.dto.H5.TournamentChatDto;
import com.ruoyi.system.mapper.BaseTournamentChatMapper;
import com.ruoyi.system.service.IBaseTournamentChatService;
import com.ruoyi.system.utils.WeChatUitl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * 球员出勤记录Service业务层处理
 * 
 * @author wangbj
 * @date 2024-04-16
 */
@Slf4j
@Service
public class BaseTournamentChatServiceImpl extends ServiceImpl<BaseTournamentChatMapper, BaseTournamentChat> implements IBaseTournamentChatService
{


    @Resource
    private TokenService tokenService;

    @Resource
    private RedisService redisService;

    @Override
    public List<BaseTournamentChat> selectByList(TournamentChatDto dto) {
        AssertUtil.notNull(dto.getTournamentId(),"比赛id不能为空!");
//        AssertUtil.notNull(dto.getDate(),"获取时间点不能为空!");
        return baseMapper.selectByList(dto);
    }

    @Override
    public int send(BaseTournamentChat dto) {
        AssertUtil.notEmpty(dto.getContext(),"发送消息不能为空!");
        AssertUtil.notEmpty(dto.getUserId(),"发消息用户不能为空!");
        AssertUtil.notNull(dto.getTournamentId(),"比赛id不能为空!");

        dto.setCreateTime(new Date());
        dto.setCreateBy(dto.getUserName());
        return this.save(dto) ? 1 : 0;
    }

    @Override
    public AjaxResult getsignature(String url) {
        log.info("url初始信息"+url);

        Map<String, Object> wxInfo = new HashMap<>();
        try {
            url = URLDecoder.decode(url, StandardCharsets.UTF_8.name());
            if(url.contains("%")){
                url = URLDecoder.decode(url, StandardCharsets.UTF_8.name());
            }
            log.info("url解码信息"+url);
//            String jsapiTicket = "";
//            String getsignature = redisService.getCacheObject("getsignature");
//            if (Objects.nonNull(getsignature)){
//                jsapiTicket = getsignature;
//            }else {
//                String accessToken = WeChatUitl.getAccessToken();
//                jsapiTicket = WeChatUitl.getTicket(accessToken);
//                redisService.setCacheObject("getsignature",jsapiTicket,5L, TimeUnit.MINUTES);
//            }
            String accessToken = WeChatUitl.getAccessToken();
            String jsapiTicket = WeChatUitl.getTicket(accessToken);

            String noncestr = UUID.randomUUID().toString().replace("-", "").substring(0, 16);//随机字符串
            String timestamp = String.valueOf(System.currentTimeMillis()/ 1000);// 时间戳
            String params = "jsapi_ticket=" + jsapiTicket + "&noncestr=" + noncestr + "&timestamp=" + timestamp + "&url=" + url;
            //将字符串进行sha1加密
            String signature = WeChatUitl.getSHA1(params);
            //微信appId
            String appId = WeChatUitl.appIdWx;
            wxInfo.put("appId", appId);
            wxInfo.put("jsapi_ticket", jsapiTicket);
            wxInfo.put("timestamp", timestamp);
            wxInfo.put("nonceStr", noncestr);
            wxInfo.put("signature", signature);
            wxInfo.put("url", url);
            log.info("获取校验成功");
            return AjaxResult.success(wxInfo);
        } catch (Exception e) {
            log.error("获取微信加密信息" + e.getMessage(), e);
            return AjaxResult.error("获取微信加密信息" + e.getMessage());
        }
    }
}
