package com.ruoyi.system.controller.H5;

import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.system.domain.BaseTournamentChat;
import com.ruoyi.system.domain.dto.H5.TournamentChatDto;
import com.ruoyi.system.service.IBaseTournamentChatService;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * 聊天室
 * 
 * @author wangbj
 * @date 2024-04-16
 */
@Api("聊天室")
@RestController
@RequestMapping("/h5/tournament_chat")
public class BaseTournamentChatController extends BaseController
{
    @Autowired
    private IBaseTournamentChatService iBaseTournamentChatService;

    /**
     * 查询消息
     */
    @GetMapping("/list")
    @Operation(summary = "查询消息", description = "查询消息")
    public AjaxResult list(TournamentChatDto dto)
    {
        return AjaxResult.success(iBaseTournamentChatService.selectByList(dto));
    }

    /**
     * 发送消息
     */
    @PostMapping("/send")
    @Operation(summary = "发送消息", description = "发送消息")
    public AjaxResult send(@RequestBody BaseTournamentChat dto)
    {
        return toAjax(iBaseTournamentChatService.send(dto));
    }

    /**
     * 获取微信加密信息
     * @return
     */
    @RequestMapping(value = "/getsignature")
    @ResponseBody
    public AjaxResult getsignature(String url) {
        return iBaseTournamentChatService.getsignature(url);

    }

}
