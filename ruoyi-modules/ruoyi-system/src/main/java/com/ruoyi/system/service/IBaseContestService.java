package com.ruoyi.system.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.domain.BaseContest;
import com.ruoyi.system.domain.dto.Web.ContestBindingDto;

/**
 * 联赛Service接口
 * 
 * @author wangbj
 * @date 2024-04-16
 */
public interface IBaseContestService extends IService<BaseContest>
{
    /**
     * 查询联赛
     * 
     * @param id 联赛主键
     * @return 联赛
     */
    public BaseContest selectBaseContestById(Long id);

    /**
     * 查询联赛列表
     * 
     * @param baseContest 联赛
     * @return 联赛集合
     */
    public List<BaseContest> selectBaseContestList(BaseContest baseContest);

    /**
     * 新增联赛
     * 
     * @param baseContest 联赛
     * @return 结果
     */
    public BaseContest insertBaseContest(BaseContest baseContest);

    /**
     * 修改联赛
     * 
     * @param baseContest 联赛
     * @return 结果
     */
    public int updateBaseContest(BaseContest baseContest);

    /**
     * 批量删除联赛
     * 
     * @param ids 需要删除的联赛主键集合
     * @return 结果
     */
    public int deleteBaseContestByIds(Long[] ids);

    /**
     * 删除联赛信息
     * 
     * @param id 联赛主键
     * @return 结果
     */
    public int deleteBaseContestById(Long id);

    List<BaseContest> gerMyList(Long userid);

    /**
     * 修改联赛状态
     * @param baseContest
     * @return
     */
    int auditStatus(BaseContest baseContest);

    /**
     * 获取联赛列表
     * @param baseContest
     * @return
     */
    List<BaseContest> selectList(BaseContest baseContest);

    /**
     * 上传大屏轮播图
     * @param baseContest
     * @return
     */
    int saveImg(BaseContest baseContest);
    /**
     * 查询联赛绑定列表
     * @param id
     * @return
     */
    List<BaseContest> getContestBinding(BaseContest baseContest);
    /**
     * 添加联赛绑定
     * @param contestBindingDto
     * @return
     */
    int contestBinding(ContestBindingDto contestBindingDto);
}
