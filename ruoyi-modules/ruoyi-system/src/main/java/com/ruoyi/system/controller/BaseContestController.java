package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import cn.hutool.core.util.ObjectUtil;
import com.ruoyi.common.security.service.TokenService;
import com.ruoyi.system.api.model.LoginUser;
import com.ruoyi.system.domain.BaseContest;
import com.ruoyi.system.service.IBaseContestService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;

import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 联赛Controller
 * 
 * @author wangbj
 * @date 2024-04-16
 */
@ApiOperation("联赛")
@RestController
@RequestMapping("/contest")
public class BaseContestController extends BaseController
{
    @Autowired
    private IBaseContestService baseContestService;

    @Autowired
    TokenService tokenService;

    /**
     * 查询联赛列表
     */
    @RequiresPermissions("system:contest:list")
    @GetMapping("/list")
    @Operation(summary = "查询联赛列表", description = "查询联赛列表")
    public TableDataInfo list(BaseContest baseContest)
    {
//        startPage();
        List<BaseContest> list = baseContestService.selectBaseContestList(baseContest);
        return getDataTable(list);
    }

    /**
     * 导出联赛列表
     */
    @Operation(summary = "导出联赛列表", description = "导出联赛列表")
    @RequiresPermissions("system:contest:export")
    @Log(title = "联赛", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseContest baseContest)
    {
        List<BaseContest> list = baseContestService.selectBaseContestList(baseContest);
        ExcelUtil<BaseContest> util = new ExcelUtil<BaseContest>(BaseContest.class);
        util.exportExcel(response, list, "联赛数据");
    }

    /**
     * 获取联赛详细信息
     */
    @Operation(summary = "获取联赛详细信息", description = "获取联赛详细信息")
    @RequiresPermissions("system:contest:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(baseContestService.selectBaseContestById(id));
    }



    /**
     * 新增联赛
     */
    @Operation(summary = "新增联赛", description = "新增联赛")
    @RequiresPermissions("system:contest:add")
    @Log(title = "联赛", businessType = BusinessType.INSERT)
    @PostMapping
    public BaseContest add(@RequestBody BaseContest baseContest)
    {
        if (ObjectUtil.isNull(baseContest.getOrganizationId())){
            throw new RuntimeException("机构id不能为空");
        }
        return baseContestService.insertBaseContest(baseContest);
    }

    /**
     * 修改联赛
     */
    @Operation(summary = "修改联赛", description = "修改联赛")
    @RequiresPermissions("system:contest:edit")
    @Log(title = "联赛", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseContest baseContest)
    {
        return toAjax(baseContestService.updateBaseContest(baseContest));
    }

    /**
     * 删除联赛
     */
    @Operation(summary = "删除联赛", description = "删除联赛")
    @RequiresPermissions("system:contest:remove")
    @Log(title = "联赛", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseContestService.deleteBaseContestByIds(ids));
    }

    /**
     * 我的联赛列表
     */
    @RequiresPermissions("system:contest:list")
    @GetMapping("/myList")
    @Operation(summary = "查询联赛列表", description = "查询联赛列表")
    public TableDataInfo myList()
    {
        LoginUser loginUser = tokenService.getLoginUser();
        startPage();
        List<BaseContest> list = baseContestService.gerMyList(loginUser.getUserid());
        return getDataTable(list);
    }
}
