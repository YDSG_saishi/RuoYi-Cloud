package com.ruoyi.system.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.BaseLiveTournament;

/**
 * 比赛直播房间Mapper接口
 * 
 * @author ruoyi
 * @date 2024-05-10
 */
public interface BaseLiveTournamentMapper extends BaseMapper<BaseLiveTournament>
{
    /**
     * 查询比赛直播房间
     * 
     * @param id 比赛直播房间主键
     * @return 比赛直播房间
     */
    public BaseLiveTournament selectBaseLiveTournamentById(Long id);

    /**
     * 查询比赛直播房间列表
     * 
     * @param baseLiveTournament 比赛直播房间
     * @return 比赛直播房间集合
     */
    public List<BaseLiveTournament> selectBaseLiveTournamentList(BaseLiveTournament baseLiveTournament);

    /**
     * 新增比赛直播房间
     * 
     * @param baseLiveTournament 比赛直播房间
     * @return 结果
     */
    public int insertBaseLiveTournament(BaseLiveTournament baseLiveTournament);

    /**
     * 修改比赛直播房间
     * 
     * @param baseLiveTournament 比赛直播房间
     * @return 结果
     */
    public int updateBaseLiveTournament(BaseLiveTournament baseLiveTournament);

    /**
     * 删除比赛直播房间
     * 
     * @param id 比赛直播房间主键
     * @return 结果
     */
    public int deleteBaseLiveTournamentById(Long id);

    /**
     * 批量删除比赛直播房间
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseLiveTournamentByIds(Long[] ids);

    /**
     * 比赛id查询推流直播
     * @param tournamentId
     * @return
     */
    default List<BaseLiveTournament> selectLiveByTournamentId(Long tournamentId){
        LambdaQueryWrapper<BaseLiveTournament> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(BaseLiveTournament::getTournamentId,tournamentId);
        wrapper.eq(BaseLiveTournament::getState,1);
        return selectList(wrapper);
    }
}
