package com.ruoyi.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.domain.BaseBanner;
import com.ruoyi.system.domain.dto.Web.BannerDto;

import java.util.List;


/**
 *
 */
public interface IBaseBannerService extends IService<BaseBanner>{

    List<BaseBanner> selectList(BannerDto dto);

    int insertBanner(BaseBanner baseBanner);
}

