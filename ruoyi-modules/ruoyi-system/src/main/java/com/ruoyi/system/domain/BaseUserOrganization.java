package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("base_user_organization")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BaseUserOrganization {
    private Long userId;
    private Long organizationId;
}
