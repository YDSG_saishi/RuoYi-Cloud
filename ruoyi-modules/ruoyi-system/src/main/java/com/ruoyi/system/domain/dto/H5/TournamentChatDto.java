package com.ruoyi.system.domain.dto.H5;

import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.sql.Date;

@Data
public class TournamentChatDto {

    @Schema(name = "tournamentId", description = "比赛id")
    @NotNull(message = "比赛id不能为空")
    private Long tournamentId;

    @Schema(name = "date", description = "上一次获取时间点,第一次进入则传空")
    @NotNull(message = "日期不能为空")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date date;
}
