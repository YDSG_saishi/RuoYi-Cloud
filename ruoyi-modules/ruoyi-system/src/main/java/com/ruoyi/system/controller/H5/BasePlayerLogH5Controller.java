package com.ruoyi.system.controller.H5;

import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.system.domain.dto.H5.KeyFiguresDto;
import com.ruoyi.system.domain.dto.H5.PlayerPKDto;
import com.ruoyi.system.service.IBaseBasketballPlayerService;
import com.ruoyi.system.service.IBasePlayerLogService;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * h5数据栏目
 */
@ApiOperation("h5数据栏目")
@RestController
@RequestMapping("/h5/playerLog")
public class BasePlayerLogH5Controller extends BaseController
{

    @Autowired
    private IBasePlayerLogService basePlayerLogService;

    /**
     * 比分走势
     */
    @GetMapping("/scoreMovement")
    @Operation(summary = "比分走势", description = "比分走势")
    public AjaxResult scoreMovement(@RequestParam("tournamentId")Long tournamentId){
        return AjaxResult.success(basePlayerLogService.scoreMovement(tournamentId));
    }

    /**
     * 关键数据
     */
    @PostMapping("/keyFigures")
    @Operation(summary = "关键数据", description = "关键数据")
    public AjaxResult keyFigures(@RequestBody KeyFiguresDto dto){
        return AjaxResult.success(basePlayerLogService.keyFigures(dto));
    }

    /**
     * 球员列表
     */
    @GetMapping("/playerList")
    @Operation(summary = "球员列表", description = "球员列表")
    public AjaxResult playerList(@RequestParam("tournamentId")Long tournamentId){
        return AjaxResult.success(basePlayerLogService.playerList(tournamentId));
    }


    /**
     * 球员PK
     */
    @PostMapping("/playerPK")
    @Operation(summary = "球员PK", description = "球员PK")
    public AjaxResult playerPK(@RequestBody PlayerPKDto dto){
        return AjaxResult.success(basePlayerLogService.playerPK(dto));
    }

    /**
     * 场均对比
     */
    @PostMapping("/averageComparison")
    @Operation(summary = "场均对比", description = "场均对比")
    public AjaxResult averageComparison(@RequestParam("tournamentId")Long tournamentId){
        return AjaxResult.success(basePlayerLogService.averageComparison(tournamentId));
    }

    /**
     * 赛季对比
     */
    @PostMapping("/seasonComparison")
    @Operation(summary = "赛季对比", description = "赛季对比")
    public AjaxResult seasonComparison(@RequestParam("tournamentId")Long tournamentId){
        return AjaxResult.success(basePlayerLogService.seasonComparison(tournamentId));
    }

    /**
     * 关键数据 得分表现类型 枚举
     */
    @PostMapping("/keyFiguresEnums")
    @Operation(summary = "关键数据 得分表现类型 枚举", description = "关键数据 得分表现类型 枚举")
    public AjaxResult keyFiguresEnums(){
        List<Map<String, Object>> list = new ArrayList<>();
        Map<String,Object> map = new HashMap<>();
        map.put("name","得分");
        map.put("id",1);
        list.add(map);

        Map<String,Object> map2 = new HashMap<>();
        map2.put("name","篮板");
        map2.put("id",9);
        list.add(map2);

        Map<String,Object> map3 = new HashMap<>();
        map3.put("name","助攻");
        map3.put("id",8);
        list.add(map3);

        Map<String,Object> map4 = new HashMap<>();
        map4.put("name","抢断");
        map4.put("id",4);
        list.add(map4);

        Map<String,Object> map5 = new HashMap<>();
        map5.put("name","盖帽");
        map5.put("id",3);
        list.add(map5);

        Map<String,Object> map6 = new HashMap<>();
        map6.put("name","失误");
        map6.put("id",10);
        list.add(map6);
        return AjaxResult.success(list);
    }
}
