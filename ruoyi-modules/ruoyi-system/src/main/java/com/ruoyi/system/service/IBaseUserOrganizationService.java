package com.ruoyi.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.domain.BaseUserOrganization;
import com.ruoyi.system.domain.BasketballPlayerAndTeam;

import java.util.List;

public interface IBaseUserOrganizationService extends IService<BaseUserOrganization> {
    void savePO(Long organizationId, Long userId);

    List<BaseUserOrganization> getUserId(Long userId);

    List<BaseUserOrganization> getUserOrganization(Long organizationId, Long userId);

    void deleteUserOrganization(Long contestId, Long userId);
}
