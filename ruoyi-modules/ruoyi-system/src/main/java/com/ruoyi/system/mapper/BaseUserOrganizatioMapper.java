package com.ruoyi.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.BaseUserOrganization;

/**
 * @author 86188
 */
public interface BaseUserOrganizatioMapper extends BaseMapper<BaseUserOrganization> {
}
