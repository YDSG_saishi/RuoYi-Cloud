package com.ruoyi.system.mapper;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.system.domain.BasePlayerLog;
import com.ruoyi.system.domain.vo.*;
import org.apache.ibatis.annotations.Param;

/**
 * 球员得分现记录Mapper接口
 * 
 * @author wangbj
 * @date 2024-04-16
 */
public interface BasePlayerLogMapper extends BaseMapper<BasePlayerLog>
{
    /**
     * 查询球员得分现记录
     * 
     * @param id 球员得分现记录主键
     * @return 球员得分现记录
     */
    public BasePlayerLog selectBasePlayerLogById(Long id);

    /**
     * 查询球员得分现记录列表
     * 
     * @param basePlayerLog 球员得分现记录
     * @return 球员得分现记录集合
     */
    public List<BasePlayerLogVO> selectBasePlayerLogList(BasePlayerLog basePlayerLog);

    /**
     * 新增球员得分现记录
     * 
     * @param basePlayerLog 球员得分现记录
     * @return 结果
     */
    public int insertBasePlayerLog(BasePlayerLog basePlayerLog);

    /**
     * 修改球员得分现记录
     * 
     * @param basePlayerLog 球员得分现记录
     * @return 结果
     */
    public int updateBasePlayerLog(BasePlayerLog basePlayerLog);

    /**
     * 删除球员得分现记录
     * 
     * @param id 球员得分现记录主键
     * @return 结果
     */
    public int deleteBasePlayerLogById(Long id);

    /**
     * 批量删除球员得分现记录
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBasePlayerLogByIds(Long[] ids);

    /**
     * 获取当前比赛队伍总分值
     * @param teamId 队伍id
     * @param tournamentId 比赛id
     * @return
     */
    Long getScore(@Param("teamId") Long teamId, @Param("tournamentId") Long tournamentId);

    List<BaseSummaryVO> getSummary(Long tournamentId);

    /**
     * 排行榜
     * @param contestId
     * @param type
     * @return
     */
    List<BaseRankingListVO> rankingList(@Param("contestId") Long contestId, @Param("type") int type);

    /**
     * 全场最佳
     * @param tournamentId
     * @param teamOneId
     * @param type 1得分 2篮板 3助攻
     * @return
     */
    BestOfTheGamePlayerVO bestOfTheGame(@Param("tournamentId") Long tournamentId,@Param("teamId") Long teamId,@Param("type") int type);

    /**
     * 球队比较
     * @param teamId
     * @param tournamentId
     * @return
     */
    BaseTeamCompareVO getTeamCompare(@Param("teamId") Long teamId,@Param("tournamentId") Long tournamentId);

    default BaseTeamCompareVO getTeamCompareTeam(@Param("teamId") Long teamId,@Param("tournamentId") Long tournamentId){
        BaseTeamCompareVO teamCompare = getTeamCompare(teamId, tournamentId);
        if (teamCompare.getFq() > 0){
            int i = teamCompare.getFq() + teamCompare.getFqNot();
            teamCompare.setPointRate(BigDecimal.valueOf(teamCompare.getFq()).divide(BigDecimal.valueOf(i),2, RoundingMode.HALF_UP));
        }
        if (teamCompare.getEf() > 0){
            int i = teamCompare.getEf() + teamCompare.getEfNot();
            teamCompare.setTwoPointRate(BigDecimal.valueOf(teamCompare.getEf()).divide(BigDecimal.valueOf(i),2, RoundingMode.HALF_UP));
        }
        if (teamCompare.getSf() > 0){
            int i = teamCompare.getSf() + teamCompare.getSfNot();
            teamCompare.setThreePointRate(BigDecimal.valueOf(teamCompare.getSf()).divide(BigDecimal.valueOf(i),2, RoundingMode.HALF_UP));
        }
        return teamCompare;
    }
    default List<BasePlayerLog> selectByContestId(Long leagueId){
        LambdaQueryWrapper<BasePlayerLog> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(BasePlayerLog::getContestId,leagueId);
        wrapper.eq(BasePlayerLog::getIsDeleted,0);
        return selectList(wrapper);
    }

    default void deleteByTournament(Long[] asList){
        BasePlayerLog log = new BasePlayerLog();
        log.setIsDeleted(1L);
        update(log, Wrappers.<BasePlayerLog>lambdaUpdate().in(BasePlayerLog::getId,asList));
    }

    default List<BasePlayerLog> selectByTournamentId(Long tournamentId){
        LambdaQueryWrapper<BasePlayerLog> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(BasePlayerLog::getTournamentId,tournamentId);
        wrapper.eq(BasePlayerLog::getIsDeleted,0);
        return selectList(wrapper);
    }

    default List<BasePlayerLog> selectByTournamentIdAndTypes(Long tournamentId, List<Long> asList){
        LambdaQueryWrapper<BasePlayerLog> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(BasePlayerLog::getTournamentId,tournamentId);
        wrapper.in(BasePlayerLog::getType,asList);
        wrapper.eq(BasePlayerLog::getIsDeleted,0);
        return selectList(wrapper);
    }

    default List<BasePlayerLog> selectByTournamentIdAndTypeAndSubsection(Long tournamentId, Integer subsection, Integer playerType){
        LambdaQueryWrapper<BasePlayerLog> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(BasePlayerLog::getTournamentId,tournamentId);
        wrapper.eq(Objects.nonNull(playerType),BasePlayerLog::getType,playerType);
        wrapper.eq(Objects.nonNull(subsection),BasePlayerLog::getSubsection,subsection);
        wrapper.eq(BasePlayerLog::getIsDeleted,0);
        return selectList(wrapper);
    }

    default List<BasePlayerLog> selectByTournamentIdAndPlayerIdAndSubsection(Long tournamentId, Long playerId, Integer subsection){
        LambdaQueryWrapper<BasePlayerLog> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(BasePlayerLog::getTournamentId,tournamentId);
        wrapper.eq(BasePlayerLog::getBasketballPlayerId,playerId);
        wrapper.eq(Objects.nonNull(subsection),BasePlayerLog::getSubsection,subsection);
        wrapper.eq(BasePlayerLog::getIsDeleted,0);
        return selectList(wrapper);
    }
}
