package com.ruoyi.system.domain.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author Wangbj
 * @date 2024年05月07日 20:51
 */
@Data
@Accessors(chain = true)
public class BaseMsgReplyVO {

    @Schema(name = "name", description = "回复昵称")
    private String name;

    @Schema(name = "msg", description = "回复内容")
    private String msg;

    @Schema(name = "replyTime", description = "回复时间")
    private Date replyTime;

    @Schema(name = "msgId", description = "主留言id")
    private Long msgId;


}
