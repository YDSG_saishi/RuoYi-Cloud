package com.ruoyi.system.domain.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class BasketBallPlayerInviteVO {

    /**
     * 球队id
     */
    private Long teamId;

    /**
     * 唯一标识符
     */
    private String code;

}
