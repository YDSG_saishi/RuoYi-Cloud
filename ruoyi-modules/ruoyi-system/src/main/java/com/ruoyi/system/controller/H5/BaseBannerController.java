package com.ruoyi.system.controller.H5;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.core.web.page.WebTableDataInfo;
import com.ruoyi.system.domain.BaseBanner;
import com.ruoyi.system.domain.dto.Web.BannerDto;
import com.ruoyi.system.service.IBaseBannerService;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * h5赛事推荐
 */
@ApiOperation("联赛")
@RestController
@RequestMapping("/h5/banner")
public class BaseBannerController extends BaseController
{
    @Autowired
    private IBaseBannerService baseBannerService;


    /**
     * 查询banner列表
     */
    @GetMapping("/list")
    @Operation(summary = "查询banner列表", description = "查询banner列表")
    public TableDataInfo list(BannerDto dto)
    {
        startPage();
        List<BaseBanner> list = baseBannerService.selectList(dto);
        return getDataTable(list);
    }

}
