package com.ruoyi.system.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.io.IOException;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.system.domain.LiveStreamCode;
import com.ruoyi.system.service.impl.LiveService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.annotations.ApiOperation;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.domain.BaseLiveTournament;
import com.ruoyi.system.service.IBaseLiveTournamentService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;

import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 比赛直播房间Controller
 * 
 * @author ruoyi
 * @date 2024-05-10
 */
@Slf4j
@ApiOperation("比赛直播房间")
@RestController
@RequestMapping("/live/tournament")
public class BaseLiveTournamentController extends BaseController {
    @Resource
    private IBaseLiveTournamentService baseLiveTournamentService;


    /**
     * 查询比赛直播房间列表
     */
    @RequiresPermissions("system:tournament:list")
    @GetMapping("/list")
    @Operation(summary = "查询比赛直播房间列表", description = "查询比赛直播房间列表")
    public TableDataInfo list(BaseLiveTournament baseLiveTournament) {
        startPage();
        List<BaseLiveTournament> list = baseLiveTournamentService.selectBaseLiveTournamentList(baseLiveTournament);
        return getDataTable(list);
    }

    /**
     * 导出比赛直播房间列表
     */
    @Operation(summary = "导出比赛直播房间列表", description = "导出比赛直播房间列表")
    @RequiresPermissions("system:tournament:export")
    @Log(title = "比赛直播房间", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseLiveTournament baseLiveTournament) {
        List<BaseLiveTournament> list = baseLiveTournamentService.selectBaseLiveTournamentList(baseLiveTournament);
        ExcelUtil<BaseLiveTournament> util = new ExcelUtil<BaseLiveTournament>(BaseLiveTournament.class);
        util.exportExcel(response, list, "比赛直播房间数据");
    }

    /**
     * 获取比赛直播房间详细信息
     */
    @Operation(summary = "获取比赛直播房间详细信息", description = "获取比赛直播房间详细信息")
    @RequiresPermissions("system:tournament:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(baseLiveTournamentService.selectBaseLiveTournamentById(id));
    }

    /**
     * 新增或修改比赛直播房间
     */
    @Operation(summary = "新增或修改比赛直播房间", description = "新增或修改比赛直播房间")
    @Log(title = "比赛直播房间", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseLiveTournament baseLiveTournament) {
        return toAjax(baseLiveTournamentService.insertBaseLiveTournament(baseLiveTournament));
    }


    /**
     * 开始直播
     */
    @Operation(summary = "开始直播", description = "开始直播")
    @GetMapping("beginLive")
    public LiveStreamCode beginLive(@RequestParam Long tournamentId,@RequestParam String endTime,Integer type) {
        log.info("获取开始直播参数-->比赛id->{},时间->{},type->{}",tournamentId,endTime,type);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return baseLiveTournamentService.beginLive(tournamentId, LocalDateTime.parse(endTime, formatter),type);
    }

    /**
     * 关闭直播
     */
    @Operation(summary = "关闭直播", description = "关闭直播 streamName房间号")
    @GetMapping("endLive")
    public AjaxResult endLive(@RequestParam String streamName) {
        return toAjax(baseLiveTournamentService.endLive(streamName));
    }

    /**
     * 比赛观看人数新增
     */
    @GetMapping("/liveNum")
    @Operation(summary = "比赛观看人数新增", description = "比赛观看人数新增")
    public AjaxResult liveNum(Long tournamentId) {
        baseLiveTournamentService.liveNum(tournamentId);
        return success();
    }
}


