package com.ruoyi.system.domain.dto.Web;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.sql.Date;

@Data
public class BannerDto {

    @Schema(name = "contestId", description = "联赛id")
    private Long contestId;

    @Schema(name = "title", description = "标题")
    private String title;
}
