package com.ruoyi.system.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.BaseTournament;
import org.apache.ibatis.annotations.Param;

/**
 * 赛事Mapper接口
 * 
 * @author wangbj
 * @date 2024-04-16
 */
public interface BaseTournamentMapper extends BaseMapper<BaseTournament>
{
    /**
     * 查询赛事
     * 
     * @param id 赛事主键
     * @return 赛事
     */
    public BaseTournament selectBaseTournamentById(Long id);

    /**
     * 查询赛事列表
     * 
     * @param baseTournament 赛事
     * @return 赛事集合
     */
    public List<BaseTournament> selectBaseTournamentList(BaseTournament baseTournament);

    /**
     * 新增赛事
     * 
     * @param baseTournament 赛事
     * @return 结果
     */
    public int insertBaseTournament(BaseTournament baseTournament);

    /**
     * 修改赛事
     * 
     * @param baseTournament 赛事
     * @return 结果
     */
    public int updateBaseTournament(BaseTournament baseTournament);

    /**
     * 删除赛事
     * 
     * @param id 赛事主键
     * @return 结果
     */
    public int deleteBaseTournamentById(Long id);

    /**
     * 批量删除赛事
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseTournamentByIds(Long[] ids);

    /**
     * 获取时间到了 但是比赛状态仍旧是未开始的
     * @return
     */
    List<BaseTournament> getListByTime();

    List<BaseTournament> selectBaseTournamentListTeam(@Param("contestId") Long contestId,
                                                      @Param("state") int state,
                                                      @Param("teamId") Long teamId);

    /**
     * 获取当前联赛 已开始或已结束的比赛
     * @param contestId
     * @return
     */
    default Long selectBeginState(Long contestId){
        LambdaQueryWrapper<BaseTournament> wrapper = new LambdaQueryWrapper<>();
        wrapper.in(BaseTournament::getState,1,2);
        wrapper.eq(BaseTournament::getContestId,contestId);
        return selectCount(wrapper);
    }
}
