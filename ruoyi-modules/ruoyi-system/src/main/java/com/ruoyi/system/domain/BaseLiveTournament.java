package com.ruoyi.system.domain;

import java.time.LocalDateTime;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 比赛直播房间对象 base_live_tournament
 * 
 * @author ruoyi
 * @date 2024-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("base_live_tournament")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BaseLiveTournament extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @Schema(name = "id", description = "主键")
    @TableId(type = IdType.AUTO)
    private Long id;

    /** 比赛id */
    @Excel(name = "比赛id")
    @Schema(name = "tournamentId", description = "比赛id")
    private Long tournamentId;

    /** 直播房间号 */
    @Excel(name = "直播房间号")
    @Schema(name = "roomNumber", description = "直播房间号")
    private String roomNumber;

    /** 直播状态:0:未开始;1:直播中;2:已关播; */
    @Excel(name = "直播状态:0:未开始;1:直播中;2:已关播;")
    @Schema(name = "state", description = "直播状态:0:未开始;1:直播中;2:已关播;")
    private Integer state;

    /** 推流地址 */
    @Excel(name = "推流地址")
    @Schema(name = "pushUrl", description = "推流地址")
    private String pushUrl;

    /** 播放地址 */
    @Excel(name = "播放地址")
    @Schema(name = "viewUrl", description = "播放地址")
    private String viewUrl;

    /** 房间结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "房间结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    @Schema(name = "liveEndTime", description = "房间结束时间")
    private LocalDateTime liveEndTime;

    /** 回放地址 */
    @Excel(name = "回放地址")
    @Schema(name = "playbackUrl", description = "回放地址")
    private String playbackUrl;

    /** 创建人id */
    @Excel(name = "创建人id")
    @Schema(name = "createUserId", description = "创建人id")
    private Long createUserId;

    /** 创建人姓名 */
    @Excel(name = "创建人姓名")
    @Schema(name = "createUserName", description = "创建人姓名")
    private String createUserName;

    /** 修改人id */
    @Excel(name = "修改人id")
    @Schema(name = "updateUserId", description = "修改人id")
    private Long updateUserId;

    /** 修改人姓名 */
    @Excel(name = "修改人姓名")
    @Schema(name = "updateUserName", description = "修改人姓名")
    private String updateUserName;

    /** 是否已删除 0:未删除;1:已删除 */
    @Excel(name = "是否已删除 0:未删除;1:已删除")
    @Schema(name = "isDeleted", description = "是否已删除 0:未删除;1:已删除")
    private Long isDeleted;

    private Integer type =0;
    /**
     * 水印id
     */
    private Long watermarkId;
}
