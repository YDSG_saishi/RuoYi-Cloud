package com.ruoyi.system.service.impl;

import java.sql.Wrapper;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.system.mapper.BaseAttendanceLogMapper;
import com.ruoyi.system.domain.BaseAttendanceLog;
import com.ruoyi.system.service.IBaseAttendanceLogService;

import javax.annotation.Resource;

/**
 * 球员出勤记录Service业务层处理
 * 
 * @author wangbj
 * @date 2024-04-16
 */
@Service
public class BaseAttendanceLogServiceImpl extends ServiceImpl<BaseAttendanceLogMapper, BaseAttendanceLog> implements IBaseAttendanceLogService
{
    @Resource
    private BaseAttendanceLogMapper baseAttendanceLogMapper;

    @Resource
    LoginUserSetUtil loginUserSetUtil;

    /**
     * 查询球员出勤记录
     * 
     * @param id 球员出勤记录主键
     * @return 球员出勤记录
     */
    @Override
    public BaseAttendanceLog selectBaseAttendanceLogById(Long id)
    {
        return baseAttendanceLogMapper.selectBaseAttendanceLogById(id);
    }

    /**
     * 查询球员出勤记录列表
     * 
     * @param baseAttendanceLog 球员出勤记录
     * @return 球员出勤记录
     */
    @Override
    public List<BaseAttendanceLog> selectBaseAttendanceLogList(BaseAttendanceLog baseAttendanceLog)
    {
        return baseAttendanceLogMapper.selectBaseAttendanceLogList(baseAttendanceLog);
    }

    /**
     * 新增球员出勤记录
     *
     * @param baseAttendanceLog 球员出勤记录
     * @return 结果
     */
    @Override
    public boolean insertBaseAttendanceLog(List<BaseAttendanceLog> baseAttendanceLog)
    {
        if (CollUtil.isEmpty(baseAttendanceLog)){
            throw new RuntimeException("首发人员不能为空");
        }
        Long tournamentId = baseAttendanceLog.get(0).getTournamentId();
        if(ObjectUtil.isNull(tournamentId)){
            throw new RuntimeException("赛事id不能为空");
        }

        List<BaseAttendanceLog> list = this.list(Wrappers.<BaseAttendanceLog>lambdaQuery().eq(BaseAttendanceLog::getTournamentId, tournamentId));

        baseAttendanceLog.forEach(vo ->{
            //如果此比赛已经存在了 对应队员 则修改就行
            list.forEach(key ->{
                if (Objects.equals(key.getBasketballPlayerId(),vo.getBasketballPlayerId())){
                    vo.setId(key.getId());
                }
            });
            //新增时间数据库有默认
//            vo.setCreateTime(DateUtils.getNowDate());
            vo.setTournamentId(tournamentId);
            loginUserSetUtil.populateFields(vo,1);
        });

        return this.saveOrUpdateBatch(baseAttendanceLog);
    }

    /**
     * 修改球员出勤记录
     *
     * @param baseAttendanceLog 球员出勤记录
     * @return 结果
     */
    @Override
    public boolean updateBaseAttendanceLog(List<BaseAttendanceLog> baseAttendanceLog)
    {
        if (CollUtil.isEmpty(baseAttendanceLog)){
            return Boolean.FALSE;
        }
        baseAttendanceLog.forEach(vo ->{

            loginUserSetUtil.populateFields(baseAttendanceLog,2);
        });
        return this.updateBatchById(baseAttendanceLog);
    }

    /**
     * 批量删除球员出勤记录
     * 
     * @param ids 需要删除的球员出勤记录主键
     * @return 结果
     */
    @Override
    public int deleteBaseAttendanceLogByIds(Long[] ids)
    {
        return baseAttendanceLogMapper.deleteBaseAttendanceLogByIds(ids);
    }

    /**
     * 删除球员出勤记录信息
     * 
     * @param id 球员出勤记录主键
     * @return 结果
     */
    @Override
    public int deleteBaseAttendanceLogById(Long id)
    {
        return baseAttendanceLogMapper.deleteBaseAttendanceLogById(id);
    }

    @Override
    public void saveList(List<BaseAttendanceLog> voListLog, Long tournamentId, Long teamId) {
        List<BaseAttendanceLog> list = this.list(Wrappers.<BaseAttendanceLog>lambdaQuery()
                .eq(BaseAttendanceLog::getTournamentId, tournamentId)
                .eq(BaseAttendanceLog::getBasketballTeamId, teamId)
        );
        if (CollUtil.isNotEmpty(list)){
            Set<Long> collect = list.stream().map(BaseAttendanceLog::getBasketballPlayerId).collect(Collectors.toSet());
            voListLog = voListLog.stream().filter(vo -> !collect.contains(vo.getBasketballPlayerId())).collect(Collectors.toList());
        }
        this.saveBatch(voListLog);
    }
}
