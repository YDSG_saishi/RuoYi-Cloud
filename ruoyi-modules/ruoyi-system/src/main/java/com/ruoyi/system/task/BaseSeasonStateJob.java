package com.ruoyi.system.task;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.ruoyi.system.domain.BaseSeason;
import com.ruoyi.system.domain.BaseTournament;
import com.ruoyi.system.mapper.BaseSeasonMapper;
import com.ruoyi.system.mapper.BaseTournamentMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Wangbj
 * @date 2024年04月22日 21:16
 */
@Component
@Slf4j
public class BaseSeasonStateJob {

    @Resource
    BaseSeasonMapper baseMapper;

//    @Scheduled(cron = "0 0/1 * * * ?")
    private void task() {
        //拿到所有时间到了，状态还是未开始的数据
        List<BaseSeason> list = baseMapper.getListByTime();
        log.info("赛季状态修正定时器开始执行，修正【{}】个",list.size());

        //后续可能会推送当前比赛开始了等等。。。


        if (CollectionUtil.isNotEmpty(list)) {
            list.forEach(item -> {
                LambdaUpdateWrapper<BaseSeason> updateWrapper = new LambdaUpdateWrapper<>();
                updateWrapper.eq(BaseSeason::getId, item.getId());
                updateWrapper.set(BaseSeason::getType, 1);
                updateWrapper.set(BaseSeason::getUpdateTime, DateUtil.date());
                baseMapper.update(null, updateWrapper);
            });
        }
        log.info("赛季状态修正定时器结束。。。。");
    }

}
