package com.ruoyi.system.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Wrapper;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.csp.sentinel.util.AssertUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.common.core.constant.CacheConstants;
import com.ruoyi.common.core.enums.PlayerLogType;
import com.ruoyi.common.core.enums.TournamentType;
import com.ruoyi.common.core.utils.CommonStreamUtil;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.redis.service.RedisService;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.system.api.domain.SysUser;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.domain.dto.H5.KeyFiguresDto;
import com.ruoyi.system.domain.dto.H5.PlayerPKDto;
import com.ruoyi.system.domain.dto.Web.BasketBallPlayerDto;
import com.ruoyi.system.domain.vo.*;
import com.ruoyi.system.domain.vo.H5.AverageComparisonVO;
import com.ruoyi.system.domain.vo.H5.KeyFiguresVO;
import com.ruoyi.system.domain.vo.H5.PlayerPKVO;
import com.ruoyi.system.domain.vo.H5.ScoreMovementVO;
import com.ruoyi.system.mapper.BaseContestMapper;
import com.ruoyi.system.mapper.BaseTournamentMapper;
import com.ruoyi.system.mapper.BasketballPlayerAndTeamMapper;
import com.ruoyi.system.service.*;
import io.swagger.models.auth.In;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.formula.functions.T;
import org.jacoco.agent.rt.internal_43f5073.core.internal.flow.IFrame;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.system.mapper.BasePlayerLogMapper;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * 球员得分现记录Service业务层处理
 *
 * @author wangbj
 * @date 2024-04-16
 */
@Slf4j
@Service
public class BasePlayerLogServiceImpl extends ServiceImpl<BasePlayerLogMapper, BasePlayerLog> implements IBasePlayerLogService {
    @Resource
    private BasePlayerLogMapper basePlayerLogMapper;

    @Resource
    LoginUserSetUtil loginUserSetUtil;

    @Resource
    BaseTournamentMapper baseTournamentMapper;

    @Resource
    private IBaseBasketballPlayerService iBaseBasketballPlayerService;
    @Resource
    private IBaseBasketballTeamService iBaseBasketballTeamService;
    @Resource
    private IBaseAttendanceLogService iBaseAttendanceLogService;
    @Resource
    private IBaseContestService iBaseContestService;
    @Resource
    private IBaseTournamentService iBaseTournamentService;
    @Resource
    private IBaseLivePlayBackEvenService iBaseLivePlayBackEvenService;
    @Resource
    private RedisService redisService;
    @Resource
    private BasketballPlayerAndTeamMapper basketballPlayerAndTeamMapper;
    @Resource
    private LiveService liveService;
    @Resource
    private IBaseLiveTournamentService baseLiveTournamentService;
    @Resource
    private BaseContestMapper baseContestMapper;


    private static Map<Long, Long> scoreMap = new HashMap<>();

    static {
        //二分
        scoreMap.put(PlayerLogType.TWO_POINT_HIT.getCode(), 2L);
        //三分
        scoreMap.put(PlayerLogType.THREE_POINT_HIT.getCode(), 3L);
        //一分
        scoreMap.put(PlayerLogType.ONE_POINT_HIT.getCode(), 1L);
        //罚球
        scoreMap.put(PlayerLogType.POINT_HIT.getCode(), 1L);
    }

    private static Map<Long, String> imgMap = new HashMap<>();
    static {
        imgMap.put(PlayerLogType.ONE_POINT_HIT.getCode(),PlayerLogType.ONE_POINT_HIT.getInfo());
        imgMap.put(PlayerLogType.TWO_POINT_HIT.getCode(),PlayerLogType.TWO_POINT_HIT.getInfo());
        imgMap.put(PlayerLogType.THREE_POINT_HIT.getCode(),PlayerLogType.THREE_POINT_HIT.getInfo());
        imgMap.put(PlayerLogType.POINT_HIT.getCode(),PlayerLogType.POINT_HIT.getInfo());
        imgMap.put(PlayerLogType.STRAL.getCode(),PlayerLogType.STRAL.getInfo());
        imgMap.put(PlayerLogType.BLOCK_SHOT.getCode(),PlayerLogType.BLOCK_SHOT.getInfo());
        imgMap.put(PlayerLogType.ASSIST.getCode(),PlayerLogType.ASSIST.getInfo());
    }

    private static List<Long> getImgList = new ArrayList<>();
    static {
        //二分
        getImgList.add(PlayerLogType.BACKBOARD.getCode());
        getImgList.add(PlayerLogType.ASSIST.getCode());
        getImgList.add(PlayerLogType.STRAL.getCode());
        getImgList.add(PlayerLogType.BLOCK_SHOT.getCode());
        getImgList.add(PlayerLogType.TWO_POINT_HIT.getCode());
//        getImgList.add(PlayerLogType.THREE_POINT_HIT.getCode());
        getImgList.add(PlayerLogType.POINT_HIT.getCode());
        getImgList.add(PlayerLogType.FOUL.getCode());
//        getImgList.add(PlayerLogType.ONE_POINT_HIT.getCode());

    }
    /**
     * 查询球员得分现记录
     *
     * @param id 球员得分现记录主键
     * @return 球员得分现记录
     */
    @Override
    public BasePlayerLog selectBasePlayerLogById(Long id) {
        return basePlayerLogMapper.selectBasePlayerLogById(id);
    }

    /**
     * 查询球员得分现记录列表
     *
     * @param basePlayerLog 球员得分现记录
     * @return 球员得分现记录
     */
    @Override
    public List<BasePlayerLogVO> selectBasePlayerLogList(BasePlayerLog basePlayerLog) {
        List<BasePlayerLogVO> basePlayerLogs = basePlayerLogMapper.selectBasePlayerLogList(basePlayerLog);
        basePlayerLogs.forEach(vo -> {
            vo.setTypeStr(PlayerLogType.getValue(vo.getType()));
        });
        return basePlayerLogs;
    }

    /**
     * 新增球员得分现记录
     *
     * @param basePlayerLog 球员得分现记录
     * @return 结果
     */
    @Override
    public int insertBasePlayerLog(BasePlayerLog basePlayerLog) {
        log.info("进入新增得分~");
        BasePlayerLog byId = this.getById(basePlayerLog.getId());
        if (Objects.nonNull(byId)){
            return 1;
        }
        basePlayerLog.setCreateTime(DateUtils.getNowDate());
        Long aLong = scoreMap.get(basePlayerLog.getType());
        if (Objects.nonNull(aLong) && aLong != 0L) {
            log.info("新增得分"+aLong+"分");
            basePlayerLog.setScore(aLong);
            basePlayerLog.setIsScore(0L);
            BaseTournament baseTournament = baseTournamentMapper.selectById(basePlayerLog.getTournamentId());
            AssertUtil.notNull(basePlayerLog, "没有获取到当前比赛");
            //更新分数
            if (Objects.equals(baseTournament.getTeamOneId(), basePlayerLog.getBasketballTeamId())) {
                baseTournament.setTeamOneScore(baseTournament.getTeamOneScore() + aLong);
            } else if (Objects.equals(baseTournament.getTeamTwoId(), basePlayerLog.getBasketballTeamId())) {
                baseTournament.setTeamTwoScore(baseTournament.getTeamTwoScore() + aLong);
            }
            //更新最新领先分数
            if(baseTournament.getTeamOneScore() > baseTournament.getTeamTwoScore()){
                long maxScore = baseTournament.getTeamOneScore() - baseTournament.getTeamTwoScore();
                baseTournament.setTeamOneScoreMax(baseTournament.getTeamOneScoreMax() > maxScore ? baseTournament.getTeamOneScoreMax() : maxScore);
            }else {
                long maxScore = baseTournament.getTeamTwoScore() - baseTournament.getTeamOneScore();
                baseTournament.setTeamTwoScoreMax(baseTournament.getTeamTwoScoreMax() > maxScore ? baseTournament.getTeamTwoScoreMax() : maxScore);
            }
            baseTournamentMapper.updateById(baseTournament);
        }
        loginUserSetUtil.populateFields(basePlayerLog, 1);

        if(Objects.equals(basePlayerLog.getType(),PlayerLogType.TEAM_FOUL.getCode())){
            basePlayerLog.setBasketballPlayerId(null);
        }
        //记录精彩画面
        String strName = imgMap.get(basePlayerLog.getType());

        boolean b = false;
        try {
             b = this.saveOrUpdate(basePlayerLog);
        }catch (Exception e){
           log.info("当前修改插入失败->>"+JSONObject.toJSONString(basePlayerLog));
        }
        //修改水印 再次混流
        if (b){
            updateLiveWatermark(basePlayerLog.getTournamentId());
        }
        iBaseLivePlayBackEvenService.saveStrName(strName,basePlayerLog.getTournamentId(),basePlayerLog.getBasketballPlayerId(),basePlayerLog.getId());
        return  b ? 1:0;
    }
    @Override
    public void updateLiveWatermark(Long tournamentId) {
        BaseTournament tournament = iBaseTournamentService.selectBaseTournamentById(tournamentId);
        if (Objects.equals(tournament.getEquipmentSwitch(),1)){
            BaseLiveTournament liveTournament = baseLiveTournamentService.selectByTournamentId(tournamentId);
            if (Objects.nonNull(liveTournament) && !Objects.equals(liveTournament.getWatermarkId(),0L)){
                BaseContest contest = baseContestMapper.selectById(tournament.getContestId());
                String subsection;
                if (Objects.equals(tournament.getSubsection(),1L)){
                    subsection = "1st";
                }else if (Objects.equals(tournament.getSubsection(),2L)){
                    subsection = "2nd";
                }else if (Objects.equals(tournament.getSubsection(),3L)){
                    subsection = "3rd";
                }else{
                    subsection = tournament.getSubsection()+"th";
                }

                liveService.updateLiveWatermark(liveTournament.getWatermarkId(),tournament.getTeamOneName(),tournament.getTeamOneScore(),subsection,
                        tournament.getTeamTwoScore(),tournament.getTeamTwoName(),contest.getContestName(),tournament.getOneFoul(),tournament.getTwoFoul());
                liveService.createCommonMixStream(liveTournament.getRoomNumber(),liveTournament.getWatermarkId(),contest.getWatermarkId());
            }
        }
    }


    /**
     * 修改球员得分现记录
     *
     * @param basePlayerLog 球员得分现记录
     * @return 结果
     */
    @Override
    public int updateBasePlayerLog(BasePlayerLog basePlayerLog) {
        basePlayerLog.setUpdateTime(DateUtils.getNowDate());
        Long aLong = 0L;
        if (Objects.nonNull(basePlayerLog.getType())){
            aLong = scoreMap.get(basePlayerLog.getType());
            aLong = Objects.isNull(aLong)?0:aLong;
            basePlayerLog.setScore(aLong);
            basePlayerLog.setIsScore(aLong==0?1L:0L);
        }

        if (Objects.nonNull(basePlayerLog.getType())) {
            BasePlayerLog basePlayerLog1 = basePlayerLogMapper.selectById(basePlayerLog.getId());
            Long score = basePlayerLog1.getScore();
            BaseTournament baseTournament = baseTournamentMapper.selectById(basePlayerLog1.getTournamentId());
            AssertUtil.notNull(basePlayerLog, "没有获取到当前比赛");
            if (Objects.equals(baseTournament.getTeamOneId(), basePlayerLog1.getBasketballTeamId())) {
                baseTournament.setTeamOneScore(baseTournament.getTeamOneScore() + aLong - score);
            } else if (Objects.equals(baseTournament.getTeamTwoId(), basePlayerLog1.getBasketballTeamId())) {
                baseTournament.setTeamTwoScore(baseTournament.getTeamTwoScore() + aLong - score);
            }
            baseTournamentMapper.updateById(baseTournament);
        }

        loginUserSetUtil.populateFields(basePlayerLog, 2);
        if(Objects.equals(basePlayerLog.getType(),PlayerLogType.TEAM_FOUL.getCode())){
            basePlayerLog.setBasketballPlayerId(null);
        }
        if (Objects.nonNull(basePlayerLog.getBasketballPlayerId())){
            iBaseLivePlayBackEvenService.updatePlayId(basePlayerLog.getBasketballPlayerId(),basePlayerLog.getId());
        }
        return basePlayerLogMapper.updateBasePlayerLog(basePlayerLog);
    }

    /**
     * 批量删除球员得分现记录
     *
     * @param ids 需要删除的球员得分现记录主键
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteBasePlayerLogByIds(Long[] ids) {
        if (ids.length != 1) {
            throw new RuntimeException("不可批量删除");
        }
        Long id = ids[0];
        BasePlayerLog vo = basePlayerLogMapper.selectById(id);
        if (Objects.isNull(vo)) {
            return 0;
        }
        Long score = vo.getScore();
        if (0L < score){
            BaseTournament baseTournament = baseTournamentMapper.selectById(vo.getTournamentId());
            AssertUtil.notNull(baseTournament, "没有获取到当前比赛");
            if (Objects.equals(baseTournament.getTeamOneId(), vo.getBasketballTeamId())) {
                baseTournament.setTeamOneScore(baseTournament.getTeamOneScore() - score);
            } else if (Objects.equals(baseTournament.getTeamTwoId(), vo.getBasketballTeamId())) {
                baseTournament.setTeamTwoScore(baseTournament.getTeamTwoScore() - score);
            }
            baseTournamentMapper.updateById(baseTournament);

        }
        iBaseLivePlayBackEvenService.deleteByPlayerLogId(id);
        int i = basePlayerLogMapper.deleteBasePlayerLogByIds(ids);
        if (i>0){
            List<Integer> listLong = redisService.getCacheList(CacheConstants.BASE_PLAYER_LOG_ID + vo.getTournamentId());
            List<Long> cacheList = JSONArray.parseArray(listLong.toString(),Long.class);
            if (!cacheList.contains(id)){
                cacheList.add(id);
                redisService.setCacheList(CacheConstants.BASE_PLAYER_LOG_ID+vo.getTournamentId(),cacheList);
            }
            //修改水印 再次混流
            updateLiveWatermark(vo.getTournamentId());
        }
        return i;
    }

    /**
     * 删除球员得分现记录信息
     *
     * @param id 球员得分现记录主键
     * @return 结果
     */
    @Override
    public int deleteBasePlayerLogById(Long id) {
        return basePlayerLogMapper.deleteBasePlayerLogById(id);
    }

    /**
     * 获得比分
     *
     * @param id 比赛id
     * @return
     */
    @Override
    public List<BasePlayerLogScoreResponseVO> getScore(Long id, Long oneTeamId, Long twoTeamId, Long subsection) {
        subsection = Objects.isNull(subsection) ? 1L:subsection;
        List<BasePlayerLog> list = this.list(Wrappers.<BasePlayerLog>lambdaQuery()
                .eq(BasePlayerLog::getTournamentId, id).eq(BasePlayerLog::getIsDeleted, 0));

        List<BasePlayerLogScoreResponseVO> basePlayerLogScoreResponseVOList = new ArrayList<>();

        getScoreTeam(oneTeamId, subsection, basePlayerLogScoreResponseVOList, list);
        getScoreTeam(twoTeamId, subsection, basePlayerLogScoreResponseVOList, list);
        basePlayerLogScoreResponseVOList.stream().forEach(vo -> vo.setTournamentId(id));
        return basePlayerLogScoreResponseVOList;
    }

    private void getScoreTeam(Long teamId, Long subsection,
                              List<BasePlayerLogScoreResponseVO> basePlayerLogScoreResponseVOList, List<BasePlayerLog> list) {
        List<BasePlayerLog> teamList = list.stream().filter(vo -> Objects.equals(vo.getBasketballTeamId(), teamId)).collect(Collectors.toList());
        Long teamScore = teamList.stream().map(BasePlayerLog::getScore).reduce(0L, Long::sum);

        long countFoul = 0;
        long countStop = 0;
        for (BasePlayerLog vo:teamList) {
            if ((Objects.equals(vo.getType(), PlayerLogType.FOUL.getCode()) ||Objects.equals(vo.getType(), PlayerLogType.TEAM_FOUL.getCode()))){
                if (subsection > 3L ? vo.getSubsection() > 3L : Objects.equals(vo.getSubsection(), subsection)){
                    countFoul ++;
                }
            }
            if (vo.getType().equals(PlayerLogType.STOP.getCode())){
                if (subsection > 2L ? vo.getSubsection() > 2L : vo.getSubsection() <= 2L){
                    countStop++;
                }
            }
        }
        Map<Long, Long> onePlayerNum = teamList.stream().filter((vo -> Objects.equals(vo.getType(), PlayerLogType.FOUL.getCode())))
                .collect(Collectors.groupingBy(BasePlayerLog::getBasketballPlayerId,
                        Collectors.mapping(BasePlayerLog::getType, Collectors.counting())));
        BasePlayerLogScoreResponseVO basePlayerLogScoreResponseVO1
                = new BasePlayerLogScoreResponseVO()
                .setTeamId(teamId)
                .setTeamScore(teamScore)
                .setCountFoul(countFoul)
                .setCountStop(countStop)
                .setPlayerNum(onePlayerNum);
        basePlayerLogScoreResponseVOList.add(basePlayerLogScoreResponseVO1);

    }

//    /**
//     * 持球时间
//     *
//     * @param tournamentId
//     * @param oneTeamId
//     * @param twoTeamId
//     * @return
//     */
//    @Override
//    public BaseBasketballTimeVO getBasketballTime(Long tournamentId, Long oneTeamId, Long twoTeamId) {
//        BaseTournament tournament = baseTournamentMapper.selectBaseTournamentById(tournamentId);
//
//        //比赛开始时间
//        Date tournamentBeginTime = tournament.getTournamentBeginTime();
//
//        //TODO 开始时间到 下次对方队伍的抢断、篮板。或者本方队伍 二分命中 三分命中视为结束球权
//
//
//        return null;
//    }

    @Override
    public Map<String, Object> realTimeData(BasePlayerLog basePlayerLog) {
        AssertUtil.notNull(basePlayerLog.getTournamentId(), "赛事id不能为空");
        Map<String, Object> mapObject = new HashMap<>();
        BaseTournament byId1 = iBaseTournamentService.getById(basePlayerLog.getTournamentId());
        Set<Long> longs1 = new HashSet<>();
        longs1.add(byId1.getTeamTwoId());
        longs1.add(byId1.getTeamOneId());
        //获取当前比赛的事件
        List<BasePlayerLog> list = this.list(Wrappers.<BasePlayerLog>lambdaQuery()
                .eq(BasePlayerLog::getTournamentId, basePlayerLog.getTournamentId())
                        .ne(BasePlayerLog::getTime,"18")
                .eq(BasePlayerLog::getIsDeleted, 0));
        if (CollUtil.isEmpty(list)) {
            return mapObject;
        }
        BaseTournament byId = iBaseTournamentService.getById(basePlayerLog.getTournamentId());
        AssertUtil.notNull(byId,"未找到当前赛事");
        //队伍一 小节总分
        Map<Long, List<BasePlayerLog>> collect = list.stream().collect(Collectors.groupingBy(BasePlayerLog::getBasketballTeamId));
        List<BaseBasketballTeam> baseBasketballTeams = iBaseBasketballTeamService.listByIds(longs1);
        Map<Long, String> baseBasketballTeamsMap = baseBasketballTeams.stream().collect(Collectors.toMap(BaseBasketballTeam::getId, BaseBasketballTeam::getTeamName, (k, v) -> k));

        List<Map<String, Object>> mapList = new ArrayList<>();
        for (Long key : longs1) {
            List<BasePlayerLog> basePlayerLogs = collect.get(key);
            List<BasePlayerLog> value = CollUtil.isEmpty(basePlayerLogs)?new ArrayList<>():basePlayerLogs;
            Map<Long, Long> teamSubsectionScoreSum = new HashMap<>();

            for (Long i = 1L; i <= byId.getSubsection(); i++) {
                //单个队伍的小节分值
                Long finalI = i;
                Long reduce = value.stream().filter(vo -> Objects.equals(vo.getSubsection(), finalI)).map(BasePlayerLog::getScore).reduce(0L, Long::sum);
                teamSubsectionScoreSum.put(i, reduce);
            }
            //队伍总数
            long teamSubsectionScoreSumTotal = teamSubsectionScoreSum.values().stream().mapToLong(Long::longValue).sum();

            Map<String, Object> map = new HashMap<>();
            map.put("teamSubsectionScoreSum", teamSubsectionScoreSum);
            map.put("teamSubsectionScoreSumTotal", teamSubsectionScoreSumTotal);
            map.put("teamName", baseBasketballTeamsMap.get(key));
            map.put("teamId", key);
            mapList.add(map);
        }

        mapObject.put("mapList", mapList);
        return mapObject;
    }

    @Override
    public List<Map<String, Object>> realTimeDataTeamGetImg(BasePlayerLog basePlayerLog) {
        AssertUtil.notNull(basePlayerLog.getTournamentId(), "赛事id不能为空");
        AssertUtil.notNull(basePlayerLog.getBasketballTeamId(), "球队id不能为空");

        List<BaseAttendanceLog> baseAttendanceLogs = iBaseAttendanceLogService.selectBaseAttendanceLogList(
                BaseAttendanceLog.builder()
                        .tournamentId(basePlayerLog.getTournamentId())
                        .basketballTeamId(basePlayerLog.getBasketballTeamId())
                        .isAttendance(1L)
                        .build());
        if (CollUtil.isEmpty(baseAttendanceLogs)) {
            return new ArrayList<>();
        }
        List<Long> collect = baseAttendanceLogs.stream().map(BaseAttendanceLog::getBasketballPlayerId).collect(Collectors.toList());
        List<BasePlayerLog> list = this.list(Wrappers.<BasePlayerLog>lambdaQuery()
                .eq(BasePlayerLog::getTournamentId, basePlayerLog.getTournamentId())
                .eq(BasePlayerLog::getBasketballTeamId, basePlayerLog.getBasketballTeamId())
                .in(BasePlayerLog::getBasketballPlayerId, collect)
                .ne(BasePlayerLog::getType,PlayerLogType.OVER.getCode())
                .eq(BasePlayerLog::getIsDeleted, 0));

        //获取联赛
        BaseAttendanceLog existFirst = CommonStreamUtil.isExistFirst(baseAttendanceLogs);
        BaseContest contest = iBaseContestService.getById(existFirst.getContestId());

        List<BaseBasketballPlayer> baseBasketballPlayers = iBaseBasketballPlayerService.listByIds(collect);
        Map<Long, String> baseBasketballPlayersMap = baseBasketballPlayers.stream().collect(Collectors.toMap(BaseBasketballPlayer::getId, BaseBasketballPlayer::getName, (k, v) -> k));
        List<Map<String, Object>> playMapList = new ArrayList<>();
        Long reduceTotal = 0L;
        Map<Long,Integer> totalMap = new HashMap<>();
        for (BaseAttendanceLog key : baseAttendanceLogs) {
            Map<String, Object> playMap = new HashMap<>();

            List<Map<String, Object>> typeMapList = new ArrayList<>();

            //获取当前用户
            List<BasePlayerLog> filter = CommonStreamUtil.filter(list, vo -> Objects.equals(vo.getBasketballPlayerId(), key.getBasketballPlayerId()));

            for (PlayerLogType keyLog : PlayerLogType.values()) {
                if (keyLog.getCode().equals(PlayerLogType.STOP.getCode())) {
                    continue;
                }
                Map<Long, List<BasePlayerLog>> typeList = CommonStreamUtil.group(filter, BasePlayerLog::getType);

                List<BasePlayerLog> basePlayerLogs = typeList.get(keyLog.getCode());
                Map<String, Object> map = new HashMap<>();
                //类型名称
                int size = 0;
                map.put("typeStr", PlayerLogType.getValue(keyLog.getCode()));
                map.put("type", keyLog.getCode());
                if (CollUtil.isNotEmpty(basePlayerLogs)) {
                    size = basePlayerLogs.size();
                }
                //类型次数
                map.put("typeCount", size);

                typeMapList.add(map);
                playMap.put("typeMapList", typeMapList);
                //统计总计
                if (totalMap.containsKey(keyLog.getCode())){
                    Integer o = totalMap.get(keyLog.getCode());
                    o = o + size;
                    totalMap.put(keyLog.getCode(),o);
                }else{
                    totalMap.put(keyLog.getCode(), size);
                }
            }
            //总分
            Long reduce = filter.stream().map(BasePlayerLog::getScore).reduce(0L, Long::sum);
            //罚球
            long notPointHit = buildTypeName(filter, PlayerLogType.POINT_HIT.getCode(), PlayerLogType.NOT_POINT_HIT.getCode(), "罚球命中率", 90, typeMapList);
            long twoNotPointHit = buildTypeName(filter, PlayerLogType.TWO_POINT_HIT.getCode(), PlayerLogType.TWO_NOT_POINT_HIT.getCode(), "二分命中率", 92, typeMapList);

            //如果是3v3
            long threeNotPointHit;
            long threePointHit;
            if (Objects.nonNull(contest) && Objects.equals(contest.getScoringWay(),1L)){
                threeNotPointHit = buildTypeName(filter, PlayerLogType.ONE_POINT_HIT.getCode(), PlayerLogType.ONE_NOT_POINT_HIT.getCode(), "一分命中率", 91, typeMapList);
                threePointHit = CommonStreamUtil.filter(filter, o -> Objects.equals(o.getType(), PlayerLogType.ONE_POINT_HIT.getCode())).size();

            }else{
                threeNotPointHit = buildTypeName(filter, PlayerLogType.THREE_POINT_HIT.getCode(), PlayerLogType.THREE_NOT_POINT_HIT.getCode(), "三分命中率", 93, typeMapList);
                threePointHit = CommonStreamUtil.filter(filter, o -> Objects.equals(o.getType(), PlayerLogType.THREE_POINT_HIT.getCode())).size();
            }
           //球员效率值
            //篮板
            int backboard = CommonStreamUtil.filter(filter, o -> Objects.equals(o.getType(), PlayerLogType.BACKBOARD.getCode())).size();
            int assist = CommonStreamUtil.filter(filter, o -> Objects.equals(o.getType(), PlayerLogType.ASSIST.getCode())).size();
            int stral = CommonStreamUtil.filter(filter, o -> Objects.equals(o.getType(), PlayerLogType.STRAL.getCode())).size();
            int blockShot = CommonStreamUtil.filter(filter, o -> Objects.equals(o.getType(), PlayerLogType.BLOCK_SHOT.getCode())).size();
            //失误
            int mistake = CommonStreamUtil.filter(filter, o -> Objects.equals(o.getType(), PlayerLogType.MISTAKE.getCode())).size();

            // 总分+篮板+助攻+抢断+盖帽 - （二分不中+三分不中+罚球不中+失误）
            long per = (reduce + backboard + assist + stral + blockShot) - (twoNotPointHit + threeNotPointHit + notPointHit + mistake);
            Map<String, Object> pointHitMap = new HashMap<>();
            //罚球
            BigDecimal divide = BigDecimal.ZERO;
//            if (per > 0){
//                divide = BigDecimal.valueOf(per).divide(BigDecimal.valueOf(100),2, RoundingMode.HALF_UP);
//            }
            pointHitMap.put("typeStr", "单场效率值");
            pointHitMap.put("typeCount", per);
            pointHitMap.put("type",94);
            typeMapList.add(pointHitMap);

            //真实命中率
            //总分 / (2* (二分出手+三分出手+ (0.44*罚球出手) ))
            //二分命中
            long twoPointHit = CommonStreamUtil.filter(filter, o -> Objects.equals(o.getType(),PlayerLogType.TWO_POINT_HIT.getCode())).size();
            //罚球命中
            long pointHit = CommonStreamUtil.filter(filter, o -> Objects.equals(o.getType(), PlayerLogType.POINT_HIT.getCode())).size();
            //二分三分总出手
            long l = twoPointHit + twoNotPointHit + threeNotPointHit + threePointHit;
            //罚球总出手
            double v = 0.44 * (pointHit + notPointHit);
            double v1 = 2 * (l + v);
            Map<String, Object> map = new HashMap<>();
            BigDecimal divide1 = BigDecimal.ZERO;
            if(reduce > 0 && v1 > 0){
                 divide1 = BigDecimal.valueOf(reduce).divide(BigDecimal.valueOf(v1),2, RoundingMode.HALF_UP);
            }
            map.put("typeStr", "真实命中率");
            map.put("typeCount", divide1);
            map.put("type",95);
            typeMapList.add(map);

            playMap.put("typeMapList", typeMapList);

            playMap.put("typeScoreSum",reduce);

            playMap.put("name", baseBasketballPlayersMap.get(key.getBasketballPlayerId()));
            playMap.put("code", key.getPlayerNumber());
            playMap.put("basketballPlayerId", key.getBasketballPlayerId());
            playMapList.add(playMap);

            reduceTotal = reduceTotal+reduce;
        }
        //处理总计
        Map<String,Object> reduceMap = new HashMap<>();
        reduceMap.put("typeScoreSum",reduceTotal);
        reduceMap.put("name", "总计");
        List<Map<String, Object>> typeMapList = new ArrayList<>();
        for (Map.Entry<Long, Integer> keyLog:totalMap.entrySet()) {
            Map<String, Object> map = new HashMap<>();
            //类型名称
            map.put("typeStr", PlayerLogType.getValue(keyLog.getKey()));
            map.put("typeCount", keyLog.getValue());
            map.put("type", keyLog.getKey());
            typeMapList.add(map);
        }
        reduceMap.put("typeMapList", typeMapList);
        playMapList.add(reduceMap);
        return playMapList;
    }
    //罚球
    private static final Integer POINTHIT = 96;
    //二分
    private static final Integer TWO_POINTHIT = 97;
    //三分
    private static final Integer THREE_POINTHIT = 98;
    @Override
    public List<Map<String, Object>> realTimeDataTeam(BasePlayerLog basePlayerLog) {
        AssertUtil.notNull(basePlayerLog.getTournamentId(), "赛事id不能为空");
        AssertUtil.notNull(basePlayerLog.getBasketballTeamId(), "球队id不能为空");

        List<BaseAttendanceLog> baseAttendanceLogs = iBaseAttendanceLogService.selectBaseAttendanceLogList(
                BaseAttendanceLog.builder()
                        .tournamentId(basePlayerLog.getTournamentId())
                        .basketballTeamId(basePlayerLog.getBasketballTeamId())
                        .isAttendance(1L)
                        .build());
        if (CollUtil.isEmpty(baseAttendanceLogs)) {
            return new ArrayList<>();
        }
        List<Long> collect = baseAttendanceLogs.stream().map(BaseAttendanceLog::getBasketballPlayerId).collect(Collectors.toList());
        List<BasePlayerLog> list = this.list(Wrappers.<BasePlayerLog>lambdaQuery()
                .eq(BasePlayerLog::getTournamentId, basePlayerLog.getTournamentId())
                .eq(BasePlayerLog::getBasketballTeamId, basePlayerLog.getBasketballTeamId())
                .in(BasePlayerLog::getBasketballPlayerId, collect)
                .ne(BasePlayerLog::getType,PlayerLogType.OVER.getCode())
                .eq(BasePlayerLog::getIsDeleted, 0));

        //获取联赛
        BaseAttendanceLog existFirst = CommonStreamUtil.isExistFirst(baseAttendanceLogs);
        BaseContest contest = iBaseContestService.getById(existFirst.getContestId());

        List<BaseBasketballPlayer> baseBasketballPlayers = iBaseBasketballPlayerService.listByIds(collect);
        Map<Long, String> baseBasketballPlayersMap = baseBasketballPlayers.stream().collect(Collectors.toMap(BaseBasketballPlayer::getId, BaseBasketballPlayer::getName, (k, v) -> k));
        List<Map<String, Object>> playMapList = new ArrayList<>();
        Long reduceTotal = 0L;
        Map<Long,Integer> totalMap = new HashMap<>();

        long pointHitAggregate = 0;
        long pointHitTotalAggregate = 0;
        long twoPointHitAggregate = 0;
        long twoPointHitTotalAggregate = 0;
        long threePointHitAggregate = 0;
        long threePointHitTotalAggregate = 0;
        String name = "三分";
        
        for (BaseAttendanceLog key : baseAttendanceLogs) {
            Map<String, Object> playMap = new HashMap<>();

            List<Map<String, Object>> typeMapList = new ArrayList<>();

            //获取当前用户
            List<BasePlayerLog> filter = CommonStreamUtil.filter(list, vo -> Objects.equals(vo.getBasketballPlayerId(), key.getBasketballPlayerId()));

            for (PlayerLogType keyLog : PlayerLogType.values()) {
                if (keyLog.getCode().equals(PlayerLogType.STOP.getCode()) ) {
                    continue;
                }
                if (keyLog.getCode().equals(PlayerLogType.TWO_NOT_POINT_HIT.getCode()) || keyLog.getCode().equals(PlayerLogType.TWO_POINT_HIT.getCode())) {
                    continue;
                }
                if (keyLog.getCode().equals(PlayerLogType.THREE_NOT_POINT_HIT.getCode()) || keyLog.getCode().equals(PlayerLogType.THREE_POINT_HIT.getCode())) {
                    continue;
                }
                if (keyLog.getCode().equals(PlayerLogType.NOT_POINT_HIT.getCode()) || keyLog.getCode().equals(PlayerLogType.POINT_HIT.getCode())) {
                    continue;
                }
                if (keyLog.getCode().equals(PlayerLogType.START.getCode()) || keyLog.getCode().equals(PlayerLogType.TEAM_FOUL.getCode())) {
                    continue;
                }

                Map<Long, List<BasePlayerLog>> typeList = CommonStreamUtil.group(filter, BasePlayerLog::getType);

                List<BasePlayerLog> basePlayerLogs = typeList.get(keyLog.getCode());
                Map<String, Object> map = new HashMap<>();
                //类型名称
                int size = 0;
                map.put("typeStr", PlayerLogType.getValue(keyLog.getCode()));
                map.put("type", keyLog.getCode());
                if (CollUtil.isNotEmpty(basePlayerLogs)) {
                    size = basePlayerLogs.size();
                }
                //类型次数
                map.put("typeCount", size);

                typeMapList.add(map);
                playMap.put("typeMapList", typeMapList);
                //统计总计
                if (totalMap.containsKey(keyLog.getCode())){
                    Integer o = totalMap.get(keyLog.getCode());
                    o = o + size;
                    totalMap.put(keyLog.getCode(),o);
                }else{
                    totalMap.put(keyLog.getCode(), size);
                }

            }
            //总分
            Long reduce = filter.stream().map(BasePlayerLog::getScore).reduce(0L, Long::sum);
            //罚球
            long notPointHit = buildTypeName(filter, PlayerLogType.POINT_HIT.getCode(), PlayerLogType.NOT_POINT_HIT.getCode(), "罚球命中率", 90, typeMapList);
            long twoNotPointHit = buildTypeName(filter, PlayerLogType.TWO_POINT_HIT.getCode(), PlayerLogType.TWO_NOT_POINT_HIT.getCode(), "二分命中率", 92, typeMapList);

            //如果是3v3
            long threeNotPointHit;
            long threePointHit;
            if (Objects.nonNull(contest) && Objects.equals(contest.getScoringWay(),1L)){
                threeNotPointHit = buildTypeName(filter, PlayerLogType.ONE_POINT_HIT.getCode(), PlayerLogType.ONE_NOT_POINT_HIT.getCode(), "一分命中率", 91, typeMapList);
                threePointHit = CommonStreamUtil.filter(filter, o -> Objects.equals(o.getType(), PlayerLogType.ONE_POINT_HIT.getCode())).size();
                name = "一分";
            }else{
                threeNotPointHit = buildTypeName(filter, PlayerLogType.THREE_POINT_HIT.getCode(), PlayerLogType.THREE_NOT_POINT_HIT.getCode(), "三分命中率", 93, typeMapList);
                threePointHit = CommonStreamUtil.filter(filter, o -> Objects.equals(o.getType(), PlayerLogType.THREE_POINT_HIT.getCode())).size();
                name = "三分";
            }
            //球员效率值
            //篮板
            int backboard = CommonStreamUtil.filter(filter, o -> Objects.equals(o.getType(), PlayerLogType.BACKBOARD.getCode())).size();
            int assist = CommonStreamUtil.filter(filter, o -> Objects.equals(o.getType(), PlayerLogType.ASSIST.getCode())).size();
            int stral = CommonStreamUtil.filter(filter, o -> Objects.equals(o.getType(), PlayerLogType.STRAL.getCode())).size();
            int blockShot = CommonStreamUtil.filter(filter, o -> Objects.equals(o.getType(), PlayerLogType.BLOCK_SHOT.getCode())).size();
            //失误
            int mistake = CommonStreamUtil.filter(filter, o -> Objects.equals(o.getType(), PlayerLogType.MISTAKE.getCode())).size();

            // 总分+篮板+助攻+抢断+盖帽 - （二分不中+三分不中+罚球不中+失误）
            long per = (reduce + backboard + assist + stral + blockShot) - (twoNotPointHit + threeNotPointHit + notPointHit + mistake);
            Map<String, Object> pointHitMap = new HashMap<>();
            //罚球
            BigDecimal divide = BigDecimal.ZERO;
//            if (per > 0){
//                divide = BigDecimal.valueOf(per).divide(BigDecimal.valueOf(100),2, RoundingMode.HALF_UP);
//            }
            pointHitMap.put("typeStr", "单场效率值");
            pointHitMap.put("typeCount", per);
            pointHitMap.put("type",94);
            typeMapList.add(pointHitMap);

            //真实命中率
            //总分 / (2* (二分出手+三分出手+ (0.44*罚球出手) ))
            //二分命中
            long twoPointHit = CommonStreamUtil.filter(filter, o -> Objects.equals(o.getType(),PlayerLogType.TWO_POINT_HIT.getCode())).size();
            //罚球命中
            long pointHit = CommonStreamUtil.filter(filter, o -> Objects.equals(o.getType(), PlayerLogType.POINT_HIT.getCode())).size();
            //二分三分总出手
            long l = twoPointHit + twoNotPointHit + threeNotPointHit + threePointHit;
            //罚球总出手
            double v = 0.44 * (pointHit + notPointHit);
            double v1 = 2 * (l + v);
            Map<String, Object> map = new HashMap<>();
            BigDecimal divide1 = BigDecimal.ZERO;
            if(reduce > 0 && v1 > 0){
                divide1 = BigDecimal.valueOf(reduce).divide(BigDecimal.valueOf(v1),2, RoundingMode.HALF_UP);
            }
            map.put("typeStr", "真实命中率");
            map.put("typeCount", divide1);
            map.put("type",95);
            typeMapList.add(map);


            //二分 (二分命中 / 二分未命中)
            Map<String, Object> twoScoreMap = new HashMap<>();
            String twoScore = twoPointHit + "/" + (twoPointHit + twoNotPointHit);
            twoScoreMap.put("typeStr", "二分");
            twoScoreMap.put("typeCount", twoScore);
            twoScoreMap.put("type",TWO_POINTHIT);
            typeMapList.add(0,twoScoreMap);

            //三分 (三分命中 / 三分未命中)
            Map<String, Object> threeScoreMap = new HashMap<>();
            String threeScore = threePointHit + "/" + (threePointHit + threeNotPointHit);
            threeScoreMap.put("typeStr", name);
            threeScoreMap.put("typeCount", threeScore);
            threeScoreMap.put("type",THREE_POINTHIT);
            typeMapList.add(0,threeScoreMap);

            //罚球 (罚球命中 / 罚球未命中)
            Map<String, Object> pointHitScoreMap = new HashMap<>();
            String pointHitScore = pointHit + "/" + (pointHit + notPointHit);
            pointHitScoreMap.put("typeStr", "罚球");
            pointHitScoreMap.put("typeCount", pointHitScore);
            pointHitScoreMap.put("type",POINTHIT);
            typeMapList.add(0,pointHitScoreMap);

            pointHitAggregate = pointHitAggregate + pointHit;
            pointHitTotalAggregate = pointHitTotalAggregate + pointHit + notPointHit;

            twoPointHitAggregate = twoPointHitAggregate + twoPointHit;
            twoPointHitTotalAggregate = twoPointHitTotalAggregate + twoPointHit + twoNotPointHit;

            threePointHitAggregate = threePointHitAggregate + threePointHit;
            threePointHitTotalAggregate = threePointHitTotalAggregate + threePointHit + threeNotPointHit;


            playMap.put("typeMapList", typeMapList);

            playMap.put("typeScoreSum",reduce);

            playMap.put("name", baseBasketballPlayersMap.get(key.getBasketballPlayerId()));
            playMap.put("code", key.getPlayerNumber());
            playMap.put("basketballPlayerId", key.getBasketballPlayerId());
            playMapList.add(playMap);

            reduceTotal = reduceTotal+reduce;

        }
        //处理总计
        Map<String,Object> reduceMap = new HashMap<>();
        reduceMap.put("typeScoreSum",reduceTotal);
        reduceMap.put("name", "总计");
        List<Map<String, Object>> typeMapList = new ArrayList<>();
        for (Map.Entry<Long, Integer> keyLog:totalMap.entrySet()) {
            Map<String, Object> map = new HashMap<>();
            //类型名称
            map.put("typeStr", PlayerLogType.getValue(keyLog.getKey()));
            map.put("typeCount", keyLog.getValue());
            map.put("type", keyLog.getKey());
            typeMapList.add(map);
        }

        Map<String, Object> twoScoreMap = new HashMap<>();
        String twoScore = twoPointHitAggregate + "/" + twoPointHitTotalAggregate;
        twoScoreMap.put("typeStr", "二分");
        twoScoreMap.put("typeCount", twoScore);
        twoScoreMap.put("type",TWO_POINTHIT);
        typeMapList.add(0,twoScoreMap);

        //三分 (三分命中 / 三分未命中)
        Map<String, Object> threeScoreMap = new HashMap<>();
        String threeScore = threePointHitAggregate + "/" + threePointHitTotalAggregate;
        threeScoreMap.put("typeStr", name);
        threeScoreMap.put("typeCount", threeScore);
        threeScoreMap.put("type",THREE_POINTHIT);
        typeMapList.add(0,threeScoreMap);
        
        Map<String, Object> pointHitScoreMap = new HashMap<>();
        String pointHitScore = pointHitAggregate + "/" + pointHitTotalAggregate;
        pointHitScoreMap.put("typeStr", "罚球");
        pointHitScoreMap.put("typeCount", pointHitScore);
        pointHitScoreMap.put("type",POINTHIT);
        typeMapList.add(0,pointHitScoreMap);
        
        reduceMap.put("typeMapList", typeMapList);
        playMapList.add(reduceMap);
        return playMapList;
    }

    /**
     *
     * @param filter 球员得分总记录
     * @param type 操作类型 PlayerLogType
     * @param name 操作名称
     * @param code 操作编号
     * @param typeMapList 返回体
     */
    public static long buildTypeName(List<BasePlayerLog> filter,Long type, Long notTyoe,String name, int code, List<Map<String, Object>> typeMapList) {
        long pointHit = CommonStreamUtil.filter(filter, o -> Objects.equals(o.getType(),type)).size();
        //罚球未中
        long notPointHit = CommonStreamUtil.filter(filter, o -> Objects.equals(o.getType(), notTyoe)).size();
        long allPointHit = notPointHit + pointHit;
        Map<String, Object> pointHitMap = new HashMap<>();
        //罚球
        BigDecimal divide = BigDecimal.ZERO;
        if (pointHit > 0){
            divide = BigDecimal.valueOf(pointHit).divide(BigDecimal.valueOf(allPointHit),2, RoundingMode.HALF_UP);
        }
        pointHitMap.put("typeStr", name);
        pointHitMap.put("typeCount", divide);
        pointHitMap.put("type",code);
        typeMapList.add(pointHitMap);

        return notPointHit;
    }

    /**
     * 获取比赛总结
     *
     * @param tournamentId
     * @return
     */
    @Override
    public String getSummary(Long tournamentId) {
        BaseTournament tournament = baseTournamentMapper.selectBaseTournamentById(tournamentId);
        if (ObjectUtil.isNull(tournament) || tournament.getState() != 2) {
            //比赛不存在 或者 未结束 都无比赛总结
            return "";
        }
        StringBuffer stringBuffer = new StringBuffer("本次比赛小节,");
        List<BaseSummaryVO> summary = baseMapper.getSummary(tournamentId);
//        summary.stream().filter(vo -> Objects.equals(vo.getBackboard()))
        //分队处理
        Map<String, List<BaseSummaryVO>> listMap = summary.stream().collect(Collectors.groupingBy(BaseSummaryVO::getTeamName));
        //拼接队伍
        listMap.forEach((k, v) -> {
            stringBuffer.append(k).append(":");
        });
        //去掉最后一个符号
        stringBuffer.setLength(stringBuffer.length() - 1);
        stringBuffer.append("\n");
        listMap.forEach((k, v) -> {
            stringBuffer.append(k).append("\n");
            v.forEach(item -> {
                stringBuffer.append(item.getPlayerName()).append("拿到").append(item.getScore()).append("分").append(item.getBackboard()).append("篮板").append("\n");
            });
        });

        return stringBuffer.toString();
    }

    /**
     * 排行榜
     *
     * @param contestId
     * @param type
     * @return
     */
    @Override
    public List<BaseRankingListVO> rankingList(Long contestId, int type,String types) {
        List<BaseRankingListVO> rankingList = getRankingList(contestId, type,types);
////        type：1:得分；2:三分；3:篮板；4:助攻；
//        List<BaseRankingListVO> list = baseMapper.rankingList(contestId, type);
//
//        if (CollUtil.isEmpty(list)){
//            return list;
//        }
//        BaseRankingListVO baseRankingListVO = list.get(0);
//        Long aLong = scoreMap.get(baseRankingListVO.getType());
//        if (Objects.nonNull(aLong)){
//            list.forEach(vo -> vo.setAverage(String.valueOf((long) Double.parseDouble(vo.getAverage())/aLong)));
//        }

        for (int i = 0; i < rankingList.size(); i++) {
            rankingList.get(i).setNoNum(i + 1);
        }

        return rankingList;
    }

    private List<BaseRankingListVO> getRankingList(Long contestId, int type,String types) {

        int typeNum = Objects.equals(type, 3) ? 9 : Objects.equals(type, 4) ? 8
                : Objects.equals(type, 2) ? 2 : Objects.equals(type, 5)?4
                :Objects.equals(type, 6)?3:Objects.equals(type, 7)?1:0;
        List<BaseRankingListVO> rankingListVOList = new ArrayList<>();
        List<BaseTournament> tournamentList = iBaseTournamentService.list(Wrappers.<BaseTournament>lambdaQuery()
                .eq(BaseTournament::getContestId, contestId)
                .in(StrUtil.isNotBlank(types),BaseTournament::getType, StrUtil.split(types, ","))
                .eq(BaseTournament::getIsDeleted, 0)
        );
        if (CollUtil.isEmpty(tournamentList)){
            return new ArrayList<>();
        }
        Set<Long> tournamentIds = tournamentList.stream().map(BaseTournament::getId).collect(Collectors.toSet());

        List<BaseAttendanceLog> attendanceLogList = iBaseAttendanceLogService.list(Wrappers.<BaseAttendanceLog>lambdaQuery()
                .in(BaseAttendanceLog::getTournamentId, tournamentIds));
        Map<Long, Set<Long>> groupedTournamentIds = attendanceLogList.stream()
                .collect(Collectors.groupingBy(
                        BaseAttendanceLog::getBasketballPlayerId, // 按basketballPlayerId分组
                        Collectors.mapping(BaseAttendanceLog::getTournamentId, // 映射为tournamentId
                                Collectors.toSet()) // 收集到Set中去重
                ));

        // 计算每个basketballPlayerId对应的去重tournamentId数量
        Map<Long, Integer> uniqueTournamentIdCountsMap = groupedTournamentIds.entrySet().stream()
                .collect(Collectors.toMap(
                        Map.Entry::getKey, // basketballPlayerId作为key
                        entry -> entry.getValue().size() // Set的大小即为去重后的tournamentId数量
                ));

        List<BasePlayerLog> playerLogList = this.list(Wrappers.<BasePlayerLog>lambdaQuery()
                .in(BasePlayerLog::getTournamentId, tournamentIds)
                .eq(!Objects.equals(typeNum,0),BasePlayerLog::getType,typeNum)
                .ne(BasePlayerLog::getBasketballPlayerId,-1)
        );
        if (CollUtil.isEmpty(playerLogList)){
            return new ArrayList<>();
        }
        Map<Long, List<BasePlayerLog>> longListMap = playerLogList.stream().filter(vo ->Objects.nonNull(vo.getBasketballPlayerId())).collect(Collectors.groupingBy(BasePlayerLog::getBasketballPlayerId));
        //获取球队
        List<BasketballPlayerAndTeam> basketballPlayerAndTeams = basketballPlayerAndTeamMapper.selectByPlayIds(longListMap.keySet(), 0);
        Map<Long, BasketballPlayerAndTeam> playerAndTeamMap = CommonStreamUtil.toMap(basketballPlayerAndTeams, BasketballPlayerAndTeam::getPlayerId);
        Set<Long> teamIds = CommonStreamUtil.transSet(basketballPlayerAndTeams, BasketballPlayerAndTeam::getTeamId);
        List<BaseBasketballTeam> baseBasketballTeams = iBaseBasketballTeamService.listByIds(teamIds);
        Map<Long, BaseBasketballTeam> teamMap = CommonStreamUtil.toMap(baseBasketballTeams, BaseBasketballTeam::getId);

        List<BaseBasketballPlayer> baseBasketballPlayers = iBaseBasketballPlayerService.listByIds(longListMap.keySet());

        Map<Long, BaseBasketballPlayer> playMap = baseBasketballPlayers.stream().collect(Collectors.toMap(BaseBasketballPlayer::getId, Function.identity(), (k, v) -> k));

        for (Map.Entry<Long, List<BasePlayerLog>> key:longListMap.entrySet()) {
            BaseRankingListVO vo = new BaseRankingListVO();
            Long playId = key.getKey();
            List<BasePlayerLog> value = key.getValue();
            //上场几次比赛
            Integer integer = uniqueTournamentIdCountsMap.get(playId);
            Long reduce = 0L;
            if (type < 2 || type == 7){
                reduce = value.stream().map(BasePlayerLog::getScore).reduce(0L, Long::sum);
            }else{
                reduce = (long) value.size();
            }
            vo.setTotalCount(Math.toIntExact(reduce));
            if (Objects.nonNull(integer)){
                BigDecimal divide = BigDecimal.valueOf(reduce).divide(BigDecimal.valueOf(integer), 2, RoundingMode.HALF_UP);
                vo.setAverage(String.valueOf(divide));
            }

            BaseBasketballPlayer playerPo = playMap.get(playId);
            if (Objects.nonNull(playerPo)){
                vo.setPlayerName(playerPo.getName());
                vo.setProfilePicture(playerPo.getProfilePicture());
            }
            if (playerAndTeamMap.containsKey(playId)){
                BasketballPlayerAndTeam basketballPlayerAndTeam = playerAndTeamMap.get(playId);
                if (teamMap.containsKey(basketballPlayerAndTeam.getTeamId())){
                    BaseBasketballTeam baseBasketballTeam = teamMap.get(basketballPlayerAndTeam.getTeamId());
                    vo.setTeamName(baseBasketballTeam.getTeamName());
                }
            }
            rankingListVOList.add(vo);
        }
        rankingListVOList =  rankingListVOList.stream().sorted(Comparator.comparing(BaseRankingListVO::getTotalCount).reversed()).collect(Collectors.toList());


        return rankingListVOList;
    }

    public static void main(String[] args) {
        DateTime dateTime = DateUtil.beginOfDay(new Date());
        long time = dateTime.getTime();
        System.out.println(time);
    }


    /**
     * 本场最佳
     *
     * @param tournamentId
     * @return
     */
    @Override
    public BestOfTheGameResponseVO bestOfTheGame(Long tournamentId) {
        // 是在不知道怎么设计了。。。。写了个屎山代码
        BaseTournament baseTournament = baseTournamentMapper.selectBaseTournamentById(tournamentId);

        if (ObjectUtil.isNull(baseTournament)){
            throw new RuntimeException("赛事不存在");
        }

        List<BestOfTheGamePlayerVO> oneList = getBestOfTheGamePlayerVOS(tournamentId, baseTournament.getTeamOneId());

        List<BestOfTheGamePlayerVO> twoList = getBestOfTheGamePlayerVOS(tournamentId, baseTournament.getTeamTwoId());

        BestOfTheGameResponseVO bestOfTheGameResponseVO = new BestOfTheGameResponseVO();

        bestOfTheGameResponseVO.setOneTeamList(oneList);
        bestOfTheGameResponseVO.setTwoTeamList(twoList);

        return bestOfTheGameResponseVO;
    }

    /**
     * 球队比较
     *
     * @param tournamentId
     * @return
     */
    @Override
    public BaseTeamCompareResponseVO teamCompare(Long tournamentId) {
        BaseTournament baseTournament = baseTournamentMapper.selectBaseTournamentById(tournamentId);

        if (ObjectUtil.isNull(baseTournament)){
            throw new RuntimeException("赛事不存在");
        }

        BaseTeamCompareResponseVO baseTeamCompareResponseVO = new BaseTeamCompareResponseVO();

        baseTeamCompareResponseVO.setOneTeam(baseMapper.getTeamCompareTeam(baseTournament.getTeamOneId(),tournamentId));
        baseTeamCompareResponseVO.setTwoTeam(baseMapper.getTeamCompareTeam(baseTournament.getTeamTwoId(),tournamentId));

        return baseTeamCompareResponseVO;
    }

    @Override
    public Map<String,Object> getTournamentScore(Long tournamentId) {
        Map<String,Object> map = new HashMap<>();

        BaseTournament baseTournament = iBaseTournamentService.selectBaseTournamentById(tournamentId);
        if (Objects.isNull(baseTournament)){
            throw new RuntimeException("未找到当前比赛");
        }
        BaseContest baseContest = iBaseContestService.getById(baseTournament.getContestId());
        AssertUtil.notNull(baseContest,"未找到当前联赛");

        //比赛名称
        map.put("contestName",baseContest.getContestName());
        //队伍1
        map.put("teamOneName",baseTournament.getTeamOneName());
        map.put("teamOneId",baseTournament.getTeamOneId());
        map.put("teamOneScore",baseTournament.getTeamOneScore());
        map.put("subsection",baseTournament.getSubsection());
        map.put("teamOneLogo",baseTournament.getTeamOneLogo());
        //队伍2
        map.put("teamTwoName",baseTournament.getTeamTwoName());
        map.put("teamTwoId",baseTournament.getTeamTwoId());
        map.put("teamTwoScore",baseTournament.getTeamTwoScore());
        map.put("teamTwoLogo",baseTournament.getTeamTwoLogo());
        map.put("tournamentBeginTime",baseTournament.getTournamentBeginTime());

        List<Map<String,Object>> listMap = new ArrayList<>();
        //获取上场球员
        List<BaseAttendanceLog> list1 = iBaseAttendanceLogService.list(Wrappers.<BaseAttendanceLog>lambdaQuery().eq(BaseAttendanceLog::getTournamentId, tournamentId));
        //获取比赛得分操作
        List<BasePlayerLog> list = this.list(Wrappers.<BasePlayerLog>lambdaQuery().eq(BasePlayerLog::getTournamentId, tournamentId));
        List<BasePlayerLog> playerLogList = list.stream().filter(vo -> baseTournament.getTeamOneId().equals(vo.getBasketballTeamId())).collect(Collectors.toList());
        List<BasePlayerLog> playerLogListTwo = list.stream().filter(vo -> baseTournament.getTeamTwoId().equals(vo.getBasketballTeamId())).collect(Collectors.toList());

        //获取队伍比分
        map.put("teamOneScore",playerLogList.stream().map(BasePlayerLog::getScore).reduce(0L,Long::sum));
        map.put("teamTwoScore",playerLogListTwo.stream().map(BasePlayerLog::getScore).reduce(0L,Long::sum));

        if (CollUtil.isNotEmpty(list1)){
            List<BaseAttendanceLog> teamOne = list1.stream().filter(vo -> baseTournament.getTeamOneId().equals(vo.getBasketballTeamId())).collect(Collectors.toList());
            Map<Long, String> teamOnePlayerMap = teamOne.stream().collect(Collectors.toMap(BaseAttendanceLog::getBasketballPlayerId, BaseAttendanceLog::getPlayerNumber, (k, v) -> k));
            List<BaseBasketballPlayer> baseBasketballPlayers = new ArrayList<>();
            if(CollUtil.isNotEmpty(teamOnePlayerMap.keySet())){
               baseBasketballPlayers = iBaseBasketballPlayerService.listByIds(teamOnePlayerMap.keySet());
            }


            for (BaseBasketballPlayer key:baseBasketballPlayers) {
                Map<String,Object> mapKey = new HashMap<>();
                mapKey.put("playerName",key.getName());
                mapKey.put("isUp",key.getIsUp());
                mapKey.put("playerNumber",teamOnePlayerMap.get(key.getId()));
                List<BasePlayerLog> collect = playerLogList.stream().filter(vo -> Objects.equals(vo.getBasketballPlayerId(), key.getId())).collect(Collectors.toList());
                Long reduce = collect.stream().map(BasePlayerLog::getScore).reduce(0L, Long::sum);
                mapKey.put("playerScore",reduce);
                long foul = collect.stream().filter(vo -> vo.getType().equals(PlayerLogType.FOUL.getCode())).count();
                mapKey.put("playerFoul",foul);
                listMap.add(mapKey);
            }
            //球员信息 号码 名称 分数 犯规
            map.put("playerListOne",listMap);

            long countFoulOne = 0;
            long countStopOne = 0;
            for (BasePlayerLog vo:playerLogList) {
                if (Objects.equals(vo.getType(), PlayerLogType.FOUL.getCode())
                        ||Objects.equals(vo.getType(), PlayerLogType.TEAM_FOUL.getCode())){
                    if (baseTournament.getSubsection() > 3L ? vo.getSubsection() > 3L : Objects.equals(vo.getSubsection(), baseTournament.getSubsection())){
                        countFoulOne++;
                    }
                }
                if (vo.getType().equals(PlayerLogType.STOP.getCode())){
                    if (baseTournament.getSubsection() > 2L ? vo.getSubsection() > 2L : vo.getSubsection() < 2L){
                        countStopOne++;
                    }
                }
            }
            map.put("homeTeamFoul",countFoulOne);
            map.put("homeTeamPause",countStopOne);



            List<BaseAttendanceLog> teamTwo = list1.stream().filter(vo -> baseTournament.getTeamTwoId().equals(vo.getBasketballTeamId())).collect(Collectors.toList());

            Map<Long, String> teamTwoPlayerMap = teamTwo.stream().collect(Collectors.toMap(BaseAttendanceLog::getBasketballPlayerId, BaseAttendanceLog::getPlayerNumber, (k, v) -> k));
            List<BaseBasketballPlayer> baseBasketballPlayersTwo = new ArrayList<>();
            if (CollUtil.isNotEmpty(teamTwoPlayerMap.keySet())){
                baseBasketballPlayersTwo = iBaseBasketballPlayerService.listByIds(teamTwoPlayerMap.keySet());
            }

            List<Map<String,Object>> listMap2 = new ArrayList<>();

            for (BaseBasketballPlayer key:baseBasketballPlayersTwo) {
                Map<String,Object> mapKey = new HashMap<>();
                mapKey.put("playerName",key.getName());
                mapKey.put("isUp",key.getIsUp());
                mapKey.put("playerNumber",teamTwoPlayerMap.get(key.getId()));

                List<BasePlayerLog> collect = playerLogListTwo.stream().filter(vo -> Objects.equals(vo.getBasketballPlayerId(), key.getId())).collect(Collectors.toList());
                Long reduce = collect.stream().map(BasePlayerLog::getScore).reduce(0L, Long::sum);
                mapKey.put("playerScore",reduce);
                long foul = collect.stream().filter(vo -> vo.getType().equals(PlayerLogType.FOUL.getCode())).count();
                mapKey.put("playerFoul",foul);
                listMap2.add(mapKey);
            }
            //球员信息 号码 名称 分数 犯规
            map.put("playerListTwo",listMap2);

            long countFoulTwo = 0;
            long countStopTwo = 0;
            for (BasePlayerLog vo:playerLogListTwo) {
                if (Objects.equals(vo.getType(), PlayerLogType.FOUL.getCode())
                        ||Objects.equals(vo.getType(), PlayerLogType.TEAM_FOUL.getCode())){
                    if (baseTournament.getSubsection() > 3L ? vo.getSubsection() > 3L : Objects.equals(vo.getSubsection(), baseTournament.getSubsection())){
                        countFoulTwo++;
                    }
                }
                if (vo.getType().equals(PlayerLogType.STOP.getCode())){
                    if (baseTournament.getSubsection() > 2L ? vo.getSubsection() > 2L : vo.getSubsection() < 2L){
                        countStopTwo++;
                    }
                }
            }
            map.put("awayTeamFoul",countFoulTwo);
            map.put("awayTeamPause",countStopTwo);
        }
        return map;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int saveBatchLog(List<BasePlayerLog> basePlayerLog) {
        if(CollUtil.isEmpty(basePlayerLog)){
            return 1;
        }
        BasePlayerLog basePlayerLog1 = basePlayerLog.get(0);
        Long tournamentId = basePlayerLog1.getTournamentId();
        BaseTournament baseTournament = baseTournamentMapper.selectById(tournamentId);
        if (Objects.isNull(baseTournament) || !Objects.equals(baseTournament.getState(),1)){
            return 1;
        }
        List<BasePlayerLog> list = this.list(Wrappers.<BasePlayerLog>lambdaQuery()
                .eq(BasePlayerLog::getTournamentId, tournamentId)
                .eq(BasePlayerLog::getIsDeleted, 0)
        );
        List<Integer> listLong = redisService.getCacheList(CacheConstants.BASE_PLAYER_LOG_ID + tournamentId);
        List<Long> cacheList = JSONArray.parseArray(listLong.toString(),Long.class);
        List<Long> collect = list.stream().map(BasePlayerLog::getId).collect(Collectors.toList());
        collect.addAll(cacheList);
        basePlayerLog.stream().filter(vo -> !collect.contains(vo.getId())).forEach(vo ->{
            vo.setContestId(SecurityUtils.getContestId());
            vo.setOrganizationId(SecurityUtils.getOrganizationId());
            vo.setCreateUserId(SecurityUtils.getUserId());
            vo.setCreateUserName(SecurityUtils.getUsername());
            insertBasePlayerLog(vo);
        });

        return 1;
    }

    @Override
    public Map<String, Object> listAll(Long tournamentId) {
        Map<String,Object> map = new HashMap<>();
        List<BasePlayerLog> list1 = this.list(Wrappers.<BasePlayerLog>lambdaQuery().eq(BasePlayerLog::getTournamentId, tournamentId).eq(BasePlayerLog::getIsDeleted, 0));
        map.put("list",list1);
        if (CollUtil.isEmpty(list1)){
            return map;
        }
        BaseTournament byId = iBaseTournamentService.getById(tournamentId);
        AssertUtil.notNull(byId,"未找到当前赛事");
        Set<Long> longs1 = new HashSet<>();
        longs1.add(byId.getTeamOneId());
        longs1.add(byId.getTeamTwoId());

        Optional<Long> max = list1.stream().map(BasePlayerLog::getSubsection).max(Comparator.naturalOrder());
        if (max.isPresent()){
            byId.setSubsection(max.get());
        }
        //队伍一 小节总分
        Map<Long, List<BasePlayerLog>> collect = list1.stream().collect(Collectors.groupingBy(BasePlayerLog::getBasketballTeamId));

        List<Map<String, Object>> mapList = new ArrayList<>();
        for (Long key : longs1) {
            List<BasePlayerLog> basePlayerLogs = collect.get(key);
            List<BasePlayerLog> value = CollUtil.isEmpty(basePlayerLogs)?new ArrayList<>():basePlayerLogs;
            Map<Long, Long> scoreNum = new HashMap<>();
            Map<Long, Long> countFoulNum = new HashMap<>();
            Map<Long, Long> countStopNum = new HashMap<>();

            for (Long i = 1L; i <= byId.getSubsection(); i++) {
                //单个队伍的小节分值
                Long finalI = i;
                Long reduce = value.stream().filter(vo -> Objects.equals(vo.getSubsection(), finalI)).map(BasePlayerLog::getScore).reduce(0L, Long::sum);
                scoreNum.put(i, reduce);
                Long countFoul =0L;
                Long countStop=0L;
                for (BasePlayerLog vo:value) {
                    if ((Objects.equals(vo.getType(), PlayerLogType.FOUL.getCode()) ||Objects.equals(vo.getType(), PlayerLogType.TEAM_FOUL.getCode()))){
                        if (finalI > 3L ? vo.getSubsection() > 3L : Objects.equals(vo.getSubsection(), finalI)){
                            countFoul ++;
                        }
                    }
                    if (vo.getType().equals(PlayerLogType.STOP.getCode())){
                        if (finalI > 2L ? vo.getSubsection() > 2L : vo.getSubsection() <= 2L){
                            countStop++;
                        }
                    }
                }
                countFoulNum.put(i,countFoul);
                countStopNum.put(i,countStop);

            }
            //队伍总数
            long teamSubsectionScoreSumTotal = scoreNum.values().stream().mapToLong(Long::longValue).sum();

            Map<String, Object> map1 = new HashMap<>();
            map1.put("scoreNum", scoreNum);
            map1.put("countFoulNum", countFoulNum);
            map1.put("countStopNum", countStopNum);
            map1.put("teamSubsectionScoreSumTotal", teamSubsectionScoreSumTotal);
            map1.put("teamId", key);
            mapList.add(map1);
        }
        map.put("score",mapList);
        return map;
    }



    @NotNull
    private List<BestOfTheGamePlayerVO> getBestOfTheGamePlayerVOS(Long tournamentId, Long teamId) {
        List<BestOfTheGamePlayerVO> oneList = new ArrayList<>();
        //队伍1的最佳得分、篮板、助攻
        BestOfTheGamePlayerVO scoreOneTeam = baseMapper.bestOfTheGame(tournamentId, teamId, 1);
        if (ObjectUtil.isNotNull(scoreOneTeam)) {
            scoreOneTeam.setType(1);
            oneList.add(scoreOneTeam);
        }else {
            oneList.add(new BestOfTheGamePlayerVO());
        }

        BestOfTheGamePlayerVO lbOneTeam = baseMapper.bestOfTheGame(tournamentId, teamId, 2);
        if (ObjectUtil.isNotNull(lbOneTeam)) {
            lbOneTeam.setType(2);
            oneList.add(lbOneTeam);
        }else {
            oneList.add(new BestOfTheGamePlayerVO());
        }

        BestOfTheGamePlayerVO zgOneTeam = baseMapper.bestOfTheGame(tournamentId, teamId, 3);
        if (ObjectUtil.isNotNull(zgOneTeam)) {
            zgOneTeam.setType(3);
            oneList.add(zgOneTeam);
        }else {
            oneList.add(new BestOfTheGamePlayerVO());
        }
        return oneList;
    }

    @Override
    public Map<String, Object> getImg(Long tournamentId) {
        BaseTournament tournament = iBaseTournamentService.selectBaseTournamentById(tournamentId);
        if(Objects.isNull(tournament)){
            throw new RuntimeException("未找到当前比赛");
        }
        BaseContest contest = iBaseContestService.getById(tournament.getContestId());
        if(Objects.isNull(contest)){
            throw new RuntimeException("未找到当前联赛");
        }

        Map<String, Object> map = new HashMap<>();
        map.put("contestImg",contest.getContestLogo());
        map.put("str1",contest.getContestName());
        map.put("str2",tournament.getTournamentBeginTime() + tournament.getAddress());
        map.put("typeStr", TournamentType.getValue(tournament.getType()));
        map.put("homeTeamId",tournament.getTeamOneId());
        map.put("homeTeamLogo",tournament.getTeamOneLogo());
        map.put("homeTeamName",tournament.getTeamOneName());
        map.put("homeTeamScore",tournament.getTeamOneScore());

        map.put("awayTeamId",tournament.getTeamTwoId());
        map.put("awayTeamLogo",tournament.getTeamTwoLogo());
        map.put("awayTeamName",tournament.getTeamTwoName());
        map.put("awayTeamScore",tournament.getTeamTwoScore());

        //全场最佳
        BestOfTheGameResponseVO bestOfTheGame = bestOfTheGame(tournamentId);
        map.put("bestOfTheGame",bestOfTheGame);

        //比分统计
        BasePlayerLog playerLog = new BasePlayerLog();
        playerLog.setTournamentId(tournamentId);
        Map<String, Object> realTimeData = realTimeData(playerLog);
        map.put("realTimeData",realTimeData);

        //球队统计
        BaseTeamCompareResponseVO teamCompare = teamCompare(tournamentId);
        map.put("teamCompare",teamCompare);

        BasePlayerLog realTimeDataTeamOne = new BasePlayerLog();
        realTimeDataTeamOne.setTournamentId(tournamentId);
        realTimeDataTeamOne.setBasketballTeamId(tournament.getTeamOneId());
        List<Map<String, Object>> realTimeDataTeamOneResp = realTimeDataTeamGetImg(realTimeDataTeamOne);


        map.put("realTimeDataTeamOneResp",realTimeDataTeamOneResp);

        BasePlayerLog realTimeDataTeamTwo = new BasePlayerLog();
        realTimeDataTeamTwo.setTournamentId(tournamentId);
        realTimeDataTeamTwo.setBasketballTeamId(tournament.getTeamTwoId());
        List<Map<String, Object>> realTimeDataTeamTwoResp = realTimeDataTeamGetImg(realTimeDataTeamTwo);

        map.put("realTimeDataTeamTwoResp",realTimeDataTeamTwoResp);

        map.put("scoringWay",contest.getScoringWay());
        return map;
    }

    @Override
    public String getContestBanner(Long contestId) {
        BaseContest byId = iBaseContestService.getById(contestId);
        if (Objects.isNull(byId)){
            throw new RuntimeException("联赛id错误");
        }
        return StrUtil.isNotBlank(byId.getBanner())?byId.getBanner():null;
    }

    @Override
    public ScoreMovementVO scoreMovement(Long tournamentId) {
        BaseTournament baseTournament = iBaseTournamentService.selectBaseTournamentAndTeamNameById(tournamentId);
        AssertUtil.notNull(baseTournament,"未找到当前比赛!");

        ScoreMovementVO scoreMovementVO = new ScoreMovementVO();
        scoreMovementVO.setTeamOneName(baseTournament.getTeamOneName());
        scoreMovementVO.setTeamOneScoreMax(baseTournament.getTeamOneScoreMax());

        scoreMovementVO.setTeamTwoName(baseTournament.getTeamTwoName());
        scoreMovementVO.setTeamTwoScoreMax(baseTournament.getTeamTwoScoreMax());


        List<BasePlayerLog> basePlayerLogs = baseMapper.selectByTournamentId(tournamentId);
        if (CollUtil.isNotEmpty(basePlayerLogs)){
            Map<Long, List<BasePlayerLog>> teamIdMap = basePlayerLogs.stream().collect(Collectors.groupingBy(BasePlayerLog::getBasketballTeamId));

            if (teamIdMap.containsKey(baseTournament.getTeamOneId())){
                List<ScoreMovementVO.ScoreMovementList> teamOneScoreMovementListList = new ArrayList<>();
                List<BasePlayerLog> basePlayerLogs1 = teamIdMap.get(baseTournament.getTeamOneId());
                for (int i = 1; i <= baseTournament.getSubsection(); i++) {
                    ScoreMovementVO.ScoreMovementList scoreMovement= new ScoreMovementVO.ScoreMovementList();
                    Long finalI = (long) i;
                    Long reduce = CommonStreamUtil.filter(basePlayerLogs1, o -> o.getSubsection() <= finalI ).stream().map(BasePlayerLog::getScore).reduce(0L, Long::sum);
                    scoreMovement.setSubsection(i);
                    scoreMovement.setScore(reduce);
                    teamOneScoreMovementListList.add(scoreMovement);
                }
                scoreMovementVO.setTeamOneScoreMovementListList(teamOneScoreMovementListList);

            }

            if (teamIdMap.containsKey(baseTournament.getTeamTwoId())){
                List<ScoreMovementVO.ScoreMovementList> teamTwoScoreMovementListList = new ArrayList<>();
                List<BasePlayerLog> basePlayerLogs1 = teamIdMap.get(baseTournament.getTeamTwoId());
                for (int i = 1; i <= baseTournament.getSubsection(); i++) {
                    ScoreMovementVO.ScoreMovementList scoreMovement= new ScoreMovementVO.ScoreMovementList();
                    Long finalI = (long) i;
                    Long reduce = CommonStreamUtil.filter(basePlayerLogs1, o -> o.getSubsection() <= finalI).stream().map(BasePlayerLog::getScore).reduce(0L, Long::sum);
                    scoreMovement.setSubsection(i);
                    scoreMovement.setScore(reduce);
                    teamTwoScoreMovementListList.add(scoreMovement);
                }
                scoreMovementVO.setTeamTwoScoreMovementListList(teamTwoScoreMovementListList);
            }
        }
        return scoreMovementVO;
    }

    @Override
    public KeyFiguresVO keyFigures(KeyFiguresDto dto) {
        KeyFiguresVO keyFiguresVO = new KeyFiguresVO();

        Long tournamentId = dto.getTournamentId();
        BaseTournament baseTournament = iBaseTournamentService.selectBaseTournamentAndTeamNameById(tournamentId);
        AssertUtil.notNull(baseTournament,"未找到当前比赛!");

        BaseContest contest = iBaseContestService.getById(baseTournament.getContestId());
        AssertUtil.notNull(baseTournament,"未找到当前联赛!");
        Long scoringWay = contest.getScoringWay();

        keyFiguresVO.setTeamOneName(baseTournament.getTeamOneName());
        keyFiguresVO.setTeamOneLogo(baseTournament.getTeamOneLogo());
        keyFiguresVO.setTeamTwoName(baseTournament.getTeamTwoName());
        keyFiguresVO.setTeamTwoLogo(baseTournament.getTeamTwoLogo());

        Long aLong = Objects.equals(scoringWay, 0L) ? PlayerLogType.THREE_POINT_HIT.getCode() : PlayerLogType.ONE_POINT_HIT.getCode();
        String name = Objects.equals(scoringWay, 0L) ? "三分" : "一分";
        keyFiguresVO.setTeamTwoScoreName(name);
        keyFiguresVO.setTeamOneScoreName(name);

        if (Objects.equals(dto.getType(),1)){
            List<BasePlayerLog> basePlayerLogs = baseMapper.selectByTournamentIdAndTypes(tournamentId, Arrays.asList(PlayerLogType.TWO_POINT_HIT.getCode(), aLong, PlayerLogType.BACKBOARD.getCode()));
            basePlayerLogs = CommonStreamUtil.filter(basePlayerLogs,o -> Objects.nonNull(o.getBasketballPlayerId()));
            Map<Long, List<BasePlayerLog>> teamIdMap = basePlayerLogs.stream().collect(Collectors.groupingBy(BasePlayerLog::getBasketballTeamId));
            //主队
            if (teamIdMap.containsKey(baseTournament.getTeamOneId())) {
                List<BasePlayerLog> basePlayerLogs1 = teamIdMap.get(baseTournament.getTeamOneId());
                keyFiguresVO.setTeamOneScore((long) CommonStreamUtil.filter(basePlayerLogs1, o -> Objects.equals(o.getType(), aLong)).size());
                keyFiguresVO.setTeamOneTwoScore(CommonStreamUtil.filter(basePlayerLogs1, o -> Objects.equals(o.getType(), PlayerLogType.TWO_POINT_HIT.getCode())).size());
                keyFiguresVO.setTeamOneBackboard(CommonStreamUtil.filter(basePlayerLogs1, o -> Objects.equals(o.getType(), PlayerLogType.BACKBOARD.getCode())).size());
            }
            //客队
            if (teamIdMap.containsKey(baseTournament.getTeamTwoId())) {
                List<BasePlayerLog> basePlayerLogs1 = teamIdMap.get(baseTournament.getTeamTwoId());
                keyFiguresVO.setTeamTwoScore((long) CommonStreamUtil.filter(basePlayerLogs1, o -> Objects.equals(o.getType(), aLong)).size());
                keyFiguresVO.setTeamTwoTwoScore(CommonStreamUtil.filter(basePlayerLogs1, o -> Objects.equals(o.getType(), PlayerLogType.TWO_POINT_HIT.getCode())).size());
                keyFiguresVO.setTeamTwoBackboard(CommonStreamUtil.filter(basePlayerLogs1, o -> Objects.equals(o.getType(), PlayerLogType.BACKBOARD.getCode())).size());
            }
        }else{
            if (Objects.equals(dto.getPlayerType(),1)){
                dto.setPlayerType(null);
            }
            List<BasePlayerLog> basePlayerLogs = baseMapper.selectByTournamentIdAndTypeAndSubsection(tournamentId, dto.getSubsection(), dto.getPlayerType());
            basePlayerLogs = CommonStreamUtil.filter(basePlayerLogs,o -> Objects.nonNull(o.getBasketballPlayerId()));

            Set<Long> playerIds = CommonStreamUtil.transSet(basePlayerLogs, BasePlayerLog::getBasketballPlayerId);
            Map<Long, BaseBasketballPlayer> playerMap = new HashMap<>();
            if (CollUtil.isNotEmpty(playerIds)){
                List<BaseBasketballPlayer> baseBasketballPlayers = iBaseBasketballPlayerService.listByIds(playerIds);
                playerMap = baseBasketballPlayers.stream().collect(Collectors.toMap(BaseBasketballPlayer::getId, Function.identity()));
            }


            Map<Long, List<BasePlayerLog>> teamIdMap = basePlayerLogs.stream().collect(Collectors.groupingBy(BasePlayerLog::getBasketballTeamId));
            //主队
            if (teamIdMap.containsKey(baseTournament.getTeamOneId())) {
                List<KeyFiguresVO.KeyFiguresList> keyFiguresListList = new ArrayList<>();

                List<BasePlayerLog> basePlayerLogs1 = teamIdMap.get(baseTournament.getTeamOneId());

                keyFiguresVO.setTeamOneScore(basePlayerLogs1.stream().map(BasePlayerLog::getScore).reduce(0L, Long::sum));

                Map<Long, List<BasePlayerLog>> playerIdMap = CommonStreamUtil.group(basePlayerLogs1, BasePlayerLog::getBasketballPlayerId);

                for (Map.Entry<Long, List<BasePlayerLog>> keyLog:playerIdMap.entrySet()) {
                    KeyFiguresVO.KeyFiguresList keyFiguresList = new KeyFiguresVO.KeyFiguresList();
                    keyFiguresList.setNum(keyLog.getValue().stream().map(BasePlayerLog::getScore).reduce(0L, Long::sum));
                    keyFiguresList.setPlayerName(playerMap.get(keyLog.getKey()).getName());
                    keyFiguresListList.add(keyFiguresList);
                }
                keyFiguresVO.setTeamOneDataList(keyFiguresListList);
            }
            //客队
            if (teamIdMap.containsKey(baseTournament.getTeamTwoId())) {
                List<KeyFiguresVO.KeyFiguresList> keyFiguresListList = new ArrayList<>();

                List<BasePlayerLog> basePlayerLogs1 = teamIdMap.get(baseTournament.getTeamTwoId());
                keyFiguresVO.setTeamTwoScore(basePlayerLogs1.stream().map(BasePlayerLog::getScore).reduce(0L, Long::sum));

                Map<Long, List<BasePlayerLog>> playerIdMap = CommonStreamUtil.group(basePlayerLogs1, BasePlayerLog::getBasketballPlayerId);

                for (Map.Entry<Long, List<BasePlayerLog>> keyLog:playerIdMap.entrySet()) {
                    KeyFiguresVO.KeyFiguresList keyFiguresList = new KeyFiguresVO.KeyFiguresList();
                    keyFiguresList.setNum(keyLog.getValue().stream().map(BasePlayerLog::getScore).reduce(0L, Long::sum));
                    keyFiguresList.setPlayerName(playerMap.get(keyLog.getKey()).getName());
                    keyFiguresListList.add(keyFiguresList);
                }
                keyFiguresVO.setTeamTwoDataList(keyFiguresListList);
            }
        }
        return keyFiguresVO;
    }

    @Override
    public PlayerPKVO playerPK(PlayerPKDto dto) {
        Long tournamentId = dto.getTournamentId();
        BaseTournament baseTournament = iBaseTournamentService.selectBaseTournamentAndTeamNameById(tournamentId);
        AssertUtil.notNull(baseTournament,"未找到当前比赛!");
        PlayerPKVO playerPKVO = new PlayerPKVO();
        List<BasePlayerLog> basePlayerLogs = baseMapper.selectByTournamentIdAndPlayerIdAndSubsection(tournamentId, dto.getPlayerId(), dto.getSubsection());
        long twoPointHit = CommonStreamUtil.filter(basePlayerLogs, o -> Objects.equals(o.getType(), PlayerLogType.TWO_POINT_HIT.getCode())).size();
        long twoNotPointHit = CommonStreamUtil.filter(basePlayerLogs, o -> Objects.equals(o.getType(), PlayerLogType.TWO_NOT_POINT_HIT.getCode())).size();

        BigDecimal twoRate= BigDecimal.ZERO;
        if (twoPointHit > 0){
            twoRate = BigDecimal.valueOf(twoPointHit).divide(BigDecimal.valueOf(twoPointHit + twoNotPointHit),2, RoundingMode.HALF_UP);
        }
        playerPKVO.setTwoPointRate(twoRate);

        long threePointHit = CommonStreamUtil.filter(basePlayerLogs, o -> Objects.equals(o.getType(), PlayerLogType.THREE_POINT_HIT.getCode())).size();
        long threeNotPointHit = CommonStreamUtil.filter(basePlayerLogs, o -> Objects.equals(o.getType(), PlayerLogType.THREE_NOT_POINT_HIT.getCode())).size();

        BigDecimal threeRate = BigDecimal.ZERO;
        if (threePointHit > 0){
            threeRate = BigDecimal.valueOf(threePointHit).divide(BigDecimal.valueOf(threePointHit + threeNotPointHit),2, RoundingMode.HALF_UP);
        }
        playerPKVO.setThreePointRate(threeRate);

        long pointHit = CommonStreamUtil.filter(basePlayerLogs, o -> Objects.equals(o.getType(), PlayerLogType.POINT_HIT.getCode())).size();
        long notPointHit = CommonStreamUtil.filter(basePlayerLogs, o -> Objects.equals(o.getType(), PlayerLogType.NOT_POINT_HIT.getCode())).size();

        BigDecimal rate = BigDecimal.ZERO;
        if (pointHit > 0){
            rate = BigDecimal.valueOf(pointHit).divide(BigDecimal.valueOf(pointHit + notPointHit),2, RoundingMode.HALF_UP);
        }
        playerPKVO.setPointRate(rate);

        long assist = CommonStreamUtil.filter(basePlayerLogs, o -> Objects.equals(o.getType(), PlayerLogType.ASSIST.getCode())).size();
        long backboard = CommonStreamUtil.filter(basePlayerLogs, o -> Objects.equals(o.getType(), PlayerLogType.BACKBOARD.getCode())).size();
        long stral = CommonStreamUtil.filter(basePlayerLogs, o -> Objects.equals(o.getType(), PlayerLogType.STRAL.getCode())).size();
        long mistake = CommonStreamUtil.filter(basePlayerLogs, o -> Objects.equals(o.getType(), PlayerLogType.MISTAKE.getCode())).size();
        long foul = CommonStreamUtil.filter(basePlayerLogs, o -> Objects.equals(o.getType(), PlayerLogType.FOUL.getCode())).size();
        long blockShot = CommonStreamUtil.filter(basePlayerLogs, o -> Objects.equals(o.getType(), PlayerLogType.BLOCK_SHOT.getCode())).size();
        Long reduce = basePlayerLogs.stream().map(BasePlayerLog::getScore).reduce(0L, Long::sum);
        playerPKVO.setScore(reduce);
        playerPKVO.setAssist(assist);
        playerPKVO.setBackboard(backboard);
        playerPKVO.setStral(stral);
        playerPKVO.setMistake(mistake);
        playerPKVO.setFoul(foul);
        playerPKVO.setBlockShot(blockShot);
        long per = (reduce + backboard + assist + stral + blockShot) - (twoNotPointHit + threeNotPointHit + notPointHit + mistake);
        playerPKVO.setPer(per);

        //真实命中率
        //总分 / (2* (二分出手+三分出手+ (0.44*罚球出手) ))
        double v = 0.44 * (pointHit + notPointHit);
        double v1 = 2 * ((twoPointHit + twoNotPointHit) + (threePointHit + threeNotPointHit) + v);
        BigDecimal divide1 = BigDecimal.ZERO;
        if(reduce > 0 && v1 > 0){
            divide1 = BigDecimal.valueOf(reduce).divide(BigDecimal.valueOf(v1),2, RoundingMode.HALF_UP);
        }
        playerPKVO.setRate(divide1);

        playerPKVO.setShootPointNum(threePointHit + twoPointHit + pointHit);
        playerPKVO.setShootTotalNum(playerPKVO.getShootPointNum() + (threeNotPointHit + twoNotPointHit + notPointHit));

        return playerPKVO;
    }

    @Override
    public List<BaseBasketballPlayer> playerList(Long tournamentId) {
        BaseTournament baseTournament = iBaseTournamentService.selectBaseTournamentAndTeamNameById(tournamentId);
        AssertUtil.notNull(baseTournament,"未找到当前比赛!");
        BasketBallPlayerDto oneTeam = new BasketBallPlayerDto();
        oneTeam.setTeamId(baseTournament.getTeamOneId());
        List<BaseBasketballPlayer> baseBasketballPlayers = iBaseBasketballPlayerService.playerList(oneTeam);
        BasketBallPlayerDto twoTeam = new BasketBallPlayerDto();
        twoTeam.setTeamId(baseTournament.getTeamTwoId());
        baseBasketballPlayers.addAll(iBaseBasketballPlayerService.playerList(oneTeam));
        return baseBasketballPlayers;
    }

    @Override
    public AverageComparisonVO averageComparison(Long tournamentId) {
        AverageComparisonVO baseTeamCompareResponseVO = new AverageComparisonVO();
        BaseTournament baseTournament = baseTournamentMapper.selectById(tournamentId);
        AssertUtil.notNull(baseTournament,"比赛id有误!");

        List<BaseTournament> tournamentList = baseTournamentMapper.selectBaseTournamentListTeam(baseTournament.getContestId(),2,baseTournament.getTeamOneId());
        if (CollUtil.isEmpty(tournamentList)){
            return baseTeamCompareResponseVO;
        }
        //能力 柱状图
        BaseTeamCompareVO teamCompare = basePlayerLogMapper.getTeamCompareTeam(baseTournament.getTeamOneId(), null);

        int size = tournamentList.size();
        CopyBaseTeamCompareVO copyTeamCompare = new CopyBaseTeamCompareVO();
        copyTeamCompare.setEf(BigDecimal.valueOf(teamCompare.getEf()).divide(BigDecimal.valueOf(size),2, RoundingMode.HALF_UP));
        copyTeamCompare.setQd(BigDecimal.valueOf(teamCompare.getQd()).divide(BigDecimal.valueOf(size),2, RoundingMode.HALF_UP));
        copyTeamCompare.setLb(BigDecimal.valueOf(teamCompare.getLb()).divide(BigDecimal.valueOf(size),2, RoundingMode.HALF_UP));
        copyTeamCompare.setGm(BigDecimal.valueOf(teamCompare.getGm()).divide(BigDecimal.valueOf(size),2, RoundingMode.HALF_UP));
        copyTeamCompare.setFq(BigDecimal.valueOf(teamCompare.getFq()).divide(BigDecimal.valueOf(size),2, RoundingMode.HALF_UP));
        copyTeamCompare.setSf(BigDecimal.valueOf(teamCompare.getSf()).divide(BigDecimal.valueOf(size),2, RoundingMode.HALF_UP));
        copyTeamCompare.setZg(BigDecimal.valueOf(teamCompare.getZg()).divide(BigDecimal.valueOf(size),2, RoundingMode.HALF_UP));
        copyTeamCompare.setFg(BigDecimal.valueOf(teamCompare.getFg()).divide(BigDecimal.valueOf(size),2, RoundingMode.HALF_UP));
        copyTeamCompare.setScore(BigDecimal.valueOf(teamCompare.getScore()).divide(BigDecimal.valueOf(size),2, RoundingMode.HALF_UP));
        copyTeamCompare.setPointRate(teamCompare.getPointRate());
        copyTeamCompare.setTwoPointRate(teamCompare.getTwoPointRate());
        copyTeamCompare.setThreePointRate(teamCompare.getThreePointRate());

        baseTeamCompareResponseVO.setOneTeam(copyTeamCompare);


        List<BaseTournament> tournamentList1 = baseTournamentMapper.selectBaseTournamentListTeam(baseTournament.getContestId(),2,baseTournament.getTeamOneId());
        if (CollUtil.isEmpty(tournamentList)){
            return baseTeamCompareResponseVO;
        }
        //能力 柱状图
        BaseTeamCompareVO teamCompare1 = basePlayerLogMapper.getTeamCompareTeam(baseTournament.getTeamTwoId(), null);

        int size1 = tournamentList1.size();
        CopyBaseTeamCompareVO copyTeamCompare1 = new CopyBaseTeamCompareVO();
        copyTeamCompare1.setEf(BigDecimal.valueOf(teamCompare1.getEf()).divide(BigDecimal.valueOf(size1),2, RoundingMode.HALF_UP));
        copyTeamCompare1.setQd(BigDecimal.valueOf(teamCompare1.getQd()).divide(BigDecimal.valueOf(size1),2, RoundingMode.HALF_UP));
        copyTeamCompare1.setLb(BigDecimal.valueOf(teamCompare1.getLb()).divide(BigDecimal.valueOf(size1),2, RoundingMode.HALF_UP));
        copyTeamCompare1.setGm(BigDecimal.valueOf(teamCompare1.getGm()).divide(BigDecimal.valueOf(size1),2, RoundingMode.HALF_UP));
        copyTeamCompare1.setFq(BigDecimal.valueOf(teamCompare1.getFq()).divide(BigDecimal.valueOf(size1),2, RoundingMode.HALF_UP));
        copyTeamCompare1.setSf(BigDecimal.valueOf(teamCompare1.getSf()).divide(BigDecimal.valueOf(size1),2, RoundingMode.HALF_UP));
        copyTeamCompare1.setZg(BigDecimal.valueOf(teamCompare1.getZg()).divide(BigDecimal.valueOf(size1),2, RoundingMode.HALF_UP));
        copyTeamCompare1.setFg(BigDecimal.valueOf(teamCompare1.getFg()).divide(BigDecimal.valueOf(size1),2, RoundingMode.HALF_UP));
        copyTeamCompare1.setScore(BigDecimal.valueOf(teamCompare1.getScore()).divide(BigDecimal.valueOf(size1),2, RoundingMode.HALF_UP));
        copyTeamCompare1.setPointRate(teamCompare1.getPointRate());
        copyTeamCompare1.setTwoPointRate(teamCompare1.getTwoPointRate());
        copyTeamCompare1.setThreePointRate(teamCompare1.getThreePointRate());

        baseTeamCompareResponseVO.setTwoTeam(copyTeamCompare1);

        return baseTeamCompareResponseVO;
    }

    @Override
    public BaseTeamCompareResponseVO seasonComparison(Long tournamentId) {
        BaseTournament baseTournament = baseTournamentMapper.selectById(tournamentId);
        AssertUtil.notNull(baseTournament,"比赛id有误!");

        BaseTeamCompareResponseVO baseTeamCompareResponseVO = new BaseTeamCompareResponseVO();
        baseTeamCompareResponseVO.setOneTeam(basePlayerLogMapper.getTeamCompareTeam(baseTournament.getTeamOneId(), null));
        baseTeamCompareResponseVO.setTwoTeam(basePlayerLogMapper.getTeamCompareTeam(baseTournament.getTeamTwoId(), null));

        return baseTeamCompareResponseVO;
    }
}
