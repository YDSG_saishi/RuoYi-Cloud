package com.ruoyi.system.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.domain.BaseTeamGrouping;
import com.ruoyi.system.service.IBaseTeamGroupingService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;

import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 联赛球队分组Controller
 * 
 * @author ruoyi
 * @date 2024-05-20
 */
@ApiOperation("联赛球队分组")
@RestController
@RequestMapping("/teamGrouping")
public class BaseTeamGroupingController extends BaseController
{
    @Autowired
    private IBaseTeamGroupingService baseTeamGroupingService;

    /**
     * 查询联赛球队分组列表
     */
    @RequiresPermissions("system:teamGrouping:list")
    @GetMapping("/list")
    @Operation(summary = "查询联赛球队分组列表", description = "查询联赛球队分组列表")
    public TableDataInfo list(BaseTeamGrouping baseTeamGrouping)
    {
        startPage();
        List<BaseTeamGrouping> list = baseTeamGroupingService.selectBaseTeamGroupingList(baseTeamGrouping);
        return getDataTable(list);
    }

    /**
     * 导出联赛球队分组列表
     */
    @Operation(summary = "导出联赛球队分组列表", description = "导出联赛球队分组列表")
    @RequiresPermissions("system:teamGrouping:export")
    @Log(title = "联赛球队分组", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseTeamGrouping baseTeamGrouping)
    {
        List<BaseTeamGrouping> list = baseTeamGroupingService.selectBaseTeamGroupingList(baseTeamGrouping);
        ExcelUtil<BaseTeamGrouping> util = new ExcelUtil<BaseTeamGrouping>(BaseTeamGrouping.class);
        util.exportExcel(response, list, "联赛球队分组数据");
    }

    /**
     * 获取联赛球队分组详细信息
     */
    @Operation(summary = "获取联赛球队分组详细信息", description = "获取联赛球队分组详细信息")
    @RequiresPermissions("system:teamGrouping:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(baseTeamGroupingService.selectBaseTeamGroupingById(id));
    }

    /**
     * 新增联赛球队分组
     */
    @Operation(summary = "新增联赛球队分组", description = "新增联赛球队分组")
    @RequiresPermissions("system:teamGrouping:add")
    @Log(title = "联赛球队分组", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseTeamGrouping baseTeamGrouping)
    {
        return toAjax(baseTeamGroupingService.insertBaseTeamGrouping(baseTeamGrouping));
    }

    /**
     * 修改联赛球队分组
     */
    @Operation(summary = "修改联赛球队分组", description = "修改联赛球队分组")
    @RequiresPermissions("system:teamGrouping:edit")
    @Log(title = "联赛球队分组", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseTeamGrouping baseTeamGrouping)
    {
        return toAjax(baseTeamGroupingService.updateBaseTeamGrouping(baseTeamGrouping));
    }

    /**
     * 删除联赛球队分组
     */
    @Operation(summary = "删除联赛球队分组", description = "删除联赛球队分组")
    @RequiresPermissions("system:teamGrouping:remove")
    @Log(title = "联赛球队分组", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseTeamGroupingService.deleteBaseTeamGroupingByIds(ids));
    }
}
