package com.ruoyi.system.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.common.core.web.page.PageDomain;
import com.ruoyi.system.domain.BaseBasketballPlayer;
import com.ruoyi.system.domain.dto.Web.BasketBallPlayerDto;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Param;

/**
 * 球员Mapper接口
 * 
 * @author wangbj
 * @date 2024-04-16
 */
public interface BaseBasketballPlayerMapper extends BaseMapper<BaseBasketballPlayer>
{
    /**
     * 查询球员
     * 
     * @param id 球员主键
     * @return 球员
     */
    public BaseBasketballPlayer selectBaseBasketballPlayerById(Long id);

    /**
     * 查询球员列表
     * 
     * @param baseBasketballPlayer 球员
     * @return 球员集合
     */
    public List<BaseBasketballPlayer> selectBaseBasketballPlayerList(BaseBasketballPlayer baseBasketballPlayer);

    /**
     * 新增球员
     * 
     * @param baseBasketballPlayer 球员
     * @return 结果
     */
    public int insertBaseBasketballPlayer(BaseBasketballPlayer baseBasketballPlayer);

    /**
     * 修改球员
     * 
     * @param baseBasketballPlayer 球员
     * @return 结果
     */
    public int updateBaseBasketballPlayer(BaseBasketballPlayer baseBasketballPlayer);

    /**
     * 删除球员
     * 
     * @param id 球员主键
     * @return 结果
     */
    public int deleteBaseBasketballPlayerById(Long id);

    /**
     * 批量删除球员
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseBasketballPlayerByIds(Long[] ids);

    /**
     * 根据球队id获取球员信息 过滤掉出勤记录未到场的
     * @param teamOneId
     * @return
     */
    List<BaseBasketballPlayer> getPlayerByTimeId(@Param("teamOneId") Long teamOneId, @Param("tournamentId") Long tournamentId);

    /**
     * 根据球队id获取球员信息 无限制
     * @param playerId
     * @return
     */
    List<BaseBasketballPlayer> getPlayerAll(Long playerId);

    /**
     * 获取球员列表
     * @param param
     */
    List<BaseBasketballPlayer> playerList(@Param("param")BasketBallPlayerDto param);

    /**
     * 查询当前队伍中 是否存在重复的号码
     * @param teamId
     * @param playerNumber
     * @return
     */
    Integer selectByPlayerNumber(@Param("teamId") Long teamId,@Param("playerNumber")  String playerNumber);
}
