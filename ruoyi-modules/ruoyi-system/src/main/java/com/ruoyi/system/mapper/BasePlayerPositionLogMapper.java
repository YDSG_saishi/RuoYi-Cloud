package com.ruoyi.system.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.BasePlayerPositionLog;
import com.ruoyi.system.domain.vo.BasePositionMsgVO;
import com.ruoyi.system.domain.vo.BaseSubstitutionRequestVO;
import org.apache.ibatis.annotations.Param;

/**
 * 球员阵位记录Mapper接口
 * 
 * @author wangbj
 * @date 2024-04-16
 */
public interface BasePlayerPositionLogMapper extends BaseMapper<BasePlayerPositionLog>
{
    /**
     * 查询球员阵位记录
     * 
     * @param id 球员阵位记录主键
     * @return 球员阵位记录
     */
    public BasePlayerPositionLog selectBasePlayerPositionLogById(Long id);

    /**
     * 查询球员阵位记录列表
     * 
     * @param basePlayerPositionLog 球员阵位记录
     * @return 球员阵位记录集合
     */
    public List<BasePlayerPositionLog> selectBasePlayerPositionLogList(BasePlayerPositionLog basePlayerPositionLog);

    /**
     * 新增球员阵位记录
     * 
     * @param basePlayerPositionLog 球员阵位记录
     * @return 结果
     */
    public int insertBasePlayerPositionLog(BasePlayerPositionLog basePlayerPositionLog);

    /**
     * 修改球员阵位记录
     * 
     * @param basePlayerPositionLog 球员阵位记录
     * @return 结果
     */
    public int updateBasePlayerPositionLog(BasePlayerPositionLog basePlayerPositionLog);

    /**
     * 删除球员阵位记录
     * 
     * @param id 球员阵位记录主键
     * @return 结果
     */
    public int deleteBasePlayerPositionLogById(Long id);

    /**
     * 批量删除球员阵位记录
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBasePlayerPositionLogByIds(Long[] ids);

    void deletePositionByToumentId(Long tournamentId);

    /**
     * 获取球员阵位信息
     * @param id
     * @param contestId
     * @param id1
     * @return
     */
    List<BasePositionMsgVO> getCreatePositionLog(@Param("tournamentId") Long tournamentId,@Param("contestId") Long contestId, @Param("id") Long id);

    /**
     * 批量新增
     * @param list
     * @return
     */
    int insertBatch(List<BasePlayerPositionLog> list );

    /**
     * 查询球员
     * @param requestVO
     * @return
     */
    List<BasePlayerPositionLog> selectByPlayer( BaseSubstitutionRequestVO requestVO);
}
