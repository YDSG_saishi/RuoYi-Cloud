package com.ruoyi.system.domain.vo;

import com.ruoyi.system.domain.BaseBasketballTeam;
import com.ruoyi.system.domain.BaseTournament;
import com.ruoyi.system.domain.BaseTournamentPersonnel;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author Wangbj
 * @date 2024年04月22日 13:24
 */
@Data
@Accessors(chain = true)
public class BaseTournamentResponseVO extends BaseTournament {

    @Schema(name = "oneTeam", description = "球队1信息")
    private BaseBasketballTeam oneTeam;

    @Schema(name = "twoTeam", description = "球队2信息")
    private BaseBasketballTeam twoTeam;

    @Schema(name = "tournamentPersonnel", description = "工作人员信息")
    private List<BaseTournamentPersonnel> tournamentPersonnelList;

    /**
     * 3v3链接
     */
    private String threeUrl;
    /**
     * 5v5链接
     */
    private String fiveUrl;
}
