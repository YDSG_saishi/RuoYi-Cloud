package com.ruoyi.system.domain;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 赛季对象 base_season
 * 
 * @author wangbj
 * @date 2024-04-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("base_season")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BaseSeason extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @TableId(type = IdType.AUTO)
    @Schema(name = "id", description = "主键")
    private Long id;

    /** 赛季名称 */
    @Excel(name = "赛季名称")
    @Schema(name = "seasonName", description = "赛季名称")
    private String seasonName;

    /** 赛季开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "赛季开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    @Schema(name = "seasonBeginTime", description = "赛季开始时间")
    private Date seasonBeginTime;

    /** 赛季结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "赛季结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    @Schema(name = "seasonEndTime", description = "赛季结束时间")
    private Date seasonEndTime;


    /** 赛季状态 */
    @Excel(name = "赛季状态", width = 30, dateFormat = "yyyy-MM-dd")
    @Schema(name = "type", description = "赛季状态：0:未开始;1:进行中;2:已结束;")
    private int type;

    /** 机构id */
    @Excel(name = "机构id")
    @Schema(name = "organizationId", description = "机构id")
    private Long organizationId;

    /** 联赛id */
    @Excel(name = "联赛id")
    @Schema(name = "contestId", description = "联赛id")
    private Long contestId;

    /** 创建人id */
    @Excel(name = "创建人id")
    @Schema(name = "createUserId", description = "创建人id")
    private Long createUserId;

    /** 创建人姓名 */
    @Excel(name = "创建人姓名")
    @Schema(name = "createUserName", description = "创建人姓名")
    private String createUserName;

    /** 修改人id */
    @Excel(name = "修改人id")
    @Schema(name = "updateUserId", description = "修改人id")
    private Long updateUserId;

    /** 修改人姓名 */
    @Excel(name = "修改人姓名")
    @Schema(name = "updateUserName", description = "修改人姓名")
    private String updateUserName;

    /** 是否已删除 0:未删除;1:已删除 */
    @Excel(name = "是否已删除 0:未删除;1:已删除")
    @Schema(name = "isDeleted", description = "是否已删除 0:未删除;1:已删除")
    private Long isDeleted;
}
