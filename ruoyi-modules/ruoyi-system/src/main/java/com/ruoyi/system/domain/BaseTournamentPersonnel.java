package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 赛事工作人员对象 base_tournament_personnel
 * 
 * @author ruoyi
 * @date 2024-04-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("base_tournament_personnel")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BaseTournamentPersonnel extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @TableId(type = IdType.AUTO)
    @Schema(name = "id", description = "主键")
    private Long id;

    /** 赛事id */
    @Excel(name = "赛事id")
    @Schema(name = "tournamentId", description = "赛事id")
    private Long tournamentId;

    /** 赛季id */
    @Excel(name = "赛季id")
    @Schema(name = "seasonId", description = "赛季id")
    private Long seasonId;

    /** 类型：1:主裁;2:副裁;3:纪录台;4:技术代表;5:赛季管理员 */
    @Excel(name = "类型：1:主裁;2:副裁;3:纪录台;4:技术代表;5:赛季管理员")
    @Schema(name = "type", description = "类型：1:主裁;2:副裁;3:纪录台;4:技术代表;5:赛季管理员")
    private Long type;

    /** 工作人员层级：0:联赛工作人员;1:比赛工作人员; */
    @Excel(name = "工作人员层级：0:联赛工作人员;1:比赛工作人员;")
    @Schema(name = "level", description = "工作人员层级：0:联赛工作人员;1:比赛工作人员;")
    private Long level;

    /** 机构id */
    @Excel(name = "机构id")
    @Schema(name = "organizationId", description = "机构id")
    private Long organizationId;

    /** 当前角色绑定的用户id */
    @Excel(name = "当前角色绑定的用户id")
    @Schema(name = "userId", description = "当前角色绑定的用户id")
    private Long userId;

    /** 联赛id */
    @Excel(name = "联赛id")
    @Schema(name = "contestId", description = "联赛id")
    private Long contestId;

    /** 创建人id */
    @Excel(name = "创建人id")
    @Schema(name = "createUserId", description = "创建人id")
    private Long createUserId;

    /** 创建人姓名 */
    @Excel(name = "创建人姓名")
    @Schema(name = "createUserName", description = "创建人姓名")
    private String createUserName;

    /** 修改人id */
    @Excel(name = "修改人id")
    @Schema(name = "updateUserId", description = "修改人id")
    private Long updateUserId;

    /** 修改人姓名 */
    @Excel(name = "修改人姓名")
    @Schema(name = "updateUserName", description = "修改人姓名")
    private String updateUserName;

    /** 是否已删除 0:未删除;1:已删除 */
    @Excel(name = "是否已删除 0:未删除;1:已删除")
    @Schema(name = "isDeleted", description = "是否已删除 0:未删除;1:已删除")
    private Long isDeleted;
    //手机号码
    @TableField(exist = false)
    private String phone;
}
