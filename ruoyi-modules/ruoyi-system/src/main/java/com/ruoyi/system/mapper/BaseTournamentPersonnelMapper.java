package com.ruoyi.system.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.BaseTournamentPersonnel;
import com.ruoyi.system.domain.vo.BaseTournamentPersonnelAttendanceVO;

/**
 * 赛事工作人员Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-16
 */
public interface BaseTournamentPersonnelMapper extends BaseMapper<BaseTournamentPersonnel>
{
    /**
     * 查询赛事工作人员
     * 
     * @param id 赛事工作人员主键
     * @return 赛事工作人员
     */
    public BaseTournamentPersonnel selectBaseTournamentPersonnelById(Long id);

    /**
     * 查询赛事工作人员列表
     * 
     * @param baseTournamentPersonnel 赛事工作人员
     * @return 赛事工作人员集合
     */
    public List<BaseTournamentPersonnel> selectBaseTournamentPersonnelList(BaseTournamentPersonnel baseTournamentPersonnel);

    /**
     * 新增赛事工作人员
     * 
     * @param baseTournamentPersonnel 赛事工作人员
     * @return 结果
     */
    public int insertBaseTournamentPersonnel(BaseTournamentPersonnel baseTournamentPersonnel);

    /**
     * 修改赛事工作人员
     * 
     * @param baseTournamentPersonnel 赛事工作人员
     * @return 结果
     */
    public int updateBaseTournamentPersonnel(BaseTournamentPersonnel baseTournamentPersonnel);

    /**
     * 删除赛事工作人员
     * 
     * @param id 赛事工作人员主键
     * @return 结果
     */
    public int deleteBaseTournamentPersonnelById(Long id);

    /**
     * 批量删除赛事工作人员
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseTournamentPersonnelByIds(Long[] ids);

    /**
     * 出勤统计
     * @param seasonId
     * @return
     */
    List<BaseTournamentPersonnelAttendanceVO> getAttendanceCount(Long seasonId);
}
