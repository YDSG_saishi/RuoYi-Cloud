package com.ruoyi.system.domain;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 赛事对象 base_tournament
 * 
 * @author wangbj
 * @date 2024-04-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("base_tournament")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BaseTournament extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @TableId(type = IdType.AUTO)
    @Schema(name = "id", description = "主键")
    private Long id;

    /** 赛事名称 */
    @Excel(name = "赛事名称")
    @Schema(name = "tournamentName", description = "赛事名称")
    private String tournamentName;

    /** 赛事开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "赛事开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    @Schema(name = "tournamentBeginTime", description = "赛事开始时间")
    private Date tournamentBeginTime;

    /** 赛事结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "赛事结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    @Schema(name = "tournamentEndTime", description = "赛事结束时间")
    private Date tournamentEndTime;

    /** 比赛类型:1:小组赛;2:淘汰赛;3:四强赛; */
    @Excel(name = "比赛类型:1:小组赛;2:淘汰赛;3:四强赛;")
    @Schema(name = "type", description = "比赛类型:1:小组赛;2:淘汰赛;3:四强赛;")
    private Long type;

    /**
     * 比赛阶段备注
     */
    @Schema(name = "typeRemake", description = "比赛阶段备注")
    private String typeRemake;

    /** 胜利球队id */
    @Excel(name = "胜利球队id")
    @Schema(name = "isWin", description = "胜利球队id")
    private Long isWin;

    /** 比赛地点 */
    @Excel(name = "比赛地点")
    @Schema(name = "address", description = "比赛地点")
    private String address;

    /** 赛季id */
    @Excel(name = "赛季id")
    @Schema(name = "seasonId", description = "赛季id")
    private Long seasonId;

    /** 机构id */
    @Excel(name = "机构id")
    @Schema(name = "organizationId", description = "机构id")
    private Long organizationId;

    /** 联赛id */
    @Excel(name = "联赛id")
    @Schema(name = "contestId", description = "联赛id")
    private Long contestId;

    /** 比赛小节 */
    @Excel(name = "比赛小节")
    @Schema(name = "subsection", description = "比赛小节")
    private Long subsection;

    /** 队伍1id */
    @Excel(name = "队伍1id")
    @Schema(name = "teamOneId", description = "队伍1id")
    private Long teamOneId;

    /** 队伍1比分 */
    @Excel(name = "队伍1比分")
    @Schema(name = "teamOneScore", description = "队伍1比分")
    private Long teamOneScore;

    /** 队伍1队服颜色 */
    @Excel(name = "队伍1队服颜色")
    @Schema(name = "teamOneColour", description = "队伍1队服颜色")
    private String teamOneColour;

    /** 队伍1最高领先分数 */
    @Excel(name = "队伍1最高领先分数")
    @Schema(name = "teamOneScoreMax", description = "队伍1最高领先分数")
    private Long teamOneScoreMax;

    /** 队伍2id */
    @Excel(name = "队伍2id")
    @Schema(name = "teamTwoId", description = "队伍2id")
    private Long teamTwoId;

    /** 队伍2比分 */
    @Excel(name = "队伍2比分")
    @Schema(name = "teamTwoScore", description = "队伍2比分")
    private Long teamTwoScore;

    /** 队伍2队服颜色 */
    @Excel(name = "队伍2队服颜色")
    @Schema(name = "teamTwoColour", description = "队伍2队服颜色")
    private String teamTwoColour;

    /** 队伍2最高领先分数 */
    @Excel(name = "队伍2最高领先分数")
    @Schema(name = "teamTwoScoreMax", description = "队伍2最高领先分数")
    private Long teamTwoScoreMax;

    /** 比赛状态：0:未开始;1:已开始;2:已结束; */
    @Excel(name = "比赛状态：0:未开始;1:已开始;2:已结束;")
    @Schema(name = "state", description = "比赛状态：0:未开始;1:已开始;2:已结束;")
    private Integer state;

    @Schema(name = "statisticsModel", description = "统计模式 1标准 2进阶")
    private Integer statisticsModel;

    /** 创建人id */
    @Excel(name = "创建人id")
    @Schema(name = "createUserId", description = "创建人id")
    private Long createUserId;

    /** 创建人姓名 */
    @Excel(name = "创建人姓名")
    @Schema(name = "createUserName", description = "创建人姓名")
    private String createUserName;

    /** 修改人id */
    @Excel(name = "修改人id")
    @Schema(name = "updateUserId", description = "修改人id")
    private Long updateUserId;

    /** 修改人姓名 */
    @Excel(name = "修改人姓名")
    @Schema(name = "updateUserName", description = "修改人姓名")
    private String updateUserName;

    /** 是否已删除 0:未删除;1:已删除 */
    @Excel(name = "是否已删除 0:未删除;1:已删除")
    @Schema(name = "isDeleted", description = "是否已删除 0:未删除;1:已删除")
    private Long isDeleted = 0L;

    private Long liveNum = 0L;

    @Excel(name = "是否开启聊天 0是 1否")
    @Schema(name = "chatFlag", description = "是否开启聊天 0是 1否")
    private Integer chatFlag;

    /**
     * 设备开关 0默认 1摄像机
     */
    private Integer equipmentSwitch;
    /**
     * 精彩回放时间间隔单位s
     */
    private Integer timeInterval;

    @Excel(name = "队伍1名称")
    @Schema(name = "teamOneName", description = "队伍1名称")
    @TableField(exist = false)
    private String teamOneName;
    @Excel(name = "队伍1头像")
    @Schema(name = "teamOneName", description = "队伍1头像")
    @TableField(exist = false)
    private String teamOneLogo;

    @Excel(name = "队伍2名称")
    @Schema(name = "teamTwoName", description = "队伍2名称")
    @TableField(exist = false)
    private String teamTwoName;
    @Excel(name = "队伍2头像")
    @Schema(name = "teamTwoLogo", description = "队伍2头像")
    @TableField(exist = false)
    private String teamTwoLogo;


    /**
     * 主队犯规
     */
    @TableField(exist = false)
    private Integer oneFoul;
    /**
     * 客队犯规
     */
    @TableField(exist = false)
    private Integer twoFoul;
    /**
     * 主队排名
     */
    @TableField(exist = false)
    private String teamOneRanking;
    /**
     * 客队排名
     */
    @TableField(exist = false)
    private String teamTwoRanking;
}
