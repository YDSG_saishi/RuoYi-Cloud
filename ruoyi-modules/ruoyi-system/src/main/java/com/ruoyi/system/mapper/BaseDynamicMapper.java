package com.ruoyi.system.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.BaseDynamic;

/**
 * 动态Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-26
 */
public interface BaseDynamicMapper extends BaseMapper<BaseDynamic>
{
    /**
     * 查询动态
     * 
     * @param id 动态主键
     * @return 动态
     */
    public BaseDynamic selectBaseDynamicById(Long id);

    /**
     * 查询动态列表
     * 
     * @param baseDynamic 动态
     * @return 动态集合
     */
    public List<BaseDynamic> selectBaseDynamicList(BaseDynamic baseDynamic);

    /**
     * 新增动态
     * 
     * @param baseDynamic 动态
     * @return 结果
     */
    public int insertBaseDynamic(BaseDynamic baseDynamic);

    /**
     * 修改动态
     * 
     * @param baseDynamic 动态
     * @return 结果
     */
    public int updateBaseDynamic(BaseDynamic baseDynamic);

    /**
     * 删除动态
     * 
     * @param id 动态主键
     * @return 结果
     */
    public int deleteBaseDynamicById(Long id);

    /**
     * 批量删除动态
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseDynamicByIds(Long[] ids);
}
