package com.ruoyi.system.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.domain.BaseCoach;

/**
 * 教练组成员Service接口
 * 
 * @author wangbj
 * @date 2024-04-16
 */
public interface IBaseCoachService extends IService<BaseCoach>
{
    /**
     * 查询教练组成员
     * 
     * @param id 教练组成员主键
     * @return 教练组成员
     */
    public BaseCoach selectBaseCoachById(Long id);

    /**
     * 查询教练组成员列表
     * 
     * @param baseCoach 教练组成员
     * @return 教练组成员集合
     */
    public List<BaseCoach> selectBaseCoachList(BaseCoach baseCoach);

    /**
     * 新增教练组成员
     * 
     * @param baseCoach 教练组成员
     * @return 结果
     */
    public int insertBaseCoach(BaseCoach baseCoach);

    /**
     * 修改教练组成员
     * 
     * @param baseCoach 教练组成员
     * @return 结果
     */
    public int updateBaseCoach(BaseCoach baseCoach);

    /**
     * 批量删除教练组成员
     * 
     * @param ids 需要删除的教练组成员主键集合
     * @return 结果
     */
    public int deleteBaseCoachByIds(Long[] ids);

    /**
     * 删除教练组成员信息
     * 
     * @param id 教练组成员主键
     * @return 结果
     */
    public int deleteBaseCoachById(Long id);
}
