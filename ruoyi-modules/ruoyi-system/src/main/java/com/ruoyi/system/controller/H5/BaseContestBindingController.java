package com.ruoyi.system.controller.H5;

import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.system.domain.BaseBanner;
import com.ruoyi.system.domain.BaseContest;
import com.ruoyi.system.domain.dto.Web.BannerDto;
import com.ruoyi.system.service.IBaseBannerService;
import com.ruoyi.system.service.IBaseContestBindingService;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * h5 联赛绑定
 */
@ApiOperation("h5联赛绑定")
@RestController
@RequestMapping("/h5/contestBinding")
public class BaseContestBindingController extends BaseController
{
    @Autowired
    private IBaseContestBindingService iBaseContestBindingService;

    /**
     * 查询联赛绑定列表
     */
    @GetMapping("/list")
    @Operation(summary = "查询联赛绑定列表", description = "查询联赛绑定列表")
    public TableDataInfo list(BaseContest dto)
    {
        startPage();
        List<BaseContest> list = iBaseContestBindingService.selectList(dto);
        return getDataTable(list);
    }

}
