package com.ruoyi.system.domain.vo;

import com.ruoyi.system.domain.BaseBasketballTeam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author Wangbj
 * @date 2024年05月07日 09:23
 */
@Data
@Accessors(chain = true)
public class BaseTournamentPersonnelResponseVO {
    @Schema(name = "userId", description = "用户id")
    private Long userId;

    @Schema(name = "nickName", description = "用户昵称")
    private String nickName;

    @Schema(name = "one", description = "主裁")
    private Integer one;

    @Schema(name = "two", description = "副裁")
    private Integer two;

    @Schema(name = "three", description = "纪录台")
    private Integer three;

    @Schema(name = "four", description = "技术代表")
    private Integer four;


}
