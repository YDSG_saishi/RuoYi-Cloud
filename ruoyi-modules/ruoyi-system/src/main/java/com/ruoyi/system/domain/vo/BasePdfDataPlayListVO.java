package com.ruoyi.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Wangbj
 * @date 2024年04月26日 00:57
 */
@Data
@Accessors(chain = true)
public class BasePdfDataPlayListVO {

    /**
     * 号码
     */
    private String playerNumber;
    /**
     * 球员名称
     */
    private String playerName;
    /**
     * 二分球出手
     */
    private Long allTwoPoint = 0L;
    /**
     * 二分球命中
     */
    private Long twoPoint = 0L;
    /**
     * 二分球得分
     */
    private Long twoPointScore = 0L;
    /**
     * 三分球出手
     */
    private Long allThreePoint = 0L;
    /**
     * 三分球命中
     */
    private Long threePoint = 0L;
    /**
     * 三分球得分
     */
    private Long threePointScore = 0L;
    /**
     * 罚球出手
     */
    private Long allPoint = 0L;
    /**
     * 罚球命中
     */
    private Long point = 0L;
    /**
     * 罚球得分
     */
    private Long pointScore = 0L;
    /**
     * 助攻
     */
    private Long assist = 0L;
    /**
     * 篮板
     */
    private Long backboard = 0L;
    /**
     * 抢断
     */
    private Long stral = 0L;
    /**
     * 失误
     */
    private Long mistake = 0L;
    /**
     * 盖帽
     */
    private Long blockShot = 0L;
    /**
     * 犯规
     */
    private Long foul = 0L;
    /**
     * 总计得分
     */
    private Long score = 0L;
    /**
     * 二分命中率
     */
    private BigDecimal twoPointRate = BigDecimal.ZERO;
    /**
     * 三分命中率
     */
    private BigDecimal threePointRate = BigDecimal.ZERO;
    /**
     * 罚球命中率
     */
    private BigDecimal pointRate = BigDecimal.ZERO;
    /**
     * 效率值
     */
    private Long per = 0L;
    /**
     * 真实命中率
     */
    private BigDecimal rate = BigDecimal.ZERO;
}
