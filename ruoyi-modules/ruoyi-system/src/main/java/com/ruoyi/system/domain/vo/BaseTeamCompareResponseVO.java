package com.ruoyi.system.domain.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author Wangbj
 * @date 2024年05月08日 22:34
 */
@Data
@Accessors(chain = true)
public class BaseTeamCompareResponseVO {
    @Schema(name = "oneTeam", description = "第一个队伍")
    BaseTeamCompareVO oneTeam;

    @Schema(name = "twoTeam", description = "第二个队伍")
    BaseTeamCompareVO twoTeam;
}
