package com.ruoyi.system.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.domain.BasePlayerPositionLog;
import com.ruoyi.system.domain.vo.BasePositionMsgVO;
import com.ruoyi.system.domain.vo.BasePositionResponseVO;
import com.ruoyi.system.domain.vo.BaseSubstitutionRequestVO;

/**
 * 球员阵位记录Service接口
 * 
 * @author wangbj
 * @date 2024-04-16
 */
public interface IBasePlayerPositionLogService extends IService<BasePlayerPositionLog>
{
    /**
     * 查询球员阵位记录
     * 
     * @param id 球员阵位记录主键
     * @return 球员阵位记录
     */
    public BasePlayerPositionLog selectBasePlayerPositionLogById(Long id);

    /**
     * 查询球员阵位记录列表
     * 
     * @param basePlayerPositionLog 球员阵位记录
     * @return 球员阵位记录集合
     */
    public List<BasePlayerPositionLog> selectBasePlayerPositionLogList(BasePlayerPositionLog basePlayerPositionLog);

    /**
     * 新增球员阵位记录
     * 
     * @param basePlayerPositionLog 球员阵位记录
     * @return 结果
     */
    public int insertBasePlayerPositionLog(BasePlayerPositionLog basePlayerPositionLog);

    /**
     * 修改球员阵位记录
     * 
     * @param basePlayerPositionLog 球员阵位记录
     * @return 结果
     */
    public int updateBasePlayerPositionLog(BasePlayerPositionLog basePlayerPositionLog);

    /**
     * 批量删除球员阵位记录
     * 
     * @param ids 需要删除的球员阵位记录主键集合
     * @return 结果
     */
    public int deleteBasePlayerPositionLogByIds(Long[] ids);

    /**
     * 删除球员阵位记录信息
     * 
     * @param id 球员阵位记录主键
     * @return 结果
     */
    public int deleteBasePlayerPositionLogById(Long id);

    /**
     * 球员阵位
     * @param tournamentId
     * @return
     */
    BasePositionResponseVO createPosition(Long tournamentId);

    /**
     * 调换球员
     * @param requestVO
     * @return
     */
    List<BasePositionMsgVO> substitution(BaseSubstitutionRequestVO requestVO);
    /**
     * 全体换人
     * @param requestVO
     * @return
     */
    BasePositionResponseVO substitutionPlayer(BaseSubstitutionRequestVO requestVO);
}
