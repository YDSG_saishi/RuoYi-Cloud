package com.ruoyi.system.controller.H5;

import cn.hutool.core.util.ObjectUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.domain.BaseBasketballTeam;
import com.ruoyi.system.domain.vo.BasePdfDataVO;
import com.ruoyi.system.service.IBaseBasketballTeamService;
import com.ruoyi.system.service.IBaseTournamentService;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;
import java.io.InputStream;
import java.util.List;

/**
 * 参赛球队
 *
 * @author wangbj
 * @date 2024-04-16
 */
@Slf4j
@ApiOperation("球队")
@RestController
@RequestMapping("/h5/basketballTeam")
public class BaseBasketballTeamH5Controller extends BaseController {
    @Autowired
    private IBaseBasketballTeamService baseBasketballTeamService;
    @Resource
    private IBaseTournamentService iBaseTournamentService;

    /**
     * 查询球队列表
     */
    @RequiresPermissions("system:team:list")
    @GetMapping("/list")
    @Operation(summary = "查询球队列表", description = "查询球队列表")
    public TableDataInfo list(BaseBasketballTeam baseBasketballTeam) {
        startPage();
        if (ObjectUtil.isNull(baseBasketballTeam.getLeagueId())) {
            throw new RuntimeException("联赛id不能为空");
        }
        List<BaseBasketballTeam> list = baseBasketballTeamService.selectBaseBasketballTeamList(baseBasketballTeam);
        return getDataTable(list);
    }

    /**
     * 查询球队数据
     */
    @GetMapping("/teamData")
    @Operation(summary = "查询球队列表", description = "查询球队列表")
    public AjaxResult teamData(@RequestParam("teamId") Long teamId) {
        return AjaxResult.success(iBaseTournamentService.teamData(teamId));
    }

    /**
     * 查询成员数据
     */
    @GetMapping("/playTeamData")
    @Operation(summary = "查询成员数据", description = "查询成员数据")
    public AjaxResult playTeamData(@RequestParam("teamId") Long teamId) {
        return AjaxResult.success(iBaseTournamentService.playTeamData(teamId));
    }

    /**
     * pdf数据
     */
    @GetMapping("/getPdfData")
    @Operation(summary = "pdf数据", description = "pdf数据")
    public AjaxResult getPdfData(@RequestParam("tournamentId") Long tournamentId) {
        return AjaxResult.success(iBaseTournamentService.getPdfData(tournamentId));
    }

    /**
     * 获取pdf
     */
    @PostMapping("/downloadPdf")
    @Operation(summary = "获取pdf", description = "获取pdf")
    public AjaxResult downloadPdf(@RequestParam("file") MultipartFile file,String filename) {
        log.info("file+"+file);
        log.info("filename+"+filename);
        return AjaxResult.success(iBaseTournamentService.downloadPdf(file,filename));
    }
}
