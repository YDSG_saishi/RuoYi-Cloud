package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.csp.sentinel.util.AssertUtil;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.system.domain.BaseBasketballTeam;
import com.ruoyi.system.domain.vo.BasketBallPlayerInviteVO;
import com.ruoyi.system.service.IBaseBasketballTeamService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;

import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 球队Controller
 *
 * @author wangbj
 * @date 2024-04-16
 */
@ApiOperation("球队")
@RestController
@RequestMapping("/basketballTeam")
public class BaseBasketballTeamController extends BaseController {
    @Autowired
    private IBaseBasketballTeamService baseBasketballTeamService;

    /**
     * 查询球队列表
     */
    @RequiresPermissions("system:team:list")
    @GetMapping("/list")
    @Operation(summary = "查询球队列表", description = "查询球队列表")
    public TableDataInfo list(BaseBasketballTeam baseBasketballTeam) {
//        startPage();
        if (ObjectUtil.isNull(baseBasketballTeam.getLeagueId())) {
            throw new RuntimeException("联赛id不能为空");
        }
        List<BaseBasketballTeam> list = baseBasketballTeamService.selectBaseBasketballTeamList(baseBasketballTeam);
        return getDataTable(list);
    }

    /**
     * 导出球队列表
     */
    @Operation(summary = "导出球队列表", description = "导出球队列表")
    @RequiresPermissions("system:team:export")
    @Log(title = "球队", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseBasketballTeam baseBasketballTeam) {
        List<BaseBasketballTeam> list = baseBasketballTeamService.selectBaseBasketballTeamList(baseBasketballTeam);
        ExcelUtil<BaseBasketballTeam> util = new ExcelUtil<BaseBasketballTeam>(BaseBasketballTeam.class);
        util.exportExcel(response, list, "球队数据");
    }

    /**
     * 获取球队详细信息
     */
    @Operation(summary = "获取球队详细信息", description = "获取球队详细信息")
    @RequiresPermissions("system:team:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(baseBasketballTeamService.selectBaseBasketballTeamById(id));
    }

    /**
     * 新增球队
     */
    @Operation(summary = "新增球队", description = "新增球队")
    @RequiresPermissions("system:team:add")
    @Log(title = "球队", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseBasketballTeam baseBasketballTeam) {
        int team = baseBasketballTeamService.insertBaseBasketballTeam(baseBasketballTeam);
        if (team == -1){
            return AjaxResult.error("名称不能为空、不能重复");
        }
        return toAjax(team);
    }

    /**
     * 修改球队
     */
    @Operation(summary = "修改球队", description = "修改球队")
    @RequiresPermissions("system:team:edit")
    @Log(title = "球队", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseBasketballTeam baseBasketballTeam) {
        int team = baseBasketballTeamService.updateBaseBasketballTeam(baseBasketballTeam);
        if (team == -1){
            return AjaxResult.error("名称不能重复");
        }
        return toAjax(team);
    }

    /**
     * 删除球队
     */
    @Operation(summary = "删除球队", description = "删除球队")
    @RequiresPermissions("system:team:remove")
    @Log(title = "球队", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(baseBasketballTeamService.deleteBaseBasketballTeamByIds(ids));
    }

    /**
     * 修改球队是否在报名列表展示
     */
    @Operation(summary = "修改球队是否在报名列表展示", description = "修改球队是否在报名列表展示")
    @RequiresPermissions("system:team:updateShow")
    @Log(title = "修改球队是否在报名列表展示", businessType = BusinessType.UPDATE)
    @PostMapping("/updateShow")
    public AjaxResult updateShow(@RequestBody BaseBasketballTeam baseBasketballTeam) {
        AssertUtil.notNull(baseBasketballTeam.getId(), "球队id不能为空");
        AssertUtil.notNull(baseBasketballTeam.getIsShow(), "是否在报名列表展示不能为空");
        return toAjax(baseBasketballTeamService.updateById(baseBasketballTeam));
    }

    /**
     * 创建邀请加入球队链接
     * @param basketBallPlayerInviteVO
     * @return
     */
    @PostMapping("create/invite")
    public R<String> createBasketBallTeamInvite(@RequestBody BasketBallPlayerInviteVO basketBallPlayerInviteVO) {

        return R.ok(baseBasketballTeamService.createBasketBallTeamInvite(basketBallPlayerInviteVO));
    }

    /**
     * 接收邀请--加入球队
     * @param basketBallPlayerInviteVO
     * @return
     */
    @PostMapping("join/team")
    public R<Boolean> agreeTeamInvite(@RequestBody BasketBallPlayerInviteVO basketBallPlayerInviteVO) {

        return R.ok(baseBasketballTeamService.agreeTeamInvite(basketBallPlayerInviteVO));
    }
}
