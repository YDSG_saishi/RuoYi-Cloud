package com.ruoyi.system.domain.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author Wangbj
 * @date 2024年04月27日 23:21
 */
@Accessors(chain = true)
@Data
public class BaseSubstitutionRequestVO {


    @Schema(name = "tournamentId", description = "比赛id")
    private Long tournamentId;

    @Schema(name = "teamId", description = "调换队伍id")
    private Long teamId;

    @Schema(name = "playerOneId", description = "调换球员Id")
    private Long playerOneId;

    @Schema(name = "playerTwoId", description = "被调换球员Id")
    private Long playerTwoId;



}
