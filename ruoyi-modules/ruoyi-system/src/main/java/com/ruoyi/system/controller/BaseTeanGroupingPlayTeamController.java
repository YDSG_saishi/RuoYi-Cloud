package com.ruoyi.system.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.domain.BaseTeanGroupingPlayTeam;
import com.ruoyi.system.service.IBaseTeanGroupingPlayTeamService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;

import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 联赛球队分组和球队关联Controller
 * 
 * @author ruoyi
 * @date 2024-05-20
 */
@ApiOperation("联赛球队分组和球队关联")
@RestController
@RequestMapping("/teanGroupingPlayTeam")
public class BaseTeanGroupingPlayTeamController extends BaseController
{
    @Autowired
    private IBaseTeanGroupingPlayTeamService baseTeanGroupingPlayTeamService;

    /**
     * 查询联赛球队分组和球队关联列表
     */
    @RequiresPermissions("system:teanGroupingPlayTeam:list")
    @GetMapping("/list")
    @Operation(summary = "查询联赛球队分组和球队关联列表", description = "查询联赛球队分组和球队关联列表")
    public TableDataInfo list(BaseTeanGroupingPlayTeam baseTeanGroupingPlayTeam)
    {
        startPage();
        List<BaseTeanGroupingPlayTeam> list = baseTeanGroupingPlayTeamService.selectBaseTeanGroupingPlayTeamList(baseTeanGroupingPlayTeam);
        return getDataTable(list);
    }

    /**
     * 导出联赛球队分组和球队关联列表
     */
    @Operation(summary = "导出联赛球队分组和球队关联列表", description = "导出联赛球队分组和球队关联列表")
    @RequiresPermissions("system:teanGroupingPlayTeam:export")
    @Log(title = "联赛球队分组和球队关联", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseTeanGroupingPlayTeam baseTeanGroupingPlayTeam)
    {
        List<BaseTeanGroupingPlayTeam> list = baseTeanGroupingPlayTeamService.selectBaseTeanGroupingPlayTeamList(baseTeanGroupingPlayTeam);
        ExcelUtil<BaseTeanGroupingPlayTeam> util = new ExcelUtil<BaseTeanGroupingPlayTeam>(BaseTeanGroupingPlayTeam.class);
        util.exportExcel(response, list, "联赛球队分组和球队关联数据");
    }

    /**
     * 获取联赛球队分组和球队关联详细信息
     */
    @Operation(summary = "获取联赛球队分组和球队关联详细信息", description = "获取联赛球队分组和球队关联详细信息")
    @RequiresPermissions("system:teanGroupingPlayTeam:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(baseTeanGroupingPlayTeamService.selectBaseTeanGroupingPlayTeamById(id));
    }

    /**
     * 新增联赛球队分组和球队关联
     */
    @Operation(summary = "新增联赛球队分组和球队关联", description = "新增联赛球队分组和球队关联")
    @RequiresPermissions("system:teanGroupingPlayTeam:add")
    @Log(title = "联赛球队分组和球队关联", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody List<BaseTeanGroupingPlayTeam> baseTeanGroupingPlayTeam)
    {
        return toAjax(baseTeanGroupingPlayTeamService.insertBaseTeanGroupingPlayTeam(baseTeanGroupingPlayTeam));
    }

    /**
     * 修改联赛球队分组和球队关联
     */
    @Operation(summary = "修改联赛球队分组和球队关联", description = "修改联赛球队分组和球队关联")
    @RequiresPermissions("system:teanGroupingPlayTeam:edit")
    @Log(title = "联赛球队分组和球队关联", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseTeanGroupingPlayTeam baseTeanGroupingPlayTeam)
    {
        return toAjax(baseTeanGroupingPlayTeamService.updateBaseTeanGroupingPlayTeam(baseTeanGroupingPlayTeam));
    }

    /**
     * 删除联赛球队分组和球队关联
     */
    @Operation(summary = "删除联赛球队分组和球队关联", description = "删除联赛球队分组和球队关联")
    @RequiresPermissions("system:teanGroupingPlayTeam:remove")
    @Log(title = "联赛球队分组和球队关联", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseTeanGroupingPlayTeamService.deleteBaseTeanGroupingPlayTeamByIds(ids));
    }
}
