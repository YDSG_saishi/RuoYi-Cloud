package com.ruoyi.system.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.domain.BaseBasketballPlayer;
import com.ruoyi.system.domain.BasePlayerLog;
import com.ruoyi.system.domain.dto.H5.KeyFiguresDto;
import com.ruoyi.system.domain.dto.H5.PlayerPKDto;
import com.ruoyi.system.domain.vo.*;
import com.ruoyi.system.domain.vo.H5.AverageComparisonVO;
import com.ruoyi.system.domain.vo.H5.KeyFiguresVO;
import com.ruoyi.system.domain.vo.H5.PlayerPKVO;
import com.ruoyi.system.domain.vo.H5.ScoreMovementVO;

/**
 * 球员得分现记录Service接口
 * 
 * @author wangbj
 * @date 2024-04-16
 */
public interface IBasePlayerLogService extends IService<BasePlayerLog>
{
    /**
     * 查询球员得分现记录
     * 
     * @param id 球员得分现记录主键
     * @return 球员得分现记录
     */
    public BasePlayerLog selectBasePlayerLogById(Long id);

    /**
     * 查询球员得分现记录列表
     * 
     * @param basePlayerLog 球员得分现记录
     * @return 球员得分现记录集合
     */
    public List<BasePlayerLogVO> selectBasePlayerLogList(BasePlayerLog basePlayerLog);

    /**
     * 新增球员得分现记录
     * 
     * @param basePlayerLog 球员得分现记录
     * @return 结果
     */
    public int insertBasePlayerLog(BasePlayerLog basePlayerLog);

    /**
     * 修改球员得分现记录
     * 
     * @param basePlayerLog 球员得分现记录
     * @return 结果
     */
    public int updateBasePlayerLog(BasePlayerLog basePlayerLog);

    /**
     * 批量删除球员得分现记录
     * 
     * @param ids 需要删除的球员得分现记录主键集合
     * @return 结果
     */
    public int deleteBasePlayerLogByIds(Long[] ids);

    /**
     * 删除球员得分现记录信息
     * 
     * @param id 球员得分现记录主键
     * @return 结果
     */
    public int deleteBasePlayerLogById(Long id);

    /**
     * 获得比分
     * @param id
     * @return
     */
    List<BasePlayerLogScoreResponseVO> getScore(Long id,Long oneTeamId,Long twoTeamId,Long subsection);

    /**
     * 持球时间
     * @param tournamentId
     * @param oneTeamId
     * @param twoTeamId
     * @return
     */
//    BaseBasketballTimeVO getBasketballTime(Long tournamentId, Long oneTeamId, Long twoTeamId);

    /**
     * 实时数据
     * @param basePlayerLog
     * @return
     */
    Map<String,Object> realTimeData(BasePlayerLog basePlayerLog);

    List<Map<String, Object>> realTimeDataTeamGetImg(BasePlayerLog basePlayerLog);

    List< Map<String,Object>> realTimeDataTeam(BasePlayerLog basePlayerLog);

    /**
     * 获取比赛总结
     * @param tournamentId
     * @return
     */
    String getSummary(Long tournamentId);

    /**
     * 排行榜
     * @param contestId
     * @param type
     * @return
     */
    List<BaseRankingListVO> rankingList(Long contestId, int type,String types);

    /**
     * 本场最佳
     * @param tournamentId
     * @return
     */
    BestOfTheGameResponseVO bestOfTheGame(Long tournamentId);

    /**
     * 球队比较
     * @param tournamentId
     * @return
     */
    BaseTeamCompareResponseVO teamCompare(Long tournamentId);

    Map<String,Object> getTournamentScore(Long tournamentId);

    int saveBatchLog(List<BasePlayerLog> basePlayerLog);

    Map<String, Object> listAll(Long tournamentId);

    Map<String, Object> getImg(Long tournamentId);

    String getContestBanner(Long contestId);

    /**
     * 比分走势
     * @param tournamentId 比赛id
     * @return
     */
    ScoreMovementVO scoreMovement(Long tournamentId);

    /**
     * 关键数据
     * @param dto
     * @return
     */
    KeyFiguresVO keyFigures(KeyFiguresDto dto);

    /**
     * 球员pk
     * @param dto
     * @return
     */
    PlayerPKVO playerPK(PlayerPKDto dto);

    /**
     * 获取球队球员列表
     * @param tournamentId
     * @return
     */
    List<BaseBasketballPlayer> playerList(Long tournamentId);

    /**
     * 场均对比
     * @param tournamentId
     * @return
     */
    AverageComparisonVO averageComparison(Long tournamentId);

    /**
     * 赛季对比
     * @param tournamentId
     * @return
     */
    BaseTeamCompareResponseVO seasonComparison(Long tournamentId);

    void updateLiveWatermark(Long tournamentId);
}
