package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.system.domain.BaseCoach;
import com.ruoyi.system.service.IBaseCoachService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;

import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 教练组成员Controller
 * 
 * @author wangbj
 * @date 2024-04-16
 */
@ApiOperation("教练组成员")
@RestController
@RequestMapping("/coach")
public class BaseCoachController extends BaseController
{
    @Autowired
    private IBaseCoachService baseCoachService;

    /**
     * 查询教练组成员列表
     */
    @RequiresPermissions("system:coach:list")
    @GetMapping("/list")
    @Operation(summary = "查询教练组成员列表", description = "查询教练组成员列表")
    public TableDataInfo list(BaseCoach baseCoach)
    {
        startPage();
        List<BaseCoach> list = baseCoachService.selectBaseCoachList(baseCoach);
        return getDataTable(list);
    }

    /**
     * 导出教练组成员列表
     */
    @Operation(summary = "导出教练组成员列表", description = "导出教练组成员列表")
    @RequiresPermissions("system:coach:export")
    @Log(title = "教练组成员", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseCoach baseCoach)
    {
        List<BaseCoach> list = baseCoachService.selectBaseCoachList(baseCoach);
        ExcelUtil<BaseCoach> util = new ExcelUtil<BaseCoach>(BaseCoach.class);
        util.exportExcel(response, list, "教练组成员数据");
    }

    /**
     * 获取教练组成员详细信息
     */
    @Operation(summary = "获取教练组成员详细信息", description = "获取教练组成员详细信息")
    @RequiresPermissions("system:coach:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(baseCoachService.selectBaseCoachById(id));
    }

    /**
     * 新增教练组成员
     */
    @Operation(summary = "新增教练组成员", description = "新增教练组成员")
    @RequiresPermissions("system:coach:add")
    @Log(title = "教练组成员", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseCoach baseCoach)
    {
        return toAjax(baseCoachService.insertBaseCoach(baseCoach));
    }

    /**
     * 修改教练组成员
     */
    @Operation(summary = "修改教练组成员", description = "修改教练组成员")
    @RequiresPermissions("system:coach:edit")
    @Log(title = "教练组成员", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseCoach baseCoach)
    {
        return toAjax(baseCoachService.updateBaseCoach(baseCoach));
    }

    /**
     * 删除教练组成员
     */
    @Operation(summary = "删除教练组成员", description = "删除教练组成员")
    @RequiresPermissions("system:coach:remove")
    @Log(title = "教练组成员", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseCoachService.deleteBaseCoachByIds(ids));
    }
}
