package com.ruoyi.system.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.domain.BaseBasketballTeam;
import com.ruoyi.system.domain.vo.BasketBallPlayerInviteVO;

/**
 * 球队Service接口
 * 
 * @author wangbj
 * @date 2024-04-16
 */
public interface IBaseBasketballTeamService extends IService<BaseBasketballTeam>
{
    /**
     * 查询球队
     * 
     * @param id 球队主键
     * @return 球队
     */
    public BaseBasketballTeam selectBaseBasketballTeamById(Long id);

    /**
     * 查询球队列表
     * 
     * @param baseBasketballTeam 球队
     * @return 球队集合
     */
    public List<BaseBasketballTeam> selectBaseBasketballTeamList(BaseBasketballTeam baseBasketballTeam);

    /**
     * 新增球队
     * 
     * @param baseBasketballTeam 球队
     * @return 结果
     */
    public int insertBaseBasketballTeam(BaseBasketballTeam baseBasketballTeam);

    /**
     * 修改球队
     * 
     * @param baseBasketballTeam 球队
     * @return 结果
     */
    public int updateBaseBasketballTeam(BaseBasketballTeam baseBasketballTeam);

    /**
     * 批量删除球队
     * 
     * @param ids 需要删除的球队主键集合
     * @return 结果
     */
    public int deleteBaseBasketballTeamByIds(Long[] ids);

    /**
     * 删除球队信息
     * 
     * @param id 球队主键
     * @return 结果
     */
    public int deleteBaseBasketballTeamById(Long id);

    /**
     * 创建球队邀请链接
     * @param basketBallPlayerInviteVO
     * @return
     */
    String createBasketBallTeamInvite(BasketBallPlayerInviteVO basketBallPlayerInviteVO);

    /**
     * 同意加入球队
     * @param basketBallPlayerInviteVO
     * @return
     */
    Boolean agreeTeamInvite(BasketBallPlayerInviteVO basketBallPlayerInviteVO);
    /**
     * 通过联赛id 获取球队
     */
    List<BaseBasketballTeam> playerTeamList(Long contestId);
}
