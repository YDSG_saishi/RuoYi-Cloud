package com.ruoyi.system.domain;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 教练组成员对象 base_coach
 * 
 * @author wangbj
 * @date 2024-04-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("base_coach")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BaseCoach extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @TableId(type = IdType.AUTO)
    @Schema(name = "id", description = "主键")
    private Long id;

    /** 姓名 */
    @Excel(name = "姓名")
    @Schema(name = "name", description = "姓名")
    private String name;

    /** 角色类型:1:主教练;2:助理教练;3:领队;4:其他工作人员;5:队医; */
    @Excel(name = "角色类型:1:主教练;2:助理教练;3:领队;4:其他工作人员;5:队医;")
    @Schema(name = "type", description = "角色类型:1:主教练;2:助理教练;3:领队;4:其他工作人员;5:队医;")
    private Long type;

    /** 成员头像 */
    @Excel(name = "成员头像")
    @Schema(name = "coachPhoto", description = "成员头像")
    private String coachPhoto;

    /** 证件类型:1:身份证;2:港澳居民往来大陆通行证;3:台湾居民往来大陆通行证;4:护照; */
    @Excel(name = "证件类型:1:身份证;2:港澳居民往来大陆通行证;3:台湾居民往来大陆通行证;4:护照;")
    @Schema(name = "documentType", description = "证件类型:1:身份证;2:港澳居民往来大陆通行证;3:台湾居民往来大陆通行证;4:护照;")
    private Long documentType;

    /** 证件号码 */
    @Excel(name = "证件号码")
    @Schema(name = "documentNumber", description = "证件号码")
    private String documentNumber;

    /** 证件照片地址 */
    @Excel(name = "证件照片地址")
    @Schema(name = "documentPhoto", description = "证件照片地址")
    private String documentPhoto;

    /** 0:男;1:女; */
    @Excel(name = "0:男;1:女;")
    @Schema(name = "sex", description = "0:男;1:女;")
    private Long sex;

    /** 出生日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "出生日期", width = 30, dateFormat = "yyyy-MM-dd")
    @Schema(name = "birthday", description = "出生日期")
    private Date birthday;

    /** 创建人id */
    @Excel(name = "创建人id")
    @Schema(name = "createUserId", description = "创建人id")
    private Long createUserId;

    /** 创建人姓名 */
    @Excel(name = "创建人姓名")
    @Schema(name = "createUserName", description = "创建人姓名")
    private String createUserName;

    /** 修改人id */
    @Excel(name = "修改人id")
    @Schema(name = "updateUserId", description = "修改人id")
    private Long updateUserId;

    /** 修改人姓名 */
    @Excel(name = "修改人姓名")
    @Schema(name = "updateUserName", description = "修改人姓名")
    private String updateUserName;

    /** 是否已删除 0:未删除;1:已删除 */
    @Excel(name = "是否已删除 0:未删除;1:已删除")
    @Schema(name = "isDeleted", description = "是否已删除 0:未删除;1:已删除")
    private Long isDeleted;

    @TableField(exist = false)
    private String typeStr;
    /**球队ID*/
    @TableField(exist = false)
    private Long playerId;
}
