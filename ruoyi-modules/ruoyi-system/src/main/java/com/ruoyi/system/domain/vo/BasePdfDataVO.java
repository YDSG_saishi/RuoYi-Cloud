package com.ruoyi.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;

/**
 * @author Wangbj
 * @date 2024年04月26日 00:57
 */
@Data
@Accessors(chain = true)
public class BasePdfDataVO {

    /**
     * 比赛开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "赛事开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    @Schema(name = "tournamentBeginTime", description = "赛事开始时间")
    private Date tournamentBeginTime;
    /**
     * 比赛地址
     */
    private String address;

    /**
     * 主队名称
     */
    private String homeTeamName;
    /**
     * 主队队伍总得分
     */
    private Long homeScore;
    /**
     * 主队第一节总分
     */
    private Long homeOneScore;
    /**
     * 主队第二节总分
     */
    private Long homeTwoScore;
    /**
     * 主队第三节总分
     */
    private Long homeThreeScore;
    /**
     * 主队第四节总分
     */
    private Long homeFourScore;
    /**
     * 主队第五节总分
     */
    private Long homeFiveScore;
    /**
     * 主队第六节总分
     */
    private Long homeSixScore;
    /**
     * 主队第七节总分
     */
    private Long homeSevenScore;

    /**
     * 主队第一节犯规
     */
    private Long homeTeamOneFoul;
    /**
     * 主队第二节犯规
     */
    private Long homeTeamTwoFoul;
    /**
     * 主队第三节犯规
     */
    private Long homeTeamThreeFoul;
    /**
     * 主队第四节犯规
     */
    private Long homeTeamFourFoul;
    /**
     * 主队第一节团队暂停
     */
    private Long homeTeamOneStop;
    /**
     * 主队第二节团队暂停
     */
    private Long homeTeamTwoStop;
    /**
     * 主队第三节团队暂停
     */
    private Long homeTeamThreeStop;
    /**
     * 主队第四节团队暂停
     */
    private Long homeTeamFourStop;
    /**
     * 主队球员列表
     */
    private List<BasePdfDataPlayListVO> pdfHomePlayerList;

    //------------客队---------------------
    /**
     * 主队名称
     */
    private String awayTeamName;
    /**
     * 客队队伍总得分
     */
    private Long awayScore;
    /**
     * 客队第一节总分
     */
    private Long awayOneScore;
    /**
     * 客队第二节总分
     */
    private Long awayTwoScore;
    /**
     * 客队第三节总分
     */
    private Long awayThreeScore;
    /**
     * 客队第四节总分
     */
    private Long awayFourScore;
    /**
     * 客队第五节总分
     */
    private Long awayFiveScore;
    /**
     * 客队第六节总分
     */
    private Long awaySixScore;
    /**
     * 客队第七节总分
     */
    private Long awaySevenScore;

    /**
     * 客队第一节犯规
     */
    private Long awayTeamOneFoul;
    /**
     * 客队第二节犯规
     */
    private Long awayTeamTwoFoul;
    /**
     * 客队第三节犯规
     */
    private Long awayTeamThreeFoul;
    /**
     * 客队第四节犯规
     */
    private Long awayTeamFourFoul;
    /**
     * 客队第一节团队暂停
     */
    private Long awayTeamOneStop;
    /**
     * 客队第二节团队暂停
     */
    private Long awayTeamTwoStop;
    /**
     * 客队第三节团队暂停
     */
    private Long awayTeamThreeStop;
    /**
     * 客队第四节团队暂停
     */
    private Long awayTeamFourStop;


    /**
     * 客队球员列表
     */
    private List<BasePdfDataPlayListVO> pdfAwayPlayerList;
}
