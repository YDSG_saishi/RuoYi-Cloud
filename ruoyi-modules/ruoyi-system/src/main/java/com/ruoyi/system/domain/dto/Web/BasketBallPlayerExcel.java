package com.ruoyi.system.domain.dto.Web;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.Date;

@Data
public class BasketBallPlayerExcel {


    /** 球员名称 */
    @Excel(name = "球员名称")
    private String name;

    /** 球衣号码 */
    @Excel(name = "球衣号码")
    private String playerNumber;

    /** 证件类型:1:身份证;2:港澳居民往来大陆通行证;3:台湾居民往来大陆通行证;4:护照; */
    @Excel(name = "证件类型:1:身份证;2:港澳居民往来大陆通行证;3:台湾居民往来大陆通行证;4:护照;")
    private Long documentType;

    /** 证件号码 */
    @Excel(name = "证件号码")
    private String documentNumber;


    /** 性别:1:男;2:女; */
    @Excel(name = "性别:1:男;2:女;")
    @Schema(name = "sex", description = "性别:1:男;2:女;")
    private Long sex;

    /** 出生日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "出生日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date birthday;

    /** 身高；单位cm */
    @Excel(name = "身高；单位cm")
    @Schema(name = "height", description = "身高；单位cm")
    private Long height;

    /** 体重；单位kg */
    @Excel(name = "体重；单位kg")
    @Schema(name = "weight", description = "体重；单位kg")
    private Long weight;
}
