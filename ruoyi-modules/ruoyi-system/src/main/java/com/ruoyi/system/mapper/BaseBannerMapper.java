package com.ruoyi.system.mapper;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.system.domain.BaseBanner;
import com.ruoyi.system.domain.BaseTournamentChat;
import com.ruoyi.system.domain.dto.H5.TournamentChatDto;
import com.ruoyi.system.domain.dto.Web.BannerDto;

import java.util.List;
import java.util.Objects;


/**
 */
public interface BaseBannerMapper extends BaseMapper<BaseBanner>{


    default List<BaseBanner> selectByList(BannerDto dto){
        LambdaQueryWrapper<BaseBanner> wrapper = Wrappers.<BaseBanner>lambdaQuery()
                .eq(BaseBanner::getContestId,dto.getContestId())
                .like(StringUtils.isNotBlank(dto.getTitle()),BaseBanner::getTitle,dto.getTitle())
                .eq(BaseBanner::getIsDeleted,0)
                .orderByDesc(BaseBanner::getCreateTime);
        return selectList(wrapper);
    }
}

