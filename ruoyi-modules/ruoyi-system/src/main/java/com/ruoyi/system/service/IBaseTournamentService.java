package com.ruoyi.system.service;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.system.domain.BaseContest;
import com.ruoyi.system.domain.BaseTournament;
import com.ruoyi.system.domain.vo.BasePdfDataVO;
import com.ruoyi.system.domain.vo.BaseTeamCompareVO;
import com.ruoyi.system.domain.vo.BaseTournamentResponseVO;
import com.ruoyi.system.domain.vo.PlayTeamDataVO;
import org.springframework.web.multipart.MultipartFile;

import javax.xml.crypto.Data;

/**
 * 赛事Service接口
 * 
 * @author wangbj
 * @date 2024-04-16
 */
public interface IBaseTournamentService extends IService<BaseTournament>
{
    /**
     * 查询赛事
     * 
     * @param id 赛事主键
     * @return 赛事
     */
    public BaseTournament selectBaseTournamentById(Long id);

    /**
     * 查询赛事列表
     * 
     * @param baseTournament 赛事
     * @return 赛事集合
     */
    public TableDataInfo selectBaseTournamentList(BaseTournament baseTournament);
    public TableDataInfo selectBaseTournamentList(Long id,int state,Long teamId,Integer pageNum,Integer pageSize);

    /**
     * 新增赛事
     * 
     * @param baseTournament 赛事
     * @return 结果
     */
    public int insertBaseTournament(BaseTournament baseTournament);

    /**
     * 修改赛事
     * 
     * @param baseTournament 赛事
     * @return 结果
     */
    public int updateBaseTournament(BaseTournament baseTournament);

    /**
     * 批量删除赛事
     *
     * @param ids 需要删除的赛事主键集合
     * @return 结果
     */
    public boolean deleteBaseTournamentByIds(Long[] ids);

    /**
     * 删除赛事信息
     * 
     * @param id 赛事主键
     * @return 结果
     */
    public int deleteBaseTournamentById(Long id);

    /**
     * 弃权
     * @param id 比赛id
     * @param abstentionId 弃权队伍id
     * @return
     */
    int abstention(Long id, Long abstentionId);

    /**
     * 结束比赛
     * @param id 比赛id
     * @return
     */
    int overTournament(Long id);

    /**
     * 切换比赛小节
     * @param id
     * @param subsection
     * @return
     */
    int switchGameSections(Long id, Long subsection);
    /**
     * 获取比赛详情包含球队球员
     * @param id
     * @return
     */
    BaseTournamentResponseVO selectById(Long id);

    /**
     *
     * @param scheduleId 比赛id
     * @return
     */
    Map<String, Object> getTeamStatsCaption(Long scheduleId);
    LinkedHashMap<String, Object> getTournamentData(Long tournamentId,Long teamId);


    BaseContest getContestLogo(Long tournamentId);

    /**
     * id获取比赛列表
     * @param tournamentIds 比赛id
     * @param state 比赛状态
     * @return
     */
    List<BaseTournament> selectByIdsAndState(List<Long> tournamentIds, int state, Date now);

    PlayTeamDataVO teamData(Long teamId);

    List<Map<String, Object>> playTeamData(Long teamId);
    /**
     * pdf数据
     */
    BasePdfDataVO getPdfData(Long tournamentId);

    String downloadPdf(MultipartFile file,String filename);

    /**
     * 查询比赛及球队名称
     * @param tournamentId
     * @return
     */
    BaseTournament selectBaseTournamentAndTeamNameById(Long tournamentId);
}
