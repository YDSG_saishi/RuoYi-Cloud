package com.ruoyi.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Map;

/**
 * @author Wangbj
 * @date 2024年04月26日 00:34
 */
@Data
@Accessors(chain = true)
public class BasePlayerLogScoreResponseVO {

    @Schema(name = "tournamentId", description = "比赛id")
    private Long tournamentId;

    @Schema(name = "teamId", description = "队伍id")
    private Long teamId;

    @Schema(name = "teamScore", description = "队伍比分")
    private Long teamScore;

    @Schema(name = "countFoul", description = "队伍犯规")
    private Long countFoul;

    @Schema(name = "countStop", description = "队伍暂停")
    private Long countStop;

    @Schema(name = "playerNum", description = "球员犯规次数")
    private Map<Long, Long> playerNum;




}
