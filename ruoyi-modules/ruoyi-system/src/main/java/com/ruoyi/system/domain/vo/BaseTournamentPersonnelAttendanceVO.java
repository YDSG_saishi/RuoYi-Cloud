package com.ruoyi.system.domain.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author Wangbj
 * @date 2024年05月07日 09:58
 */
@Data
@Accessors(chain = true)
public class BaseTournamentPersonnelAttendanceVO {
    @Schema(name = "userId", description = "用户id")
    private Long userId;

    @Schema(name = "nickName", description = "用户昵称")
    private String nickName;

    @Schema(name = "type", description = "类型")
    private Integer type;

    @Schema(name = "countPerType", description = "总数")
    private Integer countPerType;


}
