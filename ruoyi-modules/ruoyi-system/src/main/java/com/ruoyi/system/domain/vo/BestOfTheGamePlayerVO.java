package com.ruoyi.system.domain.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author Wangbj
 * @date 2024年05月08日 21:28
 */
@Data
@Accessors(chain = true)
public class BestOfTheGamePlayerVO {

    @Schema(name = "profilePicture", description = "头像")
    private String profilePicture;

    @Schema(name = "playerName", description = "球员名称")
    private String playerName;

    @Schema(name = "teamId", description = "球队id")
    private Long teamId;

    @Schema(name = "type", description = "类型 1:得分;2:篮板;3:助攻;")
    private int type;

    @Schema(name = "totalCount", description = "总计")
    private Integer totalCount;

}
