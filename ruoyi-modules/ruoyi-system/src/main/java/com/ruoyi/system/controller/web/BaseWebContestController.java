package com.ruoyi.system.controller.web;

import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.core.web.page.WebTableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.common.security.service.TokenService;
import com.ruoyi.system.domain.BaseContest;
import com.ruoyi.system.domain.dto.Web.ContestBindingDto;
import com.ruoyi.system.service.IBaseContestService;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * web联赛Controller
 */
@ApiOperation("联赛")
@RestController
@RequestMapping("/web/contest")
public class BaseWebContestController extends BaseController
{
    @Autowired
    private IBaseContestService baseContestService;

    @Autowired
    TokenService tokenService;


    /**
     * 查询联赛列表
     */
    @GetMapping("/list")
    @Operation(summary = "查询联赛列表", description = "查询联赛列表")
    public WebTableDataInfo list(BaseContest baseContest)
    {
        startPage();
        List<BaseContest> list = baseContestService.selectList(baseContest);
        return getWebDataTable(list);
    }


    /**
     * 修改审核状态
     */
    @Operation(summary = "修改审核状态", description = "修改审核状态")
    @Log(title = "修改审核状态", businessType = BusinessType.UPDATE)
    @PostMapping("/auditStatus")
    public AjaxResult auditStatus(@RequestBody BaseContest baseContest)
    {
        return toAjax(baseContestService.auditStatus(baseContest));
    }
    /**
     * 修改联赛
     */
    @Operation(summary = "修改联赛", description = "修改联赛")
    @RequiresPermissions("system:contest:edit")
    @Log(title = "联赛", businessType = BusinessType.UPDATE)
    @PostMapping("/update")
    public AjaxResult edit(@RequestBody BaseContest baseContest)
    {
        return toAjax(baseContestService.updateBaseContest(baseContest));
    }
    /**
     * 上传大屏轮播图
     */
    @Operation(summary = "上传大屏轮播图", description = "上传大屏轮播图")
    @Log(title = "上传大屏轮播图", businessType = BusinessType.UPDATE)
    @PostMapping("/saveImg")
    public AjaxResult saveImg(@RequestBody BaseContest baseContest)
    {
        return toAjax(baseContestService.saveImg(baseContest));
    }

    /**
     * 查询联赛绑定列表
     */
    @Operation(summary = "查询联赛绑定列表", description = "查询联赛绑定列表")
    @Log(title = "查询联赛绑定列表", businessType = BusinessType.UPDATE)
    @PostMapping("/getContestBinding")
    public AjaxResult getContestBinding(@RequestBody BaseContest baseContest)
    {
        return success(baseContestService.getContestBinding(baseContest));
    }
    /**
     * 添加联赛绑定
     */
    @Operation(summary = "添加联赛绑定", description = "添加联赛绑定")
    @Log(title = "添加联赛绑定", businessType = BusinessType.UPDATE)
    @PostMapping("/contestBinding")
    public AjaxResult contestBinding(@RequestBody ContestBindingDto ContestBindingDto)
    {
        return toAjax(baseContestService.contestBinding(ContestBindingDto));
    }
}
