package com.ruoyi.system.domain.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author Wangbj
 * @date 2024年05月08日 21:27
 */
@Data
@Accessors(chain = true)
public class BestOfTheGameResponseVO {
    @Schema(name = "oneTeamList", description = "队伍1")
    List<BestOfTheGamePlayerVO> oneTeamList;

    @Schema(name = "twoTeamList", description = "队伍2")
    List<BestOfTheGamePlayerVO> twoTeamList;
}
