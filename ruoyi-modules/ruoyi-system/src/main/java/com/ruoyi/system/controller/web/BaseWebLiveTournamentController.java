package com.ruoyi.system.controller.web;

import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.domain.BaseLiveTournament;
import com.ruoyi.system.domain.LiveStreamCode;
import com.ruoyi.system.service.IBaseLiveTournamentService;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * 比赛直播房间Controller
 * 
 * @author ruoyi
 * @date 2024-05-10
 */
@Slf4j
@ApiOperation("比赛直播房间")
@RestController
@RequestMapping("/web/live/tournament")
public class BaseWebLiveTournamentController extends BaseController {
    @Resource
    private IBaseLiveTournamentService baseLiveTournamentService;

    /**
     * 获取直播推流
     */
    @Operation(summary = "获取直播推流", description = "获取直播推流")
    @GetMapping("beginLive")
    public AjaxResult beginLive(@RequestParam Long tournamentId,@RequestParam String endTime,Integer type) {
        log.info("获取比分栏开始直播参数-->比赛id->{},时间->{},type->{}",tournamentId,endTime,type);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return success(baseLiveTournamentService.beginLive(tournamentId, LocalDateTime.parse(endTime, formatter),type));
    }


}


