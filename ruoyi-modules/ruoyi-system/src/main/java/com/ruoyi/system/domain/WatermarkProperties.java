package com.ruoyi.system.domain;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "watermark")
@Data
public class WatermarkProperties {

    private Long xPosition;
    private Long yPosition;
    private Long width;
    private Long height;
    private Long backgroundWidth;
    private Long backgroundHeight;

    public WatermarkProperties(Long xPosition, Long yPosition, Long width, Long height, Long backgroundWidth, Long backgroundHeight) {
        this.xPosition = xPosition;
        this.yPosition = yPosition;
        this.width = width;
        this.height = height;
        this.backgroundWidth = backgroundWidth;
        this.backgroundHeight = backgroundHeight;
    }

    public WatermarkProperties() {
        super();
    }
}
