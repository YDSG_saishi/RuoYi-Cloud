package com.ruoyi.system.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.domain.BaseTeanGroupingPlayTeam;

/**
 * 联赛球队分组和球队关联Service接口
 * 
 * @author ruoyi
 * @date 2024-05-20
 */
public interface IBaseTeanGroupingPlayTeamService extends IService<BaseTeanGroupingPlayTeam>
{
    /**
     * 查询联赛球队分组和球队关联
     * 
     * @param id 联赛球队分组和球队关联主键
     * @return 联赛球队分组和球队关联
     */
    public BaseTeanGroupingPlayTeam selectBaseTeanGroupingPlayTeamById(Long id);

    /**
     * 查询联赛球队分组和球队关联列表
     * 
     * @param baseTeanGroupingPlayTeam 联赛球队分组和球队关联
     * @return 联赛球队分组和球队关联集合
     */
    public List<BaseTeanGroupingPlayTeam> selectBaseTeanGroupingPlayTeamList(BaseTeanGroupingPlayTeam baseTeanGroupingPlayTeam);

    /**
     * 新增联赛球队分组和球队关联
     *
     * @param baseTeanGroupingPlayTeam 联赛球队分组和球队关联
     * @return 结果
     */
    public boolean insertBaseTeanGroupingPlayTeam(List<BaseTeanGroupingPlayTeam> baseTeanGroupingPlayTeam);

    /**
     * 修改联赛球队分组和球队关联
     * 
     * @param baseTeanGroupingPlayTeam 联赛球队分组和球队关联
     * @return 结果
     */
    public int updateBaseTeanGroupingPlayTeam(BaseTeanGroupingPlayTeam baseTeanGroupingPlayTeam);

    /**
     * 批量删除联赛球队分组和球队关联
     * 
     * @param ids 需要删除的联赛球队分组和球队关联主键集合
     * @return 结果
     */
    public int deleteBaseTeanGroupingPlayTeamByIds(Long[] ids);

    /**
     * 删除联赛球队分组和球队关联信息
     * 
     * @param id 联赛球队分组和球队关联主键
     * @return 结果
     */
    public int deleteBaseTeanGroupingPlayTeamById(Long id);
}
