package com.ruoyi.system.domain.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Data
public class CopyBaseTeamCompareVO {
    @Schema(name = "lb", description = "篮板")
    private BigDecimal lb = BigDecimal.ZERO;

    @Schema(name = "ef", description = "二分")
    private BigDecimal ef = BigDecimal.ZERO;

    @Schema(name = "sf", description = "三分")
    private BigDecimal sf = BigDecimal.ZERO;

    @Schema(name = "fq", description = "罚球")
    private BigDecimal fq = BigDecimal.ZERO;

    @Schema(name = "gm", description = "盖帽")
    private BigDecimal gm = BigDecimal.ZERO;

    @Schema(name = "zg", description = "助攻")
    private BigDecimal zg = BigDecimal.ZERO;

    @Schema(name = "qd", description = "抢断")
    private BigDecimal qd = BigDecimal.ZERO;

    @Schema(name = "fg", description = "犯规")
    private BigDecimal fg = BigDecimal.ZERO;
    @Schema(name = "score", description = "总分")
    private BigDecimal score = BigDecimal.ZERO;

    @Schema(name = "score", description = "罚球命中率")
    private BigDecimal pointRate = BigDecimal.ZERO;

    @Schema(name = "score", description = "二分命中率")
    private BigDecimal twoPointRate = BigDecimal.ZERO;

    @Schema(name = "score", description = "三分命中率")
    private BigDecimal threePointRate = BigDecimal.ZERO;
//
//    public CopyBaseTeamCompareVO(BaseTeamCompareVO teamCompare, int size) {
//        ef = (BigDecimal.valueOf(teamCompare.getEf()).divide(BigDecimal.valueOf(size),2, RoundingMode.HALF_UP));
//        qd = (BigDecimal.valueOf(teamCompare.getQd()).divide(BigDecimal.valueOf(size),2, RoundingMode.HALF_UP));
//        lb = (BigDecimal.valueOf(teamCompare.getLb()).divide(BigDecimal.valueOf(size),2, RoundingMode.HALF_UP));
//        gm = (BigDecimal.valueOf(teamCompare.getGm()).divide(BigDecimal.valueOf(size),2, RoundingMode.HALF_UP));
//        fq = (BigDecimal.valueOf(teamCompare.getFq()).divide(BigDecimal.valueOf(size),2, RoundingMode.HALF_UP));
//        sf = (BigDecimal.valueOf(teamCompare.getSf()).divide(BigDecimal.valueOf(size),2, RoundingMode.HALF_UP));
//        zg = (BigDecimal.valueOf(teamCompare.getZg()).divide(BigDecimal.valueOf(size),2, RoundingMode.HALF_UP));
//        fg = (BigDecimal.valueOf(teamCompare.getFg()).divide(BigDecimal.valueOf(size),2, RoundingMode.HALF_UP));
//        score = (BigDecimal.valueOf(teamCompare.getScore()).divide(BigDecimal.valueOf(size),2, RoundingMode.HALF_UP));
//
//    }
}
