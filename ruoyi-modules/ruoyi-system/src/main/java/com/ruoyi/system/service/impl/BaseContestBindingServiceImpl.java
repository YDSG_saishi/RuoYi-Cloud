package com.ruoyi.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.utils.CommonStreamUtil;
import com.ruoyi.system.domain.BaseContest;
import com.ruoyi.system.domain.BaseContestBinding;
import com.ruoyi.system.mapper.BaseContestBindingMapper;
import com.ruoyi.system.mapper.BaseContestMapper;
import com.ruoyi.system.service.IBaseContestBindingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;


/**
 * 联赛Service业务层处理
 * 
 * @author wangbj
 * @date 2024-04-16
 */
@Slf4j
@Service
public class BaseContestBindingServiceImpl extends ServiceImpl<BaseContestBindingMapper, BaseContestBinding> implements IBaseContestBindingService
{

    @Resource
    private BaseContestMapper baseContestMapper;
    @Override
    public List<BaseContest> selectList(BaseContest dto) {
        List<BaseContestBinding> contestBindingList = baseMapper.selectListById(dto.getId());
        if (CollUtil.isEmpty(contestBindingList)){
            return new ArrayList<>();
        }
        Set<Long> longs = CommonStreamUtil.transSet(contestBindingList, BaseContestBinding::getContestBindingId);
        longs.addAll( CommonStreamUtil.transSet(contestBindingList, BaseContestBinding::getContestId));
        longs.remove(dto.getId());
        List<BaseContest> baseContests = baseContestMapper.selectBatchIds(longs);
        baseContests = CommonStreamUtil.filter(baseContests,o -> Objects.equals(o.getIsDeleted(),0L) &&
                Objects.equals(o.getAuditStatus(),1));
        return baseContests;
    }
}
