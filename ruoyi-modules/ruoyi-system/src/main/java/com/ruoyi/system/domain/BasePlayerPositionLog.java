package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 球员阵位记录对象 base_player_position_log
 * 
 * @author wangbj
 * @date 2024-04-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("base_player_position_log")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BasePlayerPositionLog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @TableId(type = IdType.AUTO)
    @Schema(name = "id", description = "主键")
    private Long id;

    /** 赛事id */
    @Excel(name = "赛事id")
    @Schema(name = "tournamentId", description = "赛事id")
    private Long tournamentId;

    /** 球员id */
    @Excel(name = "球员id")
    @Schema(name = "basketballPlayerId", description = "球员id")
    private Long basketballPlayerId;

    /** 球队id */
    @Excel(name = "球队id")
    @Schema(name = "basketballTeamId", description = "球队id")
    private Long basketballTeamId;

    /** 赛季id */
    @Excel(name = "赛季id")
    @Schema(name = "seasonId", description = "赛季id")
    private Long seasonId;

    /** 机构id */
    @Excel(name = "机构id")
    @Schema(name = "organizationId", description = "机构id")
    private Long organizationId;

    /** 联赛id */
    @Excel(name = "联赛id")
    @Schema(name = "contestId", description = "联赛id")
    private Long contestId;

    /** 登场类型:0:首发;1:替补;2:上场;3:下场; */
    @Excel(name = "登场类型:0:首发;1:替补;2:上场;3:下场;")
    @Schema(name = "type", description = "登场类型:0:首发;1:替补;2:上场;3:下场;")
    private Integer type;

    /** 是否换人:0:否;1:是; */
    @Excel(name = "是否换人:0:否;1:是;")
    @Schema(name = "isSubstitution", description = "是否换人:0:否;1:是;")
    private Long isSubstitution;

    /** 排序 */
    @Excel(name = "排序")
    @Schema(name = "sort", description = "排序")
    private Integer sort;

    /** 是否登场：0:是;1:不是; */
    @Excel(name = "是否登场：0:是;1:不是;")
    @Schema(name = "isComeOn", description = "是否登场：0:是;1:不是;")
    private Integer isComeOn;

    /** 比赛小节 */
    @Excel(name = "比赛小节")
    @Schema(name = "subsection", description = "比赛小节")
    private Long subsection;

    /** 创建人id */
    @Excel(name = "创建人id")
    @Schema(name = "createUserId", description = "创建人id")
    private Long createUserId;

    /** 创建人姓名 */
    @Excel(name = "创建人姓名")
    @Schema(name = "createUserName", description = "创建人姓名")
    private String createUserName;

    /** 修改人id */
    @Excel(name = "修改人id")
    @Schema(name = "updateUserId", description = "修改人id")
    private Long updateUserId;

    /** 修改人姓名 */
    @Excel(name = "修改人姓名")
    @Schema(name = "updateUserName", description = "修改人姓名")
    private String updateUserName;

    /** 是否已删除 0:未删除;1:已删除 */
    @Excel(name = "是否已删除 0:未删除;1:已删除")
    @Schema(name = "isDeleted", description = "是否已删除 0:未删除;1:已删除")
    private Long isDeleted;

}
