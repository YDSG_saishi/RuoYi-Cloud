package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.system.mapper.BasketballPlayerAndTeamMapper;
import com.ruoyi.system.domain.BasketballPlayerAndTeam;
import com.ruoyi.system.service.IBasketballPlayerAndTeamService;

import javax.annotation.Resource;

/**
 * 球队球员教练组关联Service业务层处理
 * 
 * @author wangbj
 * @date 2024-04-16
 */
@Service
public class BasketballPlayerAndTeamServiceImpl extends ServiceImpl<BasketballPlayerAndTeamMapper, BasketballPlayerAndTeam> implements IBasketballPlayerAndTeamService
{
    @Resource
    private BasketballPlayerAndTeamMapper basketballPlayerAndTeamMapper;
    @Resource
    LoginUserSetUtil loginUserSetUtil;

    /**
     * 查询球队球员教练组关联
     * 
     * @param id 球队球员教练组关联主键
     * @return 球队球员教练组关联
     */
    @Override
    public BasketballPlayerAndTeam selectBasketballPlayerAndTeamById(Long id)
    {
        return basketballPlayerAndTeamMapper.selectBasketballPlayerAndTeamById(id);
    }

    /**
     * 查询球队球员教练组关联列表
     * 
     * @param basketballPlayerAndTeam 球队球员教练组关联
     * @return 球队球员教练组关联
     */
    @Override
    public List<BasketballPlayerAndTeam> selectBasketballPlayerAndTeamList(BasketballPlayerAndTeam basketballPlayerAndTeam)
    {
        return basketballPlayerAndTeamMapper.selectBasketballPlayerAndTeamList(basketballPlayerAndTeam);
    }

    /**
     * 新增球队球员教练组关联
     * 
     * @param basketballPlayerAndTeam 球队球员教练组关联
     * @return 结果
     */
    @Override
    public int insertBasketballPlayerAndTeam(BasketballPlayerAndTeam basketballPlayerAndTeam)
    {
        basketballPlayerAndTeam.setCreateTime(DateUtils.getNowDate());
        loginUserSetUtil.populateFields(basketballPlayerAndTeam,1);
        return basketballPlayerAndTeamMapper.insertBasketballPlayerAndTeam(basketballPlayerAndTeam);
    }

    /**
     * 修改球队球员教练组关联
     * 
     * @param basketballPlayerAndTeam 球队球员教练组关联
     * @return 结果
     */
    @Override
    public int updateBasketballPlayerAndTeam(BasketballPlayerAndTeam basketballPlayerAndTeam)
    {
        loginUserSetUtil.populateFields(basketballPlayerAndTeam,2);
        return basketballPlayerAndTeamMapper.updateBasketballPlayerAndTeam(basketballPlayerAndTeam);
    }

    /**
     * 批量删除球队球员教练组关联
     * 
     * @param ids 需要删除的球队球员教练组关联主键
     * @return 结果
     */
    @Override
    public int deleteBasketballPlayerAndTeamByIds(Long[] ids)
    {
        return basketballPlayerAndTeamMapper.deleteBasketballPlayerAndTeamByIds(ids);
    }

    /**
     * 删除球队球员教练组关联信息
     * 
     * @param id 球队球员教练组关联主键
     * @return 结果
     */
    @Override
    public int deleteBasketballPlayerAndTeamById(Long id)
    {
        return basketballPlayerAndTeamMapper.deleteBasketballPlayerAndTeamById(id);
    }
}
