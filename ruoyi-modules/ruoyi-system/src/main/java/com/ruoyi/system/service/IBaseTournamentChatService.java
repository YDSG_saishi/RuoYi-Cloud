package com.ruoyi.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.system.domain.BaseTournamentChat;
import com.ruoyi.system.domain.dto.H5.TournamentChatDto;

import java.util.List;


/**
 * 球员出勤记录Service接口
 * 
 * @author wangbj
 * @date 2024-04-16
 */
public interface IBaseTournamentChatService extends IService<BaseTournamentChat>{

    List<BaseTournamentChat> selectByList(TournamentChatDto dto);

    int send(BaseTournamentChat dto);

    AjaxResult getsignature(String url);
}

