package com.ruoyi.system.utils;

import com.ruoyi.system.domain.LiveProperties;
import com.tencentcloudapi.common.AbstractModel;

import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.common.profile.HttpProfile;
import com.tencentcloudapi.common.exception.TencentCloudSDKException;
import com.tencentcloudapi.mps.v20190612.MpsClient;
import com.tencentcloudapi.mps.v20190612.models.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class TencentCloud {
    @Autowired
    private final LiveProperties liveProperties;

    public void ProcessMedia() {
        try{
            // 实例化一个认证对象，入参需要传入腾讯云账户 SecretId 和 SecretKey，此处还需注意密钥对的保密
            // 代码泄露可能会导致 SecretId 和 SecretKey 泄露，并威胁账号下所有资源的安全性。以下代码示例仅供参考，建议采用更安全的方式来使用密钥，请参见：https://cloud.tencent.com/document/product/1278/85305
            // 密钥可前往官网控制台 https://console.cloud.tencent.com/cam/capi 进行获取
            Credential cred = new Credential(liveProperties.getSecretId(), liveProperties.getSecretKey());
            // 实例化一个http选项，可选的，没有特殊需求可以跳过
            HttpProfile httpProfile = new HttpProfile();
            httpProfile.setEndpoint("mps.tencentcloudapi.com");
            // 实例化一个client选项，可选的，没有特殊需求可以跳过
            ClientProfile clientProfile = new ClientProfile();
            clientProfile.setHttpProfile(httpProfile);
            // 实例化要请求产品的client对象,clientProfile是可选的
            MpsClient client = new MpsClient(cred, "", clientProfile);
            // 实例化一个请求对象,每个接口都会对应一个request对象
            ProcessMediaRequest req = new ProcessMediaRequest();
            MediaInputInfo mediaInputInfo1 = new MediaInputInfo();
            mediaInputInfo1.setType("URL");
            UrlInputInfo urlInputInfo1 = new UrlInputInfo();
            urlInputInfo1.setUrl("http://pullvideo.yundongshugenlive.cn/live/302PWOqWQ.m3u8?txSecret=35d83817b296e97fe5a03ca049587a8b&txTime=6662F5BD&txTimeshift=on&tsFormat=unix&tsStart=1717678175&tsEnd=1717678189");
            mediaInputInfo1.setUrlInputInfo(urlInputInfo1);

            req.setInputInfo(mediaInputInfo1);

            TaskOutputStorage taskOutputStorage1 = new TaskOutputStorage();
            taskOutputStorage1.setType("COS");
            CosOutputStorage cosOutputStorage1 = new CosOutputStorage();
            cosOutputStorage1.setBucket("ydsg-1256672631");
            cosOutputStorage1.setRegion("ap-guangzhou");
            taskOutputStorage1.setCosOutputStorage(cosOutputStorage1);

            req.setOutputStorage(taskOutputStorage1);

            req.setOutputDir("/file/");
            req.setScheduleId(10101L);
            // 返回的resp是一个ProcessMediaResponse的实例，与请求对象对应
            ProcessMediaResponse resp = client.ProcessMedia(req);
            // 输出json格式的字符串回包
            System.out.println(AbstractModel.toJsonString(resp));
        } catch (TencentCloudSDKException e) {
            System.out.println(e.toString());
        }
    }


    public  void DescribeTaskDetail() {
        try{
            // 实例化一个认证对象，入参需要传入腾讯云账户 SecretId 和 SecretKey，此处还需注意密钥对的保密
            // 代码泄露可能会导致 SecretId 和 SecretKey 泄露，并威胁账号下所有资源的安全性。以下代码示例仅供参考，建议采用更安全的方式来使用密钥，请参见：https://cloud.tencent.com/document/product/1278/85305
            // 密钥可前往官网控制台 https://console.cloud.tencent.com/cam/capi 进行获取
            Credential cred = new Credential(liveProperties.getSecretId(), liveProperties.getSecretKey());
            // 实例化一个http选项，可选的，没有特殊需求可以跳过
            HttpProfile httpProfile = new HttpProfile();
            httpProfile.setEndpoint("mps.tencentcloudapi.com");
            // 实例化一个client选项，可选的，没有特殊需求可以跳过
            ClientProfile clientProfile = new ClientProfile();
            clientProfile.setHttpProfile(httpProfile);
            // 实例化要请求产品的client对象,clientProfile是可选的
            MpsClient client = new MpsClient(cred, "", clientProfile);
            // 实例化一个请求对象,每个接口都会对应一个request对象
            DescribeTaskDetailRequest req = new DescribeTaskDetailRequest();
            req.setTaskId("2456672631-ScheduleTask-b60deed636a79705a46592e6a37db35dtt7");
            // 返回的resp是一个DescribeTaskDetailResponse的实例，与请求对象对应
            DescribeTaskDetailResponse resp = client.DescribeTaskDetail(req);
            // 输出json格式的字符串回包
            System.out.println(AbstractModel.toJsonString(resp));
        } catch (TencentCloudSDKException e) {
            System.out.println(e.toString());
        }
    }

}