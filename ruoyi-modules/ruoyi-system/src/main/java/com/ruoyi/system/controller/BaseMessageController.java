package com.ruoyi.system.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.system.domain.vo.BaseMsgReplyVO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.domain.BaseMessage;
import com.ruoyi.system.service.IBaseMessageService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;

import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 留言反馈Controller
 * 
 * @author ruoyi
 * @date 2024-05-07
 */
@ApiOperation("留言反馈")
@RestController
@RequestMapping("/message")
public class BaseMessageController extends BaseController
{
    @Autowired
    private IBaseMessageService baseMessageService;

    /**
     * 查询留言反馈列表
     */
    @RequiresPermissions("system:message:list")
    @GetMapping("/list")
    @Operation(summary = "查询留言反馈列表", description = "查询留言反馈列表")
    public TableDataInfo list(BaseMessage baseMessage)
    {
        startPage();
        List<BaseMessage> list = baseMessageService.selectBaseMessageList(baseMessage);
        return getDataTable(list);
    }

    /**
     * 导出留言反馈列表
     */
    @Operation(summary = "导出留言反馈列表", description = "导出留言反馈列表")
    @RequiresPermissions("system:message:export")
    @Log(title = "留言反馈", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseMessage baseMessage)
    {
        List<BaseMessage> list = baseMessageService.selectBaseMessageList(baseMessage);
        ExcelUtil<BaseMessage> util = new ExcelUtil<BaseMessage>(BaseMessage.class);
        util.exportExcel(response, list, "留言反馈数据");
    }

    /**
     * 获取留言反馈详细信息
     */
    @Operation(summary = "获取留言反馈详细信息", description = "获取留言反馈详细信息")
    @RequiresPermissions("system:message:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(baseMessageService.selectBaseMessageById(id));
    }

    /**
     * 新增留言反馈
     */
    @Operation(summary = "新增留言反馈", description = "新增留言反馈")
    @RequiresPermissions("system:message:add")
    @Log(title = "留言反馈", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseMessage baseMessage)
    {
        return toAjax(baseMessageService.insertBaseMessage(baseMessage));
    }

    /**
     * 修改留言反馈
     */
    @Operation(summary = "修改留言反馈", description = "修改留言反馈")
    @RequiresPermissions("system:message:edit")
    @Log(title = "留言反馈", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseMessage baseMessage)
    {
        return toAjax(baseMessageService.updateBaseMessage(baseMessage));
    }

    /**
     * 删除留言反馈
     */
    @Operation(summary = "删除留言反馈", description = "删除留言反馈")
    @RequiresPermissions("system:message:remove")
    @Log(title = "留言反馈", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseMessageService.deleteBaseMessageByIds(ids));
    }

    /**
     * 我的留言
     */
    @Operation(summary = "我的留言", description = "我的留言")
    @Log(title = "我的留言", businessType = BusinessType.DELETE)
    @GetMapping("/getMyList")
    public List<BaseMessage> getMyList()
    {
        return baseMessageService.getMyList();
    }

    /**
     * 回复
     */
    @Operation(summary = "回复", description = "回复")
    @Log(title = "回复", businessType = BusinessType.DELETE)
    @GetMapping("/reply")
    public List<BaseMsgReplyVO> reply(@RequestParam Long id,
                                      @RequestParam String msg)
    {
        return baseMessageService.reply(id,msg);
    }
}
