package com.ruoyi.system.domain.dto.H5;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class PlayerPKDto {
    /**
     * 比赛id
     */
    @Schema(name = "tournamentId", description = "比赛id")
    @NotNull(message = "比赛id不能为空")
    private Long tournamentId;

    /**
     * 关键数据 1详情 2更多数据
     */
    private Integer type;

    /**
     * 节数 1st 2nd 全场不传
     */
    @Schema(name = "subsection", description = "类型 1st 2nd")
    private Integer subsection;

    /**
     * 球员id
     */
    @NotNull(message = "球员id不能为空")
    private Long playerId;
}
