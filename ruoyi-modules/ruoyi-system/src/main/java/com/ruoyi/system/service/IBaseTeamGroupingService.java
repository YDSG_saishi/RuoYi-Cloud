package com.ruoyi.system.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.domain.BaseTeamGrouping;

/**
 * 联赛球队分组Service接口
 * 
 * @author ruoyi
 * @date 2024-05-20
 */
public interface IBaseTeamGroupingService extends IService<BaseTeamGrouping>
{
    /**
     * 查询联赛球队分组
     * 
     * @param id 联赛球队分组主键
     * @return 联赛球队分组
     */
    public BaseTeamGrouping selectBaseTeamGroupingById(Long id);

    /**
     * 查询联赛球队分组列表
     * 
     * @param baseTeamGrouping 联赛球队分组
     * @return 联赛球队分组集合
     */
    public List<BaseTeamGrouping> selectBaseTeamGroupingList(BaseTeamGrouping baseTeamGrouping);

    /**
     * 新增联赛球队分组
     * 
     * @param baseTeamGrouping 联赛球队分组
     * @return 结果
     */
    public int insertBaseTeamGrouping(BaseTeamGrouping baseTeamGrouping);

    /**
     * 修改联赛球队分组
     * 
     * @param baseTeamGrouping 联赛球队分组
     * @return 结果
     */
    public int updateBaseTeamGrouping(BaseTeamGrouping baseTeamGrouping);

    /**
     * 批量删除联赛球队分组
     * 
     * @param ids 需要删除的联赛球队分组主键集合
     * @return 结果
     */
    public int deleteBaseTeamGroupingByIds(Long[] ids);

    /**
     * 删除联赛球队分组信息
     * 
     * @param id 联赛球队分组主键
     * @return 结果
     */
    public int deleteBaseTeamGroupingById(Long id);

    List<BaseTeamGrouping> teamRankingList(Long contestId);
}
