package com.ruoyi.system.controller;

import java.util.*;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletResponse;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.common.core.enums.PlayerLogType;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.system.domain.BaseContest;
import com.ruoyi.system.domain.BasePlayerLog;
import com.ruoyi.system.domain.BaseTournament;
import com.ruoyi.system.domain.vo.BasePlayerLogVO;
import com.ruoyi.system.service.IBaseContestService;
import com.ruoyi.system.service.IBasePlayerLogService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;

import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 球员得分现记录Controller
 * 
 * @author wangbj
 * @date 2024-04-16
 */
@ApiOperation("球员得分现记录")
@RestController
@RequestMapping("/playerLog")
public class BasePlayerLogController extends BaseController
{
    @Autowired
    private IBasePlayerLogService basePlayerLogService;

    @Autowired
    private IBaseContestService iBaseContestService;
    /**
     * 查询球员得分现记录列表
     */
    @RequiresPermissions("system:log:list")
    @GetMapping("/list")
    @Operation(summary = "查询球员得分现记录列表", description = "查询球员得分现记录列表")
    public TableDataInfo list(BasePlayerLog basePlayerLog)
    {
        startPage();
        List<BasePlayerLogVO> list = basePlayerLogService.selectBasePlayerLogList(basePlayerLog);
        return getDataTable(list);
    }

    /**
     * 实时数据
     */
    @GetMapping("/realTimeData")
    @Operation(summary = "实时数据", description = "实时数据")
    public AjaxResult realTimeData(BasePlayerLog basePlayerLog)
    {
        return success(basePlayerLogService.realTimeData(basePlayerLog));
    }

    /**
     * 实时数据比赛得分栏
     */
    @GetMapping("/realTimeDataTeam")
    @Operation(summary = "实时数据比赛得分栏", description = "实时数据比赛得分栏")
    public AjaxResult realTimeDataTeam(BasePlayerLog basePlayerLog)
    {
        return success(basePlayerLogService.realTimeDataTeam(basePlayerLog));
    }

    /**
     * 导出球员得分现记录列表
     */
    @Operation(summary = "导出球员得分现记录列表", description = "导出球员得分现记录列表")
    @RequiresPermissions("system:log:export")
    @Log(title = "球员得分现记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BasePlayerLog basePlayerLog)
    {
        List<BasePlayerLogVO> list = basePlayerLogService.selectBasePlayerLogList(basePlayerLog);
        ExcelUtil<BasePlayerLogVO> util = new ExcelUtil<BasePlayerLogVO>(BasePlayerLogVO.class);
        util.exportExcel(response, list, "球员得分现记录数据");
    }

    /**
     * 获取球员得分现记录详细信息
     */
    @Operation(summary = "获取球员得分现记录详细信息", description = "获取球员得分现记录详细信息")
    @RequiresPermissions("system:log:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(basePlayerLogService.selectBasePlayerLogById(id));
    }

    /**
     * 新增球员得分现记录
     */
    @Operation(summary = "新增球员得分现记录", description = "新增球员得分现记录")
    @RequiresPermissions("system:log:add")
    @Log(title = "球员得分现记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BasePlayerLog basePlayerLog)
    {
        return toAjax(basePlayerLogService.insertBasePlayerLog(basePlayerLog));
    }
    /**
     * 新增球员得分现记录
     */
    @Operation(summary = "新增球员得分现记录", description = "新增球员得分现记录")
    @RequiresPermissions("system:log:saveBatch")
    @Log(title = "球员得分现记录", businessType = BusinessType.INSERT)
    @PostMapping("/saveBatch")
    public AjaxResult saveBatch(@RequestBody List<BasePlayerLog> basePlayerLog)
    {
        return toAjax(basePlayerLogService.saveBatchLog(basePlayerLog));
    }
    /**
     * 修改球员得分现记录
     */
    @Operation(summary = "修改球员得分现记录", description = "修改球员得分现记录")
    @RequiresPermissions("system:log:edit")
    @Log(title = "球员得分现记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BasePlayerLog basePlayerLog)
    {
        return toAjax(basePlayerLogService.updateBasePlayerLog(basePlayerLog));
    }

    /**
     * 删除球员得分现记录
     */
    @Operation(summary = "删除球员得分现记录", description = "删除球员得分现记录")
    @RequiresPermissions("system:log:remove")
    @Log(title = "球员得分现记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(basePlayerLogService.deleteBasePlayerLogByIds(ids));
    }

    /**
     * 比分
     */
    @Operation(summary = "比分", description = "比分")
    @GetMapping(value = "/getScore")
    public AjaxResult getScore(@RequestParam Long tournamentId,
                               @RequestParam Long oneTeamId,
                               @RequestParam Long twoTeamId,Long subsection)
    {
        //比赛id
        return success(basePlayerLogService.getScore(tournamentId,oneTeamId,twoTeamId,subsection));
    }

//    /**
//     * 持球时间
//     */
//    @Operation(summary = "队伍持球时间", description = "队伍持球时间")
//    @GetMapping(value = "/basketballTime")
//    public AjaxResult basketballTime(@RequestParam Long tournamentId,
//                               @RequestParam Long oneTeamId,
//                               @RequestParam Long twoTeamId)
//    {
//        //比赛id
//        return success(basePlayerLogService.getBasketballTime(tournamentId,oneTeamId,twoTeamId));
//    }

    @Operation(summary = "获取操作栏枚举", description = "获取操作栏枚举")
    @GetMapping(value = "/downBox")
    public AjaxResult downBox(Long contestId,Integer type) {
        contestId =  Objects.isNull(contestId) ? SecurityUtils.getLoginUser().getContestId() : contestId;
        BaseContest baseContest = iBaseContestService.selectBaseContestById(contestId);
        if (Objects.isNull(baseContest)){
            throw new RuntimeException("获取当前联赛失败");
        }

        List<Map<String, Object>> list = new ArrayList<>();
        for (PlayerLogType value : PlayerLogType.values()) {
            if (StringUtils.equals(value.getInfo(),"暂停")){
                continue;
            }
            if (StringUtils.equals(value.getInfo(),"全场比赛结束")){
                continue;
            }
            if (value.getCode().equals(PlayerLogType.TWO_NOT_POINT_HIT.getCode()) || value.getCode().equals(PlayerLogType.TWO_POINT_HIT.getCode())) {
                continue;
            }
            if (value.getCode().equals(PlayerLogType.THREE_NOT_POINT_HIT.getCode()) || value.getCode().equals(PlayerLogType.THREE_POINT_HIT.getCode())) {
                continue;
            }
            if (value.getCode().equals(PlayerLogType.NOT_POINT_HIT.getCode()) || value.getCode().equals(PlayerLogType.POINT_HIT.getCode())) {
                continue;
            }
            if (value.getCode().equals(PlayerLogType.START.getCode()) || value.getCode().equals(PlayerLogType.TEAM_FOUL.getCode())) {
                continue;
            }

            Map<String, Object> map = new HashMap<>();
            map.put("code",value.getCode());//类型id
            map.put("info",value.getInfo());//名称
            map.put("type",value.getType());//是否交换球权 0否 1是
            list.add(map);
        }
        String name ;
        if (Objects.equals(baseContest.getScoringWay(),0L)){
            list =  list.stream().filter(vo ->
                    !Objects.equals(vo.get("info").toString(),"一分命中") &&
                            !Objects.equals(vo.get("info").toString(),"一分不中")
            ).collect(Collectors.toList());
        name = "三分";
        }else{
            list =  list.stream().filter(vo ->
                    !Objects.equals(vo.get("info").toString(),"三分命中") &&
                            !Objects.equals(vo.get("info").toString(),"三分不中")
            ).collect(Collectors.toList());
            name = "一分";
        }
        if (Objects.nonNull(type)){
            Map<String, Object> map = new HashMap<>();
            map.put("code",90);//类型id
            map.put("info","罚球总命中率");//名称
            map.put("type",90);//是否交换球权 0否 1是
            list.add(map);

            Map<String, Object> map1 = new HashMap<>();
            map1.put("code",91);
            map1.put("info","一分总命中率");
            map1.put("type",91);
            list.add(map1);

            Map<String, Object> map2 = new HashMap<>();
            map2.put("code",92);
            map2.put("info","二分总命中率");
            map2.put("type",92);
            list.add(map2);

            Map<String, Object> map3 = new HashMap<>();
            map3.put("code",93);
            map3.put("info","三分总命中率");
            map3.put("type",93);
            list.add(map3);

            Map<String, Object> map4 = new HashMap<>();
            map4.put("code",94);
            map4.put("info","单场效率值");
            map4.put("type",94);
            list.add(map4);

            Map<String, Object> map5 = new HashMap<>();
            map5.put("code",95);
            map5.put("info","真实命中率");
            map5.put("type",95);
            list.add(map5);


        }else{
            Map<String, Object> map = new HashMap<>();
            map.put("code",90);//类型id
            map.put("info","罚球命中率");//名称
            map.put("type",90);//是否交换球权 0否 1是
            list.add(map);

            Map<String, Object> map1 = new HashMap<>();
            map1.put("code",91);
            map1.put("info","一分命中率");
            map1.put("type",91);
            list.add(map1);

            Map<String, Object> map2 = new HashMap<>();
            map2.put("code",92);
            map2.put("info","二分命中率");
            map2.put("type",92);
            list.add(map2);

            Map<String, Object> map3 = new HashMap<>();
            map3.put("code",93);
            map3.put("info","三分命中率");
            map3.put("type",93);
            list.add(map3);

            Map<String, Object> map4 = new HashMap<>();
            map4.put("code",94);
            map4.put("info","效率值");
            map4.put("type",94);
            list.add(map4);

            Map<String, Object> map5 = new HashMap<>();
            map5.put("code",95);
            map5.put("info","真实命中率");
            map5.put("type",95);
            list.add(map5);
            Map<String, Object> map6 = new HashMap<>();
            map6.put("code",97);
            map6.put("info","二分");
            map6.put("type",97);
            list.add(0,map6);

            Map<String, Object> map7 = new HashMap<>();
            map7.put("code",98);
            map7.put("info",name);
            map7.put("type",98);
            list.add(0,map7);

            Map<String, Object> map8 = new HashMap<>();
            map8.put("code",96);
            map8.put("info","罚球");
            map8.put("type",96);
            list.add(0,map8);
        }
        return success(list);
    }

    /**
     * 查询所有的比赛当前记录
     */
    @GetMapping("/listAll")
    public Map<String,Object> listAll(@RequestParam Long tournamentId)
    {
        return basePlayerLogService.listAll(tournamentId);
    }

    /**
     * 生成海报图
     */
    @GetMapping("/getImg")
    public Map<String,Object> getImg(@RequestParam Long tournamentId)
    {
        return basePlayerLogService.getImg(tournamentId);
    }
}
