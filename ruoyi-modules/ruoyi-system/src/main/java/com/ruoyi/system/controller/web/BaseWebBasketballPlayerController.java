package com.ruoyi.system.controller.web;

import com.alibaba.csp.sentinel.util.AssertUtil;
import com.github.pagehelper.PageInfo;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.PageDomain;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.core.web.page.TableSupport;
import com.ruoyi.common.core.web.page.WebTableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.system.api.domain.SysUser;
import com.ruoyi.system.domain.BaseBasketballPlayer;
import com.ruoyi.system.domain.BaseBasketballTeam;
import com.ruoyi.system.domain.dto.Web.BasketBallPlayerDto;
import com.ruoyi.system.domain.dto.Web.BasketBallPlayerExcel;
import com.ruoyi.system.service.IBaseBasketballPlayerService;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.stream.Collectors;

/**
 * web球员列表
 * 
 * @author wangbj
 * @date 2024-04-16
 */
@ApiOperation("web球员列表")
@RestController
@RequestMapping("/web/basketballPlayer")
public class BaseWebBasketballPlayerController extends BaseController
{
    @Autowired
    private IBaseBasketballPlayerService baseBasketballPlayerService;

    /**
     * 查询球队列表
     */
    @GetMapping("/playerTeamList")
    @Operation(summary = "查询球队列表", description = "查询球队列表")
    public WebTableDataInfo playerTeamList(Long contestId)
    {
        startPage();
        List<BaseBasketballTeam> list = baseBasketballPlayerService.playerTeamList(contestId);
        return getWebDataTable(list);
    }
    /**
     * 查询球员列表
     */
    @GetMapping("/playerList")
    @Operation(summary = "查询球员列表", description = "查询球员列表")
    public WebTableDataInfo playerList(BasketBallPlayerDto basketBallPlayerDto)
    {
        startPage();
        List<BaseBasketballPlayer> list = baseBasketballPlayerService.playerList(basketBallPlayerDto);
        return getWebDataTable(list);
    }

    /**
     * importPlayer
     * @param file
     * @param contestId
     * @param teamId
     * @return
     * @throws Exception
     */
    @Log(title = "导入球员", businessType = BusinessType.IMPORT)
    @PostMapping("/importPlayer")
    public AjaxResult importPlayer(MultipartFile file,Long contestId,Long teamId) throws Exception
    {
        AssertUtil.notNull(contestId,"联赛不能为空");
        AssertUtil.notNull(teamId,"比赛不能为空");
        ExcelUtil<BasketBallPlayerExcel> util = new ExcelUtil<BasketBallPlayerExcel>(BasketBallPlayerExcel.class);
        List<BasketBallPlayerExcel> playerList = util.importExcel(file.getInputStream());
        String operName = SecurityUtils.getUsername();
        String message = baseBasketballPlayerService.importPlayer(playerList,  operName,contestId,teamId);
        return success(message);
    }
}
