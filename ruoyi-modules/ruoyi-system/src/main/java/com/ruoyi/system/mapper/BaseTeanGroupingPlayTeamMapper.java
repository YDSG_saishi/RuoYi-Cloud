package com.ruoyi.system.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.BaseTeanGroupingPlayTeam;

/**
 * 联赛球队分组和球队关联Mapper接口
 * 
 * @author ruoyi
 * @date 2024-05-20
 */
public interface BaseTeanGroupingPlayTeamMapper extends BaseMapper<BaseTeanGroupingPlayTeam>
{
    /**
     * 查询联赛球队分组和球队关联
     * 
     * @param id 联赛球队分组和球队关联主键
     * @return 联赛球队分组和球队关联
     */
    public BaseTeanGroupingPlayTeam selectBaseTeanGroupingPlayTeamById(Long id);

    /**
     * 查询联赛球队分组和球队关联列表
     * 
     * @param baseTeanGroupingPlayTeam 联赛球队分组和球队关联
     * @return 联赛球队分组和球队关联集合
     */
    public List<BaseTeanGroupingPlayTeam> selectBaseTeanGroupingPlayTeamList(BaseTeanGroupingPlayTeam baseTeanGroupingPlayTeam);

    /**
     * 新增联赛球队分组和球队关联
     * 
     * @param baseTeanGroupingPlayTeam 联赛球队分组和球队关联
     * @return 结果
     */
    public int insertBaseTeanGroupingPlayTeam(BaseTeanGroupingPlayTeam baseTeanGroupingPlayTeam);

    /**
     * 修改联赛球队分组和球队关联
     * 
     * @param baseTeanGroupingPlayTeam 联赛球队分组和球队关联
     * @return 结果
     */
    public int updateBaseTeanGroupingPlayTeam(BaseTeanGroupingPlayTeam baseTeanGroupingPlayTeam);

    /**
     * 删除联赛球队分组和球队关联
     * 
     * @param id 联赛球队分组和球队关联主键
     * @return 结果
     */
    public int deleteBaseTeanGroupingPlayTeamById(Long id);

    /**
     * 批量删除联赛球队分组和球队关联
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseTeanGroupingPlayTeamByIds(Long[] ids);
}
