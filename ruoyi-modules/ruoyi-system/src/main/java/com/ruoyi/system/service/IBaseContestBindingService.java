package com.ruoyi.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.domain.BaseContest;
import com.ruoyi.system.domain.BaseContestBinding;
import com.ruoyi.system.domain.dto.Web.ContestBindingDto;

import java.util.List;

/**
 * 联赛Service接口
 * 
 * @author wangbj
 * @date 2024-04-16
 */
public interface IBaseContestBindingService extends IService<BaseContestBinding>
{

    List<BaseContest> selectList(BaseContest dto);
}
