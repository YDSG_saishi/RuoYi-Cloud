package com.ruoyi.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.system.domain.BaseUserOrganization;
import com.ruoyi.system.mapper.BaseUserOrganizatioMapper;
import com.ruoyi.system.service.IBaseUserOrganizationService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 86188
 */
@Service
public class BaseUserOrganizationServiceImpl extends ServiceImpl<BaseUserOrganizatioMapper, BaseUserOrganization> implements IBaseUserOrganizationService {
    @Override
    public void savePO(Long organizationId, Long userId) {
        List<BaseUserOrganization> userOrganization = this.getUserOrganization(organizationId, userId);
        if (CollUtil.isNotEmpty(userOrganization)){
            return;
        }
        BaseUserOrganization baseUserOrganization = new BaseUserOrganization();
        baseUserOrganization.setOrganizationId(organizationId);
        baseUserOrganization.setUserId(userId);
        this.save(baseUserOrganization);
    }

    @Override
    public List<BaseUserOrganization> getUserId(Long userId) {
        List<BaseUserOrganization> list = this.list(Wrappers.<BaseUserOrganization>lambdaQuery()
                .eq(BaseUserOrganization::getUserId, userId));
        return list;
    }

    @Override
    public List<BaseUserOrganization> getUserOrganization(Long organizationId, Long userId) {
        List<BaseUserOrganization> list = this.list(Wrappers.<BaseUserOrganization>lambdaQuery()
                .eq(BaseUserOrganization::getUserId, userId)
                .eq(BaseUserOrganization::getOrganizationId,organizationId)
        );
        return list;
    }

    @Override
    public void deleteUserOrganization(Long contestId, Long userId) {
        this.remove(Wrappers.<BaseUserOrganization>lambdaQuery()
                .eq(BaseUserOrganization::getOrganizationId, contestId)
                .eq(BaseUserOrganization::getUserId, userId)
        );
    }
}
