package com.ruoyi.system.controller.web;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.core.web.page.WebTableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.domain.BaseTournament;
import com.ruoyi.system.service.IBaseTournamentService;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 赛事Controller
 * 
 * @author wangbj
 * @date 2024-04-16
 */
@ApiOperation("赛事")
@RestController
@RequestMapping("/web/tournament")
public class BaseWebTournamentController extends BaseController
{
    @Autowired
    private IBaseTournamentService baseTournamentService;

    /**
     * 查询赛事列表
     */
    @RequiresPermissions("system:tournament:list")
    @GetMapping("/list")
    @Operation(summary = "查询赛事列表", description = "查询赛事列表")
    public WebTableDataInfo list(BaseTournament baseTournament)
    {
        startPage();
        TableDataInfo tableDataInfo = baseTournamentService.selectBaseTournamentList(baseTournament);
        WebTableDataInfo webTableDataInfo = new WebTableDataInfo();
        BeanUtils.copyProperties(tableDataInfo,webTableDataInfo);
        webTableDataInfo.setCode(200);
        return webTableDataInfo;
    }
    /**
     * 获取赛事详细信息
     */
    @Operation(summary = "获取赛事详细信息", description = "获取赛事详细信息")
    @RequiresPermissions("system:tournament:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(baseTournamentService.selectById(id));
    }

    /**
     * 修改赛事状态
     */
    @Operation(summary = "修改赛事", description = "修改赛事")
    @RequiresPermissions("system:tournament:edit")
    @Log(title = "赛事", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    public AjaxResult edit(@RequestBody BaseTournament baseTournament)
    {
        return toAjax(baseTournamentService.updateById(baseTournament));
    }
}
