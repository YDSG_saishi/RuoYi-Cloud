package com.ruoyi.system.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.system.domain.BaseBasketballTeam;
import com.ruoyi.system.service.IBaseBasketballTeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.system.mapper.BaseTeanGroupingPlayTeamMapper;
import com.ruoyi.system.domain.BaseTeanGroupingPlayTeam;
import com.ruoyi.system.service.IBaseTeanGroupingPlayTeamService;

import javax.annotation.Resource;

/**
 * 联赛球队分组和球队关联Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-05-20
 */
@Service
public class BaseTeanGroupingPlayTeamServiceImpl extends ServiceImpl<BaseTeanGroupingPlayTeamMapper, BaseTeanGroupingPlayTeam> implements IBaseTeanGroupingPlayTeamService
{
    @Autowired
    private BaseTeanGroupingPlayTeamMapper baseTeanGroupingPlayTeamMapper;
    @Resource
    LoginUserSetUtil loginUserSetUtil;
    @Autowired
    private IBaseBasketballTeamService iBaseBasketballTeamService;
    /**
     * 查询联赛球队分组和球队关联
     * 
     * @param id 联赛球队分组和球队关联主键
     * @return 联赛球队分组和球队关联
     */
    @Override
    public BaseTeanGroupingPlayTeam selectBaseTeanGroupingPlayTeamById(Long id)
    {
        return baseTeanGroupingPlayTeamMapper.selectBaseTeanGroupingPlayTeamById(id);
    }

    /**
     * 查询联赛球队分组和球队关联列表
     * 
     * @param baseTeanGroupingPlayTeam 联赛球队分组和球队关联
     * @return 联赛球队分组和球队关联
     */
    @Override
    public List<BaseTeanGroupingPlayTeam> selectBaseTeanGroupingPlayTeamList(BaseTeanGroupingPlayTeam baseTeanGroupingPlayTeam)
    {
        return baseTeanGroupingPlayTeamMapper.selectBaseTeanGroupingPlayTeamList(baseTeanGroupingPlayTeam);
    }

    /**
     * 新增联赛球队分组和球队关联
     *
     * @param baseTeanGroupingPlayTeam 联赛球队分组和球队关联
     * @return 结果
     */
    @Override
    public boolean insertBaseTeanGroupingPlayTeam(List<BaseTeanGroupingPlayTeam> baseTeanGroupingPlayTeam)
    {
        List<Long> collect = baseTeanGroupingPlayTeam.stream().map(BaseTeanGroupingPlayTeam::getBasketballTeamId).collect(Collectors.toList());
        List<BaseTeanGroupingPlayTeam> list = this.list(Wrappers.<BaseTeanGroupingPlayTeam>lambdaQuery()
                .in(BaseTeanGroupingPlayTeam::getBasketballTeamId, collect)
        );
        if (CollUtil.isNotEmpty(list)){
            List<Long> collect1 = list.stream().map(BaseTeanGroupingPlayTeam::getBasketballTeamId).collect(Collectors.toList());
            List<BaseBasketballTeam> baseBasketballTeams = iBaseBasketballTeamService.listByIds(collect1);
            String collect2 = baseBasketballTeams.stream().map(BaseBasketballTeam::getTeamName).collect(Collectors.joining(","));
            throw new RuntimeException("当前球队"+collect2+"已经存在当前分组,不可重复添加");
        }
        baseTeanGroupingPlayTeam.forEach(vo ->{
            vo.setCreateTime(DateUtils.getNowDate());
            loginUserSetUtil.populateFields(vo, 1);
        });

        return this.saveBatch(baseTeanGroupingPlayTeam);
    }
    /**
     * 修改联赛球队分组和球队关联
     * 
     * @param baseTeanGroupingPlayTeam 联赛球队分组和球队关联
     * @return 结果
     */
    @Override
    public int updateBaseTeanGroupingPlayTeam(BaseTeanGroupingPlayTeam baseTeanGroupingPlayTeam)
    {
        baseTeanGroupingPlayTeam.setUpdateTime(DateUtils.getNowDate());
        loginUserSetUtil.populateFields(baseTeanGroupingPlayTeam, 2);
        return baseTeanGroupingPlayTeamMapper.updateById(baseTeanGroupingPlayTeam);
    }

    /**
     * 批量删除联赛球队分组和球队关联
     * 
     * @param ids 需要删除的联赛球队分组和球队关联主键
     * @return 结果
     */
    @Override
    public int deleteBaseTeanGroupingPlayTeamByIds(Long[] ids)
    {
        return baseTeanGroupingPlayTeamMapper.deleteBaseTeanGroupingPlayTeamByIds(ids);
    }

    /**
     * 删除联赛球队分组和球队关联信息
     * 
     * @param id 联赛球队分组和球队关联主键
     * @return 结果
     */
    @Override
    public int deleteBaseTeanGroupingPlayTeamById(Long id)
    {
        return baseTeanGroupingPlayTeamMapper.deleteBaseTeanGroupingPlayTeamById(id);
    }
}
