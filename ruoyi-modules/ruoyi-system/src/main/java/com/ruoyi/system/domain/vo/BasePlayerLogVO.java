package com.ruoyi.system.domain.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 球员得分现记录对象 base_player_log
 * 
 * @author wangbj
 * @date 2024-04-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("base_player_log")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BasePlayerLogVO
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @TableId(type = IdType.AUTO)
    @Schema(name = "id", description = "主键")
    private Long id;

    /** 球员id */
    @Excel(name = "球员id")
    @Schema(name = "basketballPlayerId", description = "球员id")
    private Long basketballPlayerId;

    @Schema(name = "playerNumber", description = "球员号码")

    private Long playerNumber;
    @Schema(name = "name", description = "球员名称")
    private String name;
    @Schema(name = "teamName", description = "球队名称")

    private String teamName;
    private Long type;
    @Schema(name = "typeStr", description = "计分名称")
    private String typeStr;
    @Schema(name = "time", description = "计数器时间")

    private String time;
    
    private Long subsection;

    @Schema(name = "createTime", description = "记录时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;


}
