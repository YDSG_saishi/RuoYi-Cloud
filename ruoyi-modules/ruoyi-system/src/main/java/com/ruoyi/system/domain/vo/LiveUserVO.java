package com.ruoyi.system.domain.vo;

import lombok.Data;

import javax.websocket.Session;
import java.time.LocalDateTime;

@Data
public class LiveUserVO {

    /**
     * websocket交互的userId
     */
    private String websocketUserId;

//    /**
//     * 用户id
//     */
//    private Integer userId;
//
//    /**
//     * 用户昵称
//     */
//    private String nickName;

//    /**
//     * 用户头像
//     */
//    private String avatar;

    /**
     * 身份 0 = 观众 1 = 主播 2= 管理员
     */
    private Integer identity;

//    /**
//     * 状态 0=申请中 1=购买用户 2=申请通过用户 3=申请拒绝用户 4=主播添加用户
//     */
//    private Integer status;

    /**
     * websocket交互的session
     */
    private Session session;

    /**
     * 连接时间
     */
    private LocalDateTime connectTime;

    public LiveUserVO() {
        super();
    }

    public LiveUserVO(String websocketUserId, Integer identity, Session session) {
        this.websocketUserId = websocketUserId;
//        this.userId = userId;
//        this.nickName = nickName;
//        this.avatar = avatar;
        this.identity = identity;
        this.session = session;
//        this.status = status;
    }
}
