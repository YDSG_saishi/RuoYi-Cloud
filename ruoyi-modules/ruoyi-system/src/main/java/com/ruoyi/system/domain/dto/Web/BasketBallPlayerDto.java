package com.ruoyi.system.domain.dto.Web;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Data
public class BasketBallPlayerDto {

    /**
     * 联赛id
     */
    @NotNull(message = "联赛id不能为空")
    private Long contestId;
    /**
     * 球队id
     */
    @NotNull(message = "球队id不能为空")
    private Long teamId;
    /**
     * 球员id
     */
    private Long playerId;
    /**
     * 球员名称
     */
    private String playerName;
}
