package com.ruoyi.system.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.BaseSeason;

/**
 * 赛季Mapper接口
 * 
 * @author wangbj
 * @date 2024-04-16
 */
public interface BaseSeasonMapper extends BaseMapper<BaseSeason>
{
    /**
     * 查询赛季
     * 
     * @param id 赛季主键
     * @return 赛季
     */
    public BaseSeason selectBaseSeasonById(Long id);

    /**
     * 查询赛季列表
     * 
     * @param baseSeason 赛季
     * @return 赛季集合
     */
    public List<BaseSeason> selectBaseSeasonList(BaseSeason baseSeason);

    /**
     * 新增赛季
     * 
     * @param baseSeason 赛季
     * @return 结果
     */
    public int insertBaseSeason(BaseSeason baseSeason);

    /**
     * 修改赛季
     * 
     * @param baseSeason 赛季
     * @return 结果
     */
    public int updateBaseSeason(BaseSeason baseSeason);

    /**
     * 删除赛季
     * 
     * @param id 赛季主键
     * @return 结果
     */
    public int deleteBaseSeasonById(Long id);

    /**
     * 批量删除赛季
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseSeasonByIds(Long[] ids);

    /**
     * 获取到时间到了 未开始的
     * @return
     */
    List<BaseSeason> getListByTime();
}
