package com.ruoyi.system.domain.vo;

import com.ruoyi.system.domain.BasePlayerPositionLog;
import com.ruoyi.system.domain.BaseTournamentPersonnel;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author Wangbj
 * @date 2024年04月27日 21:32
 */
@Data
@Accessors(chain = true)
public class BasePositionResponseVO {

    @Schema(name = "oneTeamList", description = "第一个队伍阵位")
    private List<BasePositionMsgVO> oneTeamList;

    @Schema(name = "twoTeamList", description = "第二个队伍阵位")
    private List<BasePositionMsgVO> twoTeamList;



}
