package com.ruoyi.system.domain;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 球员对象 base_basketball_player
 * 
 * @author wangbj
 * @date 2024-04-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("base_basketball_player")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BaseBasketballPlayer extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @TableId(type = IdType.AUTO)
    @Schema(name = "id", description = "主键")
    private Long id;

    /** 球员名称 */
    @Excel(name = "球员名称")
    @Schema(name = "name", description = "球员名称")
    private String name;

    /** 0:在役;1:退役; */
    @Excel(name = "0:在役;1:退役;")
    @Schema(name = "state", description = "0:在役;1:退役;")
    private Long state;

    /** 球员头像地址 */
    @Excel(name = "球员头像地址")
    @Schema(name = "profilePicture", description = "球员头像地址")
    private String profilePicture;

    /** 球衣号码 */
    @Excel(name = "球衣号码")
    @Schema(name = "playerNumber", description = "球衣号码")
    private String playerNumber;

    /** 是否首发:0:否;1:是; */
    @Excel(name = "是否首发:0:否;1:是;")
    @Schema(name = "isUp", description = "是否首发:0:否;1:是;")
    private Integer isUp;


    /** 证件类型:1:身份证;2:港澳居民往来大陆通行证;3:台湾居民往来大陆通行证;4:护照; */
    @Excel(name = "证件类型:1:身份证;2:港澳居民往来大陆通行证;3:台湾居民往来大陆通行证;4:护照;")
    @Schema(name = "documentType", description = "证件类型:1:身份证;2:港澳居民往来大陆通行证;3:台湾居民往来大陆通行证;4:护照;")
    private Long documentType;

    /** 证件号码 */
    @Excel(name = "证件号码")
    @Schema(name = "documentNumber", description = "证件号码")
    private String documentNumber;

    /** 证件照片地址 */
    @Excel(name = "证件照片地址")
    @Schema(name = "documentPhoto", description = "证件照片地址")
    private String documentPhoto;

    /** 性别:1:男;2:女; */
    @Excel(name = "性别:1:男;2:女;")
    @Schema(name = "sex", description = "性别:1:男;2:女;")
    private Long sex;

    /** 出生日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "出生日期", width = 30, dateFormat = "yyyy-MM-dd")
    @Schema(name = "birthday", description = "出生日期")
    private Date birthday;

    /** 身高；单位cm */
    @Excel(name = "身高；单位cm")
    @Schema(name = "height", description = "身高；单位cm")
    private Long height;

    /** 体重；单位kg */
    @Excel(name = "体重；单位kg")
    @Schema(name = "weight", description = "体重；单位kg")
    private Long weight;

    /** 场上位置: 1:控球后卫;2:得分后卫;3:大前锋;4:小前锋;5:中锋; */
    @Excel(name = "场上位置: 1:控球后卫;2:得分后卫;3:大前锋;4:小前锋;5:中锋;")
    @Schema(name = "position", description = "场上位置: 1:控球后卫;2:得分后卫;3:大前锋;4:小前锋;5:中锋;")
    private Long position;

    /** 创建人id */
    @Excel(name = "创建人id")
    @Schema(name = "createUserId", description = "创建人id")
    private Long createUserId;

    /** 创建人姓名 */
    @Excel(name = "创建人姓名")
    @Schema(name = "createUserName", description = "创建人姓名")
    private String createUserName;

    /** 修改人id */
    @Excel(name = "修改人id")
    @Schema(name = "updateUserId", description = "修改人id")
    private Long updateUserId;

    /** 修改人姓名 */
    @Excel(name = "修改人姓名")
    @Schema(name = "updateUserName", description = "修改人姓名")
    private String updateUserName;

    /** 是否已删除 0:未删除;1:已删除 */
    @Excel(name = "是否已删除 0:未删除;1:已删除")
    @Schema(name = "isDeleted", description = "是否已删除 0:未删除;1:已删除")
    private Long isDeleted;

    /**球队ID*/
    @TableField(exist = false)
    private Long playerId;
    /** 不为空就是已经开始了比赛 临时加人*/
    @TableField(exist = false)
    private Long tournamentId;
    @TableField(exist = false)
    private Integer playerNumberInt;
    /**
     * 球队名称
     */
    @TableField(exist = false)
    private String playerTeamName;
}
