package com.ruoyi.system.mapper;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.BaseContest;
import com.ruoyi.system.domain.BaseContestBinding;

import java.util.List;

/**
 * 联赛Mapper接口
 * 
 * @author wangbj
 * @date 2024-04-16
 */
public interface BaseContestBindingMapper extends BaseMapper<BaseContestBinding>
{

    default List<BaseContestBinding> selectListById(Long id){
        LambdaQueryWrapper<BaseContestBinding> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(BaseContestBinding::getContestId,id).or().eq(BaseContestBinding::getContestBindingId,id);
        return selectList(wrapper);
    }
}
