package com.ruoyi.system.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.BaseOrganization;

/**
 * 机构Mapper接口
 * 
 * @author wangbj
 * @date 2024-04-16
 */
public interface BaseOrganizationMapper extends BaseMapper<BaseOrganization>
{
    /**
     * 查询机构
     * 
     * @param id 机构主键
     * @return 机构
     */
    public BaseOrganization selectBaseOrganizationById(Long id);

    /**
     * 查询机构列表
     * 
     * @param baseOrganization 机构
     * @return 机构集合
     */
    public List<BaseOrganization> selectBaseOrganizationList(BaseOrganization baseOrganization);

    /**
     * 新增机构
     * 
     * @param baseOrganization 机构
     * @return 结果
     */
    public int insertBaseOrganization(BaseOrganization baseOrganization);

    /**
     * 修改机构
     * 
     * @param baseOrganization 机构
     * @return 结果
     */
    public int updateBaseOrganization(BaseOrganization baseOrganization);

    /**
     * 删除机构
     * 
     * @param id 机构主键
     * @return 结果
     */
    public int deleteBaseOrganizationById(Long id);

    /**
     * 批量删除机构
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseOrganizationByIds(Long[] ids);
}
