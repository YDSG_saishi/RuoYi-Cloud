package com.ruoyi.system.service.impl;

import com.alibaba.csp.sentinel.util.AssertUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.security.service.TokenService;
import com.ruoyi.system.api.model.LoginUser;
import com.ruoyi.system.domain.BaseBanner;
import com.ruoyi.system.domain.dto.Web.BannerDto;
import com.ruoyi.system.mapper.BaseBannerMapper;
import com.ruoyi.system.service.IBaseBannerService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 球员出勤记录Service业务层处理
 * 
 * @author wangbj
 * @date 2024-04-16
 */
@Service
public class BaseBannerServiceImpl extends ServiceImpl<BaseBannerMapper, BaseBanner> implements IBaseBannerService
{

    @Resource
    TokenService tokenService;

    @Override
    public List<BaseBanner> selectList(BannerDto dto) {
        return baseMapper.selectByList(dto);
    }

    @Override
    public int insertBanner(BaseBanner baseBanner) {
        AssertUtil.notNull(baseBanner.getContestId(),"联赛id不能为空");
        AssertUtil.notEmpty(baseBanner.getImg(),"图片链接不能为空");
        AssertUtil.notEmpty(baseBanner.getTitle(),"标题不能为空");

        LoginUser loginUser = tokenService.getLoginUser();
        baseBanner.setCreateTime(new Date());
        baseBanner.setCreateBy(loginUser.getSysUser().getNickName());
        return save(baseBanner)?1:0;
    }
}
