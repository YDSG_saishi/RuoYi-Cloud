package com.ruoyi.system.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.domain.BaseBasketballPlayer;
import com.ruoyi.system.domain.BaseBasketballTeam;
import com.ruoyi.system.domain.dto.Web.BasketBallPlayerDto;
import com.ruoyi.system.domain.dto.Web.BasketBallPlayerExcel;

/**
 * 球员Service接口
 * 
 * @author wangbj
 * @date 2024-04-16
 */
public interface IBaseBasketballPlayerService extends IService<BaseBasketballPlayer>
{
    /**
     * 查询球员
     * 
     * @param id 球员主键
     * @return 球员
     */
    public BaseBasketballPlayer selectBaseBasketballPlayerById(Long id);

    /**
     * 查询球员列表
     * 
     * @param baseBasketballPlayer 球员
     * @return 球员集合
     */
    public List<BaseBasketballPlayer> selectBaseBasketballPlayerList(BaseBasketballPlayer baseBasketballPlayer);

    /**
     * 新增球员
     * 
     * @param baseBasketballPlayer 球员
     * @return 结果
     */
    public int insertBaseBasketballPlayer(BaseBasketballPlayer baseBasketballPlayer);

    /**
     * 修改球员
     *
     * @param baseBasketballPlayer 球员
     * @return 结果
     */
    public boolean updateBaseBasketballPlayer(BaseBasketballPlayer baseBasketballPlayer);

    /**
     * 批量删除球员
     * 
     * @param ids 需要删除的球员主键集合
     * @return 结果
     */
    public int deleteBaseBasketballPlayerByIds(Long[] ids);

    /**
     * 删除球员信息
     * 
     * @param id 球员主键
     * @return 结果
     */
    public int deleteBaseBasketballPlayerById(Long id);

    boolean getIsUp(Long teamOneId, Long teamTwoId, Integer num);

    /**
     * 获取球员列表
      * @param basketBallPlayerDto
     * @return
     */
    List<BaseBasketballPlayer> playerList(BasketBallPlayerDto basketBallPlayerDto);

    /**
     * 获取球队列表
     * @param contestId
     * @return
     */
    List<BaseBasketballTeam> playerTeamList(Long contestId);

    /**
     * 导入球员
     * @param userList
     * @param operName
     * @return
     */
    String importPlayer(List<BasketBallPlayerExcel> userList, String operName,Long contestId,Long teamId);
}
