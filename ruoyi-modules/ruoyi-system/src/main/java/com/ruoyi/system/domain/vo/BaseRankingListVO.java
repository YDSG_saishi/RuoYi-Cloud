package com.ruoyi.system.domain.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author Wangbj
 * @date 2024年05月08日 18:25
 */
@Data
@Accessors(chain = true)
public class BaseRankingListVO {

    @Schema(name = "noNum", description = "排序")
    private Integer noNum;

    @Schema(name = "profilePicture", description = "头像")
    private String profilePicture;

    @Schema(name = "playerName", description = "球员名称")
    private String playerName;

    @Schema(name = "average", description = "场均")
    private String average = "0";

    @Schema(name = "totalCount", description = "总计")
    private Integer totalCount;

    private Long type;

    @Schema(name = "teamName", description = "球队名称")
    private String teamName;

}
