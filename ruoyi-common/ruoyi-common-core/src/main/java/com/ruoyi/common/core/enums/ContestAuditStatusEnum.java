package com.ruoyi.common.core.enums;

import java.util.Objects;

/**
 *
 */
public enum ContestAuditStatusEnum {
    AUDIT(0, "审核中"),
    PASS_AUDIT(1, "审核通过"),
    NOT_AUDIT(2, "审核未通过"),
    ;
    private final int code;
    private final String info;

    ContestAuditStatusEnum(int code, String info) {
        this.code = code;
        this.info = info;
    }

    public int getCode() {
        return code;
    }

    public String getInfo() {
        return info;
    }

    public static String getValue(int key) {
        for (ContestAuditStatusEnum typeEnum : ContestAuditStatusEnum.values()) {
            if (Objects.equals(typeEnum.getCode(), key)) {
                return typeEnum.getInfo();
            }
        }
        return null;
    }
}
