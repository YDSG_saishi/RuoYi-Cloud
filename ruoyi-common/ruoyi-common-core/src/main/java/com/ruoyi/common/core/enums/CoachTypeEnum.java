package com.ruoyi.common.core.enums;

import java.util.Objects;

/**
 *
 */
public enum CoachTypeEnum {
    HEAD_COACH(1L, "主教练"),
    ASSISTANT_COACH(2L, "助理教练"),
    LEADER(3L, "领队"),
    OTHER_STAFF(4L, "其他工作人员"),
    TEAM_DOCTOR(5L, "队医"),
    ;
    private final Long code;
    private final String info;

    CoachTypeEnum(Long code, String info) {
        this.code = code;
        this.info = info;
    }

    public Long getCode() {
        return code;
    }

    public String getInfo() {
        return info;
    }

    public static String getValue(Long key) {
        for (CoachTypeEnum typeEnum : CoachTypeEnum.values()) {
            if (Objects.equals(typeEnum.getCode(), key)) {
                return typeEnum.getInfo();
            }
        }
        return null;
    }
}
