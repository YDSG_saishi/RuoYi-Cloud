package com.ruoyi.common.core.enums;

import java.util.Objects;


public enum PlayerLogType {
    TWO_POINT_HIT(1L, "二分命中",0L),
    THREE_POINT_HIT(2L, "三分命中",0L),

    BLOCK_SHOT(3L, "盖帽",0L),
    STRAL(4L, "抢断",1L),
    TWO_NOT_POINT_HIT(5L, "二分不中",0L),
    THREE_NOT_POINT_HIT(6L, "三分不中",0L),
    NOT_POINT_HIT(7L, "罚球不中",0L),
    ASSIST(8L, "助攻",0L),
    BACKBOARD(9L, "篮板",1L),
    MISTAKE(10L, "失误",0L),
    FOUL(11L, "犯规",0L),
//    PLAYINT_TIME(12L, "上场时间",0L),
    STOP(13L, "暂停",0L),
    ONE_POINT_HIT(14L, "一分命中",0L),
    POINT_HIT(15L, "罚球命中",0L),
    ONE_NOT_POINT_HIT(16L, "一分不中",0L),

    START(17L, "恢复暂停",0L),

    OVER(18L, "全场比赛结束",0L),
    TEAM_FOUL(19L, "团队犯规",0L)

    ;
    private final Long code;
    private final String info;

    private final Long type;

    PlayerLogType(Long code, String info,Long type) {
        this.code = code;
        this.info = info;
        this.type = type;
    }

    public Long getCode() {
        return code;
    }
    public Long getType() {
        return type;
    }
    public String getInfo() {
        return info;
    }

    public static String getValue(Long key) {
        for (PlayerLogType typeEnum : PlayerLogType.values()) {
            if (Objects.equals(typeEnum.getCode(), key)) {
                return typeEnum.getInfo();
            }
        }
        return null;
    }

}
