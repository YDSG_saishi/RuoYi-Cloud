package com.ruoyi.common.core.enums;

import java.util.Objects;


public enum TournamentType {
    TWO_POINT_HIT(1L, "小组赛",0L),
    THREE_POINT_HIT(2L, "淘汰赛",0L),

    BLOCK_SHOT(3L, "四强赛",0L),
    STRAL(4L, "半决赛",1L),
    TWO_NOT_POINT_HIT(5L, "决赛",0L),
    TWO_NOT_POINT_HIT_TWO(6L, "季军赛",0L),
    RANKING(7L, "排位赛",0L),
    SEASON(8L, "常规赛",0L),
    PLAY_OFF(9L, "季后赛",0L),

    ;
    private final Long code;
    private final String info;

    private final Long type;

    TournamentType(Long code, String info, Long type) {
        this.code = code;
        this.info = info;
        this.type = type;
    }

    public Long getCode() {
        return code;
    }
    public Long getType() {
        return type;
    }
    public String getInfo() {
        return info;
    }

    public static String getValue(Long key) {
        for (TournamentType typeEnum : TournamentType.values()) {
            if (Objects.equals(typeEnum.getCode(), key)) {
                return typeEnum.getInfo();
            }
        }
        return null;
    }

}
