package com.ruoyi.system.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.core.constant.SecurityConstants;
import com.ruoyi.common.core.constant.ServiceNameConstants;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.system.api.domain.SysUser;
import com.ruoyi.system.api.factory.RemoteUserFallbackFactory;
import com.ruoyi.system.api.model.LoginUser;

/**
 * 用户服务
 * 
 * @author ruoyi
 */
@FeignClient(contextId = "remoteUserService",url = ServiceNameConstants.SYSTEM_SERVICE, value = ServiceNameConstants.SYSTEM_SERVICE, fallbackFactory = RemoteUserFallbackFactory.class)
public interface RemoteUserService
{
    /**
     * 通过用户名查询用户信息
     *
     * @param username 用户名
     * @param source 请求来源
     * @return 结果
     */
    @GetMapping("/user/info/{username}")
    public R<LoginUser> getUserInfo(@PathVariable("username") String username, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 注册用户信息
     *
     * @param sysUser 用户信息
     * @param source 请求来源
     * @return 结果
     */
    @PostMapping("/user/register")
    public R<Boolean> registerUserInfo(@RequestBody SysUser sysUser, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @GetMapping("/user/userPhone/{userPhone}")
    public R<LoginUser> userPhone(@PathVariable("userPhone") String userPhone, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 通过openid查询用户信息
     * @param openId
     * @return
     */
    @GetMapping("/user/info")
    R<LoginUser> getUserInfoByOpenId(@RequestParam("openId") String openId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    @PostMapping("/user/update")
    R<?> updateUser(@RequestBody SysUser user, @RequestHeader(SecurityConstants.FROM_SOURCE)String inner);
}
