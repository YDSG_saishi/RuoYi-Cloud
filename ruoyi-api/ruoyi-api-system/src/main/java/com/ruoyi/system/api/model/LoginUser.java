package com.ruoyi.system.api.model;

import java.io.Serializable;
import java.util.Set;
import com.ruoyi.system.api.domain.SysUser;
import lombok.Data;

/**
 * 用户信息
 *
 * @author ruoyi
 */
@Data
public class LoginUser implements Serializable
{
    private static final long serialVersionUID = 1L;

    /**
     * 用户唯一标识
     */
    private String token;

    /**
     * 用户名id
     */
    private Long userid;

    /**
     * 用户名
     */
    private String username;

    /**
     * 登录时间
     */
    private Long loginTime;

    /**
     * 过期时间
     */
    private Long expireTime;

    /**
     * 当前机构id
     */
    private Long organizationId;

    /**
     * 当前联赛id
     */
    private Long contestId;

    /**
     * 计分方式:0:5v5计分;1:3v3计分;
     */
    private Long scoringWay;

    /**
     * 登录IP地址
     */
    private String ipaddr;

    /**
     * 权限列表
     */
    private Set<String> permissions;

    /**
     * 角色列表
     */
    private Set<String> roles;

    /**
     * 用户信息
     */
    private SysUser sysUser;

    /**
     * 是否新用户
     */
    private boolean newUser;

}
